import os
import argparse
import requests
from datetime import datetime


def main(package, workspace_id, folder_id, chunk_size=1024 * 1024 * 10):
    """
    Upload file to EDX

    Args:
        package (str): Package filename
        workspace_id (str): EDX workspace ID
        folder_id (str): EDX folder ID
        chunk_size (int): File chunk size (must be multiple of 1024)

    """
    # Build the filename
    current_date = datetime.today().strftime("%Y_%m_%d")
    p, f = os.path.split(package)
    file_base = f[:f.find('.')]
    file_extension = f[f.find('.'):]
    fname = f'{file_base}_{current_date}{file_extension}'

    # Setup EDX headers
    edx_api_key = os.environ.get('EDX_API_KEY', '').replace('.', '-')
    if not edx_api_key:
        raise Exception('Could not find EDX_API_KEY')

    headers = {"EDX-API-Key": edx_api_key, "User-Agent": "EDX-USER"}

    data = {
        "folder_id": folder_id,
        "resource_name": fname,
        "workspace_id": workspace_id,
        "original_size": os.path.getsize(package),
        "chunk_size": chunk_size
    }

    # Upload the file
    if os.path.exists(package):
        with open(package, "rb") as f:
            url = 'https://edx.netl.doe.gov/api/3/action/resource_create_api'
            r = None
            while True:
                chunked_stream = f.read(chunk_size)
                if not chunked_stream:
                    break

                r = requests.post(
                    url,
                    headers=headers,
                    files={'file': chunked_stream},
                    data=data,
                )
                r_json = r.json()

                # Print the response and update the stream
                if 'error' in r_json:
                    err = str(r_json['error'])
                    raise Exception(f'Received an error when uploading data: {err}')

                data['key'] = r_json['result']['key']
                print(r.json()['result']['progress'])

            # Print out final response.
            if r is not None:
                print(r.json())

    else:
        raise Exception(f'Could not find file: {package}')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--package', type=str, help='Package filename', default='current_package.zip')
    parser.add_argument('-w', '--workspace', type=str, help='EDX workspace', default='smart-gitlab-mirror')
    parser.add_argument('-f', '--folder', type=str, help='EDX folder ID', default='')
    args = parser.parse_args()
    main(args.package, args.workspace, args.folder)
