# Welcome to the ORION Toolkit!

Coming soon!

## Documentation

Our documentation is hosted [here](https://nrap.gitlab.io/orion/index.html).


## Who develops Orion?

- Lawrence Livermore National Laboratory
- Lawrence Berkeley National Laboratory


## License

Orion is distributed under the terms of the MIT license.
See [LICENSE](https://gitlab.com/NRAP/orion/-/blob/develop/LICENSE),
[COPYRIGHT](https://gitlab.com/NRAP/orion/-/blob/develop/COPYRIGHT),
[NOTICE](https://gitlab.com/NRAP/orion/-/blob/develop/NOTICE), and 
[ACKNOWLEDGEMENTS](https://gitlab.com/NRAP/orion/-/blob/develop/ACKNOWLEDGEMENTS), for details.

LLNL-CODE-842148
