# ------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2020-, Lawrence Livermore National Security, LLC
# All rights reserved
#
# See top level LICENSE, COPYRIGHT, CONTRIBUTORS, NOTICE, and ACKNOWLEDGEMENTS files for details.
# ------------------------------------------------------------------------------------------------

import pytest
import numpy as np
import tempfile
from scipy.special import exp1
import os
import sys
from pathlib import Path
from dataclasses import dataclass

package_path = os.path.abspath(Path(__file__).resolve().parents[1])
mod_path = os.path.abspath(os.path.join(package_path, 'src'))
sys.path.append(mod_path)
strive_path = os.path.abspath(os.path.join(package_path, '..', 'strive', 'src'))
sys.path.append(strive_path)


# Build a dummy grid
@dataclass(frozen=True)
class Projection:
    x: float
    y: float
    z: float
    t: float
    x_origin: float
    y_origin: float
    z_origin: float
    t_origin: float
    utm_zone_str: str
    projection_str: str
    spatial_type: str


grid = Projection(np.linspace(1, 2, 2), np.linspace(3, 4, 3), np.linspace(5, 6, 4), np.linspace(7, 8, 5), 0.0, 0.0, 0.0,
                  0.0, '10', '+proj=eqc +lat_ts=0 +lat_0=0 +lon_0=0 +x_0=0 +y_0=0 +ellps=WGS84 +units=m', 'UTM')


class TestSingleWellFlow():

    @pytest.fixture(scope='class')
    def paths(self):
        test_paths = {'root': tempfile.TemporaryDirectory()}
        test_paths['pressure_only'] = f"{test_paths['root'].name}/pressure_only.hdf5"
        test_paths['dpdt_only'] = f"{test_paths['root'].name}/dpdt_only.hdf5"
        test_paths['pressure_dpdt'] = f"{test_paths['root'].name}/pressure_dpdt.hdf5"
        return test_paths

    @pytest.fixture(scope='class')
    def rfm(self):
        from orion.pressure_models import radial_flow

        # Choose parameters so that model scaling equals 1
        rfm = radial_flow.RadialFlowModel()
        rfm.x = np.zeros(1)
        rfm.y = np.zeros(1)
        rfm.t = [np.zeros(1)]
        q = [np.array([np.pi / (1000.0 * 9.81)])]
        rfm.delta_q = [np.diff(np.insert(tmp, 0, 0)) for tmp in q]
        rfm.viscosity = 4.0 * 1000.0 * 9.81 * 1e-13
        rfm.permeability = 1.0
        rfm.storativity = 1.0
        rfm.payzone_thickness = 1.0
        rfm.enable_gravity = False

        # Finalize the model
        rfm.grid_values(grid)
        return rfm

    @pytest.fixture(scope='class')
    def data(self, rfm):
        # Evaluate the pressure model on a grid
        grid_values = np.meshgrid(grid.x, grid.y, grid.z, grid.t, indexing='ij')
        p = np.ascontiguousarray(rfm.p(*grid_values))
        dpdt = np.ascontiguousarray(rfm.dpdt(*grid_values))
        return p, dpdt

    def check_value_with_tolerance(self, value, expected):
        assert value == pytest.approx(expected, abs=1e-6)

    def test_pressure(self, rfm):
        p_a = rfm.p(1.0, 0.0, 0.0, 1.0)
        p_b = exp1(1.0)
        self.check_value_with_tolerance(p_a, p_b)

    def test_dpdt(self, rfm):
        dpdt_a = rfm.dpdt(1.0, 0.0, 0.0, 1.0)
        dpdt_b = np.exp(-1.0)
        self.check_value_with_tolerance(dpdt_a, dpdt_b)

    def test_dpdt_fd(self, rfm):
        N = 1000
        test_t = np.linspace(1.0, 10.0, N)
        test_t_mid = 0.5 * (test_t[1:] + test_t[:-1])
        p = [rfm.p(1.0, 0.0, 0.0, t) for t in test_t]
        dpdt = [rfm.dpdt(1.0, 0.0, 0.0, t) for t in test_t_mid]
        dpdt_fd = np.diff(p) / np.diff(test_t)
        assert np.allclose(dpdt, dpdt_fd)

    def test_write_gridded_files(self, rfm, data, paths):
        from orion.utilities import hdf5_wrapper
        p, dpdt = data

        with hdf5_wrapper.hdf5_wrapper(paths['pressure_only'], mode='w') as tmp:
            tmp['x'] = grid.x
            tmp['y'] = grid.y
            tmp['z'] = grid.z
            tmp['t'] = grid.t
            tmp['pressure'] = p

        with hdf5_wrapper.hdf5_wrapper(paths['dpdt_only'], mode='w') as tmp:
            tmp['x'] = grid.x
            tmp['y'] = grid.y
            tmp['z'] = grid.z
            tmp['t'] = grid.t
            tmp['dpdt'] = dpdt

        with hdf5_wrapper.hdf5_wrapper(paths['pressure_dpdt'], mode='w') as tmp:
            tmp['x'] = grid.x
            tmp['y'] = grid.y
            tmp['z'] = grid.z
            tmp['t'] = grid.t
            tmp['pressure'] = p
            tmp['dpdt'] = dpdt

    @pytest.mark.parametrize('table_name, offset', [('pressure_only', False), ('dpdt_only', True),
                                                    ('pressure_dpdt', False)])
    def test_pressure_tables(self, table_name, offset, data, paths):
        from orion.pressure_models import pressure_table

        # Load the pressure table
        pt = pressure_table.PressureTableModel()
        pt.file_name = paths[table_name]
        pt.run(grid, None, None)

        # Get the expected copy of the data
        p = data[0].copy()
        dpdt = data[1]
        if offset:
            p_init = p[:, :, :, 0].copy()
            for ii in range(len(grid.t)):
                p[:, :, :, ii] -= p_init

        # There can sometimes be derivative artefacts along the array edges,
        # so ignore these in the comparison
        assert np.allclose(p[..., 1:-1], pt.p_grid[..., 1:-1], atol=2e-5)
        assert np.allclose(dpdt[..., 1:-1], pt.dpdt_grid[..., 1:-1], atol=2e-5)

    def check_composite_table(self, table_a, table_b, table_c):
        """
        Verify table addition is done correctly
        Note: the static pressure for the seconday model is not included

        Args:
            table_a: The primary table
            table_b: The secondary table
            table_c: The composite table
        """
        # Check the gridded values
        err = np.sum(abs(table_a.p_grid + table_b.p_grid - table_c.p_grid))
        assert err == pytest.approx(0.0, abs=1e-6)

        err = np.sum(abs(table_a.dpdt_grid + table_b.dpdt_grid - table_c.dpdt_grid))
        assert err == pytest.approx(0.0, abs=1e-6)

    def test_pressure_model_add_rfm_rfm(self, rfm):
        composite_model = rfm + rfm
        self.check_composite_table(rfm, rfm, composite_model)

        # Check the interpolated values
        grid_origin = [grid.x[0], grid.y[0], grid.z[0], grid.t[0]]
        p_a = rfm.p(*grid_origin)
        p_b = composite_model.p(*grid_origin)
        assert p_b == pytest.approx(2 * p_a, abs=1e-6)

        dpdt_a = rfm.dpdt(*grid_origin)
        dpdt_b = composite_model.dpdt(*grid_origin)
        assert dpdt_b == pytest.approx(2 * dpdt_a, abs=1e-6)

    def test_pressure_model_add_rfm_table(self, rfm, paths):
        # Load the pressure table
        from orion.pressure_models import pressure_table
        pa = pressure_table.PressureTableModel()
        pa.file_name = paths['pressure_dpdt']
        pa.run(grid, None, None)

        # Build composite table
        composite_model = rfm + pa
        self.check_composite_table(rfm, pa, composite_model)

        # Check interpolation
        G = np.meshgrid(grid.x, grid.y, grid.z, grid.t, indexing='ij')
        alt_a = rfm.p(*G)
        alt_b = composite_model.p(*G)
        alt_c = pa.p(*G)

    def test_pressure_model_add_table_table(self, paths):
        # Load the pressure table
        from orion.pressure_models import pressure_table
        pa = pressure_table.PressureTableModel()
        pa.file_name = paths['pressure_dpdt']
        pa.run(grid, None, None)

        pb = pressure_table.PressureTableModel()
        pb.file_name = paths['pressure_only']
        pb.run(grid, None, None)

        # Build composite table
        composite_model = pa + pb
        self.check_composite_table(pa, pb, composite_model)

        # Check interpolation
        G = np.meshgrid(grid.x, grid.y, grid.z, grid.t, indexing='ij')
        alt_a = pa.p(*G)
        alt_b = composite_model.p(*G)
        alt_c = pb.p(*G)

    def test_pressure_model_multiply_rfm_two(self, rfm):
        composite_model = rfm * 2.0
        self.check_composite_table(rfm, rfm, composite_model)

        # Check the interpolated values
        grid_origin = [grid.x[0], grid.y[0], grid.z[0], grid.t[0]]
        p_a = rfm.p(*grid_origin)
        p_b = composite_model.p(*grid_origin)
        assert p_b == pytest.approx(2 * p_a, abs=1e-6)

        dpdt_a = rfm.dpdt(*grid_origin)
        dpdt_b = composite_model.dpdt(*grid_origin)
        assert dpdt_b == pytest.approx(2 * dpdt_a, abs=1e-6)

    def test_pressure_model_divide_rfm_three(self, rfm):
        composite_model = rfm / 3

        # Check gridded values
        err = np.sum(abs((rfm.p_grid / 3.0) - composite_model.p_grid))
        assert err == pytest.approx(0.0, abs=1e-6)
        err = np.sum(abs((rfm.dpdt_grid / 3.0) - composite_model.dpdt_grid))
        assert err == pytest.approx(0.0, abs=1e-6)

        # Check the interpolated values
        grid_origin = [grid.x[0], grid.y[0], grid.z[0], grid.t[0]]
        p_a = rfm.p(*grid_origin)
        p_b = composite_model.p(*grid_origin)
        assert p_b == pytest.approx(p_a / 3.0, abs=1e-6)

        dpdt_a = rfm.dpdt(*grid_origin)
        dpdt_b = composite_model.dpdt(*grid_origin)
        assert dpdt_b == pytest.approx(dpdt_a / 3.0, abs=1e-6)

    def test_pressure_model_subtract_rfm_rfm(self, rfm):
        composite_model = rfm - rfm

        # Check gridded values
        assert np.sum(abs(composite_model.p_grid)) == pytest.approx(0.0, abs=1e-6)
        assert np.sum(abs(composite_model.dpdt_grid)) == pytest.approx(0.0, abs=1e-6)

        # Check the interpolated values
        grid_origin = [grid.x[0], grid.y[0], grid.z[0], grid.t[0]]
        p_b = composite_model.p(*grid_origin)
        assert p_b == pytest.approx(0.0, abs=1e-6)

        dpdt_b = composite_model.dpdt(*grid_origin)
        assert dpdt_b == pytest.approx(0.0, abs=1e-6)
