import numpy as np
import random
import argparse
import os
import json

os.environ['TZ'] = 'Pacific Standard Time'
random.seed(123456)
np.random.seed(123456)


def main():
    import orion
    from orion.examples import built_in_manager
    from orion.utilities import hdf5_wrapper

    # Load baseline cases
    id_file = os.path.join(os.path.dirname(__file__), 'integrated_baseline_ids.json')
    baseline_ids = json.load(open(id_file))

    # Parse user arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--frontend', type=str, help='GUI frontend (tkinter, strive, or none)', default='')
    available_test_str = ', '.join([f'\"{k}\"' for k in baseline_ids])
    parser.add_argument('-c', '--case', type=str, help=f'Test case ({available_test_str})', default='')
    args = parser.parse_args()

    # Check args
    if args.frontend.lower() == 'matplotlib':
        args.frontend = ''

    if args.case.lower() == 'default':
        args.case = ''

    # Check for API key
    built_in_manager.remote_examples.edx_api_key = os.environ.get('EDX_API_KEY',
                                                                  'EDX API Key not found').replace('.', '-')
    if not built_in_manager.remote_examples.edx_api_key:
        raise Exception('The EDX_API_KEY environment variable must be set to run integrated tests')

    assert args.case in baseline_ids
    if args.case:
        print('Downloading test data from EDX...')
        built_in_manager.remote_examples.download_examples.append(args.case)
        built_in_manager.remote_examples.download_data()

    print('Downloading baselines from EDX...')
    if not os.path.isfile('./orion_baseline/orion_plot_data.hdf5'):
        built_in_manager.remote_examples.download_baseline(baseline_ids[args.case],
                                                           f'./orion_baseline/orion_plot_data_{args.case}.hdf5')
    assert os.path.isfile(f'./orion_baseline/orion_plot_data_{args.case}.hdf5')

    print('Building configuration...')
    cache_root = os.path.expanduser('~/.cache/orion')
    cache_file = os.path.join(cache_root, 'orion_config.json')
    os.makedirs(cache_root, exist_ok=True)
    if args.case:
        built_in_manager.compile_built_in(args.case, cache_file)
    elif os.path.isfile(cache_file):
        os.remove(cache_file)

    user_cache_file = os.path.join(cache_root, 'orion_config_user.json')
    if os.path.isfile(user_cache_file):
        os.remove(user_cache_file)

    if args.frontend == 'tkinter':
        print('Running ORION Tkinter GUI...')
        orion._frontend = 'tkinter'
        from orion.gui import orion_gui
        orion_gui.launch_gui('', profile_run=True, verbose=True)
    elif args.frontend == 'strive':
        raise Exception('ORION STRIVE integrated test is not enabled')
    else:
        print('Running ORION Engine...')
        orion._frontend = ''
        from orion.managers import orion_manager
        orion_manager.run_manager('', verbose=True)

    print('Checking for expected files...')
    assert os.path.isfile('./orion_results/spatial_forecast.png')
    assert os.path.isfile('./orion_results/orion_plot_data.hdf5')

    print('Comparing results to baseline...')
    fa = hdf5_wrapper.hdf5_wrapper('./orion_results/orion_plot_data.hdf5')
    assert fa.compare_with_tolerance(f'./orion_baseline/orion_plot_data_{args.case}.hdf5') == 0

    print('Done')


if __name__ == '__main__':
    main()
