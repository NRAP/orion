import pytest
import numpy as np
import os
import sys
from pathlib import Path

package_path = os.path.abspath(Path(__file__).resolve().parents[1])
mod_path = os.path.abspath(os.path.join(package_path, 'src'))
sys.path.append(mod_path)
strive_path = os.path.abspath(os.path.join(package_path, '..', 'strive', 'src'))
sys.path.append(strive_path)

# build test data
test_data = {}
test_data['sigma_effective'] = np.array(
    [7.290101609, 7.28996449440928, 7.28881108192424, 7.25822189906990, 7.25821948763880]) * 1e6    # MPa
test_data['coulomb_stressing_rate'] = np.array([
    0.053688128, 0.12623823360902400, 0.24847741964609800, -0.11912579462835200, 0.14564181918564800
]) * 1e6 / 365.25 / 86400    # MPa/year to Pa/s
test_data['forecast_time'] = np.array([
    -0.02202181331913520, 0.00535669455218368, 0.03273520242350260, 0.06011371029482150, 0.08749221816614750
]) * 365.25 * 86400    # years to seconds
test_data['predicted_number_interseismic_only'] = np.array(
    [8.9489390828193, 26.561591611686, 65.8515945405481, 70.9172500896793, 75.079687964342])
test_data['predicted_number_instant_interseismic'] = np.array([0.0, 17.61265253, 56.90265546, 61.96831101, 679.2158862])
test_data['larger_event_times_in_forecast'] = np.array([0.03273520242350260]) * 365.25 * 86400    # years to seconds
test_data['coulomb_stress_change'] = np.array([0.09460923]) * 1e6    # MPa to Pa

dt = np.diff(test_data['forecast_time'])

# test data for main shock detection

c0 = np.linspace(0.01, 0.09, 10)
c1 = np.linspace(0.1, 1, 200)
c2 = np.linspace(1.001, 1.1, 200)
c3 = np.linspace(1.101, 2.0, 200)
c4 = np.linspace(2.001, 2.1, 200)
c5 = np.linspace(2.101, 3.0, 200)

test_data['synthetic_catalog_time'] = np.concatenate((c0, c1, c2, c3, c4, c5)) * 365 * 86400    # years to seconds


class TestCoulombRateStateModel():

    @pytest.fixture(scope='class')
    def CRS(self):
        from orion.forecast_models import crs_model_spatial
        CRS = crs_model_spatial.CRSModel_Spatial()
        CRS.muPrime = 0.53
        CRS.background_rate_input = 326.86    # events/year
        CRS.tectonicShearStressingRate_input = 0.05232325    # MPa/year
        CRS.tectonicNormalStressingRate_input = 0.0    # MPa/year
        CRS.rate_coefficient = 0.0001
        CRS.sigma_input = 9.0    # MPa
        CRS.biot = 0.2
        CRS.rate_factor_input = 6236.0    # 1/MPa
        CRS.rate_density_lag_input = 1.0
        CRS.process_inputs()
        return CRS

    def test_InterseismicNumber(self, CRS):
        predicted_number = test_data['predicted_number_interseismic_only']
        coulomb_stressing_rate = test_data['coulomb_stressing_rate']
        sigma_effective = test_data['sigma_effective']
        num = predicted_number[0]
        rate_at_prev_step = CRS.background_rate
        for i in range(0, len(predicted_number) - 1):
            current_rate = CRS.InterseismicRate(dt[i], coulomb_stressing_rate[i + 1], rate_at_prev_step,
                                                sigma_effective[i + 1])
            num += CRS.InterseismicNumber(dt[i], coulomb_stressing_rate[i + 1], rate_at_prev_step,
                                          sigma_effective[i + 1])
            assert num == pytest.approx(predicted_number[i + 1], abs=1e-6)
            rate_at_prev_step = current_rate

    def test_NumberEvolution(self, CRS):
        CRS.enable_clustering = True
        forecast_time = test_data['forecast_time']
        predicted_number = test_data['predicted_number_instant_interseismic']
        large_event_times_in_forecast = test_data['larger_event_times_in_forecast']
        coulomb_stress_change = test_data['coulomb_stress_change']
        coulomb_stressing_rate = test_data['coulomb_stressing_rate']
        sigma_effective = test_data['sigma_effective']

        num = CRS.NumberEvolution(forecast_time, large_event_times_in_forecast, coulomb_stress_change,
                                  coulomb_stressing_rate, sigma_effective)
        assert num == pytest.approx(predicted_number, abs=1e-4)

    def test_main_shock_detection(self, CRS):
        catalog_time = test_data['synthetic_catalog_time']
        lag = int(len(catalog_time) * CRS.rate_density_lag)
        x_lag, P = CRS.rate_density(catalog_time, lag)
        padded_P = np.pad(P, pad_width=(lag, 0), mode='constant',
                          constant_values=np.mean(P))    # pad the beginning of the array with the mean value
        peak_idx = CRS.detect_peak_zscore(padded_P, lag)
        peak_idx = [i - lag for i in peak_idx]    # remove the padding
        peak_ids_original_time = [np.argmin(np.abs(catalog_time - x_lag[peak_id])) for peak_id in peak_idx]
        clusters = CRS.auto_cluster(peak_ids_original_time)
        main_shock_times = []
        for cluster in clusters:
            main_shock_times.append(catalog_time[cluster[0]])
        assert main_shock_times == pytest.approx([4009350.753768844, 30822874.3718593, 62359666.73366834])
