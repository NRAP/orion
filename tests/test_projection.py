# ------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2020-, Lawrence Livermore National Security, LLC
# All rights reserved
#
# See top level LICENSE, COPYRIGHT, CONTRIBUTORS, NOTICE, and ACKNOWLEDGEMENTS files for details.
# ------------------------------------------------------------------------------------------------

import pytest
import numpy as np
import os
import sys
from pathlib import Path

package_path = os.path.abspath(Path(__file__).resolve().parents[1])
mod_path = os.path.abspath(os.path.join(package_path, 'src'))
sys.path.append(mod_path)

# Test configuration
xy_tol = 1e-3
latlon_tol = 1e-6
grid_xy_rtol = 0.008
grid_latlon_tol = 1e-4

test_data = {
    'latitude': 0.0,
    'longitude': -3.0,
    'utm_east': 500000.0,
    'utm_north': 0.0,
    'utm_zone': 30,
    'default_east': -333958.4723798207,
    'default_north': 0.0,
    'dx': 22255.038099178695,
    'dy': 11053.00472382431,
    'dlat': 0.1,
    'dlon': 0.2
}


def test_point_projection_lat_lon_default():
    from orion.utilities import spatial
    p = spatial.Points(latitude=[test_data['latitude']], longitude=[test_data['longitude']])
    assert p.x[0] == pytest.approx(test_data['default_east'], abs=xy_tol)
    assert p.y[0] == pytest.approx(test_data['default_north'], abs=xy_tol)


def test_point_projection_lat_lon_utm():
    from orion.utilities import spatial
    zone = test_data['utm_zone']
    p = spatial.Points(latitude=[test_data['latitude']],
                       longitude=[test_data['longitude']],
                       target_projection_str=f'+proj=utm +zone={zone}')
    assert p.x[0] == pytest.approx(test_data['utm_east'], abs=xy_tol)
    assert p.y[0] == pytest.approx(test_data['utm_north'], abs=xy_tol)


def test_point_projection_x_y_default():
    from orion.utilities import spatial
    p = spatial.Points(x=[test_data['default_east']], y=[test_data['default_north']])
    assert p.latitude[0] == pytest.approx(test_data['latitude'], abs=latlon_tol)
    assert p.longitude[0] == pytest.approx(test_data['longitude'], abs=latlon_tol)


def test_point_projection_x_y_utm():
    from orion.utilities import spatial
    zone = test_data['utm_zone']
    p = spatial.Points(x=[test_data['utm_east']],
                       y=[test_data['utm_north']],
                       source_projection_str=f'+proj=utm +zone={zone}')
    assert p.x[0] == pytest.approx(test_data['default_east'], abs=xy_tol)
    assert p.y[0] == pytest.approx(test_data['default_north'], abs=xy_tol)
    assert p.latitude[0] == pytest.approx(test_data['latitude'], abs=latlon_tol)
    assert p.longitude[0] == pytest.approx(test_data['longitude'], abs=latlon_tol)


def test_point_projection_update():
    from orion.utilities import spatial
    p = spatial.Points(latitude=[test_data['latitude']], longitude=[test_data['longitude']])
    zone = test_data['utm_zone']
    p.update_projection(f'+proj=utm +zone={zone}')
    assert p.x[0] == pytest.approx(test_data['utm_east'], abs=xy_tol)
    assert p.y[0] == pytest.approx(test_data['utm_north'], abs=xy_tol)
    assert p.latitude[0] == pytest.approx(test_data['latitude'], abs=latlon_tol)
    assert p.longitude[0] == pytest.approx(test_data['longitude'], abs=latlon_tol)


def test_point_projection_utm_rounding():
    from orion.utilities import spatial
    zone = test_data['utm_zone']
    p = spatial.Points(x=[test_data['utm_east']],
                       y=[test_data['utm_north']],
                       source_projection_str=f'+proj=utm +zone={zone}',
                       target_projection_str=f'+proj=utm +zone={zone}')
    assert p.x[0] == pytest.approx(test_data['utm_east'], abs=xy_tol)
    assert p.y[0] == pytest.approx(test_data['utm_north'], abs=xy_tol)
    assert p.latitude[0] == pytest.approx(test_data['latitude'], abs=latlon_tol)
    assert p.longitude[0] == pytest.approx(test_data['longitude'], abs=latlon_tol)


def test_grid_projection_lat_lon_default():
    from orion.utilities import spatial
    lat_grid = test_data['latitude'] + np.linspace(0, test_data['dlat'], 3)
    lon_grid = test_data['longitude'] + np.linspace(0, test_data['dlon'], 4)
    g = spatial.GridAxes(latitude=lat_grid, longitude=lon_grid)
    assert len(g.x) == len(g.longitude)
    assert len(g.y) == len(g.latitude)
    assert g.x[0] == pytest.approx(test_data['default_east'], abs=xy_tol)
    assert g.y[0] == pytest.approx(test_data['default_north'], abs=xy_tol)
    assert g.x[-1] - test_data['default_east'] == pytest.approx(test_data['dx'], rel=grid_xy_rtol)
    assert g.y[-1] - test_data['default_north'] == pytest.approx(test_data['dy'], rel=grid_xy_rtol)


def test_grid_projection_lat_lon_utm():
    from orion.utilities import spatial
    lat_grid = test_data['latitude'] + np.linspace(0, test_data['dlat'], 3)
    lon_grid = test_data['longitude'] + np.linspace(0, test_data['dlon'], 4)
    zone = test_data['utm_zone']
    g = spatial.GridAxes(latitude=lat_grid, longitude=lon_grid, target_projection_str=f'+proj=utm +zone={zone}')
    assert len(g.x) == len(g.longitude)
    assert len(g.y) == len(g.latitude)
    assert g.x[0] == pytest.approx(test_data['utm_east'], abs=xy_tol)
    assert g.y[0] == pytest.approx(test_data['utm_north'], abs=xy_tol)
    assert g.x[-1] - test_data['utm_east'] == pytest.approx(test_data['dx'], rel=grid_xy_rtol)
    assert g.y[-1] - test_data['utm_north'] == pytest.approx(test_data['dy'], rel=grid_xy_rtol)


def test_grid_projection_x_y_default():
    from orion.utilities import spatial
    x_grid = test_data['utm_east'] + np.linspace(0, test_data['dx'], 3)
    y_grid = test_data['utm_north'] + np.linspace(0, test_data['dy'], 4)
    zone = test_data['utm_zone']
    g = spatial.GridAxes(x=x_grid, y=y_grid, source_projection_str=f'+proj=utm +zone={zone}')
    assert len(g.x) == len(g.longitude)
    assert len(g.y) == len(g.latitude)
    assert g.latitude[0] == pytest.approx(test_data['latitude'], abs=latlon_tol)
    assert g.longitude[0] == pytest.approx(test_data['longitude'], abs=latlon_tol)
    assert g.latitude[-1] - test_data['latitude'] == pytest.approx(test_data['dlat'], abs=grid_latlon_tol)
    assert g.longitude[-1] - test_data['longitude'] == pytest.approx(test_data['dlon'], abs=grid_latlon_tol)


def test_grid_projection_update():
    from orion.utilities import spatial
    lat_grid = test_data['latitude'] + np.linspace(0, test_data['dlat'], 3)
    lon_grid = test_data['longitude'] + np.linspace(0, test_data['dlon'], 4)
    g = spatial.GridAxes(latitude=lat_grid, longitude=lon_grid)
    zone = test_data['utm_zone']
    g.update_projection(f'+proj=utm +zone={zone}')
    assert len(g.x) == len(g.longitude)
    assert len(g.y) == len(g.latitude)
    assert g.x[0] == pytest.approx(test_data['utm_east'], abs=xy_tol)
    assert g.y[0] == pytest.approx(test_data['utm_north'], abs=xy_tol)
    assert g.x[-1] - test_data['utm_east'] == pytest.approx(test_data['dx'], rel=grid_xy_rtol)
    assert g.y[-1] - test_data['utm_north'] == pytest.approx(test_data['dy'], rel=grid_xy_rtol)


def test_grid_projection_utm_rounding():
    from orion.utilities import spatial
    x_grid = test_data['utm_east'] + np.linspace(0, test_data['dx'], 3)
    y_grid = test_data['utm_north'] + np.linspace(0, test_data['dy'], 4)
    zone = test_data['utm_zone']
    g = spatial.GridAxes(x=x_grid,
                         y=y_grid,
                         source_projection_str=f'+proj=utm +zone={zone}',
                         target_projection_str=f'+proj=utm +zone={zone}')
    assert g.x[0] == pytest.approx(test_data['utm_east'], abs=xy_tol)
    assert g.y[0] == pytest.approx(test_data['utm_north'], abs=xy_tol)
