# ------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2020-, Lawrence Livermore National Security, LLC
# All rights reserved
#
# See top level LICENSE, COPYRIGHT, CONTRIBUTORS, NOTICE, and ACKNOWLEDGEMENTS files for details.
# ------------------------------------------------------------------------------------------------

import pytest
import numpy as np
import os
import sys
from pathlib import Path

package_path = os.path.abspath(Path(__file__).resolve().parents[1])
mod_path = os.path.abspath(os.path.join(package_path, 'src'))
sys.path.append(mod_path)
# strive_path = os.path.abspath(os.path.join(package_path, '..', 'strive', 'src'))
# sys.path.append(strive_path)

test_data = {
    'x': np.array([1.5, 2.501, 3.5]),
    'y': np.array([3.5, 4.501, 5.5]),
    'z': np.array([5.5, 6.501, 7.5]),
    't': np.array([-0.5, 0.501, 1.5]) * 60 * 60 * 24,
}


def test_grid_single_cell():
    from orion.managers import grid_manager
    grid = grid_manager.GridManager()
    grid.t_min_input = 0.0
    grid.t_max_input = 1.0
    grid.dt_input = 1.0
    grid.x_origin_input = 0.0
    grid.x_min_input = 2.0
    grid.x_max_input = 3.0
    grid.dx_input = 1.0
    grid.y_origin_input = 0.0
    grid.y_min_input = 4.0
    grid.y_max_input = 5.0
    grid.dy_input = 1.0
    grid.z_origin_input = 0.0
    grid.z_min_input = 6.0
    grid.z_max_input = 7.0
    grid.dz_input = 1.0
    grid.distance_units = 'm'
    grid.spatial_type = 'General'
    grid.utm_zone_str = ''
    grid.projection_str = 'EPSG:3857'
    grid.process_inputs()

    # Check shape, cell values
    assert grid.shape == (1, 1, 1, 1)
    assert grid.areas.size == 1
    assert grid.areas[0, 0] == 1.0
    assert pytest.approx(grid.x[0], abs=1e-9) == 2.5
    assert pytest.approx(grid.y[0], abs=1e-9) == 4.5
    assert pytest.approx(grid.z[0], abs=1e-9) == 6.5
    assert pytest.approx(grid.t[0], abs=1e-9) == 0.5 * 60 * 60 * 24

    # Check digitization values
    dig_t = grid.digitize_values(test_data['t'])
    dig_xyz = grid.digitize_values(test_data['x'], test_data['y'], test_data['z'])
    dig_xyzt = grid.digitize_values(test_data['x'], test_data['y'], test_data['z'], test_data['t'])
    assert len(dig_t) == 1
    assert len(dig_xyz) == 3
    assert len(dig_xyzt) == 4
    assert np.all(dig_t == np.array([[-1, 0, -2]], dtype=int))
    assert np.all(dig_xyz == np.array([[-1, 0, -2], [-1, 0, -2], [-1, 0, -2]], dtype=int))
    assert np.all(dig_xyzt == np.array([[-1, 0, -2], [-1, 0, -2], [-1, 0, -2], [-1, 0, -2]], dtype=int))

    # Check histogram values
    hist_t = grid.histogram_values(test_data['t'])
    hist_xyz = grid.histogram_values(test_data['x'], test_data['y'], test_data['z'])
    hist_xyzt = grid.histogram_values(test_data['x'], test_data['y'], test_data['z'], test_data['t'])
    assert len(np.shape(hist_t)) == 1
    assert len(np.shape(hist_xyz)) == 3
    assert len(np.shape(hist_xyzt)) == 4
    assert hist_t[0] == 1
    assert hist_xyz[0, 0, 0] == 1
    assert hist_xyzt[0, 0, 0, 0] == 1

    # Check histogram values (edges included)
    hist_t = grid.histogram_values(test_data['t'], include_edges=True)
    hist_xyz = grid.histogram_values(test_data['x'], test_data['y'], test_data['z'], include_edges=True)
    hist_xyzt = grid.histogram_values(test_data['x'],
                                      test_data['y'],
                                      test_data['z'],
                                      test_data['t'],
                                      include_edges=True)
    assert len(np.shape(hist_t)) == 1
    assert len(np.shape(hist_xyz)) == 3
    assert len(np.shape(hist_xyzt)) == 4
    assert hist_t[0] == 3
    assert hist_xyz[0, 0, 0] == 3
    assert hist_xyzt[0, 0, 0, 0] == 3


def test_grid_multi_cell():
    from orion.managers import grid_manager
    grid = grid_manager.GridManager()
    grid.t_min_input = 0.0
    grid.t_max_input = 1.0
    grid.dt_input = 1.0
    grid.x_origin_input = 0.0
    grid.x_min_input = 2.0
    grid.x_max_input = 3.0
    grid.dx_input = 0.5
    grid.y_origin_input = 0.0
    grid.y_min_input = 4.0
    grid.y_max_input = 5.0
    grid.dy_input = 0.25
    grid.z_origin_input = 0.0
    grid.z_min_input = 6.0
    grid.z_max_input = 7.0
    grid.dz_input = 0.125
    grid.spatial_type = 'General'
    grid.utm_zone_str = ''
    grid.projection_str = 'EPSG:3857'
    grid.process_inputs()

    # Check shape, cell values
    assert grid.shape == (2, 4, 8, 1)
    assert grid.areas.shape == (2, 4)
    assert grid.areas[0, 0] == 0.125
    assert np.allclose(grid.x, np.array([2.25, 2.75]), atol=1e-9)
    assert np.allclose(grid.y, np.array([4.125, 4.375, 4.625, 4.875]), atol=1e-9)
    assert np.allclose(grid.z, np.array([6.0625, 6.1875, 6.3125, 6.4375, 6.5625, 6.6875, 6.8125, 6.9375]), atol=1e-9)
    assert np.allclose(grid.t, np.array([43200.]), atol=1e-9)

    # Check histogram values
    hist_t = grid.histogram_values(test_data['t'])
    hist_xyz = grid.histogram_values(test_data['x'], test_data['y'], test_data['z'])
    hist_xyzt = grid.histogram_values(test_data['x'], test_data['y'], test_data['z'], test_data['t'])
    assert len(np.shape(hist_t)) == 1
    assert len(np.shape(hist_xyz)) == 3
    assert len(np.shape(hist_xyzt)) == 4
    assert np.sum(hist_t) == 1
    assert np.sum(hist_xyz) == 1
    assert np.sum(hist_xyzt) == 1
    assert np.all(np.array(np.where(hist_t)) == np.array([[0]]))
    assert np.all(np.array(np.where(hist_xyz)) == np.array([[1], [2], [4]]))
    assert np.all(np.array(np.array(np.where(hist_xyzt))) == np.array([[1], [2], [4], [0]]))
