
.. _osx-instructions:

OSX & Apple M-series
=========================

Due to known issues with default visualization libraries (Tcl, Tkinter), we recommend that users either run a pre-compiled version of ORION (still in development) or use homebrew to install Python.
Otherwise, users who run ORION with pre-compiled versions of `cpython <https://www.python.org/>`_ or `anaconda <https://www.anaconda.com/>`_ may observe unexpected widget behavior or even crashing.


Homebrew Install
---------------------------------

This install method relies on the homebrew package manager, which can be installed using the instructions `here <https://brew.sh/>`_ .
To install ORION, please do the following:

Cleanup
^^^^^^^^^^^^

If you have used brew to install python before, you may need to run the following steps to clean your environment:

.. code-block:: bash

    brew uninstall tcl-tk
    brew uninstall python
    brew uninstall python-tk


Python Installation
^^^^^^^^^^^^^^^^^^^^^^^

To install the appropriate version of python and other pre-requisites into your environment, run the following:

.. code-block:: bash

    brew install python-tk@3.11
    brew install proj
    brew install geos


.. note::
   The install location may depend on the specific version of your computer.  On newer M-series macs, the python3 binary may be located in `/opt/homebrew/bin` or `~/local/Cellar/python@3.11/3.11.8/bin`.  On older Intel macs, the binary may be located in /usr/local/bin.  If you don't see the binary, try running `brew info python@3.11` or searching your home directory for `python3.11`.


Rather than working directly with the base version of python, we recommend creating a virtual environment.
This step helps to avoid conflicts between different python tools.
To build a virtual environment on your system (here we assume the location of `~/python-venv/orion`), run the following:

.. code-block:: bash

    # Create the environment
    mkdir -p ~/python-venv/
    /path/to/python3.11 -m venv ~/python-venv/orion
    
    # Load the environment to use python
    source ~/python-venv/orion/bin/activate
    python --version

    # Or access the new python executable directory
    ~/python-venv/orion/bin/python --version



ORION Installation
^^^^^^^^^^^^^^^^^^^^^^^

To complete the installation of ORION, you can install it from source:

.. code-block:: bash

    cd /path/to/download/orion
    git clone git@gitlab.com:NRAP/orion.git
    cd orion
    python -m pip install ".[pycsep]"


or from pypi:

.. code-block:: bash

    python -m pip install orion-seismic-forecast[pycsep]


.. note::
   Both of these options assume that you have loaded the python environment generated in the previous step.

