#####
ORION
#####

Welcome to the Operational Forecasting of Induced Seismicity (ORION) tool documentation.


.. grid:: 2
    :gutter: 4

    .. grid-item-card::

        Quick Start Guide
        ^^^^^^^^^^^^^^^^^^^

        New to ORION?  We will walk you through the setup process.

        +++

        .. button-ref:: QuickStart
            :expand:
            :color: info
            :click-parent:

            To the Quick Start

    .. grid-item-card::

        User Guide
        ^^^^^^^^^^^^

        Instructions on how to work with ORION interface and produce a seismic forecast for your site.

        +++

        .. button-ref:: UserGuide
            :expand:
            :color: info
            :click-parent:

            To the User Guide

    .. grid-item-card::

        Examples
        ^^^^^^^^^^^^

        Examples of ORION applied to target sites.

        +++

        .. button-ref:: Examples
            :expand:
            :color: info
            :click-parent:

            To the Examples

    .. grid-item-card::

        Forecast Models
        ^^^^^^^^^^^^^^^^^

        Descriptions of the seismic forecast models implemented in ORION.

        +++

        .. button-ref:: ForecastModels
            :expand:
            :color: info
            :click-parent:

            To the Forecast Models

    .. grid-item-card::

        Pressure Models
        ^^^^^^^^^^^^^^^^^

        Descriptions of the fluid pressure models implemented in ORION.

        +++

        .. button-ref:: PressureModels
            :expand:
            :color: info
            :click-parent:

            To the Pressure Models

    .. grid-item-card::

        Support / FAQ
        ^^^^^^^^^^^^^^^^^

        Answers to frequently asked questions about ORION.

        +++

        .. button-ref:: SupportInformation
            :expand:
            :color: info
            :click-parent:

            To the Support / FAQ


.. only:: builder_html 

   Download pdf version of documentation :download:`here <../public/orion.pdf>`.


Documentation
----------------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   quickstart
   user_guide
   examples
   forecast_models/forecast_models
   pressure_models/pressure_models
   data_formats
   statistical_parameters
   support
   osx_install
   developer_guide
   acknowledgements
   references


Indices and Tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. image:: ../src/orion/gui/smart.png
   :width: 45%

.. image:: ../src/orion/gui/nrap.png
   :width: 45%

.. image:: ../src/orion/gui/llnl.png
   :width: 45%

.. image:: ../src/orion/gui/lbnl.png
   :width: 45%
