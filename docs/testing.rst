
Testing Guidelines
===========================

ORION contains a set of unit and integrated tests that guarantee consistency and accuracy throughout the developement process.
Normally, these tests are run automatically as part of the Gitlab CI/CD process.
The instructions on this page are intended for developers who need to manually run the tests, add new tests, and/or update baselines.



Unit Testing
---------------------------

Unit tests are designed to evaluate isolated code components, and guarantee that they produce consistent outputs.


Unit Test Setup
^^^^^^^^^^^^^^^^

Before running the unit tests, please do the following:

- Install the module development pre-requisites: (i.e.: `pip install -r requirements_dev.txt` ).
- Uninstall ORION from the target system (`pip uninstall orion-seismic-forecast` ).

.. note::

    While the unit tests will run if ORION is installed, it may conflict with test coverage estimates.



Unit Test Execution
^^^^^^^^^^^^^^^^^^^^^

To run the unit tests, you should move to the ORION directory and call pytest:


.. code-block:: bash

    cd /path/to/orion
    python -m pytest --cov --cov-report term --cov-report xml:coverage.xml .


If the tests pass, then you should see some summary information in green:

.. code-block:: bash

    =============================== test session starts ===============================
    platform win32 -- Python 3.11.6, pytest-8.0.0, pluggy-1.4.0
    rootdir: C:\Users\username\path\orion
    configfile: pytest.ini
    plugins: dash-2.17.1, cov-4.1.0
    collected 38 items
    tests\test_coulomb_rate_state_model.py... [  7%]
    tests\test_grid.py...                     [ 13%]
    tests\test_projection.py...               [ 42%]
    tests\test_seismic_catalog.py...          [ 63%]
    tests\test_seismogenic_index_model.py...  [ 65%]
    tests\test_single_well_flow.py...         [ 84%]
    tests\test_wells.py...                    [100%]
    =============================== 38 passed in 5.05s ================================


Any test groups that fail will be highlighted in red.
Available diagnostic information, including the test file, the step name, and the failure mode will also be printed to the screen.


Updating Unit Tests
^^^^^^^^^^^^^^^^^^^^^^^

Unit test baselines are located in the test source, and usually include an assert statement:


.. code-block:: python

    def test_a_load_catalog(self, catalog, grid):
        catalog.load_data(grid)
        assert len(catalog) == 5000


In cases where unit test baselines need updating, you can make the appropriate change to the source code and commit/push the changes to your branch.


Adding New Unit Tests
^^^^^^^^^^^^^^^^^^^^^

Unit tests are found by the pytest tool based on file and function/class names.
To add a new set of tests, you simply need to create a new file in the *orion/tests* directory that begins with the keyword *test_*.
This file should define a set of functions that begin with the *test_* keyword and/or a class that begins with the *Test* keyword.
When ready, these tests can be added to the repository (`git add tests/new_test_name.py` ) and committed/pushed to your target branch.


Integrated Testing
---------------------------

Integrated tests are designed to evaluate the code in its entirety.
During these test, ORION will fetch and execute various configurations and compare the results to baseline files hosted on EDX.
These tests pass if the code completes without throwing any exceptions and produce outputs that are equal to the baseline (within a set tolerance).


Integrated Test Setup
^^^^^^^^^^^^^^^^^^^^^^^

Before running integrated tests, please do the following:

- Have the current version of orion installed on the target system (i.e.: `pip install .` ).
- Set the EDX_API_KEY environment on the target shell (i.e.: `export EDX_API_KEY=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx` ).  Your EDX API key can be obtained using `these instructions <https://edx.netl.doe.gov/sites/edxapidocs/obtainingAPIKey.html>`_ .


Integrated Test Execution
^^^^^^^^^^^^^^^^^^^^^^^^^^
 
To run the tests on the command line, you should move to the ORION tests directory and call the *integrated_orion.py* script with one or more of the following arguments:

- -f/--frontend = The target frontend for the test (tkinter, strive, or none)
- -c/--case = The target configuration for the test (none, "Decatur", etc.)


For example, this would run the Decatur configuration with no active frontend:


.. code-block:: bash

    cd /path/to/orion/tests
    python integrated_orion.py -c Decatur


In the Gitlab CI/CD environment, ORION is tested with various front-end configurations (none and "tkinter") and cases (none and "Decatur").
The command line output from the tests will contain information about the example/baseline download, the ORION forecast, and the baseline checks:


.. code-block:: bash

    Downloading test data from EDX...
    100% [==========] 117M/117M [00:00<00:00, 164MB/s]
    Downloading baselines from EDX...
    100% [==========] 1.41M/1.41M [00:00<00:00, 22.7MB/s]
    (2024-11-20 00:08:55,921 data_manager_base:270) Resuming from cached config
    ...
    Checking for expected files...
    Comparing results to baseline...
    ...
    Cleaning up project directory and file based variables 00:00


If the integrated tests pass, then no additional information will be printed to the screen.
For test failures, this script will print out relavent diagnostic information to the terminal and throw an error.
The most common failure mode for tests is due to baseline changes, which will be highlighted in the error message.
For example:


.. code-block:: bash

    Comparing results to baseline...
    Array //Forecast Models//spatial_exceedance_probability does not match
    Current= [0.1, 0.0]
    Target= [0.2, 0.0]
    Indices= (array([ 0,  0]), array([ 1,  9]), array([9, 5]))
    Directory structure mismatch: //Forecast Models//spatial_forecast_count
    Array //Forecast Models//spatial_forecast_rate does not match
    Current= [ 0.04497767  0.04497767  0.03991772]
    Target= [1.81650682e-04 1.81650682e-04 1.05192876e-04]
    Indices= (array([ 0,  0,  0]), array([ 0,  0,  0]), array([ 0,  1,  2]))
    ...
    AssertionError


For any failed cases, you may want to save a copy of the hdf5-format results file that is created here: *orion_results/orion_plot_data.hdf5* .


Integrated Test Rebaselining
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Integrated test rebaselining is the process used to update the tests in cases where conflicting changes should be accepted.
This might happen in cases where you make changes to a forecast model and expect the output to meaningfully change, or in cases where you add/remove output data to the file.
To rebaseline a test, complete the following steps:

- Upload a copy of the new baseline file to this `EDX Folder <https://edx.netl.doe.gov/workspace/resources/orion?folder_id=9989cf59-6d11-4c80-816e-3469d4d3677e>`_ .
- Obtain the resource ID value for any new baselines (double click the file, copy the value in the ID section).
- Update the corresponding baseline ID in the *tests/integrated_baseline_ids.json* file.
- Re-run the tests to verify that they now pass.
- Commit and push the changes to your working branch.

