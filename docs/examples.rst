
.. _ORIONExamples:


#########
Examples
#########


Available Examples
===================

The ORION team maintains the following example configurations:


Decatur
--------

The Illinois Basin Decatur Project (IBDP) carbon capture and storage test site serves as our primary development target for ORION.
This example contains the following data:

* A seismic catalog from 2011-2018 containing event locations and magnitude data :cite:`williams2019overview` .
* A pressure table model for the site derived from a TOUGH-FLAC model during the same time period :cite:`segall2015injection`.
* Well trajectories and fluid injection time histories
* Calibrated forecast model parameters estimated during the injection period.



Oklahoma
--------

The Oklahoma example contains a collection of configurations for sites across the state, focusing on induced seismic activity from waste-water injection.
These include:

* Blaine
* Cushing (2014, 2015)
* Eagleton
* Fairview
* Jones
* Manning
* Pawnee
* Prague

.. note::
   Some of these examples are a work-in-progress.




How to Obtain Examples
=======================

Example data and configurations for Orion are available on the Energy Data eXchange (EDX).
Available examples can be loaded by selecting them from the *Model/Examples* dropdown menu.

.. figure:: ./figures/example_dropdown.png

   The example page can be opened from the dropdown menu.


EDX Download
--------------------------

Please note: You will need to have an account on `EDX <https://edx.netl.doe.gov>`_ and be a member of the  `Orion workspace <https://edx.netl.doe.gov/workspace/resources/orion>`_ to download examples .
To request membership, please contact a member of the `Orion development team <https://gitlab.com/NRAP/orion/-/blob/develop/CONTRIBUTORS>`_.
Next, you will need to locate your EDX API key by clicking your username in the top-right corner of EDX, and copying the value in EDX-API-KEY.
This key is private to your account, so please do not share it with others.

You can bring up the Example Selection menu in Orion by selecting *Model/Download Examples*.
This will open up a new window where you can select the location to download examples, enter your EDX API key, and select which examples you would like to download.
When ready, you can download the targeted examples by clicking the button at the bottom of the window labeled *Download Examples*.
Once complete, the new examples should appear up in the *Model/Examples* dropdown menu
Orion will skip downloading any examples that are already present in the target directory unless the *Force Download?* option is selected.

Orion will attempt to remember the example download configuration, and resume where you left it.
Like other orion configuration data, these values are stored in the cache directory (default: *~/.cache/orion*).
Please note, that if you move the downloaded examples on the local machine or change the download directory, Orion may not be able to find them.
If this occurs, try updating the download path in the Example Download window.

.. image:: ./figures/example_selection.png
    :width: 50%


Example Format
-------------------

Orion examples files on EDX use a zip format, with top-level folders corresponding to individual scenarios. 
Each scenario folder should contain a *config.json* file and any supplementary data files required to run the problem.
Alternate configuration files named *config[alternate].json* can be included, and will be appear in the *Examples* menu as *[folder_name]_[alternate]*.
Please note: these configuration files use a special keyword `[BUILT_IN_PATH]` to indicate the path of the scenario folder on the local machine.

The active Orion configuration can be saved as a zip-format example by selecting **Model/Save config to file** from the main window, and selecting an output file with a .zip extension.
As part of this process, Orion will attempt to collect any required data for the example, and then construct an appropriate configuration file.
