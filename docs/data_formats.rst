.. _DataFormats:


Data Formats
==============


Seismic Catalog Files
-----------------------

Orion can read from a variety of seismic catalog formats, both local and from remote sources such as ComCat.
For locally hosted catalogs, we prefer that data be in one of the following csv or hdf5 formats:


csv Catalog Format
^^^^^^^^^^^^^^^^^^^^

csv format catalog files are comma-delimited text files with 1-2 header lines, depending upon their content.
Data columns can be placed in any order, and must include epoch, magnitude, depth, and either latitude/longitude or easting/northing.
The expected units for these values are seconds for epoch, and meters for depth, easting, and northing.
For catalogs that contain easting/northing information, you can supply the utm zone information in the first line of the header; otherwise, Orion will assume that the coordinate system is local.

.. code-block:: python

   utm_zone,6SU
   epoch,magnitude,depth,easting,northing
   1367956482,1.6,3.308,703178,3981454
   1367958659,1.43,3.4,703150,3981474


.. code-block:: python

   epoch,magnitude,longitude,latitude,depth
   1367956482,1.6,-96.74708,35.95636,330
   1367958659,1.43,-96.74707,35.95634,340



hdf5 Catalog Format
^^^^^^^^^^^^^^^^^^^^

hdf5 format catalog files are expected to have entries for each of the catalog features on the root level.
Similar to the csv format, the catalog mush include epoch, magnitude, depth, and either latitude/longitude or easting/northing.
The expected units for these values are seconds for epoch, and meters for depth, easting, and northing.
For catalogs that contain easting/northing information, the file can also contain an entry for the utm_zone string; otherwise, Orion will assume that the coordinate system is local.

.. code-block:: python

    {
        'epoch': [1367956482, 1367958659],
        'magnitude': [1.6, 1.43],
        'depth': [330, 348],
        'easting': [703178, 703150],
        'northing': [3981454, 3981474]
    }



Table Files
-----------

Some inputs to Orion use a CSV or HDF5 format that can contain structured or unstructured data:

- Structured: This file/folder is expected to contain children named *x*, *y*, *z*, and/or *t*, which are 1D arrays and define the dimensions of the grid. This file/folder also contains children that contain ND arrays, which match the size of the target grid. Typically, the expected grid order is *xyz*, *xyzt*, or *t*.
- Unstructured: This file/folder is expected to contain children named *x*, *y*, *z*, and/or *t*, which are 1D arrays and define the points in the dataset. This file also contains children that are 1D arrays of the same length, which define properties at the target points.


Note: When constructing these files, you may want to use the HDF5 wrapper in :mod:`orion.utilities.hdf5_wrapper`, which simplifies working with these files.
The following example shows how to write a simple, structured 3D file in the expected format:

.. code-block:: python

   import numpy as np
   from orion.utilities import hdf5_wrapper

   # Setup the data
   x = np.linspace(0, 1, 10)
   y = np.linspace(0, 2, 20)
   z = np.linspace(0, 3, 30)
   X, Y, Z = np.meshgrid(x, y, z, indexing='ij')
   p = 2 * X + 3 * Y + 4 * Z

   # Save to an hdf5 format file
   with hdf5_wrapper.hdf5_wrapper('example.hdf5', mode='w') as data:
      data['x'] = x
      data['y'] = y
      data['z'] = z
      data['p'] = p


This can be extended to 4D with some small adjustments:


.. code-block:: python

   import numpy as np
   from orion.utilities import hdf5_wrapper

   # Setup the data
   x = np.linspace(0, 1, 10)
   y = np.linspace(0, 2, 20)
   z = np.linspace(0, 3, 30)
   t = np.linspace(0, 3, 30)
   X, Y, Z, T = np.meshgrid(x, y, z, t, indexing='ij')
   p = 2 * X + 3 * Y + 4 * Z + 5 * T

   # Save to an hdf5 format file
   with hdf5_wrapper.hdf5_wrapper('example.hdf5', mode='w') as data:
      data['x'] = x
      data['y'] = y
      data['z'] = z
      data['t'] = t
      data['p'] = p


.. _WellFileFormat:

Well Files
-----------

Sets of well data such as name, location, flow rate, etc. can be specified via a *.csv* or *.xlsx* format file.
This file should include a header with some or all the entries: *name* (required), *x*, *y*, *z* (required), *latitude*, *longitude*, *t*, and *q*.
Each row should correspond to a single well, and each entry can contain any of the following: a floating point value, a comma-delimited string of floating point values (only for *.xlsx* files), or a string containing the path to a *.csv* file containing the values.

The location entries *x*, *y*, and *z* should be specified in absolute coordinates in the target reference fame (typically UTM), using units of *meters* and with positive values of *z* moving downwards.
If *latitude* and *longitude* values are given, these will have precedence over over any *x* and *y* given.
The flow entries *t* and *q* should be specified in *seconds* (as an epoch) and *m3/s*, respectively.
The following is an example of a valid well file:


.. include:: example_inputs/example_flow_table.rst


.. note::
   comma-delimited entries for a given well should have a consistent length [`len(x) == len(y) == len(z)` and `len(t) == len(q)`].
