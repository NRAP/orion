
.. _QuickStart:

##################
Quick Start Guide
##################

Installation
===========================

Pre-compiled Version
----------------------------------

For certain systems, pre-compiled versions of Orion are available on the `Release Page <https://gitlab.com/NRAP/orion/-/releases>`_.
These files are portable, so they simply need to be downloaded to the local machine and executed.

.. note::
   The *_debug* versions of ORION will launch a console that displays the current status of ORION and any potential error/warning messages that may arise during processing.

.. note::
   These executables contain their own version of python, and do not require you to install your own version.


Python Requirements
--------------------------

For users running ORION on Windows or Linux systems (Ubuntu, etc.), you will need have a version of Python (>=3.9, <3.12) available on your system.
We have tested ORION with both `regular python <https://www.python.org/>`_ and `anaconda <https://www.anaconda.com/>`_ .
For users running OSX, we recommend following these instructions: :ref:`osx-instructions` .

.. note::
   For anaconda users, we recommend users install the conda package (`conda install --channel conda-forge pycsep`) before installing ORION.


.. note::
   On shared machines, we recommend that you install ORION within a virtual Python environment (see `this for details <https://virtualenv.pypa.io/en/latest/>`_ ) to avoid dependency issues with other tools.


Other Pre-requisites
----------------------

Some of the python packages that ORION uses have non-Python pre-requisites that may not be present on your system (proj, geos, shapely).
To install these pre-requisites, we recommend doing the following:

- (Windows) Install `OSGeo4W <https://trac.osgeo.org/osgeo4w/>`_ 
- (OSX) Use homebrew to install the pre-requisites (`brew install proj`, `brew install geos`).
- (Ubuntu) Use apt-get to install the pre-requisites (`apt-get install libgeos proj-bin`)
- (Anaconda) Install the pycsep conda package (`conda install --channel conda-forge pycsep`)


Install from pip
---------------------------

To install ORION using the pip package manager, you simply need to run:

.. code-block:: bash

   python -m pip install orion-seismic-forecast


Install from Source
---------------------------

To install ORION from it's source you must first clone the gitlab repository.
You can then install it within an existing Python environment using `pip`:

.. code-block:: bash

   cd /path/to/download/orion
   git clone git@gitlab.com:NRAP/orion.git
   cd orion
   python3 -m pip install .


.. _OptionalFeatures:


Running Orion From the Command Line
====================================

To open the Orion GUI, run the following from the command-line:

.. code-block:: bash

   orion_forecast


The *-f* argument indicates which graphical frontend to use.
Options include *tkinter* (default), *strive*, and *none*:

.. code-block:: bash

   orion_forecast -f strive --config filename.json


Note: if using the *strive* option, your terminal will indicate where interface is running.
To view it, simply copy/paste the URL into your browser of choice.

.. code-block:: bash

   Dash is running on http://127.0.0.1:8050/


As it is running, Orion will maintain a copy of its configuration in the user cache (located at `~/.cache/orion/orion_config.json`).
When opening Orion, the code will attempt to resume using this file.
If you encounter any error opening Orion, we recommend that you delete the cached file and try again.

