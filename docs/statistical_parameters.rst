
.. _StatisticalParams:


##########################
Statistical Parameters
##########################


Gutenberg-Richter relationship
==============================

We assume that the catalogs here all follow the exponential Gutenberg-Richter relationship :cite:`Gutenberg1944`:

.. math::
    \log_{10}N = a - bM_c

An example frequency magnitude distribution (FMD) following the Gutenberg-Richter law is shown here for the case of New Zealand. 
Red error bars frequency counting errors :math:`\sqrt N` to one standard deviation.

.. image:: ./figures/statistical_parameters_NZ_FMD.png


Magnitude of Completeness
=========================

The magnitude of completeness, :math:`M_c` is the lowest magnitude at which 100% of the earthquakes in a space-time volume are detected :cite:`Rydelek1989,Mignan2012`.
This parameter determines the range over which the data can be analyzed without artifacts associated with incompleteness.
It is a fundamental constraint on the data used for the determination of the relevant model parameters, such as the a -and b-values.
A robust estimate of the value of :math:`M_c` is crucial to avoid undersampling (if the estimate is too high) or biased parameter values (if the estimate is too low and uses incomplete data).
In ORION, we currently allow two methods to estimate :math:`M_c`: the Maximum Curvature (MaxC) method :cite:`Wyss1999,Wiemer2000` and the b-value stability method :cite:`Wiemer2000,Cao2002`.

The MaxC method is based on the evaluation of changes in the frequency-magnitude distribution and calculates the highest value of the first derivative of the cumulative frequency curve.
This is equal to the frequency-magnitude bin with the highest number of events.
It is the basis of the b-value stability method, which is the default method in ORION.
This method relies on the assumption that b-value estimates increase for cut-off magnitudes less than :math:`M_c` and remain constant for cut-off magnitudes greater than or equal to :math:`M_c`.
It selects :math:`M_c` as the as the magnitude above which the b-value stabilises within error of the b-value.
This method is commonly used when the focus of study is the b-value parameter and has been recognized as a reliable and accurate method for the estimate of :math:`M_c` :cite:`Roberts2015`.

Gutenberg-Richter b-value
=========================

The b-value is the gradient of the frequency-magnitude distribution following a Gutenberg-Richter law and provides the ratio of small to large earthquakes in a catalog.
We allow two methods in which the b-value is calculated: via the maximum likelihood estimate (MLE) :cite:`Aki1965` and via the b-positive method :cite:`vanderElst2021`.

Both methods use a maximum likelihood estimate to estimate b, with the MLE method using all event magnitudes above :math:`M_c` (emphasizing the importance of a robust estimate of :math:`M_c`):

.. math::
    \tilde b = \frac{log_{10}(e)}{\bar m - (M_c - \Delta m/2)}

where :math:`\tilde b` is the estimate of the b-value, :math:`\bar m` is the mean magnitude and :math:`\Delta m` is the magnitude bin width, set to 0.1.

whereas the b-positive method uses only magnitudes where the difference between successive earthquakes is positive. Negative differences are ignored. 
This method is the default in ORION as it does not require an estimate of :math:`M_c`. 


Gutenberg-Richter a-value
=========================

The a-value is the number of events above magnitude zero and can be estimated from the best fit b-value and the total number of events above the completeness threshold, or an extrapolation of the best fit to the data below :math:`M_c`.
This can be calculated by rearranging the Gutenberg-Richter relationship to give 

.. math::
    a = 10^{\log_{10}N + b(M_c + 0.05)}