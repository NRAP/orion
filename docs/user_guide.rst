
.. _UserGuide:

##########
User Guide
##########


ORION Setup
===========

To setup ORION on your machine, please follow these instructions: :ref:`QuickStart` .



Running ORION
===============

If you are working with a pre-compiled version of ORION, you can start as you would any other program (double-clicking the ORION icon, etc.).
Otherwise, to start ORION from the command line, open your terminal and run the following command:

.. code-block:: bash

   orion_forecast


.. note::
   If the above command does not work, you may need to activate your python environment or call the orion_forecast script using its full path.


.. note::
   The orion_forecast can accept a number of command-line options.  To see these, try running `orion_forecast --help`



First-time Run / Quickstart Wizard
-----------------------------------

If you are running ORION for the first time (or have cleared the ORION cache), the code will open a quickstart wizard to help you through the setup progress.

.. note::
   You can close the quickstart wizard at any time if you would like to load an example or configure ORION manually.

.. note::
   You can re-open the quickstart wizard at any time by selecting `Model/Quickstart` from the program dropdown menu.


.. figure:: ./figures/quickstart_menu.png

   The quickstart can be opened from the dropdown menu.



The starting page for the quickstart wizard will ask you how you would like to use ORION.
Depending on your answer, the quickstart wizard will ask you to enter information about your target location, any seismic activity, well information, etc.
For new users, we recommend selecting either "Understand earthquake activity in my community" or "Evaluate risks following a recorded earthquake event".
At any point, you can select the `Next` button at the bottom of the screen to move forwards, or click `Previous` to move backwards.

.. figure:: ./figures/quickstart_landing.png

   Landing page for the quickstart wizard.
   Click *Next* to move forwards and *Previous* to move backwards.


.. note::
   If the `Next` button does not advance the quickstart wizard, you may be missing required information on the current page.


Selecting the second option "Evaluate risks following a recorded earthquake event" will present you with a page that asks about the target event (this is the only difference between this and the other landing page option).
Here, you can enter the time period to search for the event.
You can also add an optional earthquake identifier, which can be obtained by selecting the event from the `USGS Earthquakes Map <https://earthquake.usgs.gov/earthquakes/map/?extent=8.84165,-137.37305&extent=56.84897,-58.62305&range=week&baseLayer=street&settings=true>`_ and going to the event overview page.

.. figure:: ./figures/quickstart_event_details.png

   Event detail step for the quickstart wizard.


Moving onto the next page, the quickstart wizard will then ask you about how far to search for events, how much historical data to consider, and how far to forecast into the future.

.. figure:: ./figures/quickstart_search_size.png

   Search window step for the quickstart wizard.


Then, depending on whether you specified an earthquake event ID in a previous step, the quickstart wizard may ask you for details about your target location.
This can be given in terms of a zip/postal code or via a target latitude/longitude.

.. figure:: ./figures/quickstart_location.png

   Location information step for the quickstart wizard.


Selecting `Next` one more time will bring you to the end of the wizard.
At this point, ORION will attempt to process your requests, build a configuration file, and generate a forecast.
The following figure was generated using the quickstart wizard for a zip code in the San Francisco Bay Area.

.. figure:: ./figures/quickstart_result_bay_area.png

   Spatial forecast tab generated for the Bay Area quickstart example.
   This case does not include a pressure model, so the spatial forecast is uniform over the target area.


.. figure:: ./figures/quickstart_result_bay_area_temporal.png

   Forecast model tab generated for the Bay Area quickstart example.
   Again, this case does not include a pressure model, so the temporal forecast
   is only based on the observed seismic activity.


.. note::
   Certain tabs may or may not be visible depending on options you selected in the quikstart wizard.  See :ref:`Changing Visibility` for more details.


Forecast Results
-----------------

ORION model forecast results come in two forms: spatial and temporal.
The spatial forecast are visualized up on the :ref:`Spatial Forecast` tab, and show in relationship to observed seismic activity, known wells, and map data.
These data are shown as 2D snapshot in time, an can be moved backwards/forwards in time via the slider bar at the bottom of the window.
The box to the right of the spatial plot can control which layers are active, and which fields the background color correspond to.

The temporal forecasts are visualized on the :ref:`Forecast Models Page` tab, and include probabilistic estimates of seismic activity and a forecast time series.
The bar chart and dial plot indicate the probability that a seismic event exceeding a certain magnitude will occur within the next X days.
These magnitudes and time periods can be edited by the user by selecting `Model/Configure`, selecting the `Forecast Model` tab, and setting new values in the `Magnitude Exceedance Plots` section.

The time series forecast at the bottom of the screen shows the evolution of the individual forecast models over time, the observed catalog data, and the ensemble (or "optimal") forecast model.
The style of these plots can be changed on the same configuration page.
By default, the time series show the cumulative number of events over time (instead of the instantaneous rate), and the forecast models show the average over the active pressure models (as opposed to a range or tornado-style plot of tracks.)



Closing / Resuming ORION
-------------------------

When ORION closes, it attempts to create a cache directory (default = `~/.cache/ORION`), where it can store previous configuration data, examples, etc.
During the ORION startup process, it will check this directory for the last configuration and attempt to resume where it left off.

.. note::
   If ORION fails to start, it may be due to out of date configuration files in the cache directory.  Try deleting this directory and start again.


Input Data Formats
-------------------

Depending on user inputs, ORION can load data from a variety of local file types or from publicly available data streams, such as the `USGS ComCat System <https://earthquake.usgs.gov/data/comcat/>`_.
More information on data formats can be found here: :ref:`DataFormats`.


.. note::
   To use the ComCat system, ORION requires the optional pycsep package.  See the instructions here for additional details: :ref:`OptionalFeatures`.



Saving / Loading Data
-----------------------

The `File` option in the top dropdown menu contains options for exporting data and figures from ORION:

- `Save figures` will export all of the active figures as `.png` files.
- `Save timelapse` will export the active figures as `.png` files for each of the active timesteps in the model.
- `Save data` will export the underling plot data as an `.hdf5` format file.


The `Model` option in the top dropdown menu contains options for importing and exporting configuration data in ORION:

- `Load config from file` will open up a window where you can browse for and select a valid ORION configuration file.  After selecting OK, ORION will updates its configuration and re-generate any active plots.
- `Save config to file` will open up a window where you can select a path to save the current ORION configuration to a file.
- `Download example inputs` will open a new window with options for downloading ORION examples from EDX.  See :ref:`ORIONExamples` for additional details.
- `Examples` contains a child menu with any downloaded ORION examples ready for use.  Selecting one of these will load the target configuration and re-generate figures.



ORION Interface
================

When running the ORION graphical user interface (GUI), a new window will open on the local machine.
At the top of the window, there are a set of dropdown menus that can be used to load data, save figures, open the configuration window, and load a set of pre-constructed examples.
We recommend that users load the Illinois Basin Decatur Project (IBDP) example by selecting *Model/Built In/Decatur*, and then inspect the configuration window by selecting *Model/Configure*.

Once ORION is configured, you can trigger pressure and/or seismic forecast calculations using one of the three buttons at the bottom of the interface.
As these calculations complete, they will create and populate the figures in the main body of the interface, which is organized into a set of tabs: *Spatial Forecast*, *Seismic Catalog*, *Forecast Models*, *Pressure*, *Fluid Injection*, *Geologic Model*, and *Log*.
For figures that display snapshots of data over time, the active time can be set via the time-slider at the bottom of the interface or via the configuration window.


Spatial Forecast
----------------

The Spatial Forecast page displays spatial forecast information on the user-defined grid, well locations, and the location of seismic events (up to the active time).
To the right of figure, there is a set of checkboxes that can be used to turn on/off the various layers of the plot.
The *Seismic Forecast* layer shows the estimated seismic event frequency (number of events per year) at points in the grid, and the *Probability* layer shows the estimated likelihood that an event greater than a target magnitude will occur during the next period of time.
The minimum magnitude and time frame can be set in the configuration window under *Forecast Models* (*Time range*, *Dial magnitude*).

.. figure:: ./figures/spatial_forecast_tab.png

   The spatial forecast page shows time slices of the spatial forecast, the location of seismic events, and any wells.
   The slider bar at the bottom of the window can be used to advance the time slice shown in this plot.
   The box to the right hand side of the plot can be used to turn on or off image layers.


.. note::
   If the background map is not visible, it may be blocked by your network.
   To avoid this issue, try selecting the *Allow self-signed certs* option from the *Appearance* page in the Model Configuration window.


Forecast Models Page
---------------------

The *Forecast Models* page displays the results of the seismic forecast models for the entire domain.
These include:

#. A bar chart showing the estimated likelihood that an event greater than a set of target magnitudes will occur during the next period of time. The magnitude distribution and time frame can be set via the *Configuration* window under *Forecast Models* (*Time range*, *Bar chart bins*)
#. A dial plot showing the estimated likelihood for a target magnitude. As before, the parameters for this plot can be set via the *Configuration* window under *Forecast Models* (*Time range*, *Dial magnitude*)
#. A time series plot showing the observed number of events, the estimated number of events produced by each forecast model, and an ensemble forecast.

.. figure:: ./figures/forecast_model_tab.png

   The Forecast Models page shows temporal seismic forecast information in a variety of formats.
   Layers can be turned on/off via the checkboxes to the right of the time series plot.


Seismic Catalog
---------------

The *Seismic Catalog* page displays key information about the active seismic catalog:

#. Map view: This shows the location and magnitude of events. The data are projected onto a local UTM grid, with a local origin in the southwest.
#. Magnitude distribution: This shows how often different sized seismic events occur. The red trend-line shows the current fit for the Gutenberg-Richter parameters, and the red triangle indicates the smallest magnitude where we have a complete catalog.
#. Time series: This shows the size of events over time in days, with the origin set to the first event measured in the catalog.
#. b-value variations: This shows how the Gutenberg-Richter b-value changes over time, which believed to be a key indicator of future seismic activity.

.. figure:: ./figures/seismic_catalog_tab.png

   The seismic catalog page shows a variety of features related to the observed events.


3D Overview
---------------

The *3D Overview* page displays ORION data including seismic data and well trajectories.
This plot can be rotated by clicking/dragging the left mouse button, zoomed by clicking/dragging the right mouse button, and panned by clicking/dragging the center mouse button. 


.. figure:: ./figures/3d_overview_tab.png

   The 3D overview page shows a variety of features related to the observed events.


Pressure
--------

The *Pressure* page displays the results of the active fluid pressure model for the entire domain, including the current pressure and pressurization rate (dpdt).
This page also displays the active well locations and the current seismic events.

.. figure:: ./figures/pressure_tab.png

   The pressure model page shows the spatial distribution of pressure or pressurization rate over the target area.
   The box to the right hand side of the plot can be used to turn on or off image layers.


.. note::
   This tab is only visible to *Operator* and *Super User* users by default (see :ref:`Changing Visibility`).



Fluid Injection
---------------

The *Fluid Injection* page includes four figures:

#. Well location: The location of the active wells
#. Flow rate: The fluid injection rate by well over time
#. Fluid volume: The overall fluid volume injected into the reservoir over time
#. Pressure: The estimated pressure at the monitor wells over time

.. figure:: ./figures/fluid_injection_tab.png

   The fluid injection page shows information about wells, fluid injection rates, etc.


.. note::
   This tab is only visible to *Operator* and *Super User* users by default (see :ref:`Changing Visibility`).


Well Database
-------------

The *Well Database* shows the location of wells pulled from external data sources.
It also shows the location of active wells in the model, the grid extents, and current seismic activity.
The external well database can be updated under :ref:`Well Database Configuration`.

.. figure:: ./figures/well_database_tab.png

   The well database page shows available wells for ORION forecast calculations.


.. note::
   This tab is only visible to *Operator* and *Super User* users by default (see :ref:`Changing Visibility`).



Configuration
==================

ORION can be configured either using the GUI (recommended) or via a JSON configuration file.
When you exit the code or manually save, ORION will write the configuration JSON file to the user cache (*~/.cache/ORION*) or to a user-requested location.
When orion_forecast is called with the `--config filename.json` argument, ORION will apply this configuration after launching.
If this argument is not given, and a cached configuration file is detected, then ORION will attempt to apply this file instead.

To open the GUI configuration menu, select *Menu/Configure* at the top of the main screen.
Like the main ORION window, the main body of the configuration interface contains tabs, which can be selected to display key information.
When the configuration window is closed, or the *Apply* button is pressed, the changes will be sent to ORION, and any active figures will be updated.
The configuration can also be saved to or loaded from a file by clicking on the *Save Config* and *Load Config* buttons.


ORION Configuration
-------------------

The landing page (*ORION*) for the configuration window contains settings that control the behavior of ORION and the active plots.
These include the snapshot time for active plots, the point size to be used for plots, the level of log messages to be produced, and the location of the log file.

.. image:: ./figures/configure_visibility.png


Changing Visibility
^^^^^^^^^^^^^^^^^^^^^

Some plot and configuration tabs are hidden for certain user types.
If you don't see the figures/options you are expecting, try changing the *User Type* from the dropdown menu.
Alternatively, you can  manually select tabs to show in the *Tab Visibility* box and press the *Apply* button to update the interface (or close the config window).


Grid Configuration
---------------------

The *General* page contains settings for the reference time and spatio-temporal grid.
Internally, the ORION forecasting engine works with relative timestamps, with `t = 0s` indicating the *present* time.
If the *Reference Time* parameter is left blank, then ORION will use the actual current time in its calculation.
Otherwise, this parameter can be set to a value in the past, to allow for testing or *retrospective* forecasting.
The *Time range* and *Plot range* parameters define the temporal grid and plot extents for the analysis, and are specified in relative time in days.

The *X Range*, *Y Range*, and *Z Range* parameters are specified relative to the *Spatial Origin* parameter.
Other spatial input parameters in ORION are typically given as absolute locations in the UTM grid or in latitude/longitude.

If you are using the UTM option, you may need to set the *zone* (e.g., `14S`) when requesting catalog data from ComCat.
This parameter will be automatically calculated if you are using a catalog on the local machine that includes latitude/longitude information.

Note: if the checkbox labeled *Permit Modification* is selected, ORION may attempt to modify the grid dimensions to match the extents of certain input files.

.. image:: ./figures/configure_general.png


Seismic Catalog Configuration
-----------------------------

The *Seismic Catalog* page contains the location of the target catalog, or instructions to fetch it.

The *Catalog Path* entry can be used to specify the path to a catalog on the local machine.
Clicking on the *:* button to the right of this box will open a file explorer, which you can use to navigate to the target file.
See :ref:`Seismic Catalog Files` for a description of the available formats.

The *Declustering Algorithms* box can be used to specify which (if any) catalog declustering methods should be applied to the active catalog.
See the `bruces python package <https://keurfonluu.github.io/bruces/>`_ for additional details on these methods.

The *Use ComCat* option can be used to download seismic catalog from public sources.
The bounds of this query in time and space are the same as the ORION grid (see the previous section), and the minimum catalog is set via *Min Magnitude Request*.

.. note::
   If *Catalog Path* is not empty, *ComCat* requests will be ignored.

.. note::
   The *Use ComCat* feature requires the optional *pycsep* package to fetch catalogs from the internet.
   See the instructions here for additional details: :ref:`OptionalFeatures`.


.. image:: ./figures/configure_seismic_catalog.png


Forecast Model Configuration
----------------------------

The *Forecast Models* page contains a set of nested set of tabs, which control the general settings for forecast calculations and forecast-specific parameters.
The checkbox at the top of the main page can be used to switch between cumulative and instantaneous forecast calculations.
The *Plot type* dropdown box contains the following options for visualizing forecast results:

- All: Show an individual line for each combination of active forecast model and pressure model.
- Average: Show the average forecast result for each method.
- Range: Show the range of forecast results for each method.


The *Magnitude Exceedance Plots* section includes settings for various probabilistic calculations, which look at the probability of a seismic event exceeding a target magnitude (*Dial Magnitude* or *Bar Chart Bins*) over the next time period (*Time Range*).
Some of these plots use a stoplight coloring system, which is configured via *Color Thresholds*.

The two checkboxes at the bottom of the main page can be used to modify the forecast calculation scheme.
The *Permissive* option will instruct ORION to catch any error produced by the forecasting model (otherwise the code could exit if it encounters an error).
The *Parallel calculations* option will instruct ORION to calculate the active forecast models in parallel, which can help to speed up the calculation and keep the GUI responsive.
Note: See the specific forecast model documentation for information on their configuration parameters.

.. image:: ./figures/configure_forecast.png


Pressure Model Configuration
----------------------------

The *Pressure* page contains a child window, which control the general settings for fluid pressure calculations and pressure model-specific parameters.
The main page contains a dropdown box *Pressure Model* where you can select the active image layer (pressure or pressurization rate) in the GUI.
The buttons at the bottom of this page can be used to remove the currently selected model or add a new pressure model to ORION.
See :ref:`PressureModels` for additional details on these models.

.. image:: ./figures/configure_pressure.png


.. note::
   This tab is only visible to *Operator* and *Super User* users by default (see :ref:`Changing Visibility`).


Fluid Injection Model Configuration
-----------------------------------

The *Fluid Injection* page contains a single file entry that points to a *.csv* or *.xlsx* file containing well and fluid injection data.
See this page for details on this format: :ref:`WellFileFormat`.


.. image:: ./figures/configure_fluid_injection.png


.. note::
   This tab is only visible to *Operator* and *Super User* users by default (see :ref:`Changing Visibility`).


Well Database Configuration
---------------------------

The *Well Database* page contains options to obtain well information from external sources:

- *Data Source*: Select the source of the data (Note: only the Oklahoma Corporation Commission is currently available)
- *Request Range*: Set the start/stop time for the well data request (If either of these are empty, then ORION will choose the maximum/minimum available times for the dataset)
- *Autopick Time Buffer*: For pressure calculations, use well data this amount of time before/after the grid

The local well database is stored in the ORION cache directory (*~/.cache/ORION/*) in an HDF5 format.
The buttons at the bottom of the page do the following:

- *Update data*: Request well data using the current configuration and update the database
- *Clear data*: Remove any existing well data
- *Autopick wells*: Add any available wells within the grid to the pressure calculation

.. image:: ./figures/configure_well_database.png


.. note::
   This tab is only visible to *Operator* and *Super User* users by default (see :ref:`Changing Visibility`).


Appearance Configuration
----------------------------

The *Appearance* page contains settings that control colormaps and display options.
The *Add map layer* option will attempt to add a map to spatial plots.
The *Allow self-signed certs* option is required to generate maps on certain networks.


.. image:: ./figures/configure_appearance.png

