
.. _PackagingGuidelines:

######################
Packaging Guidelines
######################

Pyinstaller
===========================

ORION uses the `PyInstaller <https://pyinstaller.org/en/stable/index.html>`_ tool to create portable executable files that can be run on target systems.
The following sections provide guidelines on the required run files, specification (.spec) files, and compilation. 


Run File
----------------------------------

PyInstaller requires an entry point into the application.
In ORION's case, we provide separate entry points for the `legacy <https://gitlab.com/NRAP/orion/-/blob/develop/src/orion/_pyinstaller/run-orion.py>`_ and `strive <https://gitlab.com/NRAP/orion/-/blob/develop/src/orion/_pyinstaller/run-orion-strive.py>`_ versions.


Specifications File
----------------------------------

The pyinstaller .spec file defines the entry points into the application, compilation/run-time options, and any required assets.
PyInstaller allows applications to be compiled in single-file mode (a single file that is un-packed and run on-the-fly) and single-folder model (a directory that looks like a typical software installation).
Both of these options provide portable applications, and come with different pros/cons with regards to efficiency and ease of use.

ORION provides .spec files for both the legacy and strive-versions of the GUI, and for both single-file and single-directory modes.
For example, see this file: `spec <https://gitlab.com/NRAP/orion/-/blob/develop/src/orion/_pyinstaller/orion.spec>`_ .

Key configuration options include:

1) `filename.py`: The application entry script.
1) `console`: Opens a terminal at run-time where print/log messages are shown.
2) `icon`: A .png file that includes the icon for the target package.
3) `name`: The name for the executable file


In the example spec file, there are a couple of items to note:

1) For PyInstaller to analyze the application, it is required to traverse the target run file.  This can be an expensive process, with a large degree of recursion.  As such, PyInstaller often suggests increasing the global recursion limit in Python at the head of the file: `sys.setrecursionlimit(sys.getrecursionlimit() * 5)` .
2) We set the name of the executable based on the version string provided by ORION.
3) Sometimes PyInstaller may miss required package metadata or run-time files.  These can be set manually to the analysis object by adding the output of the `copy_metadata` and/or `collect_data_files` methods to the `datas` list.
4) Any packages that are *hidden* from PyInstaller can be added to the `hiddenimports` list.
5) Any packages that should be ingored by PyInstaller can be added to the `excludes` list.


Hook Files
--------------------------

PyInstaller expects that modules define a "hook file" that defines any resources that may be required for the module.
These are exposed via the `pyinstaller40/hook-dirs` entry point in the module's `setup.cfg` file.
In some cases, external python modules will not contain their own hook information, and you will be required to write your own.
For example, see this `file <https://gitlab.com/NRAP/orion/-/blob/develop/src/orion/_pyinstaller/hook-orion.py>`_ .


Compilation
--------------------------

Before compiling the executable, make sure that the pyinstaller package (`pip install pyinstaller`) and any required modules are installed (including the package that contains the .spec file).
To compile the executable, run the following commands:

.. code-block:: bash

   cd /path/to/orion/src/orion/_pyinstaller
   pyinstaller oroin.spec


The above process may take several minutes to run.
If any errors occur that are related to missing libraries, python modules, etc., you may need to update your target .spec file.
On completion, the target executable or file will be written to the `dist` folder.
We recommend that after you create the executable, you try running it and check the console output.
If python produces an error related to missing module or files, you may need to update the `datas` or `hiddenimports` entries in the .spec file.

.. note::
   The created files are designed to be *portable* and can be copied/run on a machine running a compatible operating system.  For single-directory targets, the entire directory can be zipped/unpacked on the target system.


.. note::
   PyInstaller is not capable of cross-compilation.  A separate executable must be created on each target system (OSX, Windows, etc.).

