
.. _ForecastModels:

################
Forecast Models
################

Available Forecast Models
==========================

The following seismic forecast models are implemented in Orion:

.. toctree::
   :maxdepth: 1

   coupled_coulomb_rate_state
   rate_and_state_ode
   seismogenic_index_model


Ensemble Forecast Calculation
=============================

The performance of individual seismic forecasting method can vary depending on site conditions and/or user configuration.
To address this, Orion generates an 'ensemble' or best-guess forecast using one of the following methods:

- Linear Grid: This approach uses a small period of test data in the user-defined grid to estimate the optimal weight and bias for active forecast models. These resultant weights can then be inspected and tuned in the configuration menu.
- ML Style: This method is under development. Our goal is to take a model that is pre-trained on historical data to produce the ensemble forecast. If desired, this model can then be updated to better match site conditions using transfer learning.


