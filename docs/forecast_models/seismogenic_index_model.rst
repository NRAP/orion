Seismogenic Index (SI) 
======================

The seismogenic index model estimates the seismogenic index (a scalar value) defined by the seismotectonic features of a region :cite:`Shapiro2010` originally and modified to include pressurization rate from the injection :cite:`langenbruch2018`.
In ORION, a further modified version is used, replacing the pore pressurization rate :math:`\dot p` with the Coulomb stressing rate :math:`\dot S`:

.. math::
    \dot S = \dot \tau + \mu(\dot \sigma + \dot p)

where :math:`\dot \tau` is the shear stressing rate (Pa/s), :math:`\mu` is the coefficient of friction and :math:`\dot \sigma` is the normal stressing rate (Pa/s). 
This Coulomb stressing rate can then replace the pore pressurization rate :math:`\dot p` (positive in tension), now considering the changes in tectonic stresses and pore fluid pressure effects.

Therefore, the seismogenic index is:

.. math::
    SI_{(i,j)} = \log_{10}N_{(i,j)} - \log_{10} \sum_{t < t_c} \dot S_{(i,j)}^2 + bM_c

where :math:`N` is the number of events within a given grid cell, :math:`b` is the Gutenberg-Richter b-value and :math:`M_c` is the magnitude of completeness. 
The SI is computed on a spatial grid :math:`(i,j)` at every time step :math:`t` up to the calibration period :math:`t_c`, which is expressed as the time-percentage of the injection period.
The :math:`b` and :math:`M_c` parameters are computed for the entire catalog up to the time :math:`t_c` = 100. This results in a scalar SI for each grid cell that is used to obtain event rates via:

.. math::
    R_{(i,j)} = \dot S_{(i,j)}^2 10^{SI_{(i,j)} - bM_c}

SI Input parameters
--------------------
- shear stressing rate, :math:`\dot \tau` (default is 7e-4)
- normal stressing rate, :math:`\dot \sigma` (default is 1e-6)
- Coefficient of friction, :math:`\mu` (default is 0.6)
- pore pressurization rate, :math:`\dot p` (obtained from pressure data, the default is zero prior to injection)
- number of events, :math:`N` (when N = 0, SI is calculated using :math:`\log_{10} \sum_{t < t_c} \dot S_{(i,j)}^2`)
- b-value, :math:`b` (estimated from complete catalog, default is 1.0)
- magnitude of completeness, :math:`M_c` (estimated from entire catalog)
- calibration period, :math:`t_c` (default 100%, subsets data to use any given amount of data to calibrate the SI and forecast the remainder for the remaining period)
