Coupled Coulomb Rate-State
==========================

The coupled Coulomb Rate-State (CRS) earthquake rate equations describe the evolution of seismicity rate as a function of stressing history :cite:`dieterich1994constitutive,dieterich2007applications,ferris1962theory,hager2021process`. 
The number of events is simply found by integrating the rates. The solutions for the number of events are listed below the rate solutions for each set of equations.


CRS Input Parameters
---------------------

The relevant parameters for the CRS model are as follows:

- Nominal coefficient of friction, :math:`{\mu_{0}}`
- Biot coefficient, :math:`{b}`
- Tectonic normal stressing, :math:`{\sigma}` (MPa)
- Tectonic normal stressing rate, :math:`{\dot{\sigma}}` (MPa/year)
- Tectonic shear stressing rate, :math:`{\dot{\tau}}` (MPa/year)
- Normal stress dependency, :math:`{\alpha}`
- Background seismicity rate, :math:`{r}` (events/years)
- Seismicity rate scaling factor, :math:`{RF}` (1/MPa)
- Enable clustering effects, (yes/no) :cite:`kroll2017delayed`


Input Parameter Selection
-------------------------

- Nominal coefficient of friction: Laboratory values of friction for intact rock range from 0 to 1, default values are on the order of 0.6
- Biot coefficient: Value should be consistent with the one used in the geomechanical simulations for computing pressure and/or stress
- Tectonic normal stress: Derived from lithostatic stress at average hypocentral earthquake depth. Default is 30 MPa
- Tectonic normal stressing rate: Estimated based on geodetic surveys and deformation modeling. Default value = 0 MPa/year
- Tectonic shear stressing rate, Estimated based on geodetic surveys and deformation modeling. Default value = 0.00035 MPa/year for mid-continent US
- Normal stress dependency: On the order of 0 to the nominal coefficient of friction
- Background seismicity rate: Estimated from historical seismicity catalogs over time periods with stable magnitude of completeness
- Seismicity rate scaling factor: scaling factor that relates the forecasted number of events to the observed number of events by scaling the reference stressing rate :math:`{\dot{S_{r}}}`. In future versions, this parameter will be included in the parameter optimization
- Enable clustering effects, (yes/no): Click yes, when significant mainshock/aftershock sequences are present in the observed seismicity data to enable enhanced parameter fitting.

The instantaneous earthquake rate due to a step change in Coulomb failure stress (CFS) is given by,

.. math::

    R = R_0\exp\left(\frac{\Delta{CFS}}{a\sigma}\right)

where :math:`{R_{0}}` is the steady-state background seismicity rate before the stress step, :math:`{a}` is the rate-state constitutive parameter, and :math:`{\sigma}` is the normal stress. The earthquake rate between stress steps, assuming a 
constant stressing rate is given by:

.. math::

    R(t) = \frac{1}{\frac{\eta}{\dot{S}} + \left(\frac{1}{r} 
    - \frac{\eta}{\dot{S}}\right)\exp\left(\frac{-\dot{S}t}{a\sigma}\right)}

.. math::

    N(t) = \frac{a\sigma}{\eta}\ln\left[\left(1 
            + \frac{R_{0}\eta}{\dot{S}}\right) 
            + \frac{R_{0}\eta}{\dot{S}}\exp\left(\frac{\dot{S}t}{a\sigma}\right)\right]

where :math:`{\eta}` is the ratio of the reference stressing rate, :math:`{\dot{S_{r}}}` to the reference background seismicity rate, :math:`{r}` (i.e., :math:`{\eta = \dot{S_{r}}/r}`). 

Computing the earthquake rate forecast is quite simple, assuming the pore-pressure and stresssing rate history due to injection is discrete.

- Step 1: Compute the rate at the time of the stress step with Equation 1 of this section
- Step 2: Allow the earthquake rate to evolve with time between the stress steps with Equation 2 of this section
- Step 3: Iterate for the duration of the pressure/stress change time series

Machine learning and numerical optimization techniques are used to solve for the best-fitting parameter values. The free parameters include the :math:`{\Delta{CFS}}`, :math:`{a}`, :math:`{\sigma}`, :math:`{r}`, and :math:`{\dot{S_{r}}}`. 
However, we can restrict the range of the values of these parameters, or allow only a subset of this list to vary during the optimization process. For example, if we believe that our estimate of the pressure change is accurate, this parameter can be held fixed, while we optimize the solution by varying the other parameters. 


.. image:: ../figures/configure_forecast_crs.png
