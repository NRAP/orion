ORION GUI Schema
=================

.. literalinclude:: ./data/gui_api_schema.json
   :language: json
   :linenos:


.. only:: builder_html

   :download:`Download schema file <./data/gui_api_schema.json>`.
