
.. _SupportInformation:


##########
Support
##########


FAQ
=========================

.. collapse:: What data do I need to run ORION?

    ORION requires location/grid information, a seismic catalog, and a pressure model to generate a seismic forecast.  Under certain circumstances, ORION can collect the required data from public data sources such as the ANSS Comprehensive Earthquake Catalog (ComCat) and state-operated well databases.


.. collapse:: Do you have example input data / configuration files for ORION?

    We have a set of example datasets and configuration files that are hosted on `EDX <https://edx.netl.doe.gov/workspace/resources/orion>`_ (note: these require an active EDX account to access). See `these instructions <https://nrap.gitlab.io/orion/examples.html#how-to-download-examples>`_ for additional details on using/downloading examples.


.. collapse:: How do I generate a forecast for my target location?

    There a couple of ways to setup ORION for your target location.  If this is your first time launching the code, ORION will likely open a quickstart wizard that will help to setup your initial configuration.  This wizard can also be accessed by selecting "Model/Quickstart" from the menubar.  More detailed configuration options for ORION can be accessed by clicking on "Model/Configure" from the menubar.


.. collapse:: Where can I download ORION?

    The latest version of ORION can be downloaded from the `ORION Release <https://gitlab.com/NRAP/orion/-/releases>`_ page.  We are currently offering pre-compiled executables for windows systems (OSX releases are in-development).  Other options for downloading ORION include cloning the `ORION Repository <https://gitlab.com/NRAP/orion>`_ with git, or installing it from `pypi <https://pypi.org/project/orion-seismic-forecast/>`_ with pip.


.. collapse:: How/where does ORION store information?

    Runtime logs and config files are output to the user's cache directory when ORION is closed (typically "~/.cache/orion" or "C:\Users\username\.cache\orion").  When ORION is started, it will attempt to resume from these files if available.  This cache directory is also used to store downloaded example configurations and data files.  If you are attempting to use public well information, these data are also stored in the cache directory (typically "~/.cache/well_database" or "C:\Users\username\.cache\well_database").


.. collapse:: ORION stopped working after upgrading versions

    If you have used ORION on your system before, you may have old input files present in your cache directory  (typically "~/.cache/orion" or "C:\Users\username\.cache\orion"). We recommend deleting this directory and re-downloading any examples you need.


.. collapse:: What should I do if I received an error from Windows Defender?

    If you are using the pre-compiled version of ORION, you may see a blue box with a warning message from Windows Defender ("Microsoft Defender SmartScreen prevented an unrecognized app from starting. Running this app might put your PC at risk.") the first time you attempt to run the executable.  To continue, try clicking the text "More info" and then "Run anyway" from the bottom of the window.


.. collapse:: ORION stopped working properly after I upgraded my operating system.  How do I fix this?

    If certain widgets stop working (dropdown menus, etc.) after an OS upgrade, this may be due to a problem with your python installation.  To fix this, we recommend that you uninstall any existing python distributions and install new versions from `python.org <https://www.python.org/>`_ or `Anaconda <https://www.anaconda.com/download>`_ .


.. collapse:: ORION does not behave as expected on OSX.  How do I fix this?
    
    There are known issues with the Tcl/Tkinter libraries that come bundled with OSX.  If you are using a version of Python from python.org or anaconda, the legacy interface may contain widgets that behave unexpectedly or crash.  We recommend that users either run a pre-compiled version of ORION (in development) or follow these instructions: :ref:`osx-instructions`.


.. collapse:: What should I do if ORION produced an unexpected result, froze suddenly, etc.?

    Runtime logs and config files are output to the user cache directory when ORION is closed (typically "~/.cache/orion" or "C:\Users\username\.cache\orion").  If the error occurs on startup, try deleting the cache directory (e.g.: "rm -r ~/.cache/orion") and restarting ORION.  You can also inspect the logs for any warnings, errors, or exceptions that may have been triggered by ORION.  In many cases, these will provide suggestions for fixing the input configuration (correcting bad files, etc.).  If you are unable to resolve the issue, then please contact the `Orion development team <contact-project+nrap-orion-18601784-issue-@incoming.gitlab.com>`_ with a description of the issue, the version of ORION you are using, and your operating system type/version.  Also, please attach the following files in your cache directory to your message if they are present: "~/.cache/orion/orion_log.txt", "~/.cache/orion/orion_config.json", and "~/.cache/orion/orion_config_user.json".


.. collapse:: How do I export data from ORION?

    The active plots and their underlying data (in 'hdf5' format) can be exported by selecting "File/Save figures" or "File/Save data" from the top menubar.

.. collapse:: What is the Gutenberg-Richter (GR) law?

    The Gutenberg-Richter law was defined by Beno Gutenberg and Charles Richter in the 1940s :cite:`Gutenberg1944`.
    It states that the earthquake magnitudes are distributed exponentially, i.e. the relationship will be a straight line between a logarithmic y-axis (frequency) and the magnitude x-axis (the magnitude is already a logarithmic parameter).
    It is given by :math:`\log_{10}N = a-bM` where a is a constant and b is the scaling parameter or the slope/gradient of this relationship.
    N is the number of earthquakes with a magnitude larger or equal to M.
    This relationship helps us make important assumptions when estimating earthquake hazard.


Other Questions
==========================

Please send any question to the `Orion development team <contact-project+nrap-orion-18601784-issue-@incoming.gitlab.com>`_.








