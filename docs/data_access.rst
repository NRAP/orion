Forecast Model Construction
===========================

Model File
----------

To create a new forecast model, we recommend making a copy of `orion/forecast_models/seismogenic_index_model.py` and placing it in the  *forecast_models* directory.
Make sure to choose a descriptive name for the file and the new forecast class name at the top of the file.
When it is ready to be included into the manager, you will need to add it to the list of available models in `orion.forecast_models.__init__.list_`.


Model Initialization
--------------------

A forecast model will have access to the data structures in the :class:`orion.forecast_models.forecast_model_base.ForecastModel` base class (:ref:`Forecast Models API`).
You can add model-specific attributes via the :meth:`orion.forecast_models.forecast_model_base.ForecastModel.set_class_options` method:

.. literalinclude:: ../src/orion/forecast_models/forecast_model_base.py
   :pyobject: ForecastModel.set_class_options

These attributes can be called elsewhere within the model via the `self` variable.
They can also be accessed in other objects, as attributes of the model instance.
At a minimum, each forecast model is expected to set the attributes :attr:`self.short_name` and :attr:`self.long_name` (these are used in plotting, GUI routines).


Forecast Generation
-------------------

The :meth:`orion.forecast_models.forecast_model_base.ForecastModel.generate_forecast` method is called by the forecast manager, and is expected to generate the forecast.
It has the following arguments:

.. literalinclude:: ../src/orion/forecast_models/forecast_model_base.py
   :pyobject: ForecastModel.generate_forecast

- `grid`: This contains information about the requested background grid (spatial, temporal extents, resolution, etc).
- `seismic_catalog`: This contains the seismic catalog for the current time-slice under consideration.
- `pressure`: This is the active pressure model interpolator.
- `geologic_model`: This is the geologic model, which currently contains information such as the permeability field and in-situ stress.
- `forecast_length`: The requested forecast length (beginning at the end of the time slice)

The goal of a forecast model is to set the following attributes:

- `self.forecast_time`: A :class:`numpy.ndarray` of time values (seconds). Note: it is OK if this doesn't match the other models, since they will be re-interpolated onto a common grid
- `self.spatialNumberForecast`: A :class:`numpy.ndarray` of earthquake count values for each grid point and time values.
- `self.forecast_cumulative_event_count`: A :class:`numpy.ndarray` of cumulative earthquake count values.


Seismic Catalog Access
----------------------

To generate and train seismic forecasts, it is necessary to look at various slices of the seismic catalog.
To enable this behavior, there are two approaches available for interacting with the raw catalog data:

1. Using the data accessors (e.g., `x = self.catalog.get_utm_east_slice()`). These will return the current data being considered by the forecast manager.
2. Directly accessing the data in the structure (e.g., `x = self.catalog.utm_east`). This will return the entire catalog, and is not preferred, due to the additional work required.

The target data slice can be updated by calling the :meth:`orion.forecast_models.forecast_model_base.ForecastModel.catalog.set_time_slice` method.
When new data is loaded or the time slice is changed, the code will re-calculate the seismic characteristics.
The following example shows how to use these structures:

.. code-block:: python

  # Example data access
  # Note: you can see what data lives in this class
  #       by looking at this class, and it's base class
  #       (forecast_model_base.ForecastModel)

  # Note: print statements should start with four spaces
  #       to match the forecast manager indentation
  #       Also, c-style print statements are preferred
  print('    (data handling demo)')

  # Seismic catalog holds raw data, shared seismic
  # characteristics (a, b, etc.)
  print('    Length of catalog = %i' % (self.catalog.N))
  print('    Gutenberg-Richter: a = %1.2f, b = %1.2f' % (self.catalog.a_value, self.catalog.b_value))

  # Calculate values directly from the base data
  mean_x = np.mean(self.catalog.get_utm_east_slice())
  mean_y = np.mean(self.catalog.get_utm_north_slice())
  mean_z = np.mean(self.catalog.get_depth_slice())
  print('    location mean = (%1.2f, %1.2f, %1.2f) m' % (mean_x, mean_y, mean_z))

  # Note: Time values within Orion are given as the Unix epoch
  #       (number of seconds since Jan 1, 1970)
  #       Also, catalog entries should be sorted by time
  start_time_str = datetime.datetime.fromtimestamp(self.catalog.epoch[0]).strftime('%m/%d/%Y')
  t_range = self.catalog.epoch[-1] - self.catalog.epoch[0]
  print('    Catalog start time: %s' % (start_time_str))
  print('    Catalog time: %1.1f days' % (t_range / (60 * 60 * 24)))

  # For now, a forecast model is expected to set these two
  # values, which represent the forecasted moment rate in time
  # Note: It's OK if the time vector doesn't match the other
  #       forecast models, since they will be re-interpolated
  #       onto a common grid
  print('    (setting forecast to random array)')
  self.forecast_time = np.linspace(0, 1, 100)
  self.forecast_moment_rate = abs(np.random.randn(100))


Pressure Model Access
---------------------

The primary method to interact with a pressure model is through its interpolators *p* and *dpdt* .
For example, to get the estimated pressure at a given location, you could call:

.. code-block:: python

  # Each of these is in the local coordinate system:
  xyz = [1.0, 2.0, 3.0]  # Target location (m)
  t = 40.0               # Current time (s)
  p = pressure.p(xyz[0], xyz[1], xyz[2], t)


Geologic Model Access
---------------------

Similar to the pressure model, the primary method to interact with values is through their interpolator.
The values that are currently available include:

- `permeability` (mD)
- `sigma_xx` (Pa)
- `sigma_yy` (Pa)
- `sigma_zz` (Pa)
- `sigma_xy` (Pa)
- `sigma_xz` (Pa)
- `sigma_yz` (Pa)

For example, to get the estimated permeability at a given location, you could call:

.. code-block:: python

  # Each of these is in the local coordinate system:
  xyz = [1.0, 2.0, 3.0]  # Target location (m)
  k = geologic_model.permeability(xyz[0], xyz[1], xyz[2])


Model Documentation
-------------------

Each method and attribute for the model should be documented.
This is done via the Google-format docstrings (denoted by the triple-quotes).
These docstrings are parsed and included within the Orion documentation.
The following describes the various options for docstrings:
.. _Docstrings: https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html


Adding an Attribute to the GUI
------------------------------

To add an attribute to the GUI, it needs to be added to the :attr:`orion.forecast_models.forecast_model_base.ForecastModel.gui_elements` dict (see the above example).
The name of each entry must point to an existing attribute in the forecast model.
When the GUI is executing, it will periodically update the target values.
At a minimum a GUI definition must include a `"element_type"` and `"position"`.
The following GUI element types are currently supported:

- button: a button to trigger a function
- check: adds a single check option for a boolean flag
- checkbox: adds a group of check options for a dictionary containing boolean flags
- dropdown: a menu used to select from a list of strings. This type expects that values (a list of strings) is set
- entry: a text-based entry for string/float variables
- slider: adds a sliderbar to set a float value. This type expects that the following attributes are also set: range (a list of two floats representing the lower/upper values), interval (a float indicating the spacing of sliderbar ticks), and resolution (a float indicating the resolution of the slider) 
- text: draws the text stored in the target variable

The position argument denotes the row/column to place the GUI element.
Note: if there is already an object there, they may be drawn over each other.
Another key attribute is `"label"`.
If this is set, it will draw the label text to the left of the desired GUI element.

Some gui element types support an optional entry `"command"`, which can either be the name of a function or one of the following: `"file"` (opens a file dialogue, and sets the variable with user input), `"add child"` (appends a new element to the current notebook), `"remove child"` (removes the currently selected element in the notebook).
These are activated when the user interacts with the object (selecting an option from a dropdown menu) or clicking the button to it's right.

