Radial Flow Pressure Model
==========================

The radial flow pressure model is based on the transient Theis solution for an infinite, homogeneous reservoir :cite:`ferris1962theory`.
The solution relies on the following parameters:

- Fluid viscosity, :math:`{\mu}`
- Fluid permeability, :math:`{k}`
- Reservoir storativity, :math:`{s}`
- Reservoir thickness, :math:`{H}`
- Well locations, :math:`{x_{i}, y_{i}, z_{i}}`
- Well pumping start time, :math:`{t_{i}}`
- Well pumping rate, :math:`{q_{i}}`

To estimate the pressure and pressurization rate at points in the space (:math:`{x, y, z}`) and time (:math:`{t}`), we calculate the transmissivity of the reservoir, :math:`{T}`:

.. math::

   T = \frac{ \rho g k H }{mu}

where :math:`{\rho}` is the fluid density and :math:`g` is the gravitational coefficient.
The Theis solution is 2D, so the distance from each well to the current point, :math:`{r_{i}}`, is given by:

.. math::

   r_{i} = \sqrt{(x - x_{i})^{2} + (y - y_{i})^{2}}

The dimensionless well parameter, :math:`{u_{i}}`, is given by:

.. math::

   u_{i} = \frac{r_{i}^{2} s}{4 T (t - t_{i})}

The fluid pressure, :math:`{p}`, is given by the superposition of each well contribution:

.. math::
   p =  \frac{\rho g}{4 pi T} \left( z + \sum_{i=1}^{N} q_{i} W(u_{i}) \right)

where :math:`{N}` is the number of wells and :math:`{W}` is the exponential integral (:math:`{E_{1}}`).
Similarly, the pressurization rate :math:`{\frac{\partial p}{\partial t}}` is given by:

.. math::
   \frac{\partial p}{\partial t} = \frac{\rho g}{4 pi T} \sum_{i=1}^{N}\frac{q_{i}}{(t - t_{i})} exp(-u_{i})

For wells that have time-varying flow rates, the well contributions are estimated by separating the time-series into a set of step-wise functions:

.. math::
   q_{i} = q(t_{i}) - q(t_{i - 1})

The following image shows the configuration window for the radial flow model:

.. image:: ../figures/configure_pressure_radial_flow.png
