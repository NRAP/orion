Table Pressure Model
====================

The table-based pressure model is used to import an external pressure model.
In the configuration menu, Orion requires the path to a table file (see :ref:`Table Files`) on the local machine.
The table can be structured (a 4D table with axes corresponding to *x*, *y*, *z*, *t*) or unstructured (sets of 1D arrays of *x*, *y*, *z*, *t*), and should contain pressure and/or dpdt estimates.
If pressure or dpdt are missing from the tables, Orion will attempt to integrate/differentiate the available data to estimate them from the remaining values.

.. image:: ../figures/configure_pressure_table.png
