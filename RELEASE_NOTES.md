
Version v1.0.0 -- Release date 2024-07-01
==========================================
* Improved seismic characeristics estimation
  * b-value
  * Magnitude of completeness
* Updated seismic forecast models
  * Implemented the SAIF forecast model
  * General upgrades to CRS model
  * General upgrades to SI model
* Improved spatial projections of point data and grids
  * Changed pyproj and pycsep to hard dependencies
  * Added options for Lat/Lon, UTM, and General inputs (using Proj codes)
  * Added per-input source projections
  * Added spatial projection unit tests
* Added spatial unit specifications
  * Spatial units allowed for user inputs
  * Grid manager units will be mirrored on figure pages
* Added integrated tests with baseline projection
  * Targets include the default case, IBDP case
* Implemented various bugfixes
  * Font size
  * Repeated run issues
  * Logger configuration
