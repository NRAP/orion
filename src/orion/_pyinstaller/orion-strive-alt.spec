# -*- mode: python ; coding: utf-8 -*-
from PyInstaller.utils.hooks import copy_metadata, collect_data_files, collect_all
from orion import __version__
import sys

sys.setrecursionlimit(sys.getrecursionlimit() * 5)
version_str = '_'.join(__version__.split('.'))

datas = []
datas += copy_metadata('pyproj')
datas += copy_metadata('obspy')
datas += collect_data_files('torchmin', include_py_files=True)

# Data needed for strive version
datas += copy_metadata('dash_daq')
datas += collect_data_files('dash_daq', include_py_files=True)
datas += copy_metadata('dash_vtk')
datas += collect_data_files('dash_vtk', include_py_files=True)

excludes = ['tensorflow', 'keras']
hiddenimports = ['csep', 'rasterio']


a = Analysis(
    ['run-orion.py'],
    pathex=[],
    binaries=[],
    datas=datas,
    hiddenimports=hiddenimports,
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=excludes,
    noarchive=False,
)


pyz = PYZ(a.pure)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name=f'orion_v_{version_str}',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=True,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
    icon=['..\\gui\\smart_orb.png'],
)
coll = COLLECT(
    exe,
    a.binaries,
    a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name=f'orion_v_{version_str}',
)
