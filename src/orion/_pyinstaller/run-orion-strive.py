# ------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2020-, Lawrence Livermore National Security, LLC
# All rights reserved
#
# See top level LICENSE, COPYRIGHT, CONTRIBUTORS, NOTICE, and ACKNOWLEDGEMENTS files for details.
# ------------------------------------------------------------------------------------------------
"""Entry point for pyinstaller"""


def main():
    import sys
    import logging

    # Setup
    logger = logging.getLogger('strive')
    logger.setLevel(logging.DEBUG)

    if sys.platform.startswith('win'):
        import multiprocessing
        logger.debug('Setting multiprocessing configuration for windows devices...')
        multiprocessing.freeze_support()

    logger.info('Launching ORION')
    import orion

    try:
        orion._frontend = 'strive'
        from orion.gui import orion_strive
        orion_strive.run('', verbose=True, debug=False)
    except Exception as e:
        print(e)
        import time
        while True:
            time.sleep(10)

    logger.info('Done!')


if __name__ == "__main__":
    main()
