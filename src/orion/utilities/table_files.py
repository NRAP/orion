# ------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2020-, Lawrence Livermore National Security, LLC
# All rights reserved
#
# See top level LICENSE, COPYRIGHT, CONTRIBUTORS, NOTICE, and ACKNOWLEDGEMENTS files for details.
# ------------------------------------------------------------------------------------------------
"""
table_files.py
-----------------------
"""

import os
import numpy as np
import logging
from scipy.interpolate import LinearNDInterpolator, NearestNDInterpolator, RegularGridInterpolator, interp1d
from orion.utilities import function_wrappers, hdf5_wrapper, file_io, spatial

logger = logging.getLogger('strive')


class TableInterpolator():

    def __init__(self, x, v, structured=True):
        if structured:
            xb = tuple(x)
            vb = np.ascontiguousarray(v)
            self.interp_a = RegularGridInterpolator(xb, vb, bounds_error=False)
            self.interp_b = RegularGridInterpolator(xb, vb, bounds_error=False, fill_value=None, method='nearest')
        else:
            self.interp_a = LinearNDInterpolator(x, v, fill_value=np.nan)
            self.interp_b = NearestNDInterpolator(x, v)

    def __call__(self, *xargs):
        va = self.interp_a(*xargs)
        Ia = np.isnan(va)
        if np.sum(Ia):
            vb = self.interp_b(*xargs)
            va[Ia] = vb[Ia]
        return va


def load_table_files(data, axes_names=['x', 'y', 'z', 't'], source_projection='', target_projection='', offset={}):
    """
    Load structured or unstructured property values

    Args:
        data (dict): Table entries
        axes_names (list): List of potential axes names
        source_projection (str): Source projection
        target_projection (str): Target projection
        offset (dict): Optional offset values

    Returns:
        dict: Data interpolators
    """
    logger.debug('Checking table file shape')
    file_io.check_table_shape(data)
    table_interpolators = {}
    pnames = [k for k in data.keys() if k not in axes_names]

    # Check to see which axes are present
    active_axes = [k for k in axes_names if k in data.keys()]
    function_mask = [ii for ii, k in enumerate(axes_names) if k in active_axes]

    # Apply the offsets
    for k in active_axes:
        data[k] += offset.get(k, 0.0)

    # Check to see if the data is structured/unstructured
    if (len(np.shape(data[pnames[0]])) > 1):
        logger.debug('Table files appear to be structured')

        # Manage the projection
        if source_projection and target_projection and (source_projection != target_projection):
            if ('x' in data) and ('y' in data):
                g = spatial.GridAxes(**data)
                data['x'] = g.x
                data['y'] = g.y
            elif ('latitude' in data) and ('longitude' in data):
                g = spatial.GridAxes(**data)
                data['x'] = g.x
                data['y'] = g.y
                active_axes[active_axes.find('longitude')] = 'x'
                active_axes[active_axes.find('latitude')] = 'y'

        # Assemble axes
        points = []
        for k in active_axes:
            points.append(data[k])

        for p in pnames:
            logger.debug(f'Generating grid interpolator: {p}')
            tmp = TableInterpolator(points, data[p])
            table_interpolators[p] = function_wrappers.masked_fn(tmp, function_mask, list_arg=True)

    else:
        # Unstructured data
        logger.debug('Table files appear to be unstructured')

        # Manage the projection
        if source_projection and target_projection and (source_projection != target_projection):
            if ('x' in data) and ('y' in data):
                p = spatial.Points(**data)
                data['x'] = p.x
                data['y'] = p.y
            elif ('latitude' in data) and ('longitude' in data):
                p = spatial.Points(**data)
                data['x'] = p.x
                data['y'] = p.y
                active_axes[active_axes.find('longitude')] = 'x'
                active_axes[active_axes.find('latitude')] = 'y'

        # Assemble the points
        points = []
        for k in active_axes:
            points.append(np.reshape(data[k], (-1, 1)))

        tmp = np.meshgrid(*points, indexing='ij')
        points = [np.reshape(x, (-1, 1), order='F') for x in tmp]
        points = np.ascontiguousarray(np.squeeze(np.concatenate(points, axis=1)))

        # Load the data
        for p in pnames:
            if (points.ndim == 1):
                logger.debug(f'Generating 1D interpolator: {p}')
                pval = np.squeeze(data[p])
                tmp = interp1d(points, pval, kind='linear', bounds_error=False, fill_value=(pval[0], pval[-1]))
                table_interpolators[p] = function_wrappers.masked_fn(tmp, function_mask)
            else:
                logger.debug(f'Generating ND interpolator: {p}')
                pval = np.reshape(data[p], (-1, 1), order='F')
                tmp = TableInterpolator(points, pval, structured=False)
                table_interpolators[p] = function_wrappers.masked_fn(tmp, function_mask)

    return table_interpolators


def save_table_files(fname, data, axes_names=['x', 'y', 'z', 't']):
    fname = os.path.expanduser(os.path.normpath(fname))

    if ('hdf5' in fname):
        root_dir = os.path.dirname(fname)
        if root_dir:
            os.makedirs(root_dir, exist_ok=True)
        with hdf5_wrapper.hdf5_wrapper(fname, mode='w') as f:
            for k, v in data.items():
                f[k] = np.ascontiguousarray(v)
    elif ('.' not in fname):
        os.makedirs(fname, exist_ok=True)
        for k in axes_names:
            np.savetxt(os.path.join(fname, f'{k}.csv'), data[k], delimiter=',')
        for k in data.keys():
            if k not in axes_names:
                tmp = np.reshape(np.ascontiguousarray(data[k]), (-1), order='f')
                np.savetxt(os.path.join(fname, f'{k}.csv'), tmp, delimiter=',')

    else:
        logger.warning('Table files can be saved to an .hdf5 format file or a folder (containing .csv)')
