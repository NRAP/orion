import numpy as np
import logging
import re
from pyproj import Proj

logger = logging.getLogger('strive')


def parse_utm_zone_str(zone_str):
    """
    Parse utm zone inputs

    Args:
        zone_str (str): UTM zone string

    Returns:
        str: Parsed UTM zone
    """
    valid_zone_id = re.sub("[^0-9]", "", zone_str)
    if not valid_zone_id:
        logger.error(f'Could not parse UTM zone input: {zone_str}... Defaulting to zone 1')
        valid_zone_id = "1"

    zone_int = int(valid_zone_id)
    if (zone_int < 1) or (zone_int > 60):
        logger.error(f'Given UTM zone ({zone_int}) is out of range (1, 60)... Defaulting to zone 1')
        valid_zone_id = "1"

    return valid_zone_id


class Points():
    """
    Class for managing groups of points and projections

    Attributes:
        x (np.ndarray): X values (m)
        y (np.ndarray): Y values (m)
        easting (np.ndarray): Easting values (m)
        northing (np.ndarray): Northing values (m)
        latitude (np.ndarray): Latitude values (degrees)
        longitude (np.ndarray): Longitude values (degrees)
    """

    def __init__(self, **kwargs):
        self.update_points(**kwargs)

    def __getitem__(self, arg):
        """
        Accessor used for slicing the points object
        """
        p = Points(latitude=self._latitude[arg],
                   longitude=self._longitude[arg],
                   x=self._x[arg],
                   y=self._y[arg],
                   target_projection_str=self.projection_str)
        return p

    def __len__(self):
        return len(self._latitude)

    def copy(self):
        """
        Copy method for points object
        """
        p = Points(latitude=self._latitude.copy(),
                   longitude=self._longitude.copy(),
                   x=self._x.copy(),
                   y=self._y.copy(),
                   target_projection_str=self.projection_str)
        return p

    def update_points(self,
                      latitude=None,
                      longitude=None,
                      x=None,
                      y=None,
                      easting=None,
                      northing=None,
                      utm_zone=0,
                      source_projection_str='',
                      target_projection_str='',
                      **kwargs):
        """
        Update the points object:

        Args:
            latitude (np.ndarray): Latitude values (degrees)
            longitude (np.ndarray): Longitude values (degrees)
            x (np.ndarray): X values (m)
            y (np.ndarray): Y values (m)
            easting (np.ndarray): Easting values (m)
            northing (np.ndarray): Northing values (m)
            utm_zone (int): UTM zone id
            source_projection_str (str): Projection code for input values
            target_projection_str (str): Projection code for target values
        """
        if utm_zone:
            source_projection_str = f'+proj=utm +zone={utm_zone}'
            if not target_projection_str:
                target_projection_str = source_projection_str

        source_projection_str = source_projection_str.strip()
        if not source_projection_str:
            source_projection_str = 'EPSG:3857'

        target_projection_str = target_projection_str.strip()
        if not target_projection_str:
            target_projection_str = 'EPSG:3857'

        self.projection_str = target_projection_str
        self.projection = Proj(target_projection_str)

        if easting is not None:
            x = easting
        if northing is not None:
            y = northing

        if (latitude is not None) and (longitude is not None):
            self._latitude = np.array(latitude)
            self._longitude = np.array(longitude)
        elif (x is not None) and (y is not None):
            try:
                inv_proj = Proj(source_projection_str)
                self._longitude, self._latitude = inv_proj(np.array(x), np.array(y), inverse=True)
            except Exception as e:
                print(e)
                logger.error('Failed to convert x/y inputs to latitude/longitude')
                self._longitude = np.linspace(0, 1, len(x))
                self._latitude = np.linspace(0, 1, len(y))
        else:
            raise Exception('Coordinate conversion requires either (x, y) or (lat, lon)')

        if (x is not None) and (y is not None) and (source_projection_str == target_projection_str):
            self._x = np.array(x)
            self._y = np.array(y)
        else:
            try:
                self._x, self._y = self.projection(self._longitude, self._latitude)
            except Exception as e:
                print(e)
                logger.error('Failed to convert latitude/longitude inputs to x/y')
                self._x = np.linspace(0, 1, len(longitude))
                self._y = np.linspace(0, 1, len(latitude))

    def append_points(self, **kwargs):
        """
        Append new points to the object.
        Uses the same args as update_points
        """
        new_points = Points(**kwargs)
        new_x, new_y = self.projection(new_points.longitude, new_points.latitude)
        self._latitude = np.concatenate([self.latitude, new_points.latitude], axis=0)
        self._longitude = np.concatenate([self.longitude, new_points.longitude], axis=0)
        self._x = np.concatenate([self._x, new_x], axis=0)
        self._y = np.concatenate([self._y, new_y], axis=0)

    def update_projection(self, target_projection_str='EPSG:3857'):
        """
        Update the objects target projection

        Args:
            target_projection_str (str): New projection code for target values
        """
        self.projection = Proj(target_projection_str)
        self._x, self._y = self.projection(self._longitude, self._latitude)

    @property
    def x(self):
        res = self._x.view()
        res.flags.writeable = False
        return res

    @property
    def y(self):
        res = self._y.view()
        res.flags.writeable = False
        return res

    @property
    def easting(self):
        return self.x

    @property
    def northing(self):
        return self.y

    @property
    def latitude(self):
        res = self._latitude.view()
        res.flags.writeable = False
        return res

    @property
    def longitude(self):
        res = self._longitude.view()
        res.flags.writeable = False
        return res


class GridAxes():
    """
    Class for managing grid axes and projections

    Attributes:
        x (np.ndarray): X values (m)
        y (np.ndarray): Y values (m)
        easting (np.ndarray): Easting values (m)
        northing (np.ndarray): Northing values (m)
        latitude (np.ndarray): Latitude values (degrees)
        longitude (np.ndarray): Longitude values (degrees)
    """

    def __init__(self, **kwargs):
        self.Nx = 0
        self._points = None
        self.update_grid(**kwargs)

    def update_grid(self,
                    latitude=None,
                    longitude=None,
                    x=None,
                    y=None,
                    easting=None,
                    northing=None,
                    source_projection_str='EPSG:3857',
                    target_projection_str='EPSG:3857'):
        """
        Update the points object:

        Args:
            latitude (np.ndarray): Latitude values (degrees)
            longitude (np.ndarray): Longitude values (degrees)
            x (np.ndarray): X values (m)
            y (np.ndarray): Y values (m)
            easting (np.ndarray): Easting values (m)
            northing (np.ndarray): Northing values (m)
            utm_zone (int): UTM zone id
            source_projection_str (str): Projection code for input values
            target_projection_str (str): Projection code for target values
        """
        if easting is not None:
            x = easting
        if northing is not None:
            y = northing

        if (latitude is not None) and (longitude is not None):
            self.Nx = len(longitude)
            Ny = len(latitude)
            latitude_edge = np.zeros(self.Nx) + latitude[0]
            longitude_edge = np.zeros(Ny) + longitude[0]
            tmp_latitude = np.concatenate([latitude_edge, latitude], axis=0)
            tmp_longitude = np.concatenate([longitude, longitude_edge], axis=0)
            self._points = Points(latitude=tmp_latitude,
                                  longitude=tmp_longitude,
                                  source_projection_str=source_projection_str,
                                  target_projection_str=target_projection_str)
        elif (x is not None) and (y is not None):
            self.Nx = len(x)
            Ny = len(y)
            x_edge = np.zeros(Ny) + x[0]
            y_edge = np.zeros(self.Nx) + y[0]
            tmp_x = np.concatenate([x, x_edge], axis=0)
            tmp_y = np.concatenate([y_edge, y], axis=0)
            self._points = Points(x=tmp_x,
                                  y=tmp_y,
                                  source_projection_str=source_projection_str,
                                  target_projection_str=target_projection_str)
        else:
            raise Exception('Coordinate conversion requires either (x, y) or (lat, lon)')

    def update_projection(self, target_projection_str='EPSG:3857'):
        """
        Update the objects target projection

        Args:
            target_projection_str (str): New projection code for target values
        """
        self._points.update_projection(target_projection_str)

    @property
    def x(self):
        return self._points.x[:self.Nx]

    @property
    def y(self):
        return self._points.y[self.Nx:]

    @property
    def latitude(self):
        return self._points.latitude[self.Nx:]

    @property
    def longitude(self):
        return self._points.longitude[:self.Nx]


def estimate_utm_zone(latitude, longitude):
    """
    Estimate the current UTM zone

    Args:
        latitude (float): Target latitude
        longitude (float): Target longitude

    Returns:
        int: UTM zone number
    """
    zone = int(np.floor((longitude + 180.0) / 6.0)) + 1

    # Special cases for Norway and Svalbard
    if (latitude >= 56.0) and (latitude < 64.0) and (longitude >= 3.0) and (longitude < 12.0):
        zone = 32

    if (latitude >= 72.0) and (latitude < 84.0):
        if (longitude >= 0.0) and (longitude < 9.0):
            zone = 31
        elif (longitude >= 9.0) and (longitude < 21.0):
            zone = 33
        elif (longitude >= 21.0) and (longitude < 33.0):
            zone = 35
        elif (longitude >= 33.0) and (longitude < 42.0):
            zone = 37

    return zone
