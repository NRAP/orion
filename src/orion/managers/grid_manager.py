# ------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2020-, Lawrence Livermore National Security, LLC
# All rights reserved
#
# See top level LICENSE, COPYRIGHT, CONTRIBUTORS, NOTICE, and ACKNOWLEDGEMENTS files for details.
# ------------------------------------------------------------------------------------------------
"""
grid_manager.py
-----------------------
"""

import re
import numpy as np
from orion.managers import manager_base
from orion.utilities import timestamp_conversion, spatial
from orion.managers.manager_base import recursive


class GridManager(manager_base.ManagerBase):
    """
    Grid manager class

    Attributes:
        ref_time_str (str): Reference time string in dd/mm/yyyy format
        spatial_type (str): Style of spatial inputs (default='UTM')
        t_min (float): Minimum time (s)
        t_max (float): Maximum time (s)
        dt (float): Target time resolution (s)
        x_min (float): Minimum X value (m)
        x_max (float): Maximum X value (m)
        dx (float): Target resolution in the X direction (m)
        y_min (float): Minimum Y value (m)
        y_max (float): Maximum Y value (m)
        dy (float): Target resolution in the Y direction (m)
        z_min (float): Minimum Z value (m)
        z_max (float): Maximum Z value (m)
        dz (float): Target resolution in the Z direction (m)
    """

    def set_class_options(self, **kwargs):
        """
        Grid manager initialization
        """

        # Set the shorthand name
        self.short_name = 'General'

        # Time control
        self.time_header = 'Time'
        self.t_origin = 0.0
        self.ref_time_str = '0.0'
        self.t_min = 0.0
        self.t_max = 1.0
        self.dt = 1.0
        self.t_min_input = 0.0
        self.t_max_input = 100.0
        self.dt_input = 100.0
        self.snapshot_time = 1000.0

        # Plot extents (local time, days)
        self.plot_time_min = 0.0
        self.plot_time_max = 100.0

        # Spatial control
        self.spatial_header = '\nSpatial'
        self._distance_scale = {'m': 1.0, 'km': 1.0e-3, 'ft': 3.281, 'miles': 0.000621371}
        self.available_distance_units = ['m', 'km', 'ft', 'miles']
        self.distance_units = 'm'
        self.available_spatial_types = ['General', 'UTM', 'Lat/Lon']
        self.spatial_type = 'UTM'
        self.current_spatial_type = 'UTM'
        self.utm_zone_str = '9'
        # self.projection_str = 'EPSG:3857'
        self.projection_str = ''

        # Note: The default origin is at the center of the UTM zone
        self.xy_header = '\nX/Y Grid'
        self.x_origin_input = 500000.0
        self.x_min_input = 0.0
        self.x_max_input = 1000.0
        self.dx_input = 1000.0
        self.y_origin_input = 0.0
        self.y_min_input = 0.0
        self.y_max_input = 1000.0
        self.dy_input = 1000.0
        self.x_origin = 500000.0
        self.x_min = 0.0
        self.x_max = 1000.0
        self.dx = 1000.0
        self.y_origin = 0.0
        self.y_min = 0.0
        self.y_max = 1000.0
        self.dy = 1000.0

        self.latlon_header = '\nLatitude/Longitude Grid'
        self.longitude_min = 0.0
        self.longitude_max = 1.0
        self.dlongitude = 0.1
        self.latitude_min = 0.0
        self.latitude_max = 1.0
        self.dlatitude = 0.1

        self.z_header = '\nZ Grid'
        self.z_origin = 0.0
        self.z_min = 0.0
        self.z_max = 100.0
        self.dz = 100.0
        self.z_origin_input = 0.0
        self.z_min_input = 0.0
        self.z_max_input = 100.0
        self.dz_input = 100.0

    def set_data(self, **kwargs):
        """
        Setup data holders
        """
        self._grid_axes = None

        # Values at cell centers
        self._t_cells = np.zeros(0)
        self._x_cells = np.zeros(0)
        self._y_cells = np.zeros(0)
        self._z_cells = np.zeros(0)
        self._latitude_cells = np.zeros(0)
        self._longitude_cells = np.zeros(0)

        # Values at nodes
        self._t_nodes = np.zeros(0)
        self._x_nodes = np.zeros(0)
        self._y_nodes = np.zeros(0)
        self._z_nodes = np.zeros(0)
        self._latitude_nodes = np.zeros(0)
        self._longitude_nodes = np.zeros(0)

        self._areas = np.zeros((1, 1))

    def set_gui_options(self, **kwargs):
        """
        Setup interface options
        """
        self.set_visibility_all()

        # Add gui elements
        self.gui_elements['time_header'] = {'element_type': 'text', 'position': [0, 0]}
        self.gui_elements['ref_time_str'] = {
            'element_type': 'entry',
            'label': 'Reference Time',
            'position': [1, 0],
            'units': timestamp_conversion.time_units,
            'units_span': 10
        }

        self.gui_elements['t_min_input'] = {'element_type': 'entry', 'label': 'Time Range:  min', 'position': [2, 0]}
        self.gui_elements['t_max_input'] = {'element_type': 'entry', 'label': 'max', 'position': [2, 1]}
        self.gui_elements['dt_input'] = {
            'element_type': 'entry',
            'label': 'dt',
            'position': [2, 2],
            'units': '(days)',
            'units_span': 4
        }

        self.gui_elements['plot_time_min'] = {'element_type': 'entry', 'label': 'Plot time:  min', 'position': [3, 0]}
        self.gui_elements['plot_time_max'] = {
            'element_type': 'entry',
            'label': 'max',
            'position': [3, 1],
            'units': '(days)',
            'units_span': 4
        }

        # Spatial
        self.gui_elements['spatial_header'] = {'element_type': 'text', 'position': [4, 0]}
        self.gui_elements['distance_units'] = {
            'element_type': 'dropdown',
            'label': 'Distance units',
            'position': [5, 0],
            'values': self.available_distance_units,
        }
        self.gui_elements['spatial_type'] = {
            'element_type': 'dropdown',
            'label': 'Spatial style',
            'position': [6, 0],
            'values': self.available_spatial_types,
            'pre_update': 'frame',
            'post_update': 'frame'
        }
        self.gui_elements['projection_str'] = {'element_type': 'entry', 'label': 'Projection code', 'position': [7, 0]}
        self.gui_elements['utm_zone_str'] = {'element_type': 'entry', 'label': '(or) UTM zone', 'position': [7, 1]}

        self.gui_elements['x_origin_input'] = {'element_type': 'entry', 'label': 'Grid origin: X', 'position': [8, 0]}
        self.gui_elements['y_origin_input'] = {'element_type': 'entry', 'label': 'Y', 'position': [8, 1]}
        self.gui_elements['z_origin_input'] = {'element_type': 'entry', 'label': 'Z', 'position': [8, 2]}

        self.gui_elements['x_min_input'] = {'element_type': 'entry', 'label': 'X Range:  min', 'position': [9, 0]}
        self.gui_elements['x_max_input'] = {'element_type': 'entry', 'label': 'max', 'position': [9, 1]}
        self.gui_elements['dx_input'] = {'element_type': 'entry', 'label': 'dx', 'position': [9, 2]}
        self.gui_elements['y_min_input'] = {'element_type': 'entry', 'label': 'Y Range:  min', 'position': [10, 0]}
        self.gui_elements['y_max_input'] = {'element_type': 'entry', 'label': 'max', 'position': [10, 1]}
        self.gui_elements['dy_input'] = {'element_type': 'entry', 'label': 'dy', 'position': [10, 2]}

        self.gui_elements['latitude_min'] = {
            'element_type': 'entry',
            'label': 'Latitude Range:  min',
            'position': [11, 0]
        }
        self.gui_elements['latitude_max'] = {'element_type': 'entry', 'label': 'max', 'position': [11, 1]}
        self.gui_elements['dlatitude'] = {'element_type': 'entry', 'label': 'dlat', 'position': [11, 2], 'units': '(o)'}
        self.gui_elements['longitude_min'] = {
            'element_type': 'entry',
            'label': 'Longitude Range:  min',
            'position': [12, 0]
        }
        self.gui_elements['longitude_max'] = {'element_type': 'entry', 'label': 'max', 'position': [12, 1]}
        self.gui_elements['dlongitude'] = {
            'element_type': 'entry',
            'label': 'dlon',
            'position': [12, 2],
            'units': '(o)'
        }

        self.gui_elements['z_min_input'] = {'element_type': 'entry', 'label': 'Z Range:  min', 'position': [13, 0]}
        self.gui_elements['z_max_input'] = {'element_type': 'entry', 'label': 'max', 'position': [13, 1]}
        self.gui_elements['dz_input'] = {'element_type': 'entry', 'label': 'dz', 'position': [13, 2]}

    @recursive
    def process_inputs(self):
        """
        Build the x, y, z, and t axes of the target grid
        """
        self.logger.debug('Setting up the grid')
        self.t_origin = timestamp_conversion.convert_timestamp(self.ref_time_str)

        # Process units
        self.x_origin = self.x_origin_input / self.distance_scale
        self.x_min = self.x_min_input / self.distance_scale
        self.x_max = self.x_max_input / self.distance_scale
        self.dx = self.dx_input / self.distance_scale
        self.y_origin = self.y_origin_input / self.distance_scale
        self.y_min = self.y_min_input / self.distance_scale
        self.y_max = self.y_max_input / self.distance_scale
        self.dy = self.dy_input / self.distance_scale
        self.z_origin = self.z_origin_input / self.distance_scale
        self.z_min = self.z_min_input / self.distance_scale
        self.z_max = self.z_max_input / self.distance_scale
        self.dz = self.dz_input / self.distance_scale

        # Check that inputs are in the expected order
        if self.t_max_input < self.t_min_input:
            self.logger.warning("Input grid has (t_max < t_min)...  Swapping values")
            tmp = self.t_min_input
            self.t_min_input = self.t_max_input
            self.t_max_input = tmp

        if self.x_max < self.x_min:
            self.logger.warning("Input grid has (x_max < x_min)...  Swapping values")
            tmp = self.x_min
            self.x_min = self.x_max
            self.x_max = tmp

        if self.y_max < self.y_min:
            self.logger.warning("Input grid has (y_max < y_min)...  Swapping values")
            tmp = self.y_min
            self.y_min = self.y_max
            self.y_max = tmp

        if self.z_max < self.z_min:
            self.logger.warning("Input grid has (z_max < z_min)...  Swapping values")
            tmp = self.z_min
            self.z_min = self.z_max
            self.z_max = tmp

        # Convert t inputs from days to seconds
        t_scale = 60.0 * 60.0 * 24.0
        self.t_min = self.t_min_input * t_scale
        self.t_max = self.t_max_input * t_scale
        self.dt = abs(self.dt_input) * t_scale

        # Setup grid values
        epsilon = 1e-9
        Nt = max(int(np.round((self.t_max - self.t_min + epsilon) / (abs(self.dt)) + epsilon)), 1) + 1
        Nx = max(int(np.round((self.x_max - self.x_min + epsilon) / (abs(self.dx)) + epsilon)), 1) + 1
        Ny = max(int(np.round((self.y_max - self.y_min + epsilon) / (abs(self.dy)) + epsilon)), 1) + 1

        rlat = self.latitude_max - self.latitude_min + epsilon
        Nlat = max(int(np.round(rlat / (abs(self.dlatitude)) + epsilon)), 1) + 1
        rlon = self.longitude_max - self.longitude_min + epsilon
        Nlon = max(int(np.round(rlon / (abs(self.dlongitude)) + epsilon)), 1) + 1
        Nz = max(int(np.round((self.z_max - self.z_min + epsilon) / (abs(self.dz)) + epsilon)), 1) + 1

        # Setup grids
        if not self.current_spatial_type:
            self.current_spatial_type = self.spatial_type

        # Manage projection
        # TODO: Make sure that lat/lon values do not exceed bounds
        # TODO: Check to make sure that lat/lon are in the correct order
        # TODO: Test lat/lon input scheme
        if (self.spatial_type == 'UTM') and self.utm_zone_str:
            self.utm_zone_str = spatial.parse_utm_zone_str(self.utm_zone_str)
            self.projection_str = f'+proj=utm +zone={self.utm_zone_str}'

        if (self.spatial_type in ['General', 'UTM']):
            tmp_x = np.linspace(self.x_min, self.x_max, Nx) + self.x_origin
            tmp_y = np.linspace(self.y_min, self.y_max, Ny) + self.y_origin

            self._grid_axes = spatial.GridAxes(x=tmp_x,
                                               y=tmp_y,
                                               source_projection_str=self.projection_str,
                                               target_projection_str=self.projection_str)
            self.latitude_min = self._grid_axes.latitude[0]
            self.latitude_max = self._grid_axes.latitude[-1]
            self.dlat = self._grid_axes.latitude[1] - self._grid_axes.latitude[0]
            self.longitude_min = self._grid_axes.longitude[0]
            self.longitude_max = self._grid_axes.longitude[-1]
            self.dlon = self._grid_axes.longitude[1] - self._grid_axes.longitude[0]

        else:
            self._grid_axes = spatial.GridAxes(longitude=np.linspace(self.longitude_min, self.longitude_max, Nlon),
                                               latitude=np.linspace(self.latitude_min, self.latitude_max, Nlat),
                                               target_projection_str=self.projection_str)
            self.x_min = self._grid_axes.x[0] - self.x_origin
            self.x_max = self._grid_axes.x[-1] - self.x_origin
            self.dx = self._grid_axes.x[1] - self._grid_axes.x[0]
            self.y_min = self._grid_axes.y[0] - self.y_origin
            self.y_max = self._grid_axes.y[-1] - self.y_origin
            self.dy = self._grid_axes.y[1] - self._grid_axes.y[0]

        # Setup grid node values
        self._t_nodes = np.linspace(self.t_min, self.t_max, Nt)
        self._x_nodes = self._grid_axes.x
        self._y_nodes = self._grid_axes.y
        self._z_nodes = np.linspace(self.z_min, self.z_max, Nz) + self.z_origin
        self._latitude_nodes = self._grid_axes.latitude
        self._longitude_nodes = self._grid_axes.longitude

        # Setup grid cell center values
        self._t_cells = 0.5 * (self._t_nodes[1:] + self._t_nodes[:-1])
        self._x_cells = 0.5 * (self._x_nodes[1:] + self._x_nodes[:-1])
        self._y_cells = 0.5 * (self._y_nodes[1:] + self._y_nodes[:-1])
        self._z_cells = 0.5 * (self._z_nodes[1:] + self._z_nodes[:-1])
        self._latitude_cells = 0.5 * (self._latitude_nodes[1:] + self._latitude_nodes[:-1])
        self._longitude_cells = 0.5 * (self._longitude_nodes[1:] + self._longitude_nodes[:-1])

        # Calculate grid areas
        dx = np.diff(self._x_nodes)
        dy = np.diff(self._y_nodes)
        self._areas = np.outer(dx, dy)

    def get_lat_lon_box(self):
        return self._latitude_nodes[0], self._latitude_nodes[-1], self._longitude_nodes[0], self._longitude_nodes[-1]

    def get_plot_range(self, xb=[], yb=[], zb=[]):
        x_range = np.array([self.x_min, self.x_max]).astype(float) + self.x_origin
        y_range = np.array([self.y_min, self.y_max]).astype(float) + self.y_origin
        z_range = np.array([self.z_min, self.z_max]).astype(float) + self.z_origin

        if len(xb):
            x_range[0] = min(x_range[0], np.amin(xb))
            x_range[1] = max(x_range[1], np.amax(xb))
        if len(yb):
            y_range[0] = min(y_range[0], np.amin(yb))
            y_range[1] = max(y_range[1], np.amax(yb))
        if len(zb):
            z_range[0] = min(z_range[0], np.amin(zb))
            z_range[1] = max(z_range[1], np.amax(zb))

        return x_range, y_range, z_range

    def get_plot_data(self, projection):
        plot_data = {
            'x': self.x,
            'y': self.y,
            'z': self.z,
            't': self.t,
            'latitude': self.latitude,
            'longitude': self.longitude,
            'x_nodes': self.x_nodes,
            'y_nodes': self.y_nodes,
            'z_nodes': self.z_nodes,
            't_nodes': self.t_nodes,
            'latitude_nodes': self.latitude_nodes,
            'longitude_nodes': self.longitude_nodes,
            'x_origin': self.x_origin,
            'y_origin': self.y_origin,
            'z_origin': self.z_origin,
            't_origin': self.t_origin,
            'snapshot_time': self.snapshot_time,
            'distance_units': self.distance_units,
            'distance_scale': self.distance_scale
        }
        return plot_data

    def get_digitize_axes(self, N):
        grid_order = []
        if (N == 1):
            grid_order = [self._t_nodes]
        elif (N == 3):
            grid_order = [self._x_nodes, self._y_nodes, self._z_nodes]
        elif (N == 4):
            grid_order = [self._x_nodes, self._y_nodes, self._z_nodes, self._t_nodes]
        else:
            raise Exception('Unrecognized number of dimensions for grid digitization')
        return grid_order

    def digitize_values(self, *args, include_edges=False):
        """
        Find the bin IDs for a set of points

        Args:
            args (list): List of points to digitize (1=t, 3=xyz, 4=xyzt)

        Returns:
            list: list of 1D arrays containing grid indices
        """
        grid_order = self.get_digitize_axes(len(args))

        grid_id = []
        for v, axis_nodes in zip(args, grid_order):
            M = len(axis_nodes) - 1
            tmp = np.digitize(v, axis_nodes) - 1
            if include_edges:
                grid_id.append(np.maximum(np.minimum(tmp, M - 1), 0))
            else:
                tmp[tmp >= M] = -2
                grid_id.append(tmp)
        return grid_id

    def histogram_values(self, *args, include_edges=False):
        """
        Calculate the histogram for a set of points

        Args:
            args (list): List of points to calculate histogram (1=t, 3=xyz, 4=xyzt)

        Returns:
            np.ndarray: ND histogram of points
        """
        grid_id = self.digitize_values(*args, include_edges=include_edges)
        if not include_edges:
            Ia = np.all(np.array(grid_id) >= 0, axis=0)
            for ii in range(len(args)):
                grid_id[ii] = grid_id[ii][Ia]

        grid_order = self.get_digitize_axes(len(args))
        count = np.zeros([len(x) - 1 for x in grid_order], dtype=int)
        np.add.at(count, tuple(grid_id), 1)
        return count

    @property
    def distance_scale(self):
        return self._distance_scale[self.distance_units]

    def get_axes_ticks(self, x_range, y_range, z_range, Ntick=3):
        """
        Get the axes tick positions and labels

        Args:
            x_range (list): The range of the x-axis
            y_range (list): The range of the y-axis
            z_range (list): The range of the z-axis
            Ntick (int): The number of ticks per dimension
        """
        # Choose the label format
        tick_format = '%1.0f'
        dx = (x_range[1] - x_range[0]) * self.distance_scale
        if (dx < 1e-3) or (dx > 1e5):
            tick_format = '%1.2e'
        elif (dx < 10.0):
            tick_format = '%1.2f'
        else:
            tick_format = '%1.0f'

        # Setup the ticks
        xt = np.linspace(x_range[0], x_range[1], 3)
        yt = np.linspace(y_range[0], y_range[1], 3)
        zt = np.linspace(z_range[0], z_range[1], 3)
        xt_label = [tick_format % tmp for tmp in (xt - self.x_origin) * self.distance_scale]
        yt_label = [tick_format % tmp for tmp in (yt - self.y_origin) * self.distance_scale]
        zt_label = [tick_format % tmp for tmp in (zt - self.z_origin) * self.distance_scale]
        return xt, yt, zt, xt_label, yt_label, zt_label

    def format_matplotlib_axes(self, ax, Ndim=2, x_range=[], y_range=[], z_range=[], xb=[], yb=[], zb=[]):
        """
        Format matplotlib axes

        Args:
            ax (matplotlib.axes): Figure axes
            Ndim (int): Figure dimensions
            x_range (list): The range of the x-axis
            y_range (list): The range of the y-axis
            z_range (list): The range of the z-axis
            xb (list): Test points for bounds checking in x-direction
            yb (list): Test points for bounds checking in y-direction
            zb (list): Test points for bounds checking in z-direction
        """
        # Use the grid dimensions as a default
        tmp_x_range, tmp_y_range, tmp_z_range = self.get_plot_range(xb=xb, yb=yb, zb=zb)
        if not len(x_range):
            x_range = tmp_x_range
        if not len(y_range):
            y_range = tmp_y_range
        if not len(z_range):
            z_range = tmp_z_range

        xt, yt, zt, xt_label, yt_label, zt_label = self.get_axes_ticks(x_range, y_range, z_range)
        ax.set_xlabel(f'Easting ({self.distance_units})')
        ax.set_ylabel(f'Northing ({self.distance_units})')
        ax.set_xlim(x_range)
        ax.set_ylim(y_range)
        ax.set_xticks(xt, xt_label)
        ax.set_yticks(yt, yt_label)
        if (Ndim == 3):
            ax.set_zlabel(f'Z ({self.distance_units})')
            ax.set_zlim(z_range)
            ax.set_zticks(zt, zt_label)

    @property
    def latitude(self):
        """
        Get the cell center latitude coordinates
        """
        return self._latitude_cells

    @property
    def longitude(self):
        """
        Get the cell center longitude coordinates
        """
        return self._longitude_cells

    @property
    def x(self):
        """
        Get the cell center x coordinates
        """
        return self._x_cells

    @property
    def y(self):
        """
        Get the cell center y coordinates
        """
        return self._y_cells

    @property
    def z(self):
        """
        Get the cell center z coordinates
        """
        return self._z_cells

    @property
    def t(self):
        """
        Get the cell center t coordinates
        """
        return self._t_cells

    @property
    def latitude_nodes(self):
        """
        Get the cell center latitude coordinates
        """
        return self._latitude_nodes

    @property
    def longitude_nodes(self):
        """
        Get the cell center longitude coordinates
        """
        return self._longitude_nodes

    @property
    def x_nodes(self):
        """
        Get the cell center x coordinates
        """
        return self._x_nodes

    @property
    def y_nodes(self):
        """
        Get the cell center y coordinates
        """
        return self._y_nodes

    @property
    def z_nodes(self):
        """
        Get the cell center z coordinates
        """
        return self._z_nodes

    @property
    def t_nodes(self):
        """
        Get the cell center t coordinates
        """
        return self._t_nodes

    @property
    def shape(self):
        """
        Get the number of grid cell along each dimension
        """
        return (len(self._x_cells), len(self._y_cells), len(self._z_cells), len(self._t_cells))

    @property
    def areas(self):
        """
        Get the cell areas
        """
        return self._areas
