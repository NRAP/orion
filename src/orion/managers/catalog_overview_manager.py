# ------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2020-, Lawrence Livermore National Security, LLC
# All rights reserved
#
# See top level LICENSE, COPYRIGHT, CONTRIBUTORS, NOTICE, and ACKNOWLEDGEMENTS files for details.
# ------------------------------------------------------------------------------------------------
"""
seismic_catalog.py
-----------------------
"""

from orion.managers import manager_base
from orion.utilities.plot_config import gui_colors
from orion.utilities.other import build_nan_delimited_array
from orion import _frontend
import numpy as np


class CatalogOverviewManager(manager_base.ManagerBase):
    """
    Structure for holding 3D plot information
    """

    def set_class_options(self, **kwargs):
        """
        Seismic catalog initialization function

        """

        # Set the shorthand name
        self.short_name = '3D Overview'

    def set_gui_options(self, **kwargs):
        """
        Setup interface options
        """
        self.set_visibility_all()

        overview_help = 'This figure shows the 3D distribution of seismic events, including their time and magnitude.  It also shows the location of any wells.'

        if ('no_figures' not in kwargs):
            fig_size = (10, 6)
            if _frontend == 'strive':
                fig_size = (90, 85)

            self.figures = {
                '3D_view': {
                    'position': [0, 0],
                    'size': fig_size,
                    '3D_option': True,
                    'layer_config': True,
                    'target': 'catalog_view_3D',
                    'figure_type': '3D',
                    'optional_layers': ['Seismic Catalog', 'Wells'],
                    'help_text': overview_help
                }
            }

    def catalog_view_3D(self, plot_data):
        seismic_plot_data = plot_data['Seismic Catalog']
        well_plot_data = plot_data['Fluid Injection']
        grid = plot_data['General']
        distance_scale = grid['distance_scale']
        distance_units = grid['distance_units']
        time_scale = 1.0 / (60 * 60 * 24.0)

        layers = {
            'Seismic Catalog': {
                'x': (seismic_plot_data['x'] - grid['x_origin']) * distance_scale,
                'y': (seismic_plot_data['y'] - grid['y_origin']) * distance_scale,
                'z': (seismic_plot_data['z'] - grid['z_origin']) * distance_scale,
                'c': (seismic_plot_data['time'] - grid['t_origin']) * time_scale,
                's': seismic_plot_data['point_size'],
                't': {
                    'Magnitude': seismic_plot_data['magnitude']
                },
                'type': 'scatter'
            },
            'Wells': {
                'x': (well_plot_data['x'] - grid['x_origin']) * distance_scale,
                'y': (well_plot_data['y'] - grid['y_origin']) * distance_scale,
                'z': (well_plot_data['z'] - grid['z_origin']) * distance_scale,
                't': {
                    'Well': well_plot_data['name']
                },
                'type': 'scatter',
                'marker': 'diamond',
                'marker_color': 'gray'
            }
        }

        # Check if well trajectories should be rendered
        well_names = well_plot_data['name']
        if len(well_names):
            xt = build_nan_delimited_array(well_plot_data['x_trajectory'])
            yt = build_nan_delimited_array(well_plot_data['y_trajectory'])
            zt = build_nan_delimited_array(well_plot_data['z_trajectory'])
            layers['Wells (path)'] = {
                'x': (xt - grid['x_origin']) * distance_scale,
                'y': (yt - grid['y_origin']) * distance_scale,
                'z': (zt - grid['z_origin']) * distance_scale,
                'type': 'line',
                'marker_color': 'gray'
            }

        # Build axes labels
        axes = {
            'x': f'X ({distance_units})',
            'y': f'Y ({distance_units})',
            'z': f'Z ({distance_units})',
            'c': 'Time (days)',
            's': 'Marker'
        }
        return layers, axes

    def generate_plots(self, **kwargs):
        seismic_catalog = kwargs.get('seismic_catalog')
        grid = kwargs.get('grid')
        appearance = kwargs.get('appearance')
        wells = kwargs.get('wells')
        seismic_catalog.reset_slice()

        # Get data
        t_scale = 60 * 60 * 24.0
        t = (seismic_catalog.epoch_slice - grid.t_origin) / t_scale
        magnitude = seismic_catalog.magnitude_slice
        M = len(magnitude)
        magnitude_range = [0.0, 1.0]
        if M > 0:
            magnitude_range = [np.amin(magnitude), np.amax(magnitude)]
        point_scale = 2.0
        ms_point_size = point_scale * (3**(1 + magnitude - magnitude_range[0]))

        # Map/3D view
        ax = self.figures['3D_view']['handle'].axes[0]
        ax.cla()
        ca = ax.scatter(seismic_catalog.easting_slice,
                        seismic_catalog.northing_slice,
                        seismic_catalog.depth_slice,
                        s=ms_point_size,
                        c=t,
                        cmap=gui_colors.point_colormap,
                        edgecolors=gui_colors.microseismic_style['markeredgecolor'],
                        linewidths=0.5,
                        label='Catalog',
                        depthshade=0)

        # Wells
        xw = np.zeros(0)
        yw = np.zeros(0)
        zw = np.zeros(0)
        xt = np.zeros(0)
        yt = np.zeros(0)
        zt = np.zeros(0)
        if len(wells.x):
            xw = wells.x
            yw = wells.y
            zw = wells.z
            xt = [np.concatenate([tmp, np.array([np.nan])], axis=0) for tmp in wells.x_trajectory]
            yt = [np.concatenate([tmp, np.array([np.nan])], axis=0) for tmp in wells.y_trajectory]
            zt = [np.concatenate([tmp, np.array([np.nan])], axis=0) for tmp in wells.z_trajectory]
            xt = np.concatenate(xt, axis=0)
            yt = np.concatenate(yt, axis=0)
            zt = np.concatenate(zt, axis=0)

        ax.scatter(xw, yw, zw, label='Wells', depthshade=0, **gui_colors.well_style)
        ax.plot(xt, yt, zt, label='Well Path', **gui_colors.alt_line_style)

        # Colorbar
        if 'colorbar' not in self.figures['3D_view']:
            self.figures['3D_view']['colorbar'] = self.figures['3D_view']['handle'].colorbar(ca, ax=ax)
            self.figures['3D_view']['colorbar'].set_label('t (days)')
        self.figures['3D_view']['colorbar'].update_normal(ca)

        # Setup axes
        grid.format_matplotlib_axes(ax,
                                    xb=seismic_catalog.easting_slice,
                                    yb=seismic_catalog.northing_slice,
                                    zb=seismic_catalog.depth_slice,
                                    Ndim=3)
        x_range, y_range, z_range = grid.get_plot_range(zb=seismic_catalog.depth_slice)
        ax.set_zlim(z_range[::-1])
