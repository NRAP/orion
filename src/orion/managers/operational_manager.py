# ------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2020-, Lawrence Livermore National Security, LLC
# All rights reserved
#
# See top level LICENSE, COPYRIGHT, CONTRIBUTORS, NOTICE, and ACKNOWLEDGEMENTS files for details.
# ------------------------------------------------------------------------------------------------
"""
operational_manager.py
--------------------------------------
"""
from orion.managers import forecast_manager
from orion.managers import manager_base
from orion.utilities import timestamp_conversion, plot_tools
from orion.utilities.plot_config import gui_colors
from orion import _frontend
import matplotlib.pyplot as plt
import copy
import numpy as np
from PIL import ImageColor


class OperationalManager(manager_base.ManagerBase):
    """
    A wrapper class around the ForecastManager to manage operational
    management strategies 

    Attributes:
    """

    def set_class_options(self, **kwargs):
        self.short_name = 'Operational Management'
        self.long_name = 'Operational Management Strategies'
        self.reduction_percentage_str = ''
        self.reduction_start_time_str = '0'
        self.reduction_start_time = 0.0
        self.reduction_labels = []
        self.oms_results = {}
        self.oms_ensemble = {}

    def set_gui_options(self, **kwargs):
        # self.set_visibility_operator()
        self.set_visibility_all()
        self.gui_elements['reduction_percentage_str'] = {
            'element_type': 'entry',
            'label': 'Reduction Percentage',
            'position': [4, 0],
            'units': '% (list of percentages)'
        }
        self.gui_elements['reduction_start_time_str'] = {
            'element_type': 'entry',
            'label': 'Reduction Start',
            'position': [5, 0],
            'units': timestamp_conversion.time_units
        }

        # Setup figures
        fig_size_a = (5, 3)
        fig_size_b = (8, 3)
        if _frontend == 'strive':
            fig_size_a = (45, 45)
            fig_size_b = (90, 60)

        self.figures = {
            'dial_plot_reduced': {
                'position': [0, 1],
                'size': fig_size_a,
                'static': True,
                'slice': ['case'],
                'target': 'exceedance_dial_plot_reduced'
            },
            'reduction_exceedance_plot': {
                'position': [0, 0],
                'size': fig_size_a,
                'target': 'reduction_exceedance_plot'
            },
            'temporal_reduction_plot': {
                'position': [1, 0],
                'size': fig_size_b,
                'columnspan': 4,
                'layer_config': True,
                'target': 'temporal_reduction_plot'
            },
        }

    def process_inputs(self):
        self.reduction_start_time = timestamp_conversion.convert_timestamp(self.reduction_start_time_str)

    def run(self, grid, seismic_catalog, pressure_manager, wells, geologic_model, forecast, root_manager):
        """
        ##TODO: give description and args
        """
        # Setup
        self.oms_results = {}
        self.oms_ensemble = {}
        pressure_reductions = []

        # Copy the catalog, and set to use local data only
        filtered_catalog = seismic_catalog.set_slice(inplace=False)
        filtered_catalog.calculate_spatial_parameters(grid)
        filtered_catalog.set_slice_type('local')

        # Check OPM reductions
        if not self.reduction_percentage_str:
            self.logger.debug('No operational changes specified')
            return

        try:
            pressure_reductions = [float(x) for x in self.reduction_percentage_str.split(',')]
        except Exception as e:
            self.logger.error('Received unexpected pressure reduction string')
            self.logger.error(self.reduction_percentage_str)
            self.logger.error(repr(e))

        for ii in range(len(pressure_reductions)):
            p = pressure_reductions[ii]
            if (p < 0) or (p > 100):
                self.logger.warning('Operational Manager requires pressure reduction values between 0 and 100')
                self.loger.warning('Clipping value at index {ii}: {p}')
                pressure_reductions[ii] = min(max(p, 0), 100)

        self.reduction_labels = ['%1.1f' % (x) for x in pressure_reductions]

        # Progress bar setup
        Nr = len(self.reduction_labels)
        Nx, Ny, Nz, Nt = grid.shape
        delta_progress = 25.0 / max(Nr, 1)
        t_start = self.reduction_start_time - grid.t_origin

        # TODO: This method relies on scaling the primary pressure model
        #       In the future, this should be done using physics-based pressure models
        pressure_model_names = list(pressure_manager.children.keys())
        if not len(pressure_model_names):
            self.logger.warning('The operational manager requires at least one active pressure model (none were found)')
            return

        k = pressure_model_names[0]
        preferred_model = pressure_manager.children[k]

        for ii in range(Nr):
            reduction = -0.001 * pressure_reductions[ii]
            label = self.reduction_labels[ii]
            root_manager.run_status = f'OPM Reduction {label}'
            root_manager.run_percent = 75 + ii * delta_progress

            ## TODO: built in operator for multiplication is NOT commutative
            self.oms_results[label] = {}
            delta_pressure = preferred_model * reduction
            for ka, kb, kc, temporal_forecast, spatial_forcast in forecast.generate_model_forecasts_sub(
                    grid, filtered_catalog, pressure_manager, wells, geologic_model, delta_pressure=delta_pressure):

                if ka not in self.oms_results[label]:
                    self.oms_results[label][ka] = {}

                if kb not in self.oms_results[label][ka]:
                    self.oms_results[label][ka][kb] = {}

                self.oms_results[label][ka][kb][kc] = {
                    'temporal': temporal_forecast,
                    'spatial': spatial_forcast,
                    'weight': 1.0
                }

            # Use the weights from the reference model to build the ensemble for this realization
            forecast_cumulative_event_count = np.zeros(Nt)
            spatial_forecast_count = np.zeros((Nt, Nx, Ny))
            for ka, fa in self.oms_results[label].items():
                for kb, fb in fa.items():
                    for kc, fc in fb.items():
                        w = forecast.current_forecasts[ka][kb][kc]['weight']
                        forecast_cumulative_event_count += w * fc['temporal']
                        spatial_forecast_count += w * fc['spatial']

            self.oms_ensemble[label] = {'temporal': forecast_cumulative_event_count, 'spatial': spatial_forecast_count}

            # Estimate exceedance values for this realization
            tmp = forecast.estimate_magnitude_exceedance_probability_sub(grid, filtered_catalog,
                                                                         forecast_cumulative_event_count,
                                                                         spatial_forecast_count)
            self.oms_ensemble[label]['temporal_exceedance_probability_bar'] = tmp[0]
            self.oms_ensemble[label]['temporal_exceedance_probability'] = tmp[1]
            self.oms_ensemble[label]['spatial_exceedance_probability'] = tmp[2]

    def get_plot_data(self, projection):
        tmp = {'ensemble': self.oms_ensemble, 'all': self.oms_results}
        return

    def exceedance_dial_plot_reduced(self, plot_data):
        # Setup dial steps
        forecast_data = plot_data['Forecast Models']
        color_thresholds = forecast_data['exceedance_color_thresholds']
        steps = [{
            'range': [0, color_thresholds['yellow']],
            'color': 'green'
        }, {
            'range': [color_thresholds['yellow'], color_thresholds['red']],
            'color': 'yellow'
        }, {
            'range': [color_thresholds['red'], 100.0],
            'color': 'red'
        }]

        # Setup default plot
        layers = {'exceedance': {'x': 0.0, 'c': 'gray', 'type': 'dial', 'steps': steps}}
        axes = {'title': '(exceedance estimate unavailable)', 'x_range': [0, 100]}

        # Check for exceedance data
        Nr = len(self.reduction_labels)
        if not Nr:
            return layers, axes

        # Check for case data
        case_val = self.figures['dial_plot_reduced']['slice_values']['case']
        ii = int(round(case_val * (Nr - 1)))
        label = self.reduction_labels[ii]
        if label not in self.oms_ensemble:
            return layers, axes

        # Build the dial
        m = forecast_data['exceedance_dial_magnitude']
        t = forecast_data['exceedance_time']
        dial_label = f'p(magnitude > {m:1.1f}) within {t:1.1f} days for {label}% pressure reduction'
        layers = {
            'exceedance': {
                'x': self.oms_ensemble[label]['temporal_exceedance_probability'],
                'c': 'gray',
                'type': 'dial',
                'steps': steps
            }
        }
        axes = {'title': dial_label, 'x_range': [0, 100]}
        return layers, axes

    def reduction_exceedance_plot(self, plot_data):
        # Setup default layers
        layers = {}
        axes = {
            'title': 'Operational Managagment Impact',
            'x': 'Reduction Percentage (%)',
            'y': 'Spatial Exceedance Probability'
        }

        avaliable_opm_cases = [label for label in self.reduction_labels if label in self.oms_ensemble]
        Nr = len(avaliable_opm_cases)
        if not Nr:
            self.logger.error('No OMS results available to plot')
            return layers, axes

        forecast_data = plot_data['Forecast Models']
        m = forecast_data['exceedance_dial_magnitude']
        t = forecast_data['exceedance_time']
        p = forecast_data['exceedance_dial_probability']

        # Convert reduction labels to floats for plotting on the x-axis
        reduction_percentages = np.array([float(label) for label in avaliable_opm_cases])
        reduction_probability = np.array(
            [self.oms_ensemble[label]['temporal_exceedance_probability'] for label in avaliable_opm_cases])
        rmin = max(np.amin(reduction_percentages) - 10.0, 0.0)
        rmax = min(np.amax(reduction_percentages) + 10.0, 100.0)
        baseline_percentages = np.array([rmin, rmax])
        baseline_probability = np.array([p, p])

        # Build layers
        layers['Exceedance Reduction'] = {'x': reduction_percentages, 'y': reduction_probability, 'type': 'scatter'}
        layers['Baseline'] = {'x': baseline_percentages, 'y': baseline_probability, 'type': 'line'}
        return layers, axes

    def temporal_reduction_plot(self, plot_data):
        seismic_plot_data = plot_data['Seismic Catalog']
        forecast_data = plot_data['Forecast Models']
        grid = plot_data['General']

        # Collect baseline and catalog data
        time_scale = 1.0 / (60.0 * 60.0 * 24.0)
        t = grid['t'] * time_scale
        catalog_ne = seismic_plot_data.get('catalog_cumulative_event_count', np.zeros(0))
        baseline_ensemble = forecast_data['temporal_forecast_ensemble']

        # Build layers
        layers = {
            'Catalog': {
                'x': t,
                'y': catalog_ne,
                'type': 'line'
            },
            'Baseline': {
                'x': t,
                'y': baseline_ensemble,
                'type': 'line'
            }
        }

        for label, reduction_ensemble in self.oms_ensemble.items():
            layers[f'OPM Reduction {label}'] = {'x': t, 'y': reduction_ensemble['temporal'], 'type': 'line'}

            # # Debugging information
            # for ka, fa in self.oms_results[label].items():
            #     for kb, fb in fa.items():
            #         for kc, fc in fb.items():
            #             # case = f'OPM Reduction {label} ({ka}, {kb}, {kc})'
            #             case = f'OPM Reduction {label} ({ka})'
            #             layers[case] = {'x': t, 'y': fc['temporal'], 'type': 'line'}

            #             tmp = forecast_data['temporal_forecast_count'].get(ka, {}).get(kb, {}).get(kc, {})
            #             layers[case + '(base)'] = {'x': t, 'y': tmp.get('temporal', np.zeros(len(t))), 'type': 'line'}

        axes = {'x': 'Time (days)', 'y': 'Event Count'}
        return layers, axes

    def generate_plots(self, **kwargs):
        # Collect data
        self.logger.debug('Generating forecast manager plots')
        grid = kwargs.get('grid')
        seismic_catalog = kwargs.get('seismic_catalog')
        forecasts = kwargs.get('forecasts')

        # Setup timing
        time_scale = 1.0 / (60.0 * 60.0 * 24.0)

        # Plot colors
        color_thresholds = [forecasts.exceedance_color_yellow_threshold, forecasts.exceedance_color_red_threshold]

        # Dial plot
        ax = self.figures['dial_plot_reduced']['handle'].axes[0]
        ax.cla()

        Nr = len(self.reduction_labels)
        p_baseline = forecasts.exceedance_dial_plot_probability
        p = forecasts.exceedance_dial_plot_probability
        label = '0'
        if Nr:
            case_val = self.figures['dial_plot_reduced']['slice_values']['case']
            ii = int(round(case_val * (Nr - 1)))
            label = self.reduction_labels[ii]
            p = self.oms_ensemble[label]['temporal_exceedance_probability']

        plot_tools.exceedance_dial_plot(ax, p, color_lims=color_thresholds)
        ax.set_title(
            f'p(magnitude > {forecasts.exceedance_dial_plot_magnitude:1.1f}) within {forecasts.exceedance_plot_time_input:1.1f} days for {label}% pressure reduction'
        )

        # Reduction plot
        ax = self.figures['reduction_exceedance_plot']['handle'].axes[0]
        ax.cla()

        rmin = 0.0
        rmax = 100.0
        reduction_percentages = []
        reduction_probability = []
        avaliable_opm_cases = [label for label in self.reduction_labels if label in self.oms_ensemble]
        if len(avaliable_opm_cases):
            reduction_percentages = np.array([float(label) for label in avaliable_opm_cases])
            reduction_probability = np.array(
                [self.oms_ensemble[label]['temporal_exceedance_probability'] for label in avaliable_opm_cases])
            rmin = max(np.amin(reduction_percentages) - 10.0, 0.0)
            rmax = min(np.amax(reduction_percentages) + 10.0, 100.0)

        baseline_percentages = np.array([rmin, rmax])
        baseline_probability = np.array([p_baseline, p_baseline])

        ax.plot(reduction_percentages, reduction_probability, 'b*', label='Exceedance Reduction')
        ax.plot(baseline_percentages, baseline_probability, 'r', label='Baseline')
        ax.set_xlabel('Reduction Percentage (%)')
        ax.set_ylabel('Spatial Exceedance Probability')
        ax.set_title('Operational Managagment Impact')
        ax.legend(loc=2, ncol=2)

        # Event count plot
        ax = self.figures['temporal_reduction_plot']['handle'].axes[0]
        ax.cla()
        max_number_events = 1
        catalog_ne_t = [0]

        if seismic_catalog:
            seismic_catalog.set_slice(magnitude_range=[seismic_catalog.magnitude_completeness, 100])

            # Check the time range
            if not len(forecasts.time_range):
                forecasts.time_range = [-1.0, 0.0]
                if seismic_catalog:
                    forecasts.time_range[0] = np.amin(seismic_catalog.epoch_slice) - grid.t_origin

            # Get the catalog event rate
            catalog_ne_t = grid.t * time_scale
            catalog_ne = seismic_catalog.cumulative_count
            catalog_ne = forecasts.check_plot_style(catalog_ne_t, catalog_ne)

            # Plot the saved catalog event rate
            max_number_events = np.amax(catalog_ne)
            line_style = gui_colors.periodic_line_style(0)
            line_style['linewidth'] = 2
            ax.plot(catalog_ne_t, catalog_ne, label='Catalog', **line_style)
        else:
            # Add an empty plot for figure control
            ax.plot([], [], 'k', linewidth=4, label='Catalog')

        # Joint forecast
        tf = (forecasts.forecast_time - grid.t_origin) * time_scale
        nf = forecasts.check_plot_style(forecasts.forecast_time, forecasts.forecast_cumulative_event_count)
        if len(nf):
            max_number_events = max(max_number_events, np.amax(nf))
        line_style = gui_colors.periodic_line_style(1)
        line_style['linewidth'] = 2
        ax.plot(tf, nf, label='Ensemble', **line_style)

        for ii, (label, reduction_ensemble) in enumerate(self.oms_ensemble.items()):
            line_style = gui_colors.periodic_line_style(ii + 2)
            ax.plot(tf, reduction_ensemble['temporal'], label=f'OPM Reduction {label}', **line_style)

        ax.set_xlabel('Time (days)')
        ax.set_ylabel('Event Count')
        ax.legend(loc=2, ncol=2)
