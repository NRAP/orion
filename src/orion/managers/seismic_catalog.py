# ------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2020-, Lawrence Livermore National Security, LLC
# All rights reserved
#
# See top level LICENSE, COPYRIGHT, CONTRIBUTORS, NOTICE, and ACKNOWLEDGEMENTS files for details.
# ------------------------------------------------------------------------------------------------
"""
seismic_catalog.py
-----------------------
"""

from orion import optional_packages
from orion.managers import manager_base
from orion.managers.manager_base import read_only_array, block_thread
from orion.utilities import timestamp_conversion, hdf5_wrapper, spatial
from orion.utilities.plot_config import gui_colors
from orion import _frontend
import numpy as np
from scipy import ndimage
import os
import datetime
import collections
from dataclasses import dataclass
import csep
import copy
import threading

decluster_kwargs = {
    "gardner-knopoff": {
        "window": "uhrhammer"
    },
    "nearest-neighbor": {
        "d": 1.6,
        "eta_0": None,
        "alpha_0": 1.5,
        "use_depth": False,
        "seed": 0
    },
    "reasenberg": {
        "rfact": 10,
        "xmeff": None,
        "xk": 0.5,
        "tau_min": 1.0,
        "tau_max": 10.0,
        "p": 0.95
    },
}


@dataclass(frozen=True)
class ComcatRequest:
    min_time: float = 0.0
    max_time: float = 0.0
    min_magnitude: float = 0.0
    min_latitude: float = 0.0
    max_latitude: float = 0.0
    min_longitude: float = 0.0
    max_longitude: float = 0.0


def bounds_are_same(bounds_a, bounds_b, tol=1e-9):
    same_bounds = True
    for k, x in bounds_a.items():
        for ii, y in enumerate(x):
            tmp = abs(y - bounds_b[k][ii])
            if tmp > tol:
                same_bounds = False
    return same_bounds


class SeismicCatalog(manager_base.ManagerBase):
    """
    Structure for holding seismic catalog information

    Attributes:
        epoch (ndarray): Event timestamps (seconds)
        latitude (ndarray): Event latitudes (degrees)
        longitude (ndarray): Event longitudes (degrees)
        depth (ndarray): Event depths (m)
        easting (ndarray): Eastings in target projection (m)
        northing (ndarray): Northings in target projection (m)
        magnitude (ndarray): Event magnitude magnitudes
        magnitude_bins (ndarray): magnitude magnitude bin edges
        varying_b_time (ndarray): Times for estimating sub-catalog b-values
        varying_b_value (ndarray): Gutenberg-Richter b-values over time
    """

    def set_class_options(self, **kwargs):
        """
        Seismic catalog initialization function

        """

        # Set the shorthand name
        self.short_name = 'Seismic Catalog'

        # Source
        self.projection = ''
        self.catalog_source = ''

        # Comcat filter
        self.use_comcat = 0
        self.comcat_min_magnitude = 0

        # Declustering
        self.type = ""
        self.decluster_algorithms = {k: False for k in decluster_kwargs}
        self.decluster_d = decluster_kwargs["nearest-neighbor"]["d"]
        self.decluster_rfact = decluster_kwargs["reasenberg"]["rfact"]
        self.decluster_xk = decluster_kwargs["reasenberg"]["xk"]
        self.decluster_tau_min = decluster_kwargs["reasenberg"]["tau_min"]
        self.decluster_tau_max = decluster_kwargs["reasenberg"]["tau_max"]
        self.decluster_p = decluster_kwargs["reasenberg"]["p"]

        # a, b-value methods
        self.a_value_default = 1.0
        self.b_value_default = 1.0
        self.b_value_min_points = 10
        self.b_value_methods = {
            "b-positive": gutenberg_richter_b_positive,
            "maximum likelihood estimate": gutenberg_richter_b_a_mle
        }
        self.current_b_value_method = "maximum likelihood estimate"

        # Etc
        self._default_magnitude_completeness = 2.5
        self._global_slice_type = 1
        self.plot_all_events = 1

        # Mapping data loader methods to file extension
        self._loader_map = {
            ".dat": self.load_catalog_zmap,
            ".txt": self.load_catalog_txt,
            ".csv": self.load_catalog_csv,
            ".hdf5": self.load_catalog_hdf5,
        }

    def set_data(self, **kwargs):
        """
        Setup data holders
        """
        # Source
        self._old_catalog_source = ''
        self._catalog_last_modified = -10000
        self.comcat_request = ComcatRequest()
        self.comcat_request_complete = False

        # Location
        self._points = spatial.Points(latitude=np.zeros(0), longitude=np.zeros(0))
        self._depth = np.zeros(0)

        # Timing
        self._epoch = np.zeros(0)
        self._slice_indices = []
        self._slice_indices_valid = []
        self._time_range = [-1e99, 1e99]

        # Size, distribution
        self._magnitude = np.zeros(0)
        self._magnitude_bins = np.zeros(0)
        self._cumulative_frequency = np.zeros(0)
        self._a_value = 0.0
        self._b_value = 0.0
        self._varying_b_time = np.zeros(0)
        self._varying_b_value = np.zeros(0)
        self._magnitude_completeness = -3.0
        self._magnitude_completeness_slice = -3.0
        self._magnitude_range = [-1e99, 1e99]

        # Count/rate information
        self._cumulative_count = np.zeros(0)
        self._spatial_count = np.zeros((0, 0, 0))
        self._spatial_rate = np.zeros((0, 0, 0))
        self._spatial_density_count = np.zeros((0, 0, 0))
        self._spatial_density_rate = np.zeros((0, 0, 0))
        self._spatial_max_magnitude = np.zeros((0, 0, 0))

        # Declustering
        self._slice_indices_pre_decluster = []
        self._slice_indices_mc_grid_bounds = []
        self._declustering_indices = {}
        self._previous_decluster_attributes = {}

        # Etc
        self._recalculate_grid_data = True
        self._valid_bounds_kwargs = {}
        self._previous_valid_bounds_kwargs = {}
        self.other_data = {}

    def set_gui_options(self, **kwargs):
        """
        Setup interface options
        """
        self.set_visibility_all()

        # Figures
        fig_size = (5, 3)
        if _frontend == 'strive':
            fig_size = (90, 85)

        catalog_help = 'This figure shows the location and magnitude of observed seismic events.'
        distribution_help = 'This figure shows the distribution of seismic magnitudes and the Gutenberg Richter model fit.'
        time_help = 'This figure shows the magnitude of events over time.'
        bvalue_help = 'This figure shows the evolution of the Gutenberg Richter b-value over time.'

        self.figure = {}
        if ('no_figures' not in kwargs):
            self.figures = {
                'map_view_catalog': {
                    'position': [0, 0],
                    'size': fig_size,
                    'target': 'catalog_map_view',
                    'help_text': catalog_help
                },
                'magnitude_distribution': {
                    'position': [0, 1],
                    'size': fig_size,
                    'target': 'catalog_magnitude_distribution',
                    'help_text': distribution_help
                },
                'time_series': {
                    'position': [1, 0],
                    'size': fig_size,
                    'target': 'catalog_time_series',
                    'help_text': time_help
                },
                'b_value_time': {
                    'position': [1, 1],
                    'size': fig_size,
                    'target': 'catalog_b_value_time',
                    'help_text': bvalue_help
                }
            }

        # Add gui elements
        self.gui_elements['catalog_source'] = {
            'element_type': 'file',
            'command': 'file',
            'label': 'Catalog Path',
            'position': [0, 0],
            'filetypes': [('hdf5', '*.hdf5'), ('txt', '*.txt'), ('csv', '*.csv'), ('dat', '*.dat'), ('all', '*')]
        }

        self.gui_elements['use_comcat'] = {'element_type': 'check', 'label': 'Use ComCat', 'position': [1, 0]}

        self.gui_elements['comcat_min_magnitude'] = {
            'element_type': 'entry',
            'label': 'Min Magnitude Request',
            'position': [2, 0]
        }

        # Declustering GUI elements
        self.gui_elements['decluster_algorithms'] = {
            'element_type': 'checkbox',
            'position': [0, 1],
            'ncol': 1,
            'header': 'Declustering Algorithms:'
        }
        self.gui_elements['decluster_d'] = {
            'element_type': 'entry',
            'label': 'Fractal dimension (NN)',
            'position': [4, 0]
        }
        self.gui_elements['decluster_rfact'] = {
            'element_type': 'entry',
            'label': 'Number of crack radii (RS)',
            'position': [5, 0]
        }
        self.gui_elements['decluster_xk'] = {
            'element_type': 'entry',
            'label': 'Magnitude scaling factor (RS)',
            'position': [6, 0]
        }
        self.gui_elements['decluster_tau_min'] = {
            'element_type': 'entry',
            'label': 'Minimum look ahead time (RS)',
            'units': '(day)',
            'position': [4, 1]
        }
        self.gui_elements['decluster_tau_max'] = {
            'element_type': 'entry',
            'label': 'Maximum look ahead time (RS)',
            'units': '(day)',
            'position': [5, 1]
        }
        self.gui_elements['decluster_p'] = {
            'element_type': 'entry',
            'label': 'Confidence level (RS)',
            'position': [6, 1]
        }
        self.gui_elements['current_b_value_method'] = {
            'element_type': 'dropdown',
            'label': 'b-value method',
            'position': [7, 0],
            'values': list(self.b_value_methods.keys())
        }
        self.gui_elements['plot_all_events'] = {'element_type': 'check', 'label': 'Plot all events', 'position': [8, 0]}

    def __len__(self):
        """Return the length of catalog"""
        return len(self._epoch)

    def __bool__(self):
        """
        Quick test to see if data is loaded

        Returns:
            bool: Flag to indicate whether data is loaded

        """
        return self.__len__() > 0

    def __getitem__(self, islice):
        """Return a sliced copy of the catalog"""
        if isinstance(islice, slice):
            tmp = np.arange(len(self))
            islice = tmp[islice]

        elif np.ndim(islice) == 0:
            islice = np.array([islice])

        elif np.ndim(islice) > 1:
            raise ValueError()

        # Sort by epoch
        idx = np.argsort(self._epoch[islice])
        islice = islice[idx]

        sliced_catalog = copy.copy(self)
        sliced_catalog._lock = threading.Lock()
        sliced_catalog._points = self._points[islice]
        sliced_catalog._depth = self._depth[islice]
        sliced_catalog._epoch = self._epoch[islice]
        sliced_catalog._magnitude = self._magnitude[islice]
        sliced_catalog._slice_indices_pre_decluster = self._slice_indices_pre_decluster[islice]
        sliced_catalog._slice_indices_mc_grid_bounds = self._slice_indices_mc_grid_bounds[islice]
        sliced_catalog.catalog_source = f"{self.catalog_source}_slice"
        sliced_catalog.reset_slice()

        if self._declustering_indices:
            sliced_catalog._declustering_indices = {k: v[islice] for k, v in self._declustering_indices.items()}

        return sliced_catalog

    @block_thread
    def append_catalog(self, catalog=None, fname='', **kwarg_catalog):
        """
        Append new seismic catalog data to the current object.
        The new catalog data can be specified as a SeismicCatalog instance,
        as a filename to load, or as keyword arguments (see self.load_catalog_dict for details)

        Args:
            catalog (SeismicCatalog): The target catalog data to add
            fname (str): The filename containing new catalog data to add
            kwarg_catalog (dict): The catalog data as a keyword arguments
        """
        # Process input data
        if catalog is None:
            catalog = SeismicCatalog()
            catalog.projection = self.projection

        if fname:
            catalog.catalog_source = fname
            catalog.load_data()

        if kwarg_catalog:
            catalog.load_catalog_dict(kwarg_catalog)

        # Append the catalog
        self._epoch = np.concatenate([self._epoch, catalog._epoch], axis=0)
        self._magnitude = np.concatenate([self._magnitude, catalog._magnitude], axis=0)
        self._depth = np.concatenate([self._depth, catalog._depth], axis=0)
        self._points.append_points(latitude=catalog._latitude,
                                   longitude=catalog._longitude,
                                   target_projection_str=self._points.projection_str)

        # Reset declustering parameters
        self._declustering_indices = {}
        self._previous_decluster_attributes = {}
        self._previous_valid_bounds_kwargs = None

        # Copy additional data
        N = len(catalog)
        M = len(self)
        for k in self.other_data.keys():
            new_data = catalog.other_data.get(k, np.full(N, np.nan))
            self.other_data[k] = np.concatenate([self.other_data[k], new_data], axis=0)

        for k in catalog.other_data.keys():
            if k not in self.other_data:
                orig_data = np.full(M, np.nan)
                self.other_data[k] = np.concatenate([orig_data, catalog.other_data[k]], axis=0)

        self.reset_slice()

    @block_thread
    def load_data(self, grid):
        """
        Load the seismic catalog if necessary
        """
        self.projection = grid.projection_str
        if self.catalog_source:
            # Check to see if the file exists
            f = os.path.expanduser(os.path.normpath(self.catalog_source))
            if not os.path.isfile(f):
                self.logger.error(f'Cannot find seismic catalog file: {f}')
                self.set_data()
                return

            # Check the file status
            update_catalog = (self.catalog_source != self._old_catalog_source)
            if not update_catalog:
                ft = os.path.getmtime(f)
                if (ft > self._catalog_last_modified + 1.0):
                    self.logger.info('Catalog file update detected')
                    self._catalog_last_modified = ft
                    update_catalog = True

            if update_catalog:
                ext = os.path.splitext(f)[-1]
                if ext in self._loader_map:
                    try:
                        self._loader_map[ext](f)
                    except Exception as e:
                        self.logger.error(f'Failed to load catalog: {f}')
                        self.logger.error(repr(e))
                        self.set_data()
                        return
                else:
                    self.logger.error(f"Unrecognized catalog type: {f}")
                    self.set_data()
                    return

                if self.N >= self.b_value_min_points:
                    self._magnitude_completeness = max_curvature(self._magnitude, mbin=0.1)[0]
                else:
                    self._magnitude_completeness = self._default_magnitude_completeness
                self._previous_valid_bounds_kwargs = None
                self._old_catalog_source = self.catalog_source

        elif self.use_comcat:
            box = grid.get_lat_lon_box()
            new_comcat_request = ComcatRequest(
                min_time=grid.t_min + grid.t_origin,
                max_time=grid.t_max + grid.t_origin,
                min_magnitude=self.comcat_min_magnitude,
                min_latitude=box[0],
                max_latitude=box[1],
                min_longitude=box[2],
                max_longitude=box[3],
            )

            if new_comcat_request != self.comcat_request:
                self.comcat_request = new_comcat_request
                self.load_comcat_catalog()
                if self.N >= self.b_value_min_points:
                    self._magnitude_completeness = max_curvature(self._magnitude, mbin=0.1)[0]
                else:
                    self._magnitude_completeness = self._default_magnitude_completeness
                self._previous_valid_bounds_kwargs = None

        # Setup the valid bounds
        self._valid_bounds_kwargs = {
            'magnitude_range': [self.magnitude_completeness, 100],
            'x_range': [grid.x_nodes[0], grid.x_nodes[-1]],
            'y_range': [grid.y_nodes[0], grid.y_nodes[-1]],
            'z_range': [grid.z_nodes[0], grid.z_nodes[-1]],
            'time_range': [grid.t_nodes[0] + grid.t_origin, grid.t_nodes[-1] + grid.t_origin],
            'N': [len(grid.x_nodes), len(grid.y_nodes),
                  len(grid.z_nodes), len(grid.t_nodes)]
        }

        # Finalize the slice
        self.reset_slice()
        self.check_declustering_requests()
        self.check_grid_slice_data(grid)

    def check_declustering_requests(self):
        """
        Check whether declustering parameters have changed
        and apply them if necessary
        """
        decluster_attributes = {
            "algorithms": self.decluster_algorithms,
            "d": self.decluster_d,
            "rfact": self.decluster_rfact,
            "xk": self.decluster_xk,
            "tau_min": self.decluster_tau_min,
            "tau_max": self.decluster_tau_max,
            "p": self.decluster_p,
        }

        # Always attempt to apply declustering when the catalog has been loaded/changed
        # Otherwise, re-apply declustering if any parameters have changed
        cond = True
        if self._previous_decluster_attributes:
            for k, v in decluster_attributes.items():
                vref = self._previous_decluster_attributes[k]
                cond = not (v == vref if k == "algorithms" else np.allclose(v, vref))
                if cond:
                    break

        self._previous_decluster_attributes.update(decluster_attributes)

        # Decluster the full catalog
        if cond:
            if not self._declustering_indices:
                self._declustering_indices = {"full": np.ones(self.N, dtype=bool)}

            for algorithm, enabled in self.decluster_algorithms.items():
                if not enabled:
                    continue

                kwargs = {
                    k: decluster_attributes[k] if k in decluster_attributes else v
                    for k, v in decluster_kwargs[algorithm].items()
                }

                idx = self.decluster(algorithm, return_indices=True, **kwargs)
                self._declustering_indices[algorithm] = np.zeros(self.N, dtype=bool)
                self._declustering_indices[algorithm][idx] = True

    def load_catalog_dict(self, data):
        """
        Load the seismic catalog from a dictionary.

        Required entries in the catalog include: epoch, magnitude, depth
        Location entries can include one of the following:
        * latitude, longitude
        * easting, northing (local coordinates)
        * easting, northing, utm_zone

        Args:
            data (dict): catalog dictionary
        """
        # Sort values by epoch
        Ia = np.argsort(data['epoch'])

        self._epoch = data['epoch'][Ia]
        self._magnitude = data['magnitude'][Ia]
        self._depth = data['depth'][Ia]

        # Load location information
        if ('utm_zone' in data):
            data['utm_zone'] = spatial.parse_utm_zone_str(data['utm_zone'])
        elif ('source_projection_str' not in data):
            data['source_projection_str'] = self.projection
        self._points = spatial.Points(**data, target_projection_str=self.projection)[Ia]

        # Reset declustering parameters
        self._declustering_indices = {}
        self._previous_decluster_attributes = {}

        self.other_data = {}
        targets = [
            'magnitude', 'depth', 'longitude', 'latitude', 'easting', 'northing', 'utm_zone', 'source_projection_str'
        ]
        for k in data:
            if k not in targets:
                if len(data[k]) == self.N:
                    self.other_data[k] = data[k][Ia]
                else:
                    # If lengths differ, do not sort
                    self.other_data[k] = data[k]

        self.reset_slice()

    def load_catalog_array(self, **xargs):
        """
        Initialize catalog from pre-loaded arrays.
        Required arguments include: epoch, magnitude, depth
        Location entries can include one of the following:
        * latitude, longitude
        * easting, northing (local coordinates)
        * easting, northing, utm_zone

        Additional arguments will be placed in the other_data dict

        Args:
            epoch (np.ndarray): 1D array of event time in epoch
            depth (np.ndarray): 1D array of event depths
            magnitude (np.ndarray): 1D array of event magnitudes
            longitude (np.ndarray): 1D array of event longitudes
            latitude (np.ndarray): 1D array of event latitudes
            easting (np.ndarray): 1D array of event eastings
            northing (np.ndarray): 1D array of event northings
            utm_zone (str): UTM zone string (e.g.: '4SU')
        """
        self.load_catalog_dict(xargs)

    def load_catalog_hdf5(self, filename):
        """
        Load the seismic catalog from an hdf5 format file.
        See load_catalog_dict for required entries

        Args:
            filename (str): catalog file name
        """
        self.logger.info(f"Loading catalog from hdf5 format file: {filename}")
        with hdf5_wrapper.hdf5_wrapper(filename) as data:
            self.load_catalog_dict(data.get_copy())

    def load_catalog_csv(self, filename):
        """
        Reads .csv format seismic catalog files
        The file should have an optional first line with the zone information "utm_zone, zone_id"
        or general projection information "projection, projection_str"
        and a line with variable names separated by commas
        See load_catalog_dict for required entries

        Args:
            filename (str): catalog file name

        """
        self.logger.info(f"Loading catalog from csv format file: {filename}")
        value_names = []
        data = {}
        header_size = 1
        with open(filename) as f:
            line = f.readline()[:-1]
            if ('utm_zone' in line):
                header_size += 1
                data['utm_zone'] = line.split(',')[1].strip()
                line = f.readline()[:-1]
            elif ('projection' in line):
                header_size += 1
                data['source_projection_str'] = line.split(',')[1].strip()
                line = f.readline()[:-1]
            value_names = [x.strip() for x in line.split(',')]

        tmp = np.loadtxt(filename, delimiter=',', skiprows=header_size, unpack=True)
        for ii, k in enumerate(value_names):
            data[k] = tmp[ii]
        self.load_catalog_dict(data)

    def load_catalog_zmap(self, filename):
        """
        Reads zmap (.dat) format seismic catalog files

        Args:
            filename (str): catalog file name

        """
        # Check the file format
        self.logger.debug('Loading zmap-format seismic catalog data')
        if '.dat' not in filename:
            raise Exception('File format not recognized')

        # Load the data
        latitude, longitude, year, month, day, magnitude, depth, hour, minute, second = np.loadtxt(filename,
                                                                                                   unpack=True)
        epoch = timestamp_conversion.convert_time_arrays(year, month, day, hour, minute, second)
        self.load_catalog_array(latitude=latitude, longitude=longitude, magnitude=magnitude, depth=depth, epoch=epoch)

    def load_catalog_txt(self, filename):
        """
        Reads .txt format seismic catalog files (oklahoma catalog)

        Args:
            filename (str): catalog file name

        """
        # Load the data
        # Note: depth is expected in km
        self.logger.debug('Loading txt-format seismic catalog data')
        longitude, latitude, depth, magnitude, epoch, dec_year = np.loadtxt(filename, unpack=True, skiprows=1)
        self.load_catalog_array(latitude=latitude,
                                longitude=longitude,
                                magnitude=magnitude,
                                depth=depth * 1e3,
                                epoch=epoch)

    def load_comcat_catalog(self):
        self.logger.info('Attempting to load comcat catalog')
        ta = datetime.date.fromtimestamp(self.comcat_request.min_time)
        tb = datetime.date.fromtimestamp(self.comcat_request.max_time)
        try:
            catalog = csep.query_comcat(ta,
                                        tb,
                                        min_magnitude=self.comcat_request.min_magnitude,
                                        min_longitude=self.comcat_request.min_longitude,
                                        max_longitude=self.comcat_request.max_longitude,
                                        min_latitude=self.comcat_request.min_latitude,
                                        max_latitude=self.comcat_request.max_latitude,
                                        verbose=True)

            # Note: pycsep seems to return milliseconds for epoch and km for depth
            self.load_catalog_array(latitude=catalog.get_latitudes(),
                                    longitude=catalog.get_longitudes(),
                                    magnitude=catalog.get_magnitudes(),
                                    depth=catalog.get_depths() * 1e3,
                                    epoch=catalog.get_epoch_times() * 1e-3)

            # Save a copy of the last comcat request
            fname = os.path.expanduser(os.path.join('~', '.cache', 'orion', 'last_comcat_result.hdf5'))
            self.save_catalog_hdf5(fname)

        except Exception as e:
            self.logger.error('Could not fetch comcat catalog')
            self.logger.error(e)

        self.comcat_request_complete = True
        self.logger.info('Done!')

    def get_catalog_as_dict(self):
        """
        Save key catalog entries to a dict

        Returns:
            dict: A dictionary of catalog data
        """
        data = self.other_data.copy()
        data['epoch'] = self.epoch_slice
        data['magnitude'] = self.magnitude_slice
        data['depth'] = self.depth_slice
        data['longitude'] = self.longitude_slice
        data['latitude'] = self.latitude_slice
        data['easting'] = self.easting_slice
        data['northing'] = self.northing_slice
        data['projection'] = self._points.projection_str
        return data

    def save_catalog_hdf5(self, filename):
        """
        Save the seismic catalog to an hdf5 format file

        Args:
            filename (str): catalog file name
        """
        self.logger.info(f'Saving catalog to hdf5 format file: {filename}')
        catalog = self.get_catalog_as_dict()
        with hdf5_wrapper.hdf5_wrapper(filename, mode='w') as data:
            for k, value in catalog.items():
                data[k] = value

    def save_catalog_csv(self, filename):
        """
        Save the seismic catalog as a .csv format file

        Args:
            filename (str): catalog file name

        """
        self.logger.info(f'Saving catalog to csv format file: {filename}')
        catalog = self.get_catalog_as_dict()

        # Build the header
        header = ''
        if catalog['projection']:
            header += f"projection,{catalog['projection']}\n"
            del catalog['projection']
        header_keys = sorted(catalog.keys())
        header += ','.join(header_keys)

        # Split any tensor data
        initial_catalog_keys = list(catalog)
        for k in initial_catalog_keys:
            if isinstance(catalog[k], np.ndarray):
                M = np.shape(catalog[k])
                if (len(M) > 1):
                    tmp = np.reshape(catalog[k], (M[0], -1))
                    for ii in range(np.shape(tmp)[1]):
                        catalog[f"{k}_{ii}"] = np.squeeze(tmp[:, ii])
                    del catalog[k]

        # Assemble the data, padding where necessary to keep a consistent length
        N = max([len(catalog[k]) for k in catalog])
        for k in catalog:
            M = len(catalog[k])
            if M < N:
                catalog[k] = np.resize(catalog[k], N)

        # Save the data
        data = np.concatenate([np.expand_dims(catalog[k], -1) for k in header_keys], axis=1)
        np.savetxt(filename, data, delimiter=',', comments='', header=header)

    def calculate_seismic_characteristics(
        self,
        magnitude_bin_res=0.1,
        time_segments=10,
    ):
        """
        Generate various seismic characteristics

        Args:
            magnitude_bin_res (float): bin spacing for calculating b, a values
            time_segments (int): number of segments to calculate b values over time

        """
        self.logger.debug('Calculating catalog seismic characteristics')

        # Get the slice data
        m_slice = self.magnitude_slice
        t_slice = self.epoch_slice

        if len(t_slice):
            # Determine the global b, a values
            self._b_value, self._a_value = self.estimate_b_a_value(m_slice)
            if len(m_slice) >= self.b_value_min_points:
                self._magnitude_completeness_slice = max_curvature(m_slice, mbin=0.1)[0]
            else:
                self._magnitude_completeness_slice = 0.0
            self._magnitude_bins = np.arange(np.min(m_slice), np.max(m_slice) + 1, 0.1)
            self._cumulative_frequency = np.histogram(m_slice, self._magnitude_bins)[0]

            # Calculate the b-value as a function of time
            self._varying_b_value = np.zeros(time_segments)
            t_bins = np.linspace(np.amin(t_slice) - 60.0, np.amax(t_slice) + 60.0, time_segments + 1)
            self._varying_b_time = t_bins[:-1] + 0.5 * (t_bins[1] - t_bins[0])
            bin_ids = np.digitize(t_slice, t_bins) - 1
            for ii in range(time_segments):
                Isplit = np.where(bin_ids == ii)
                self._varying_b_value[ii] = self.estimate_b_a_value(m_slice[Isplit])[0]
        else:
            self._a_value = 0.0
            self._b_value = 0.0
            self._magnitude_completeness_slice = -1.0
            self._magnitude_bins = np.linspace(-3.0, 3.0, 7)
            self._cumulative_frequency = np.zeros(0)
            self._varying_b_value = np.zeros(0)
            self._varying_b_time = np.zeros(0)

    def reset_slice(self):
        """
        Set the catalog time slice to fit the entire catalog
        """
        self._time_range = [-1e99, 1e99]
        self._magnitude_range = [-1e99, 1e99]
        self._slice_indices = np.ones(self.N, dtype=bool)

        self.check_grid_mc_bounds()
        self._slice_indices_valid = self._slice_indices_mc_grid_bounds.copy()
        self.type = "full"

        if self.N:
            self.calculate_seismic_characteristics()

    def get_slice_indices(self,
                          time_range=[-1e99, 1e99],
                          magnitude_range=[-1e99, 1e99],
                          x_range=[-1e99, 1e99],
                          y_range=[-1e99, 1e99],
                          z_range=[-1e99, 1e99],
                          minimum_interevent_time=0.0,
                          **kwargs):
        """
        Get catalog slice indices

        Args:
            time_range (list): list of sub-catalog min/max times (epoch time)
            magnitude_range (list): list of sub-catalog min/max event magnitudes
            x_range (list): list of sub-catalog min/max x locations
            y_range (list): list of sub-catalog min/max y locations
            z_range (list): list of sub-catalog min/max z locations
            minimum_interevent_time (float): only include events if this amount of time has elapsed since the last
        """
        valid_points = np.ones(self.N, dtype=bool)

        slice_messages = []
        if time_range[0] > -1e98:
            slice_messages.append(f"t_min={time_range[0]:1.1f} s")
            valid_points[self._epoch < time_range[0]] = False

        if time_range[1] < 1e98:
            slice_messages.append(f"t_max={time_range[1]:1.1f} s")
            valid_points[self._epoch > time_range[1]] = False

        if x_range[0] > -1e98:
            slice_messages.append(f"x_min={x_range[0]:1.1f} s")
            valid_points[self._easting < x_range[0]] = False

        if x_range[1] < 1e98:
            slice_messages.append(f"x_max={x_range[1]:1.1f} s")
            valid_points[self._easting > x_range[1]] = False

        if y_range[0] > -1e98:
            slice_messages.append(f"y_min={y_range[0]:1.1f} s")
            valid_points[self._northing < y_range[0]] = False

        if y_range[1] < 1e98:
            slice_messages.append(f"y_max={y_range[1]:1.1f} s")
            valid_points[self._northing > y_range[1]] = False

        if z_range[0] > -1e98:
            slice_messages.append(f"z_min={z_range[0]:1.1f} s")
            valid_points[self._depth < z_range[0]] = False

        if z_range[1] < 1e98:
            slice_messages.append(f"z_max={z_range[1]:1.1f} s")
            valid_points[self._depth > z_range[1]] = False

        if magnitude_range[0] > -1e98:
            slice_messages.append(f"m_min={magnitude_range[0]:1.1f}")
            valid_points[self._magnitude < magnitude_range[0]] = False

        if magnitude_range[1] < 1e98:
            slice_messages.append(f"m_max={magnitude_range[1]:1.1f}")
            valid_points[self._magnitude > magnitude_range[1]] = False

        if minimum_interevent_time > 0.0:
            last_t = -1e99
            for i, (ti, valid_point) in enumerate(zip(self._epoch, valid_points)):
                if valid_point:
                    if ti - last_t < minimum_interevent_time:
                        valid_points[i] = False
                    else:
                        last_t = ti

        if len(slice_messages):
            s = ', '.join(slice_messages)
            N = np.sum(valid_points)
            self.logger.debug(f'Slicing catalog: {s}')
            self.logger.debug(f'View contains {N} events')

        return valid_points

    def check_grid_mc_bounds(self):
        # Check for valid inputs
        if not self._valid_bounds_kwargs:
            self._slice_indices_mc_grid_bounds = np.ones(len(self), dtype=int)
            return

        # Check to see if the set has already been created
        if self._previous_valid_bounds_kwargs:
            same_bounds = bounds_are_same(self._previous_valid_bounds_kwargs, self._valid_bounds_kwargs)

            if same_bounds:
                return

        # Set data and flags
        self._recalculate_grid_data = True
        self._previous_valid_bounds_kwargs = self._valid_bounds_kwargs.copy()
        self._slice_indices_mc_grid_bounds = self.get_slice_indices(**self._valid_bounds_kwargs)

    @block_thread
    def set_slice(self, type_=None, inplace=True, **kwargs):
        """
        Set the catalog time slice

        Args:
            type_ (str): catalog type
            inplace (bool): if True, set slice in-place
            kwargs (dict): Arguments to get_slice_indices
        """
        self.logger.debug('Setting seismic catalog slice')

        # Set default values
        for k in ['magnitude_range', 'x_range', 'y_range', 'z_range', 'time_range']:
            if k not in kwargs:
                kwargs[k] = [-1e99, 1e99]

        if inplace:
            self._time_range = kwargs.get('time_range', [-1e99, 1e99])
            self._magnitude_range = kwargs.get('magnitude_range', [-1e99, 1e99])

        self._slice_indices_pre_decluster = self.get_slice_indices(**kwargs)
        self.check_grid_mc_bounds()
        return self.finalize_slice(type_, inplace)

    def finalize_slice(self, type_=None, inplace=True):
        """
        Apply the declustering method and finalize the catalog realization

        Args:
            type_ (str): catalog type
            inplace (bool): if True, set slice in-place
        """
        slice_indices = self._slice_indices_pre_decluster.copy()
        if self._declustering_indices and (type_ is not None):
            slice_indices = np.logical_and(slice_indices, self._declustering_indices[type_])

        if inplace:
            slice_indices_valid = np.logical_and(slice_indices, self._slice_indices_mc_grid_bounds)
            self._slice_indices = slice_indices
            self._slice_indices_valid = slice_indices_valid
            target = self

        else:
            target = self[slice_indices]

        if len(slice_indices):
            target.calculate_seismic_characteristics()

        if type_ not in self._declustering_indices:
            # self.logger.error(f"Unknown catalog type '{type_}'")
            target.type = self.type

        else:
            target.type = type_

        if not inplace:
            return target

    def save_csep_ascii_format(self, fname):
        self.logger.info('Exporting catalog in csep ascii format...')
        with open(os.path.expanduser(os.path.normpath(fname)), 'w') as f:
            f.write('lon,lat,mag,time_string,depth,catalog_id,event_id\n')
            latitude = self.latitude_slice
            longitude = self.longitude_slice
            depth = self.depth_slice
            epoch = self.epoch_slice
            magnitude = self.magnitude_slice
            for i, (lat, lon, mag, t, z) in enumerate(zip(latitude, longitude, magnitude, epoch, depth)):
                f.write(f"{lat},{lon},{mag},{timestamp_conversion.get_time_str_pycsep(t)},{z * 1e-3},orion,{i:06d}\n")
        self.logger.debug('Finished writing catalog')

    def get_scaled_point_size(self, x, point_scale=0.5):
        x_range = [0.0, 1.0]
        N = len(x)
        if N > 1:
            x_range = [np.amin(x), np.amax(x)]

        b = (50.0)**(1.0 / (x_range[1] - x_range[0]))
        b = min(max(b, 1.25), 3.0)
        point_size = point_scale * (b**(1 + x - x_range[0]))
        return point_size

    def calculate_spatial_parameters(self, grid):
        """
        Calculate key spatial parameters on the grid

        Args:
            grid (orion.managers.grid_manager.GridManager): The target grid
        """
        # Note: only calculate properties for valid local points
        # (within the grid, and M > Mc)
        previous_slice_type = self._global_slice_type
        self.set_slice_type('local')
        count_xyzt = grid.histogram_values(self.easting_slice,
                                           self.northing_slice,
                                           self.depth_slice,
                                           self.epoch_slice - grid.t_origin,
                                           include_edges=False)
        count_txy = np.sum(np.moveaxis(count_xyzt, -1, 0), axis=3)
        self._spatial_count = np.cumsum(count_txy, axis=0)
        self._cumulative_count = np.sum(self._spatial_count, axis=(1, 2))

        # Estimate the rate
        dt = np.diff(grid.t)
        dt = np.reshape(np.concatenate([dt[:1], dt], axis=0), (-1, 1, 1))
        self._spatial_rate = count_txy / dt
        self._spatial_density_rate = self._spatial_rate / np.expand_dims(grid.areas, 0)

        # Estimate max magnitude
        Ia = grid.digitize_values(self.easting_slice,
                                  self.northing_slice,
                                  self.depth_slice,
                                  self.epoch_slice - grid.t_origin,
                                  include_edges=True)

        m_min = -3.0
        if len(self.magnitude_slice):
            m_min = np.amin(self.magnitude_slice)
        tmp = np.zeros_like(count_xyzt) - m_min

        for ii in range(len(self.magnitude_slice)):
            Ib = tuple([max(d[ii] - 1, 0) for d in Ia])
            tmp[Ib] = max(tmp[Ib], self.magnitude_slice[ii])
        self._spatial_max_magnitude = np.sum(np.moveaxis(tmp, -1, 0), axis=3)

        # Reset the slice type if necessary
        if previous_slice_type:
            self.set_slice_type('global')

    def check_grid_slice_data(self, grid):
        """
        Build key parameters for plotting

        Args:
            grid (orion.managers.grid_manager.GridManager): The target grid
        """
        self.reset_slice()
        if self._recalculate_grid_data:
            self.calculate_spatial_parameters(grid)
            self._recalculate_grid_data = False

        if self.plot_all_events:
            self.set_slice_type('global')
        else:
            self.set_slice_type('local')

    @block_thread
    def get_plot_data(self, projection):
        self.check_grid_slice_data(projection)
        return {
            'x': self.easting_slice,
            'y': self.northing_slice,
            'z': self.depth_slice,
            'latitude': self.latitude_slice,
            'longitude': self.longitude_slice,
            'time': self.epoch_slice,
            'magnitude': self.magnitude_slice,
            'point_size': self.scaled_point_size_slice,
            'catalog_cumulative_event_count': self.cumulative_count,
            'catalog_spatial_count': self.spatial_count
        }

    def catalog_map_view(self, plot_data):
        tmp = plot_data['Seismic Catalog']
        grid = plot_data['General']
        distance_scale = grid['distance_scale']
        distance_units = grid['distance_units']

        layers = {
            'ms': {
                'x': (tmp['x'] - grid['x_origin']) * distance_scale,
                'y': (tmp['y'] - grid['y_origin']) * distance_scale,
                'c': (tmp['time'] - grid['t_origin']) / (60 * 60 * 24.0),
                's': tmp['point_size'],
                't': {
                    'Magnitude': tmp['magnitude']
                },
                'type': 'scatter'
            }
        }
        axes = {'x': f'X ({distance_units})', 'y': f'Y ({distance_units})', 'c': 'Time (days)', 's': 'Marker'}
        return layers, axes

    def catalog_magnitude_distribution(self, plot_data):
        count = 10**(self._a_value - self._b_value * self._magnitude_bins)
        bins = self._magnitude_bins
        bin_height = self._cumulative_frequency

        layers = {
            'gutenberg_richter_ba': {
                'x': bins,
                'y': count,
                'type': 'line'
            },
            'magnitude_distribution': {
                'x': bins,
                'y': bin_height,
                'type': 'bar'
            }
        }
        axes = {'x': 'Magnitude', 'y': 'Count', 'log_y': True}
        return layers, axes

    def catalog_time_series(self, plot_data):
        t_scale = 60 * 60 * 24.0
        grid = plot_data['General']
        t = (self.epoch_slice - grid['t_origin']) / t_scale
        magnitude = self.magnitude_slice
        z = self.depth_slice

        layers = {'ms': {'x': t, 'y': magnitude, 'c': z, 'type': 'scatter'}}
        axes = {'x': 'Time (days)', 'y': 'Magnitude', 'c': 'Depth (m)'}
        return layers, axes

    def catalog_b_value_time(self, plot_data):
        t_scale = 60 * 60 * 24.0
        grid = plot_data['General']
        t = (self._varying_b_time - grid['t_origin']) / t_scale
        b = self._varying_b_value

        layers = {'ms': {'x': t, 'y': b, 'type': 'line'}}
        axes = {'x': 'Time (days)', 'y': 'b-value'}
        return layers, axes

    def generate_plots(self, **kwargs):
        from matplotlib.ticker import MaxNLocator
        import matplotlib.pyplot as plt

        self.logger.debug('Generating seismic catalog plots')
        grid = kwargs.get('grid')
        appearance = kwargs.get('appearance')
        self.reset_slice()

        # Set plot data
        t_scale = 60 * 60 * 24.0
        x_range, y_range, z_range = grid.get_plot_range()
        if self.N:
            magnitude = self.magnitude_slice
            t = (self.epoch_slice - grid.t_origin) / t_scale
            M = len(magnitude)
            magnitude_range = [0.0, 1.0]
            if M > 0:
                magnitude_range = [np.amin(magnitude), np.amax(magnitude)]
            ms_point_size = self.scaled_point_size_slice

            # Map/3D view
            x_range = [np.amin(self.easting_slice), np.amax(self.easting_slice)]
            y_range = [np.amin(self.northing_slice), np.amax(self.northing_slice)]
            x_range[0] = min(x_range[0], grid.x[0])
            x_range[1] = max(x_range[1], grid.x[-1])
            y_range[0] = min(y_range[0], grid.y[0])
            y_range[1] = max(y_range[1], grid.y[-1])

            ax = self.figures['map_view_catalog']['handle'].axes[0]
            ax.cla()
            ax.xaxis.set_major_locator(MaxNLocator(5))
            ax.yaxis.set_major_locator(MaxNLocator(5))
            ca = None
            ca = ax.scatter(self.easting_slice,
                            self.northing_slice,
                            s=ms_point_size,
                            c=t,
                            cmap=gui_colors.point_colormap,
                            edgecolors='k',
                            linewidths=0.1)
            if 'colorbar' not in self.figures['map_view_catalog']:
                self.figures['map_view_catalog']['colorbar'] = self.figures['map_view_catalog']['handle'].colorbar(
                    ca, ax=ax)
                self.figures['map_view_catalog']['colorbar'].set_label('t (days)')
            self.figures['map_view_catalog']['colorbar'].update_normal(ca)

            # Magnitude distribution
            tmp_N = 10**(self._a_value - self._b_value * self._magnitude_bins)
            tmp_w = self._magnitude_bins[1] - self._magnitude_bins[0]
            # complete_N = 1.5 * 10**(self._a_value - self._b_value * self._magnitude_completeness)

            ax = self.figures['magnitude_distribution']['handle'].axes[0]
            ax.cla()
            ax.bar(self._magnitude_bins[:-1], self._cumulative_frequency, tmp_w, **gui_colors.histogram_style)
            ax.semilogy(self._magnitude_bins,
                        tmp_N,
                        label=f"b={self._b_value:1.2f}, a={self._a_value:1.2f}",
                        **gui_colors.alt_line_style)
            ax.legend(loc=1)

            # Time series
            ax = self.figures['time_series']['handle'].axes[0]
            ax.cla()
            if M > 0:
                sh = ax.stem(t,
                             magnitude,
                             linefmt=gui_colors.line_style['color'],
                             markerfmt='None',
                             bottom=np.floor(np.amin(magnitude)))
                plt.setp(sh[1], linewidth=0.5)
                ax.plot(t, magnitude, **gui_colors.point_style, markersize=3)
            if grid.plot_time_min < grid.plot_time_max:
                ax.set_xlim(grid.plot_time_min, grid.plot_time_max)
            else:
                ax.set_xlim(grid.t_min / t_scale, grid.t_max / t_scale)
            ax.set_ylim(magnitude_range)

            # B value with time
            ax = self.figures['b_value_time']['handle'].axes[0]
            ax.cla()
            ax.plot((self._varying_b_time - grid.t_origin) / t_scale, self._varying_b_value, **gui_colors.line_style)
            if grid.plot_time_min < grid.plot_time_max:
                ax.set_xlim(grid.plot_time_min, grid.plot_time_max)
            else:
                ax.set_xlim(grid.t_min / t_scale, grid.t_max / t_scale)
        else:
            ax = self.figures['map_view_catalog']['handle'].axes[0]
            ax.cla()
            ca = ax.plot([], [])
            ax.set_xlim(x_range)
            ax.set_ylim(y_range)

            ax = self.figures['magnitude_distribution']['handle'].axes[0]
            ax.cla()
            ax.semilogy([], [])
            ax.set_xlim([0, 1])
            ax.set_ylim(1, 10)

            # Time series
            ax = self.figures['time_series']['handle'].axes[0]
            ax.cla()
            ax.plot([], [])
            ax.set_xlim(grid.t_min / t_scale, grid.t_max / t_scale)
            ax.set_ylim([0, 1])

            # B value with time
            ax = self.figures['b_value_time']['handle'].axes[0]
            ax.cla()
            ax.plot([], [])
            ax.set_xlim(grid.t_min / t_scale, grid.t_max / t_scale)

        # Setup figure axes labels, titles
        ax = self.figures['map_view_catalog']['handle'].axes[0]
        grid.format_matplotlib_axes(ax, xb=self.easting_slice, yb=self.northing_slice)
        ax.set_title('Map View')

        ax = self.figures['magnitude_distribution']['handle'].axes[0]
        ax.set_xlabel('Magnitude')
        ax.set_ylabel('N')
        ax.set_title('Magnitude Distribution')

        ax = self.figures['time_series']['handle'].axes[0]
        ax.set_xlabel('Time (day)')
        ax.set_ylabel('magnitude')
        ax.set_title('Time Series')

        ax = self.figures['b_value_time']['handle'].axes[0]
        ax.set_xlabel('Time (day)')
        ax.set_ylabel('b-value')
        ax.set_title('b-value Variations')

    def decluster(self, algorithm, return_indices=False, **kwargs):
        """
        Decluster catalog.

        Args:
            algorithm (str): declustering algorithm {'gardner-knopoff', 'nearest-neighbor', 'reasenberg'}
            return_indices (bool): if True, returns indices of background events instead of declustered catalog

        """
        import bruces

        if algorithm == "nearest-neighbor" and "w" not in kwargs:
            kwargs["w"] = self._b_value

        # Filter out events with magnitude lower than Mc
        imc = np.flatnonzero(self._magnitude > self._magnitude_completeness)

        self.logger.info(f"Declustering catalog using algorithm '{algorithm}'")
        cat = bruces.Catalog(
            origin_times=self._epoch[imc].astype("datetime64[ms]"),
            eastings=self._easting[imc],
            northings=self._northing[imc],
            depths=self._depth[imc] * 1.0e-3,
            magnitudes=self._magnitude[imc],
        )
        idx = cat.decluster(algorithm, return_indices=True, **kwargs)
        idx = imc[idx]

        return idx if return_indices else self[idx]

    def decluster_realizations(self, inplace=True):
        """
        Iterate over all declustering realizations for the current slice

        Args:
            inplace (bool): if True, set slice in-place
        """
        return DeclusterMethodIterator(self, inplace)

    def estimate_b_a_value(self, magnitudes):
        """
        Estimate the Gutenberg Richter b, a values

        Args:
            magnitudes (np.ndarray): Magnitude values

        Returns:
            tuple: b, a value
        """
        N = len(magnitudes)
        if N < self.b_value_min_points:
            self.logger.debug(
                f'b-value calculations require a minimum of {self.b_value_min_points} events (actual={N})')
            return self.b_value_default, self.a_value_default

        if self.current_b_value_method not in self.b_value_methods:
            self.logger.warning(f'Requested b-value method was not found: {self.current_b_value_method}')
            return self.b_value_default, self.a_value_default

        try:
            b = self.b_value_methods[self.current_b_value_method](magnitudes)
            if b is None:
                return self.b_value_default, self.a_value_default

            # Check to see if this function returned (b, a) or just b
            if isinstance(b, tuple) and (len(b) == 2):
                return b

            # If only b was returned, then estimate a here
            if len(self._cumulative_frequency):
                bin_center = 0.5 * (self._magnitude_bins[1:] + self._magnitude_bins[:-1])
                tmp = np.log10(self._cumulative_frequency + 1) + b * bin_center
                a = np.amax(tmp)
                return b, a
            else:
                return self.b_value_default, a
        except Exception as e:
            self.logger.error(repr(e))
            self.logger.error(f'b-value calculation ({self.current_b_value_method}) resulted in an exception')
            return self.b_value_default, self.a_value_default

    @property
    def N(self):
        """Length of the catalog"""
        return len(self)

    @property
    def declustering_indices(self):
        """Get the catalog declustering indices"""
        return self._declustering_indices

    def set_slice_type(self, slice_type='global'):
        """
        Set the catalog slice type

        Args:
            slice_type (str): Slice type (global or valid)
        """
        self._global_slice_type = (slice_type == 'global')

    @property
    def _data_slice(self):
        """
        Get the catalog slice indices
        """
        if self._global_slice_type:
            return self._slice_indices
        else:
            return self._slice_indices_valid

    @property
    def _latitude(self):
        """
        Get the catalog latitude array
        """
        return self._points.latitude

    @property
    def latitude_slice(self):
        """
        Get the catalog latitude slice
        """
        return read_only_array(self._latitude[self._data_slice])

    @property
    def _longitude(self):
        """
        Get the catalog longitude array
        """
        return self._points.longitude

    @property
    def longitude_slice(self):
        """
        Get the catalog longitude slice
        """
        return read_only_array(self._longitude[self._data_slice])

    @property
    def depth_slice(self):
        """
        Get the catalog depth slice
        """
        return read_only_array(self._depth[self._data_slice])

    @property
    def _easting(self):
        """
        Get the catalog easting array
        """
        return self._points.easting

    @property
    def easting_slice(self):
        """
        Get the catalog easting slice
        """
        return self._easting[self._data_slice]

    @property
    def _northing(self):
        """
        Get the catalog northing array
        """
        return self._points.northing

    @property
    def northing_slice(self):
        """
        Get the catalog northing slice
        """
        return self._northing[self._data_slice]

    @property
    def epoch_slice(self):
        """
        Get the catalog time slice
        """
        return read_only_array(self._epoch[self._data_slice])

    @property
    def magnitude_slice(self):
        """
        Get the catalog magnitude slice
        """
        return read_only_array(self._magnitude[self._data_slice])

    @property
    def scaled_point_size_slice(self):
        """
        Get the catalog magnitude slice
        """
        return read_only_array(self.get_scaled_point_size(self.magnitude_slice))

    @property
    def cumulative_frequency(self):
        """
        Get the cumulative frequency
        """
        return read_only_array(self._cumulative_frequency)

    @property
    def cumulative_count(self):
        """
        Get the catalog cumulative count
        """
        return read_only_array(self._cumulative_count)

    @property
    def spatial_count(self):
        """
        Get the catalog spatial count
        """
        return read_only_array(self._spatial_count)

    @property
    def spatial_rate(self):
        """
        Get the catalog spatial rate
        """
        return read_only_array(self._spatial_rate)

    @property
    def spatial_density_count(self):
        """
        Get the catalog spatial density count
        """
        return read_only_array(self._spatial_density_count)

    @property
    def spatial_density_rate(self):
        """
        Get the catalog spatial density rate
        """
        return read_only_array(self._spatial_density_rate)

    @property
    def spatial_max_magnitude(self):
        """
        Get the catalog spatial max magnitude
        """
        return read_only_array(self._spatial_max_magnitude)

    @property
    def a_value(self):
        """
        Get the Gutenberg-Richter a-value
        """
        return self._a_value

    @property
    def b_value(self):
        """
        Get the Gutenberg-Richter b-value
        """
        return self._b_value

    @property
    def varying_b(self):
        """
        Get the time varying b-value time vector and value
        """
        return read_only_array(self._varying_b_time), read_only_array(self._varying_b_value)

    @property
    def magnitude_completeness(self):
        """
        Get the estimated catalog magnitude of completeness
        """
        return self._magnitude_completeness


def gutenberg_richter_b_positive(magnitudes):
    """
    Estimate the Gutenberg Richter b value using the b-positive method

    Args:
        magnitudes (np.ndarray): Magnitude values

    Returns:
        float: b value
    """
    differences = []

    for i in range(len(magnitudes[:-1])):
        diff = magnitudes[i + 1] - magnitudes[i]
        differences.append(diff)

    addINT = 0
    differences.insert(0, addINT)

    mdiff_pos = [x for x in differences if x > 0]
    delta_mag_mean = np.mean(mdiff_pos)
    minmagThreshold = 0.1

    # estimating the mle b-value
    tmp_b = np.log10(np.e) / (delta_mag_mean - minmagThreshold)

    return tmp_b


def frequency_magnitude_dist(magnitudes, mbin):
    """
    Computes the frequency-magnitude distribution (frequency_magnitude_dist) for a series of magnitudes.
    Returns discrete & cumulative earthquake frequency_magnitude_dist.

    Args:
        magnitudes (np.ndarray): a list or 1D array of magnitudes
        mbin (float): the precision with which to return magnitudes

    Returns:
        frequency_magnitude_dist: a named tuple consisting of:
            nmags: the number of events
            m_bins: the bin edges for determining earthquake frequency
            dis_mf: the discrete frequency of earthquakes
            cum_mf: the cumulative frequency of earthquakes 
    """
    Nmag = len(magnitudes)
    minmag = np.min(magnitudes)
    maxmag = np.max(magnitudes)
    m_bins = np.arange(minmag, maxmag + 0.8, mbin)    # 0.8 to extend best fit discrete/cumulative lines
    nbins = len(m_bins)
    dis_mf = np.zeros(nbins)
    cum_mf = np.zeros(nbins)
    for i in range(nbins):
        cum_mf[i] = np.sum(magnitudes > m_bins[i] - mbin / 2)
    dis_mf = np.absolute(np.diff(np.concatenate((cum_mf, [0]), axis=0)))
    fmd = collections.namedtuple('frequency_magnitude_dist', ['Nmag', 'm_bins', 'dis_mf', 'cum_mf'])
    return fmd(Nmag, m_bins, dis_mf, cum_mf)


def max_curvature(magnitudes, mbin):
    """
    Calculates the catalogue completeness magnitude by the maximum curvature method (Wyss et al., 1999; Wiemer & Wyss (2000))

    Args:
        magnitudes (np.ndarray): a list or 1D array of magnitudes
        mbin (float): the precision at which to analyze magnitudes

    Returns:
        mc: the maximum-curvature completeness magnitude 
    """
    FMD = frequency_magnitude_dist(magnitudes, mbin)
    mc = FMD.m_bins[FMD.dis_mf == np.max(FMD.dis_mf)] + 0.2    ## common correction
    return mc


def gutenberg_richter_b_a_mle(magnitudes):
    """
    Estimate the Gutenberg Richter a and b value using the maximum likelihood method 

    Args:
        magnitudes (ndarray): 1D array of magnitudes above mc

    Returns:
        b-value (float)
        a-value (float)
    """

    tmp_mc = max_curvature(magnitudes, mbin=0.1)
    mc = tmp_mc[0]
    mags_above_mc = magnitudes[magnitudes > mc]
    if not len(mags_above_mc):
        return

    # estimating the mle b-value
    tmp_b = np.log10(np.e) / (np.mean(mags_above_mc) - (mc + 0.05))
    tmp_a = np.log10(len(mags_above_mc)) + tmp_b * (mc + 0.05)
    return tmp_b, tmp_a


class DeclusterMethodIterator():

    def __init__(self, catalog: SeismicCatalog, inplace: bool):
        self.catalog = catalog
        self.inplace = inplace
        self.decluster_ordered = sorted(catalog.declustering_indices.keys())
        if not self.decluster_ordered:
            self.decluster_ordered = [None]
        self.n = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.n < len(self.decluster_ordered):
            decluster_method = self.decluster_ordered[self.n]
            self.catalog.finalize_slice(type_=decluster_method, inplace=self.inplace)
            self.n += 1
            if not decluster_method:
                decluster_method = 'full'
            return decluster_method, self.catalog

        else:
            raise StopIteration
