# ------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2020-, Lawrence Livermore National Security, LLC
# All rights reserved
#
# See top level LICENSE, COPYRIGHT, CONTRIBUTORS, NOTICE, and ACKNOWLEDGEMENTS files for details.
# ------------------------------------------------------------------------------------------------
"""
forecast_manager.py
-----------------------
"""

from orion.managers import manager_base
from orion.forecast_models.target_forecast_models import list_ as forecast_models_list
from orion.utilities import plot_tools, statistical_methods, other
from orion.utilities.plot_config import gui_colors
from orion import _frontend
import numpy as np
from sklearn import linear_model
from orion.managers.manager_base import recursive
from PIL import ImageColor


class ForecastManager(manager_base.ManagerBase):
    """
    A class for managing the various seismic
    forecasting methods generated via ORION

    Attributes:
        forecast_data_start (str): The start time (mm/dd/yyyy) for the analysis (empty = beginning of catalog)
        forecast_data_stop (str): The end time (mm/dd/yyyy) for the analysis (empty = end of catalog)
        percent_ensemble_train (int): The percentage of the catalog to reserve for training the ensemble forecast
        percent_ensemble_test (int): The percentage of the catalog to reserve for testing the ensemble forecast
        train_split_epoch (float): Timestamp at the end of the training segment
        forecast_split_epoch (float): Timestamp at the end of the testing segment
        forecast_end_epoch (float): Timestamp at the end of the forecast
        forecast_length (float): The length of the requested forecast (years)
        time_range (list): The current time slice under considration
        forecast_time (list): A list of time vectors produced by the forcast models
        forecast_cumulative_event_count (list): A list of forecast result vectors produced by the forcast models
    """

    def set_class_options(self, **kwargs):
        """
        Forecast manager initialization

        Setup empty data holders, configuration options,
        data sources, and gui configuration
        """

        # Set the shorthand name
        self.short_name = 'Forecast Models'

        # Add the available forecast models
        self.child_classes += forecast_models_list

        # Forecast options
        self.forecast_options_list = ['Use Grid', 'ML Training Style']
        self.current_forecast_option = self.forecast_options_list[0]

        # Ensemble calculation
        self.percent_model_train = 33
        self.percent_ensemble_train = 33
        self.percent_ensemble_test = 33
        self.train_split_epoch = 0.0
        self.forecast_split_epoch = 0.0
        self.forecast_end_epoch = 0.0

        # Exceedance plots
        self.exceedance_header = '\nMagnitude Exceedance Plots'
        self.exceedance_plot_time_input = 30.0
        self.exceedance_plot_time = 30.0
        self.exceedance_dial_plot_magnitude = 3.0
        self.exceedance_bar_plot_min_magnitude = 0.0
        self.exceedance_bar_plot_max_magnitude = 3.0
        self.exceedance_bar_plot_magnitude_number = 4
        self.exceedance_color_yellow_threshold = 30.0
        self.exceedance_color_red_threshold = 60.0

    def set_user_options(self, **kwargs):
        # Processing
        self.use_multiprocessing = 0

        # Plots
        self.useCumulativePlots = 1
        self.use_spatial_cumulative_plots = 0
        self.available_plot_methods = ['All', 'Average', 'Range']
        self.plot_method = self.available_plot_methods[0]

    def set_data(self, **kwargs):
        """
        Setup data holders
        """
        # Forecast objects
        self.current_forecasts = {}
        self.time_range = []
        self.forecast_time = np.zeros(0)
        self.forecast_time_bins = np.zeros(0)
        self.forecast_cumulative_event_count = np.zeros(0)
        self.spatial_forecast_count = np.zeros((0, 0, 0))
        self.spatial_forecast_density_count = np.zeros((0, 0, 0))
        self.spatial_forecast_density_rate = np.zeros((0, 0, 0))
        self.spatial_forecast_exceedance = np.zeros((0, 0))
        self.exceedance_dial_plot_probability = 0.0
        self.exceedance_bar_plot_magnitudes = np.zeros(0)
        self.exceedance_bar_plot_probabilities = np.zeros(0)
        self._forecasts_running = False
        # self.delta_pressure = 1.0

    def set_gui_options(self, **kwargs):
        """
        Setup interface options
        """
        # Add figure handles
        self.set_visibility_all()

        fig_size_a = (5, 3)
        fig_size_b = (8, 3)
        if _frontend == 'strive':
            fig_size_a = (45, 45)
            fig_size_b = (90, 60)

        exceedance_help = 'This figure shows the estimated probability that a future seismic event will have a magnitude greater than or equal to the indicated values.'
        dial_help = 'This gauge shows the estimated probability that a future seismic event will exceed the given magnitude.'
        forecast_help = 'This figure shows the observed number of seismic events over time, forecasted values by model, and the ensemble forecast.'

        self.figures = {
            'exceedance_month': {
                'position': [0, 0],
                'size': fig_size_a,
                'static': True,
                'target': 'exceedance_bar',
                'help_text': exceedance_help
            },
            'dial_plot': {
                'position': [0, 1],
                'size': fig_size_a,
                'static': True,
                'target': 'exceedance_dial',
                'help_text': dial_help
            },
            'event_count': {
                'position': [1, 0],
                'size': fig_size_b,
                'columnspan': 4,
                'layer_config': True,
                'target': 'forecast_lines',
                'help_text': forecast_help,
                'widgets': ['plot_method'],
            }
        }

        # Add Gui elements
        self.gui_elements['useCumulativePlots'] = {
            'element_type': 'check',
            'label': 'Cumulative plot (temporal)',
            'position': [0, 0]
        }
        self.gui_elements['use_spatial_cumulative_plots'] = {
            'element_type': 'check',
            'label': 'Cumulative plot (spatial)',
            'position': [1, 0]
        }

        self.gui_elements['plot_method'] = {
            'element_type': 'dropdown',
            'label': 'Plot type',
            'position': [2, 0],
            'values': self.available_plot_methods
        }

        self.gui_elements['exceedance_header'] = {'element_type': 'text', 'position': [4, 0]}

        self.gui_elements['exceedance_plot_time_input'] = {
            'element_type': 'entry',
            'label': 'Time range',
            'units': '(days)',
            'position': [5, 0],
            'units_span': 4
        }

        self.gui_elements['exceedance_dial_plot_magnitude'] = {
            'element_type': 'entry',
            'label': 'Dial magnitude',
            'position': [6, 0]
        }

        self.gui_elements['exceedance_bar_plot_min_magnitude'] = {
            'element_type': 'entry',
            'label': 'Bar plot bins',
            'position': [7, 0]
        }

        self.gui_elements['exceedance_bar_plot_max_magnitude'] = {'element_type': 'entry', 'position': [7, 1]}

        self.gui_elements['exceedance_bar_plot_magnitude_number'] = {
            'element_type': 'entry',
            'units': '(min, max, N)',
            'position': [7, 2]
        }

        self.gui_elements['exceedance_color_yellow_threshold'] = {
            'element_type': 'entry',
            'label': 'Color thresholds',
            'position': [8, 0]
        }

        self.gui_elements['exceedance_color_red_threshold'] = {
            'element_type': 'entry',
            'units': '(yellow, red, %)',
            'position': [8, 1],
            'units_span': 4
        }

        self.gui_elements['use_multiprocessing'] = {
            'element_type': 'check',
            'label': 'Parallel calculation',
            'position': [9, 0],
            'user': True
        }

    @recursive
    def process_inputs(self):
        # Setup bins
        self.exceedance_bar_plot_magnitudes = np.linspace(self.exceedance_bar_plot_max_magnitude,
                                                          self.exceedance_bar_plot_min_magnitude,
                                                          self.exceedance_bar_plot_magnitude_number)
        self.exceedance_bar_plot_probabilities = np.zeros(self.exceedance_bar_plot_magnitude_number)
        self.exceedance_dial_plot_probability = 0.0
        self.exceedance_plot_time = self.exceedance_plot_time_input * 60 * 60 * 24.0

    def get_alerts(self):
        """
        Get alert messages for the seismic forecast

        Returns:
            The warning and alert message strings
        """
        warning_msg = ''
        alert_msg = ''
        M = self.exceedance_dial_plot_magnitude
        t = self.exceedance_plot_time_input
        r = self.exceedance_color_red_threshold
        y = self.exceedance_color_yellow_threshold

        if self.exceedance_dial_plot_probability > r:
            alert_msg = f'The probablility of a seismic event exceeding Magnitude {M} event is greater than {r}% for the next {t} days!  Please see the Operational Management tab for additional details.'

        elif self.exceedance_dial_plot_probability > y:
            warning_msg = f'The probablility of a seismic event exceeding Magnitude {M} event is greater than {y}% for the next {t} days!  Please see the Operational Management tab for additional details.'

        return warning_msg, alert_msg

    def run(self, grid, seismic_catalog, pressure_manager, wells, geologic_model, root_manager):
        """
        Chooses the forecast manager style.

        Args:
            grid (orion.managers.grid_manager.GridManager): The Orion grid manager
            seismic_catalog (orion.managers.seismic_catalog.SeismicCatalog): The current seismic catalog
            pressure_manager (orion.managers.pressure_manager.PressureManager): The pressure manager
            wells (orion.managers.well_manager.WellManager): The well data
            geologic_model (orion.managers.geologic_model_manager.GeologicModelManager): The current geological model
            root_manager (orion.managers.orion_manager.OrionManager): The root manager
        """
        filtered_catalog = seismic_catalog.set_slice(inplace=False)
        filtered_catalog.calculate_spatial_parameters(grid)
        filtered_catalog.set_slice_type('local')

        if (self.current_forecast_option == 'Use Grid'):
            self.run_grid_style(grid, filtered_catalog, pressure_manager, wells, geologic_model, root_manager)
        elif (self.current_forecast_option == 'ML Training Style'):
            self.run_ml_style(grid, filtered_catalog, pressure_manager, wells, geologic_model)
        else:
            self.logger.error(f'Unrecognized forecast manager method: {self.current_forecast_option}')

    def run_grid_style(self, grid, seismic_catalog, pressure_manager, wells, geologic_model, root_manager):
        """
        Runs the forecast manager on the grid, expects results to be gridded

        Args:
            grid (orion.managers.grid_manager.GridManager): The Orion grid manager
            seismic_catalog (orion.managers.seismic_catalog.SeismicCatalog): The current seismic catalog
            pressure_manager (orion.managers.pressure_manager.PressureManager): The pressure manager
            wells (orion.managers.well_manager.WellManager): The well data
            geologic_model (orion.managers.geologic_model_manager.GeologicModelManager): The current geological model
            root_manager (orion.managers.orion_manager.OrionManager): The root manager
        """
        self.logger.info('Running manager in grid-style...')
        rolling_update = (_frontend == 'strive')
        self.generate_model_forecasts(grid,
                                      seismic_catalog,
                                      pressure_manager,
                                      wells,
                                      geologic_model,
                                      root_manager,
                                      rolling_update=True)

        # Generate the ensemble forecast
        self.logger.info('Calculating ensemble forecast...')
        self.estimate_weights_linear_regression(seismic_catalog)
        self.calculate_weighted_cumulative_forecast(grid)

    def estimate_magnitude_exceedance_probability(self, grid, seismic_catalog):
        tmp = self.estimate_magnitude_exceedance_probability_sub(grid, seismic_catalog,
                                                                 self.forecast_cumulative_event_count,
                                                                 self.spatial_forecast_count)
        self.exceedance_bar_plot_probabilities = tmp[0]
        self.exceedance_dial_plot_probability = tmp[1]
        self.spatial_forecast_exceedance = tmp[2]

    def estimate_magnitude_exceedance_probability_sub(self, grid, seismic_catalog, forecast_cumulative_event_count,
                                                      spatial_forecast_count):
        """
        Estimates the probability events will exceed a given user-defined magnitude (self.exceedance_dial_plot_magnitude,
        self.exceedance_bar_plot_magnitudes) during the next time period (self.exceedance_plot_time_input).
        The calculation is performed for the entire area, and for individual grid cells.

        Args:
            grid (orion.managers.grid_manager.GridManager): The Orion grid manager
            seismic_catalog (orion.managers.seismic_catalog.SeismicCatalog): The current seismic catalog
        """
        # Note: Should we be considering all of catalog here,
        #       or a user-defined past period?  This can certainly effect the b-value
        # Setup default values
        err = [np.zeros(len(self.exceedance_bar_plot_magnitudes)), 0.0, np.zeros((len(grid.x), len(grid.y)))]

        # Global values
        seismic_catalog.reset_slice()
        if not len(forecast_cumulative_event_count):
            return err

        # Set the active slice
        ts = (grid.snapshot_time * 60 * 60 * 24.0)
        Ia = np.argmin(abs(ts + grid.t_origin - self.forecast_time))

        # Target sizes
        Nx = len(grid.x)
        Ny = len(grid.y)
        M = np.shape(spatial_forecast_count)
        spatial_forecast_exceedance = np.zeros((Nx, Ny))

        # Estimate exceedance
        exceedance_bar_plot_probabilities = statistical_methods.poisson_probability(
            self.forecast_time, forecast_cumulative_event_count, seismic_catalog.b_value,
            seismic_catalog.magnitude_completeness, self.exceedance_bar_plot_magnitudes, self.exceedance_plot_time)

        if np.isnan(seismic_catalog.magnitude_completeness):
            self.logger.debug("There are not enough active events to estimate exceedance")
            return err

        tmp = statistical_methods.poisson_probability(self.forecast_time, forecast_cumulative_event_count,
                                                      seismic_catalog.b_value, seismic_catalog.magnitude_completeness,
                                                      np.array([self.exceedance_dial_plot_magnitude]),
                                                      self.exceedance_plot_time)
        exceedance_dial_plot_probability = tmp[0]

        # Grid based values
        if (Ia == 0):
            if len(spatial_forecast_count) > 1:
                Ia = -1
            else:
                return err

        t = self.forecast_time[:Ia]
        if ((M[1] == Nx) & (M[2] == Ny)):
            spatial_exceedance_magnitude = np.array([self.exceedance_dial_plot_magnitude])
            for ii in range(Nx):
                for jj in range(Ny):
                    c = spatial_forecast_count[:Ia, ii, jj]
                    tmp = statistical_methods.poisson_probability(t, c, seismic_catalog.b_value,
                                                                  seismic_catalog.magnitude_completeness,
                                                                  spatial_exceedance_magnitude,
                                                                  self.exceedance_plot_time)
                    spatial_forecast_exceedance[ii, jj] = tmp[0]

        return exceedance_bar_plot_probabilities, exceedance_dial_plot_probability, spatial_forecast_exceedance

    def calculate_weighted_cumulative_forecast(self, grid):
        """
        Calculate the weighted cumulative spatial and temporal forecasts

        Args:
            grid (orion.managers.grid_manager.GridManager): The Orion grid manager
        """

        Nx, Ny, Nz, Nt = grid.shape
        self.forecast_cumulative_event_count = np.zeros(Nt)
        self.spatial_forecast_count = np.zeros((Nt, Nx, Ny))

        # for fa in self.current_forecasts.values():
        for k, fa in self.current_forecasts.items():
            for fb in fa.values():
                for fc in fb.values():
                    Ia = np.where(~np.isnan(fc['temporal']))
                    Ib = np.where(~np.isnan(fc['spatial']))
                    self.forecast_cumulative_event_count[Ia] += fc['temporal'][Ia] * fc['weight']
                    self.spatial_forecast_count[Ib] += fc['spatial'][Ib] * fc['weight']

        # Calculate event/density estimates (N/m^2, N/s*m^2)
        self.spatial_forecast_density_count = self.spatial_forecast_count / np.expand_dims(grid.areas, 0)
        if (Nt > 1):
            self.spatial_forecast_density_rate = other.derivative(self.spatial_forecast_density_count, grid.t, axis=0)
        else:
            self.spatial_forecast_density_rate = self.spatial_forecast_density_count / grid.dt

    def generate_model_forecasts(self,
                                 grid,
                                 seismic_catalog,
                                 pressure_manager,
                                 wells,
                                 geologic_model,
                                 root_manager,
                                 rolling_update=False):
        """
        Generate forecasts for the current time slice

        Args:
            grid (orion.managers.grid_manager.GridManager): The Orion grid manager
            seismic_catalog (orion.managers.seismic_catalog.SeismicCatalog): The current seismic catalog
            pressure_manager (orion.managers.pressure_manager.PressureManager): The pressure manager
            wells (orion.managers.well_manager.WellManager): The well data
            geologic_model (orion.managers.geologic_model_manager.GeologicModelManager): The current geological model
            forecast_length (float): The length of the requested forecast (seconds)
            delta_pressure: instance of a pressure model 
            root_manager (orion.managers.orion_manager.OrionManager): The root manager
        """
        self.current_forecasts = {}
        self.forecast_time = grid.t + grid.t_origin
        self.forecast_time_bins = grid.t_nodes + grid.t_origin
        if not seismic_catalog.N:
            return

        for ka, kb, kc, temporal_forecast, spatial_forcast in self.generate_model_forecasts_sub(grid,
                                                                                                seismic_catalog,
                                                                                                pressure_manager,
                                                                                                wells,
                                                                                                geologic_model,
                                                                                                root_manager,
                                                                                                progress_start=50,
                                                                                                progress_end=75):
            if ka not in self.current_forecasts:
                self.current_forecasts[ka] = {}

            if kb not in self.current_forecasts[ka]:
                self.current_forecasts[ka][kb] = {}

            self.current_forecasts[ka][kb][kc] = {
                'temporal': temporal_forecast,
                'spatial': spatial_forcast,
                'weight': 1.0
            }
            if rolling_update:
                self.estimate_weights_linear_regression(seismic_catalog)
                self.calculate_weighted_cumulative_forecast(grid)
                self.estimate_magnitude_exceedance_probability(grid, seismic_catalog)

    def generate_model_forecasts_sub(self,
                                     grid,
                                     seismic_catalog,
                                     pressure_manager,
                                     wells,
                                     geologic_model,
                                     root_manager=None,
                                     delta_pressure=None,
                                     progress_start=0,
                                     progress_end=100):

        target_pressure = {}
        for kb, pressure in pressure_manager.children.items():
            if delta_pressure is not None:
                target_pressure[kb] = pressure + delta_pressure
            else:
                target_pressure[kb] = pressure

        # Generate forecasts for active models
        self._forecasts_running = True
        active_children = [k for k in self.children.keys() if self.children[k].active]
        Nforecast = len(active_children)
        for ii, ka in enumerate(active_children):
            if root_manager is not None:
                root_manager.run_status = self.children[ka].short_name
                root_manager.run_percent = progress_start + ((progress_end - progress_start) * ii // Nforecast)
            for kb, pressure in target_pressure.items():
                for kc, catalog in seismic_catalog.decluster_realizations():
                    try:
                        tmp = self.children[ka].generate_forecast(grid, catalog, pressure, wells, geologic_model)
                        # Check that the result is the correct length
                        if tmp is not None:
                            min_val = np.nanmin(tmp[0])
                            max_val = np.nanmax(tmp[0])
                            if (min_val < 0.0) or (max_val > 1e9):
                                self.logger.error(
                                    f'Detected out of range values: model={ka}, pressure realization={kb}, decluster realization={kc}'
                                )
                                self.logger.error(f'min_value={min_val}, max_value={max_val}')
                                tmp[0][:] = 0.0
                                tmp[1][:] = 0.0

                            yield ka, kb, kc, tmp[0], tmp[1]

                    except Exception as e:
                        self.logger.error(
                            f'Forecast calculation failed: model={ka}, pressure realization={kb}, decluster realization={kc}'
                        )
                        self.logger.error(f"Exception: {repr(e)}")
        self._forecasts_running = False

    def parse_timing_requests(self):
        """
        Parse the timing requests for forecast training
        """
        # Set the time range
        if self.forecast_data_start:
            self.time_range[0] = float(self.forecast_data_start) * 60.0 * 60.0 * 24.0

        if self.forecast_data_stop:
            self.time_range[1] = float(self.forecast_data_stop) * 60.0 * 60.0 * 24.0

        # Setup model splitting
        self.percent_ensemble_train = int(self.percent_ensemble_train)
        self.percent_ensemble_test = int(self.percent_ensemble_test)

        # Check to make sure values are reasonable
        if (self.percent_ensemble_train < 0):
            self.percent_ensemble_train = 0
            self.logger.warning('Warning: percent_ensemble_train must be >= 0, setting value to 0...')

        if (self.percent_ensemble_test < 0):
            self.percent_ensemble_test = 0
            self.logger.warning('Warning: percent_ensemble_test must be >= 0, setting value to 0...')

        ensemble_percent = self.percent_ensemble_train + self.percent_ensemble_test
        if (ensemble_percent > 100):
            self.percent_ensemble_train = int(self.percent_ensemble_train * 0.01 / ensemble_percent)
            self.percent_ensemble_test = int(self.percent_ensemble_test * 0.01 / ensemble_percent)
            self.logger.warning(
                'Warning: percent_ensemble_train + percent_ensemble_test must be <= 100, rescaling values...')

        self.percent_model_train = 100 - self.percent_ensemble_train - self.percent_ensemble_test
        catalog_length = self.time_range[1] - self.time_range[0]
        self.train_split_epoch = self.time_range[0] + catalog_length * 0.01 * self.percent_model_train
        dc = catalog_length * 0.01 * (self.percent_model_train + self.percent_ensemble_train)
        self.forecast_split_epoch = self.time_range[0] + dc
        self.forecast_end_epoch = self.time_range[1] + self.forecast_length * 60.0 * 60.0 * 24.0 * 365.25

    def estimate_weights_linear_regression(self, seismic_catalog):
        """
        Estimate decision tree linear weights

        Args:
            seismic_catalog (orion.managers.seismic_catalog.SeismicCatalog): The current seismic catalog
        """
        # Flatten the forecast results
        flat_keys = []
        flat_forecasts = []
        for ka, fa in self.current_forecasts.items():
            for kb, fb in fa.items():
                for kc, fc in fb.items():
                    flat_keys.append([ka, kb, kc])
                    flat_forecasts.append(fc['temporal'])

        # Use sklearn to perform a multiple linear regression
        clf = linear_model.LinearRegression(positive=True)
        catalog_ne = seismic_catalog.cumulative_count

        if len(catalog_ne) and len(flat_forecasts):
            m = np.transpose(np.array(flat_forecasts))
            m[~np.isfinite(m)] = 0
            clf.fit(m, catalog_ne)
            for ii, k in enumerate(flat_keys):
                self.current_forecasts[k[0]][k[1]][k[2]]['weight'] = clf.coef_[ii]

    def run_ml_style(self, grid, seismic_catalog, pressure_manager, wells, geologic_model, root_manager):
        """
        Runs the forecast manager and generates an ensemble forecast

        Args:
            grid (orion.managers.grid_manager.GridManager): The Orion grid manager
            seismic_catalog (orion.managers.seismic_catalog.SeismicCatalog): The current seismic catalog
            pressure_manager (orion.managers.pressure_manager.PressureManager): The pressure manager
            wells (orion.managers.well_manager.WellManager): The well data
            geologic_model (orion.managers.geologic_model_manager.GeologicModelManager): The current geological model
            root_manager (orion.managers.orion_manager.OrionManager): The root manager
        """
        # Note: these need to be updated to work:
        self.logger.warning('ML style forecasts not enabled')
        return

        # Setup timing
        self.time_range = [-1.0, 0.0]
        if seismic_catalog:
            self.time_range[0] = np.amin(seismic_catalog.epoch_slice) - grid.t_origin
        self.parse_timing_requests()

        # Set the catalog slices for individual model training
        train_slice = [self.time_range[0], self.train_split_epoch]
        train_slice = np.array(train_slice) + grid.t_origin
        seismic_catalog.set_slice(time_range=train_slice, seismic_characteristics_dt=grid.dt * 60 * 60 * 24)

        # Generate the model forecasts for the training data
        self.logger.info('Training ensemble forecast...')
        self.generate_model_forecasts(grid, seismic_catalog, pressure_manager, wells, geologic_model, root_manager)

        # Determine the weights using a simple linear regression
        # Set the catalog slices for individual ensemble training, forecasting
        train_slice[1] = self.forecast_split_epoch + grid.t_origin
        seismic_catalog.set_slice(time_range=train_slice)
        self.estimate_weights_linear_regression(seismic_catalog)

        # Generate the ensemble forecast
        self.logger.info('Calculating ensemble forecast...')
        seismic_catalog.reset_slice()
        self.generate_model_forecasts(grid, seismic_catalog, pressure_manager, geologic_model, root_manager)
        self.calculate_weighted_cumulative_forecast(grid)

        # Reset the global time slice
        self.logger.info('Done!')

    def check_plot_style(self, t, x):
        """
        Handle user requests for cumulative/rate plots

        Args:
            t (np.ndarray): Times
            x (np.ndarray): Cumulative values

        Returns:
            np.ndarray: Cumulative or rate-based values
        """
        if not len(x):
            return x
        elif self.useCumulativePlots:
            return x
        else:
            ts = 60.0 * 60.0 * 24.0
            return other.derivative(x, t) * ts

    def get_plot_data(self, projection):
        rate_scale = 1e6 * 60.0 * 60.0 * 24.0 * 365.25
        rate_units = '#/year*km^2'
        target_rate_field = self.spatial_forecast_density_rate * rate_scale
        exceedance_tiled = np.tile(np.expand_dims(self.spatial_forecast_exceedance, 0), (len(target_rate_field), 1, 1))
        t_exc = self.exceedance_plot_time / (60 * 60 * 24.0)
        exceedance_str = f'%, M>{self.exceedance_dial_plot_magnitude}, t<{t_exc} days'
        color_thresholds = {
            'yellow': self.exceedance_color_yellow_threshold,
            'red': self.exceedance_color_red_threshold
        }

        return {
            'spatial_forecast_rate': target_rate_field,
            'spatial_forecast_rate_units': rate_units,
            'spatial_forecast_count': self.spatial_forecast_density_count,
            'spatial_forecast_count_units': '#/km^2',
            'spatial_exceedance_probability': exceedance_tiled,
            'spatial_exceedance_probability_units': exceedance_str,
            'temporal_forecast_count': self.current_forecasts,
            'temporal_exceedance_probability': self.exceedance_dial_plot_probability,
            'temporal_forecast_ensemble': self.forecast_cumulative_event_count,
            'exceedance_color_thresholds': color_thresholds,
            'exceedance_time': self.exceedance_plot_time_input,
            'exceedance_dial_magnitude': self.exceedance_dial_plot_magnitude,
            'exceedance_dial_probability': self.exceedance_dial_plot_probability,
            'exceedance_bar_magnitudes': self.exceedance_bar_plot_magnitudes,
            'exceedance_bar_probability': self.exceedance_bar_plot_probabilities
        }

    def exceedance_bar(self, plot_data):
        exceedance_title = f'Exceedance probability within {self.exceedance_plot_time_input:1.1f} days'
        c = []
        for p in self.exceedance_bar_plot_probabilities:
            if p < self.exceedance_color_yellow_threshold:
                c.append('green')
            elif p < self.exceedance_color_red_threshold:
                c.append('yellow')
            else:
                c.append('red')

        layers = {
            'exceedance': {
                'y': self.exceedance_bar_plot_magnitudes,
                'x': self.exceedance_bar_plot_probabilities,
                'c': c,
                'type': 'bar',
                'orientation': 'h'
            }
        }
        axes = {'x': exceedance_title, 'y': 'Magnitude'}
        if self._forecasts_running:
            axes['title'] = ' (Note: Some forecast models are still running)'

        return layers, axes

    def exceedance_dial(self, plot_data):
        dial_label = f'p(magnitude > {self.exceedance_dial_plot_magnitude:1.1f}) within {self.exceedance_plot_time_input:1.1f} days'

        steps = [{
            'range': [0, self.exceedance_color_yellow_threshold],
            'color': 'green'
        }, {
            'range': [self.exceedance_color_yellow_threshold, self.exceedance_color_red_threshold],
            'color': 'yellow'
        }, {
            'range': [self.exceedance_color_red_threshold, 100.0],
            'color': 'red'
        }]

        layers = {
            'exceedance': {
                'x': self.exceedance_dial_plot_probability,
                'c': 'gray',
                'type': 'dial',
                'steps': steps
            }
        }
        axes = {'title': dial_label, 'x_range': [0, 100]}
        return layers, axes

    def forecast_lines(self, plot_data):
        seismic_plot_data = plot_data['Seismic Catalog']
        grid = plot_data['General']
        time_scale = 1.0 / (60.0 * 60.0 * 24.0)
        t = (self.forecast_time - grid['t_origin']) * time_scale

        catalog_ne = seismic_plot_data.get('catalog_cumulative_event_count', np.zeros(0))
        catalog_ne_t = grid['t'] * time_scale

        plotly_colors = [
            '#636EFA', '#EF553B', '#00CC96', '#AB63FA', '#FFA15A', '#19D3F3', '#FF6692', '#B6E880', '#FF97FF', '#FECB52'
        ]
        catalog_rgb = ImageColor.getcolor(plotly_colors[0], "RGB")
        ensemble_rgb = ImageColor.getcolor(plotly_colors[1], "RGB")

        layers = {
            'Catalog': {
                'x': catalog_ne_t,
                'y': catalog_ne,
                'type': 'line',
                'color': 'rgba({},{},{},1.0)'.format(*catalog_rgb)
            },
            'Ensemble': {
                'x': t,
                'y': self.forecast_cumulative_event_count,
                'type': 'line',
                'color': 'rgba({},{},{},1.0)'.format(*ensemble_rgb)
            }
        }

        ensemble_type = 'all'
        if (self.plot_method == 'Average'):
            ensemble_type = 'average'
        elif (self.plot_method == 'Range'):
            ensemble_type = 'range'

        for plot_id, (ka, forecast_model) in enumerate(self.children.items()):
            main_label = forecast_model.short_name
            main_hex = plotly_colors[plot_id + 2 % len(plotly_colors)]
            main_rgb = ImageColor.getcolor(main_hex, "RGB")
            rgb_solid = 'rgba({},{},{},1.0)'.format(*main_rgb)

            if ka in self.current_forecasts:
                realizations = {}
                for kb, fb in self.current_forecasts[ka].items():
                    for kc, fc in fb.items():
                        realizations[f'{main_label}_{kb}_{kc}'] = fc['temporal']

                layers[main_label] = {
                    'x': t,
                    'y': realizations,
                    'type': 'ensemble',
                    'color': rgb_solid,
                    'ensemble_type': ensemble_type
                }

        axes = {'x': 'Time (days)', 'y': 'Event Count'}
        return layers, axes

    def generate_plots(self, **kwargs):
        # Collect data
        self.logger.debug('Generating forecast manager plots')
        grid = kwargs.get('grid')
        seismic_catalog = kwargs.get('seismic_catalog')

        # Setup timing
        time_scale = 1.0 / (60.0 * 60.0 * 24.0)

        # Plot colors
        color_thresholds = [self.exceedance_color_yellow_threshold, self.exceedance_color_red_threshold]

        # Bar plot
        ax = self.figures['exceedance_month']['handle'].axes[0]
        ax.cla()
        plot_tools.exceedance_bar_plot(ax,
                                       self.exceedance_bar_plot_magnitudes,
                                       self.exceedance_bar_plot_probabilities,
                                       color_lims=color_thresholds)
        ax.set_title(
            f'Probability of a seismic event exceeding\n target magnitude within {self.exceedance_plot_time_input:1.1f} days'
        )

        # Dial plot
        ax = self.figures['dial_plot']['handle'].axes[0]
        ax.cla()
        plot_tools.exceedance_dial_plot(ax, self.exceedance_dial_plot_probability, color_lims=color_thresholds)
        ax.set_title(
            f'p(magnitude > {self.exceedance_dial_plot_magnitude:1.1f}) within {self.exceedance_plot_time_input:1.1f} days'
        )

        # Event count plot
        ax = self.figures['event_count']['handle'].axes[0]
        ax.cla()
        max_number_events = 1
        catalog_ne_t = [0]

        if seismic_catalog:
            seismic_catalog.set_slice(magnitude_range=[seismic_catalog.magnitude_completeness, 100])

            # Check the time range
            if not len(self.time_range):
                self.time_range = [-1.0, 0.0]
                if seismic_catalog:
                    self.time_range[0] = np.amin(seismic_catalog.epoch_slice) - grid.t_origin

            # Get the catalog event rate
            catalog_ne_t = grid.t * time_scale
            catalog_ne = seismic_catalog.cumulative_count
            catalog_ne = self.check_plot_style(catalog_ne_t, catalog_ne)

            # Plot the saved catalog event rate
            max_number_events = np.amax(catalog_ne)
            line_style = gui_colors.periodic_line_style(0)
            line_style['linewidth'] = 2
            ax.plot(catalog_ne_t, catalog_ne, label='Catalog', **line_style)
        else:
            # Add an empty plot for figure control
            ax.plot([], [], 'k', linewidth=4, label='Catalog')

        # Joint forecast
        tf = (self.forecast_time - grid.t_origin) * time_scale
        nf = self.check_plot_style(self.forecast_time, self.forecast_cumulative_event_count)
        if len(nf):
            max_number_events = max(max_number_events, np.amax(nf))
        line_style = gui_colors.periodic_line_style(1)
        line_style['linewidth'] = 2
        ax.plot(tf, nf, label='Ensemble', **line_style)

        # Child forecasts
        plot_id = 2
        for ka, forecast_model in self.children.items():
            main_label = forecast_model.short_name
            if ka in self.current_forecasts:
                realizations = []
                for kb, fb in self.current_forecasts[ka].items():
                    for kc, fc in fb.items():
                        nf = self.check_plot_style(self.forecast_time, fc['temporal'])
                        max_number_events = max(max_number_events, np.amax(nf))
                        if (self.plot_method == 'All'):
                            realization_label = main_label
                            if (len(self.current_forecasts[ka]) > 1) or (len(fb) > 1):
                                realization_label = f'{main_label}_{kb}_{kc}'
                            line_style = gui_colors.periodic_line_style(plot_id)
                            plot_id += 1
                            ax.plot(tf, nf, label=realization_label, **line_style)
                        else:
                            realizations.append(nf)

                if (self.plot_method == 'Average'):
                    nf = np.nanmean(realizations, axis=0)
                    line_style = gui_colors.periodic_line_style(plot_id)
                    plot_id += 1
                    ax.plot(tf, nf, label=main_label, **line_style)

                elif (self.plot_method == 'Range'):
                    nf_min = np.nanmin(realizations, axis=0)
                    nf_max = np.nanmax(realizations, axis=0)
                    c = gui_colors.periodic_color_style(plot_id)['color']
                    plot_id += 1
                    ax.fill_between(tf, nf_min, nf_max, label=main_label, color=c)

            else:
                line_style = gui_colors.periodic_line_style(plot_id)
                plot_id += 1
                ax.plot([], [], label=main_label, **line_style)

        # Training/testing indicators
        if (self.current_forecast_option == 'ML Training Style'):
            ta = self.train_split_epoch * time_scale
            tb = self.forecast_split_epoch * time_scale
            tc = catalog_ne_t[-1] * time_scale
            ax.plot([ta, ta], [0, max_number_events], 'k--')
            ax.plot([tb, tb], [0, max_number_events], 'k--')
            ax.plot([tc, tc], [0, max_number_events], 'k--')

            if (tb > ta):
                ax.text(0.5 * (ta + tb),
                        0.9 * max_number_events,
                        '(ensemble training)',
                        horizontalalignment='center',
                        verticalalignment='center')
            if (tc > tb):
                ax.text(0.5 * (tb + tc),
                        0.9 * max_number_events,
                        '(ensemble testing)',
                        horizontalalignment='center',
                        verticalalignment='center')

        # Finalize the figure axes
        ax.legend(loc=2, ncol=2)
        if (max_number_events > 0):
            ax.set_ylim([0, max_number_events])
        if (grid.plot_time_min < grid.plot_time_max):
            ax.set_xlim([grid.plot_time_min, grid.plot_time_max])
        else:
            ax.set_xlim([grid.t_min * time_scale, grid.t_max * time_scale])
        ax.set_xlabel('Time (day)')
        if self.useCumulativePlots:
            ax.set_ylabel('Cumulative Event Count')
        else:
            ax.set_ylabel('Event Rate (#/day)')
        ax.set_title('Event Count')
