# ------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2020-, Lawrence Livermore National Security, LLC
# All rights reserved
#
# See top level LICENSE, COPYRIGHT, CONTRIBUTORS, NOTICE, and ACKNOWLEDGEMENTS files for details.
# ------------------------------------------------------------------------------------------------
"""
well_manager.py
-----------------------
"""

import os
import re
import numpy as np
from orion.managers import manager_base
from orion.utilities.plot_config import gui_colors
from orion.utilities import spatial
from orion import _frontend
from scipy.integrate import cumulative_trapezoid
from scipy.interpolate import interp1d
from functools import wraps


def check_well_length(original_fn):
    """
    Decorator that handles empty well wellbore data
    """

    @wraps(original_fn)
    def check_well_length_wrapped(self, *xargs, **kwargs):
        if len(self._location_offset):
            return original_fn(self, *xargs, **kwargs)
        else:
            return np.zeros(0)

    return check_well_length_wrapped


class WellManager(manager_base.ManagerBase):
    """
    A class for managing well information

    Attributes:
        net_volume (np.ndarray): Cumulative fluid injection time series (at grid.t)
        net_q (np.ndarray): Net fluid injection rate time series (at grid.t)
    """

    def set_class_options(self, **kwargs):
        """
        Well manager initialization
        """

        # Set the shorthand name
        self.short_name = 'Fluid Injection'
        self._well_general_keys = ['name']
        self._well_location_keys = ['latitude', 'longitude', 'x', 'y', 'z']
        self._well_flow_keys = ['t', 'q']
        self._well_table_columns = self._well_general_keys + self._well_location_keys + self._well_flow_keys
        self.well_table_file = ''
        self.last_well_table_file = ''
        self.utm_zone_str = ''
        self.projection_str = ''
        self.source_projection_str = ''

    def set_data(self, **kwargs):
        """
        Setup data holders
        """
        self.well_table = [{k: '' for k in ['name'] + self._well_table_columns}]
        self._well_names = []
        self._points = spatial.Points(latitude=np.zeros(0), longitude=np.zeros(0))
        self._z = np.zeros(0)
        self._t = []
        self._q = []
        self._location_offset = np.zeros(0, dtype=int)
        self._flow_offset = np.zeros(0, dtype=int)
        self._net_volume = np.zeros(0)
        self._net_volume_t = np.zeros(0)

    def set_gui_options(self, **kwargs):
        """
        Setup interface options
        """
        self.set_visibility_operator()

        # Gui elements
        self.gui_elements['well_table'] = {'element_type': 'table', 'label': 'Well Data', 'position': [0, 0]}
        if (_frontend != 'strive'):
            self.gui_elements['well_table_file'] = {
                'element_type': 'entry',
                'command': 'file',
                'label': 'Well table file',
                'position': [0, 0],
                'filetypes': [('csv', '*.csv'), ('xlsx', '*.xlsx'), ('xls', '*.xls'), ('all', '*')]
            }
        self.gui_elements['well_table'] = {'element_type': 'table', 'label': 'Well Data', 'position': [0, 0]}
        self.gui_elements['projection_str'] = {'element_type': 'entry', 'label': 'Projection code', 'position': [1, 0]}
        self.gui_elements['utm_zone_str'] = {'element_type': 'entry', 'label': '(or) UTM zone', 'position': [1, 1]}

        # Figures
        fig_size = (5, 3)
        if _frontend == 'strive':
            fig_size = (90, 60)

        self.figures['flow'] = {'position': [0, 1], 'size': fig_size, 'target': 'well_flow_rate', 'layer_config': True}
        self.figures['spatial'] = {'position': [0, 0], 'size': fig_size, 'target': 'spatial_wells'}
        self.figures['volume'] = {'position': [1, 0], 'size': fig_size, 'target': 'injection_volume'}

    def add_table_row(self, **kwargs):
        row = {k: kwargs.get(k, '') for k in ['name'] + self._well_table_columns}
        self.well_table.append(row)

    def load_data(self, grid):
        """
        Load well data and process the results
        """
        # Read from an external file
        if (_frontend != 'strive') and self.well_table_file and (self.last_well_table_file != self.well_table_file):
            self.logger.debug('Parsing the well table file')
            import pandas as pd
            df = pd.DataFrame()
            if '.csv' in self.well_table_file:
                df = pd.read_csv(self.well_table_file)
            elif '.xls' in self.well_table_file:
                df = pd.read_excel(self.well_table_file)
            else:
                self.logger.error(f'Unrecognized well file type: {self.well_table_file}')
                return

            for c in self._well_table_columns:
                if c not in df:
                    df[c] = [''] * df.shape[0]
                    self.logger.warning(f'Required column ({c}) not found in table file...  Adding empty values')

            self.well_table = df[self._well_table_columns].to_dict('records')
            self.last_well_table_file = self.well_table_file

        # Check the projection information
        if self.utm_zone_str:
            self.utm_zone_str = spatial.parse_utm_zone_str(self.utm_zone_str)
            self.source_projection_str = f'+proj=utm +zone={self.utm_zone_str}'
        elif self.projection_str:
            self.source_projection_str = self.projection_str
        else:
            self.source_projection_str = grid.projection_str

        # Process table entries
        self.logger.debug('Parsing well table entries')
        location_size = []
        flow_size = []
        serial_data = {k: [] for k in self._well_table_columns}

        for well in self.well_table:
            if not well['name']:
                continue

            # Parse row data
            w = {'name': well['name']}
            for k in self._well_location_keys + self._well_flow_keys:
                v = well.get(k, '')
                if isinstance(v, str):
                    v = v.strip()
                    if ',' in v:
                        w[k] = np.array([float(x) for x in v.split(',')])
                    elif ('.csv' in v):
                        f = os.path.expanduser(os.path.normpath(v))
                        if os.path.isfile(f):
                            w[k] = np.loadtxt(f, delimiter=',', dtype=float)
                        else:
                            self.logger.error(f'File not found when processing well table data: {v}')
                    elif v:
                        w[k] = np.array([float(v)])
                    else:
                        w[k] = np.zeros(0)
                elif isinstance(v, list):
                    w[k] = np.array(v)
                else:
                    w[k] = np.array([v])

            # Check the size of location values
            Nloc = np.amax([len(w.get(k, [])) for k in self._well_location_keys])
            for k in self._well_location_keys:
                Nw = len(w[k])
                if Nw < Nloc:
                    w[k] = np.resize(w[k], Nloc)
                    w[k][Nw:] = np.nan

            # Check well depth order (expected order = bottom-up)
            w['z'][np.isnan(w['z'])] = grid.z_nodes[0]
            if Nloc > 1:
                if w['z'][-1] > w['z'][0]:
                    for k in self._well_location_keys:
                        w[k] = w[k][::-1]

            # Check the size of flow values
            Nflow = np.amax([len(w.get(k, [])) for k in self._well_flow_keys])
            if Nflow:
                for k in self._well_flow_keys:
                    Nw = len(w[k])
                    if Nw < Nflow:
                        w[k] = np.resize(w[k], Nflow)
                        w[k][Nw:] = np.nan
            else:
                w['t'] = np.zeros(1)
                w['q'] = np.zeros(1)

            # Record serial data
            location_size.append(Nloc)
            flow_size.append(Nflow)
            for k, v in w.items():
                serial_data[k].append(v)

        # Process location data
        self.logger.debug('Parsing serialized well data')
        for k in self._well_location_keys + self._well_flow_keys:
            if len(serial_data[k]):
                serial_data[k] = np.concatenate(serial_data[k], axis=0)

        Ia = ~np.isnan(serial_data['latitude']) & ~np.isnan(serial_data['longitude'])
        Ib = ~np.isnan(serial_data['x']) & ~np.isnan(serial_data['y']) & ~Ia

        if np.sum(Ia):
            pa = spatial.Points(latitude=serial_data['latitude'][Ia],
                                longitude=serial_data['longitude'][Ia],
                                target_projection_str=grid.projection_str)
            serial_data['x'][Ia] = pa.x
            serial_data['y'][Ia] = pa.y

        if np.sum(Ib):
            pb = spatial.Points(x=serial_data['x'][Ib],
                                y=serial_data['y'][Ib],
                                source_projection_str=self.source_projection_str,
                                target_projection_str=grid.projection_str)
            serial_data['x'][Ib] = pb.x
            serial_data['y'][Ib] = pb.y
            serial_data['latitude'][Ib] = pb.latitude
            serial_data['longitude'][Ib] = pb.longitude

        # Check for unconverted values
        fill_values = {
            'latitude': grid.latitude_nodes[0],
            'longitude': grid.longitude_nodes[0],
            'x': grid.x_nodes[0],
            'y': grid.y_nodes[0],
            'z': grid.z_nodes[0]
        }

        for k in self._well_location_keys:
            Ia = np.isnan(serial_data[k])
            Na = np.sum(Ia)
            if Na:
                self.error(f'Found {Na} unconverted {k} values when parsing well data')
                serial_data[k][Ia] = fill_values[k]

        # Process flow data
        Ia = np.isnan(serial_data['t'])
        Na = np.sum(Ia)
        if np.sum(Ia):
            serial_data['t'][Ia] = grid.t_nodes[0]
            self.error(f'Found {Na} unconverted time values when parsing well data')
            self.error('Make sure that t and q entries have the same length')

        Ia = np.isnan(serial_data['q'])
        Na = np.sum(Ia)
        if np.sum(Ia):
            serial_data['q'][Ia] = 0.0
            self.error(f'Found {Na} unconverted flow values when parsing well data')
            self.error('Make sure that t and q entries have the same length')

        # Save the serialized data
        self._well_names = serial_data['name']
        self._points = spatial.Points(source_projection_str=grid.projection_str,
                                      target_projection_str=grid.projection_str,
                                      **serial_data)
        self._z = serial_data['z']
        self._t = serial_data['t']
        self._q = serial_data['q']
        if len(location_size):
            self._location_offset = np.concatenate([np.zeros(1), np.cumsum(location_size)], axis=0).astype(int)
        else:
            self._location_offset = np.zeros(0)

        if len(flow_size):
            self._flow_offset = np.concatenate([np.zeros(1), np.cumsum(flow_size)], axis=0).astype(int)
        else:
            self._flow_offset = np.zeros(0)

        self.estimate_net_volume(grid)

    def deserialize_data(self, x, offsets):
        return [x[offsets[ii]:offsets[ii + 1]] for ii in range(len(offsets) - 1)]

    @property
    def well_names(self):
        return self._well_names

    @property
    @check_well_length
    def latitude(self):
        return self._points.latitude[self._location_offset[:-1]]

    @property
    @check_well_length
    def longitude(self):
        return self._points.longitude[self._location_offset[:-1]]

    @property
    @check_well_length
    def x(self):
        return self._points.x[self._location_offset[:-1]]

    @property
    @check_well_length
    def y(self):
        return self._points.y[self._location_offset[:-1]]

    @property
    @check_well_length
    def z(self):
        return self._z[self._location_offset[:-1]]

    @property
    @check_well_length
    def latitude_trajectory(self):
        return self.deserialize_data(self._points.latitude, self._location_offset)

    @property
    @check_well_length
    def longitude_trajectory(self):
        return self.deserialize_data(self._points.longitude, self._location_offset)

    @property
    @check_well_length
    def x_trajectory(self):
        return self.deserialize_data(self._points.x, self._location_offset)

    @property
    @check_well_length
    def y_trajectory(self):
        return self.deserialize_data(self._points.y, self._location_offset)

    @property
    @check_well_length
    def z_trajectory(self):
        return self.deserialize_data(self._z, self._location_offset)

    @property
    @check_well_length
    def t(self):
        return self._t[self._flow_offset[:-1]]

    @property
    @check_well_length
    def q(self):
        return self._q[self._flow_offset[:-1]]

    @property
    def t_series(self):
        return self.deserialize_data(self._t, self._flow_offset)

    @property
    def q_series(self):
        return self.deserialize_data(self._q, self._flow_offset)

    def get_well_trajectories(self, grid):
        """
        Get well trajectories separated by nan values for plotting
        """
        return self.x_trajectory, self.y_trajectory, self.z_trajectory

    def get_plot_data(self, projection):
        res = {
            'name': np.array(self.well_names),
            'x': self.x,
            'y': self.y,
            'z': self.z,
            'latitude': self.latitude,
            'longitude': self.longitude,
            'x_trajectory': self.x_trajectory,
            'y_trajectory': self.y_trajectory,
            'z_trajectory': self.z_trajectory
        }
        return res

    def estimate_net_volume(self, grid):
        t = self.t_series
        q = self.q_series

        self._net_volume_t = grid.t + grid.t_origin
        net_q = np.zeros(len(self._net_volume_t))
        for ii in range(len(t)):
            if (len(t[ii]) == 1):
                ta = np.array([-1e15, t[ii][0] - 1e-6, t[ii][0], 1e15])
                qa = np.array([0.0, 0.0, q[ii][0], q[ii][0]])
                q_interp = interp1d(ta, qa)
                net_q += q_interp(self._net_volume_t)
            elif (len(t[ii]) > 1):
                q_interp = interp1d(t[ii], q[ii], bounds_error=False, fill_value=(0, q[ii][-1]))
                net_q += q_interp(self._net_volume_t)
        self._net_volume = cumulative_trapezoid(net_q, self._net_volume_t, initial=0.0)

    def spatial_wells(self, plot_data):
        grid = plot_data['General']
        well_plot_data = plot_data['Fluid Injection']
        distance_scale = grid['distance_scale']
        distance_units = grid['distance_units']

        layers = {
            'wells': {
                'x': (self.x - grid['x_origin']) * distance_scale,
                'y': (self.y - grid['y_origin']) * distance_scale,
                'z': (self.z - grid['z_origin']) * distance_scale,
                't': {
                    'Well': np.array(self.well_names)
                },
                'type': 'scatter'
            }
        }
        axes = {
            'x': f'X ({distance_units})',
            'y': f'Y ({distance_units})',
        }
        return layers, axes

    def well_flow_rate(self, plot_data):
        grid = plot_data['General']
        t_scale = 1.0 / (60.0 * 60.0 * 24.0)
        t_origin = grid['t_origin']
        delta_plot = grid['t'][-1] - grid['t'][0]

        names = self.well_names
        t = self.t_series
        q = self.q_series

        layers = {}
        for ii in range(len(names)):
            layers[names[ii]] = {'x': (t[ii] - t_origin) * t_scale, 'y': q[ii], 'type': 'line'}
        xr = [grid['t'][0] * t_scale, grid['t'][-1] * t_scale]
        axes = {'x': 'Time (days)', 'y': 'q', 'x_range': xr}
        return layers, axes

    def injection_volume(self, plot_data):
        grid = plot_data['General']
        t_scale = 1.0 / (60.0 * 60.0 * 24.0)
        t = (self._net_volume_t - grid['t_origin']) * t_scale
        xr = [grid['t'][0] * t_scale, grid['t'][-1] * t_scale]
        layers = {'volume': {'x': t, 'y': self._net_volume * 1e-6, 'type': 'line'}}
        axes = {'x': 'Time (days)', 'y': 'Volume (m3)', 'x_range': xr}
        return layers, axes

    def generate_plots(self, **kwargs):
        """
        Generates diagnostic plots for the seismic catalog,
        fluid injection, and forecasts

        """
        # Collect data
        grid = kwargs.get('grid')
        pressure = kwargs.get('pressure')
        wells = kwargs.get('wells')

        # Setup
        max_labels = 9
        t_scale = 1.0 / (60 * 60 * 24.0)

        # Collect data
        x_range, y_range, z_range = grid.get_plot_range()
        well_names = self._well_names
        Nw = len(well_names)

        # Location plot
        ax = self.figures['spatial']['handle'].axes[0]
        ax.cla()
        ax.plot(self.x, self.y, 'r^')
        for ii in range(Nw):
            ax.annotate(well_names[ii], (self.x[ii], self.y[ii]))
        ax.set_title('Well Locations')
        grid.format_matplotlib_axes(ax)

        # Flow data
        ax = self.figures['flow']['handle'].axes[0]
        ax.cla()
        t = self.t_series
        q = self.q_series
        for ii in range(Nw):
            line_style = gui_colors.periodic_line_style(ii)
            if (ii < max_labels):
                line_style['label'] = well_names[ii]
            ax.plot((t[ii] - grid.t_origin) * t_scale, q[ii], **line_style)
        if (grid.plot_time_min < grid.plot_time_max):
            ax.set_xlim([grid.plot_time_min, grid.plot_time_max])
        else:
            ax.set_xlim([grid.t_min * t_scale, grid.t_max * t_scale])
        ax.set_xlabel('Time (day)')
        ax.set_ylabel('Flow Rate (m3/s)')
        ax.set_title('Flow Rate')
        if (Nw > 0):
            ax.legend(loc=1)

        # Volume data
        tb = (self._net_volume_t - grid.t_origin) / (60 * 60 * 24.0)
        ax = self.figures['volume']['handle'].axes[0]
        ax.cla()
        ax.plot(tb, self._net_volume * 1e-6, **gui_colors.line_style)
        ax.set_xlim([grid.t_min * t_scale, grid.t_max * t_scale])
        ax.set_xlabel('Time (day)')
        ax.set_ylabel('Net Injection (Mm^3)')
        ax.set_title('Fluid Volume')
