from orion.utilities import other, timestamp_conversion, unit_conversion, spatial
from strive import data_manager_base
import time
import numpy as np


class LocationStep(data_manager_base.DataManagerBase):

    def set_class_options(self, **kwargs):
        self.name = 'What location are you interested in?'
        self.postal_code_str = ''
        self.invalid_str_message = '(enter a valid location)'
        self.country_str = 'USA'
        self.or_str = 'or'
        self.latitude_str = ''
        self.longitude_str = ''

    def set_gui_options(self, **kwargs):
        self.gui_elements['postal_code_str'] = {'element_type': 'entry', 'label': 'Zip/Postal Code:'}
        self.gui_elements['country_str'] = {'element_type': 'entry', 'label': 'Country:'}
        self.gui_elements['or_str'] = {'element_type': 'text'}
        self.gui_elements['latitude_str'] = {'element_type': 'entry', 'label': 'Latitude:', 'units': '(degrees, N)'}
        self.gui_elements['longitude_str'] = {'element_type': 'entry', 'label': 'Longitude:', 'units': '(degrees, E)'}


class SearchRadiusStep(data_manager_base.DataManagerBase):

    def set_class_options(self, **kwargs):
        self.name = 'What area would you like to search?'
        self.search_radius = 100.0
        self.distance_units = 'km'
        self.available_distance_units = ['m', 'km', 'ft', 'miles', 'degrees']

    def set_gui_options(self, **kwargs):
        self.gui_elements['search_radius'] = {'element_type': 'entry', 'label': 'Search radius'}
        self.gui_elements['distance_units'] = {
            'element_type': 'dropdown',
            'label': 'Distance units',
            'values': self.available_distance_units
        }


class TimeRangeStep(data_manager_base.DataManagerBase):

    def set_class_options(self, **kwargs):
        self.name = 'What times are you interested in?'
        self.ref_time = time.time()
        self.time_past = 100.0
        self.time_future = 100.0
        self.time_units = 'day'
        self.available_time_units = ['second', 'day', 'year']

    def set_gui_options(self, **kwargs):
        self.gui_elements['ref_time'] = {'element_type': 'date', 'label': 'Reference Time'}
        self.gui_elements['time_past'] = {'element_type': 'entry', 'label': 'Time to search for activity in the past'}
        self.gui_elements['time_future'] = {'element_type': 'entry', 'label': 'Time to forecast activity in the future'}
        self.gui_elements['time_units'] = {
            'element_type': 'dropdown',
            'label': 'Time units',
            'values': self.available_time_units
        }


class QuickstartWizard(data_manager_base.DataManagerBase):

    def set_class_options(self, **kwargs):
        """
        Setup the wizard steps
        """
        self.name = 'ORION Quickstart Wizard'
        self.child_classes = [LocationStep, SearchRadiusStep, TimeRangeStep]

    def run(self, **wizard_results):
        """
        Process the wizard results

        Returns:
            config (dict): Generated configuration values
        """
        from orion.managers import orion_manager
        self.logger.debug('Processing ORION quickstart wizard results')

        # Process the wizard results
        # Get unit scales
        units = unit_conversion.UnitManager()
        unit_scales = units.unitMatcher.target
        unit_scales['degrees'] = 1.0

        # Parse time values
        time_res = wizard_results['TimeRangeStep']
        time_scale = unit_scales[time_res['time_units']] / unit_scales['day']
        ts = timestamp_conversion.get_time_string(time_res['ref_time'])
        time_past = time_res['time_past'] * time_scale
        time_future = time_res['time_future'] * time_scale
        dt = (time_past + time_future) / 100.0

        # Location
        location_res = wizard_results['LocationStep']
        latitude = 0.0
        longitude = 0.0
        address = None
        if location_res['postal_code_str'] and location_res['country_str']:
            tmp = other.parse_zip_code(location_res['postal_code_str'], location_res['country_str'])
            if tmp is not None:
                latitude, longitude, address = tmp
        elif location_res['latitude_str'] and location_res['longitude_str']:
            try:
                latitude = float(location_res['latitude_str'])
                longitude = float(location_res['longitude_str'])
                address = other.estimate_address(latitude, longitude)
            except Exception:
                pass

        # Search region
        radius_res = wizard_results['SearchRadiusStep']
        distance_units = radius_res['distance_units']
        p = spatial.Points(latitude=[latitude], longitude=[longitude])
        r = radius_res['search_radius']
        r *= unit_scales[distance_units]
        dr = r / 10.0

        # Create a temporary instance of the target class
        manager = orion_manager.OrionManager(is_wizard=True)

        # Setup the seismic catalog
        catalog = manager.children["SeismicCatalog"]
        catalog.use_comcat = 1
        catalog.catalog_source = ""

        # Setup the grid manager
        grid = manager.children["GridManager"]
        grid.ref_time_str = ts
        grid.t_min_input = -time_past
        grid.t_max_input = time_future
        grid.plot_time_min = -time_past
        grid.plot_time_max = time_future
        grid.dt_input = dt
        if distance_units == 'degrees':
            grid.distance_units = 'km'
            grid.spatial_type = 'Lat/Lon'
            grid.utm_zone_str = ''
            grid.projection_str = 'EPSG:3857'
            grid.longitude_min = longitude - r
            grid.longitude_max = longitude + r
            grid.dlongitude = dr
            grid.latitude_min = latitude - r
            grid.latitude_max = latitude + r
            grid.dlatitude = dr
            grid.x_origin_input = p.x[0] * grid.distance_scale
            grid.y_origin_input = p.y[0] * grid.distance_scale

        else:
            pb = spatial.Points(x=[p.x[0] - r, p.x[0] + r], y=[p.y[0] - r, p.y[0] + r])
            utm_zone_a = spatial.estimate_utm_zone(pb.latitude[0], pb.longitude[0])
            utm_zone_b = spatial.estimate_utm_zone(pb.latitude[1], pb.longitude[1])
            pc = spatial.Points(latitude=[latitude], longitude=[longitude], utm_zone=utm_zone_a)

            grid.distance_units = distance_units
            if utm_zone_a == utm_zone_b:
                grid.spatial_type = 'UTM'
                grid.utm_zone_str = str(utm_zone_a)
                grid.projection_str = ''
                grid.x_origin_input = pc.x[0] * grid.distance_scale
                grid.y_origin_input = pc.y[0] * grid.distance_scale
            else:
                grid.spatial_type = 'General'
                grid.utm_zone_str = ''
                grid.projection_str = 'EPSG:3857'
                grid.x_origin_input = p.x[0] * grid.distance_scale
                grid.y_origin_input = p.y[0] * grid.distance_scale
            grid.z_origin_input = 0.0
            grid.x_min_input = -r * grid.distance_scale
            grid.x_max_input = r * grid.distance_scale
            grid.dx_input = dr * grid.distance_scale
            grid.y_min_input = -r * grid.distance_scale
            grid.y_max_input = r * grid.distance_scale
            grid.dy_input = dr * grid.distance_scale

        # Set default z range to 1km
        grid.z_min_input = 0.0
        grid.z_max_input = 1000.0 * grid.distance_scale
        grid.dz_input = 1000.0 * grid.distance_scale
        grid.process_inputs()

        # Try loading the catalog data to set the depth range
        catalog.load_data(grid)
        z = catalog.depth_slice
        if len(z):
            grid.z_min_input = np.floor(np.amin(z) * grid.distance_scale)
            grid.z_max_input = np.ceil(np.amax(z) * grid.distance_scale)
            grid.dz_input = grid.z_max_input - grid.z_min_input

        config = manager.get_config_recursive()
        return config
