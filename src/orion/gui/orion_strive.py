# ------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2020-, Lawrence Livermore National Security, LLC
# All rights reserved
#
# See top level LICENSE, COPYRIGHT, CONTRIBUTORS, NOTICE, and ACKNOWLEDGEMENTS files for details.
# ------------------------------------------------------------------------------------------------
"""
orion_strive.py
--------------------------------------
"""
from strive.dash import dash_gui_base
import orion
import os
from PIL import Image
from dash import html


class OrionSTRIVE(dash_gui_base.DashGUIBase):
    """
    STRIVE-based Orion gui

    Attributes:
        main_buttons (dict): An object to hold the control buttons in the gui
    """

    def get_help_info(self):
        # Help text
        help_text = f'Orion (v{orion.__version__}) with funding support from the United States '
        help_text += 'Department of Energy’s Office of Fossil Energy and Carbon Management through the '
        help_text += 'Science-informed Machine Learning to Accelerate Real-Time (SMART) Decisions in Subsurface '
        help_text += 'Applications Initiative and the National Risk Assessment Partnership (NRAP). '
        help_text += 'This work was performed under the auspices of the U.S. Department of Energy by Lawrence '
        help_text += 'Livermore National Laboratory under contract DE-AC52-07NA27344, and is '
        help_text += 'released under the identifier LLNL-CODE-842148.'

        # Workflow
        workflow_text = 'The ORION workflow includes the following elements:'
        gui_source_path = os.path.split(__file__)[0]
        workflow_path = os.path.join(gui_source_path, 'orion_workflow.png')
        workflow_image = Image.open(workflow_path).resize((878, 385), Image.Resampling.LANCZOS)

        # Logos
        all_logos_path = os.path.join(gui_source_path, 'logos_all.png')
        all_logos_image = Image.open(all_logos_path).resize((574, 389), Image.Resampling.LANCZOS)
        help_div = html.Div(children=[
            html.P(help_text),
            html.Br(),
            html.P(workflow_text),
            html.Img(src=workflow_image),
            html.Hr(),
            html.Img(src=all_logos_image)
        ])

        return help_div


def run(config_fname='', profile_run=False, verbose=False, **kwargs):
    """
    Launch the Orion STRIVE gui

    Args:
        config_fname (str): Name of the orion config file
    """

    # Initialize orion
    manager = None
    quickstart = None
    try:
        from orion.managers import orion_manager
        from orion.gui import orion_quickstart_strive
        manager = orion_manager.OrionManager(config_fname=config_fname,
                                             skip_data_load=True,
                                             frontend='strive',
                                             verbose=verbose)
        quickstart = orion_quickstart_strive.QuickstartWizard()
    except Exception as e:
        print('Failed to load orion')
        print(e)

    # Launch the gui
    if manager:
        title_url = 'https://edx.netl.doe.gov/dataset/orion-operational-forecasting-of-induced-seismicity'
        banner_url_map = {
            'Documentation': {
                'url': 'https://nrap.gitlab.io/orion/'
            },
            'EDX': {
                'url': 'https://edx.netl.doe.gov/workspace/resources/orion'
            }
        }

        gui_source_path = os.path.split(__file__)[0]
        logo_file = os.path.join(gui_source_path, 'orion_logo_simple.png')
        example_root = os.path.join(manager.cache_root, 'examples')

        current_path = os.path.split(__file__)[0]
        example_config = os.path.join(current_path, 'example_config.json')

        gui = OrionSTRIVE(manager=manager,
                          quickstart_manager=quickstart,
                          banner_url=banner_url_map,
                          logo=logo_file,
                          title_url=title_url,
                          banner_header='ORION',
                          banner_subheader='(Operational Forecasting of Induced Seismicity)',
                          collapsible_depth=2,
                          example_root=example_root,
                          example_config=example_config)
        gui.build_interface()
        gui.run(**kwargs)
