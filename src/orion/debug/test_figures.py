import sys
import os
from pathlib import Path

mod_path = os.path.abspath(Path(__file__).resolve().parents[2])
sys.path.append(mod_path)
strive_path = os.path.abspath(os.path.join(mod_path, '..', '..', 'strive', 'src'))
sys.path.append(strive_path)

import orion
from orion.managers import grid_manager, seismic_catalog
from orion.pressure_models import pressure_table
from orion.forecast_models.seismogenic_index_model import SeismogenicIndexModel
from orion.utilities.statistical_methods import poisson_probability

import numpy as np
import matplotlib as mpl

import matplotlib.pyplot as plt
import h5py
import datetime
from datetime import *
import pickle
from scipy.signal import windows
from scipy.ndimage import convolve, median_filter
import time
from mpl_toolkits.axes_grid1 import make_axes_locatable

import logging

logging.basicConfig(level=logging.ERROR)
logging.getLogger('strive').setLevel(logging.ERROR)


def create_grid(
        x_min,
        x_max,    ### this is just the range in m (i.e. 338500 - 335500)
        dx,
        y_min,
        y_max,
        dy,
        z_min,
        z_max,
        dz,
        x_origin,
        y_origin,
        z_origin,
        t_min_input,
        t_max_input,
        dt_input,
        utm_zone_str='16'):
    grid = grid_manager.GridManager()
    grid.utm_zone_str = utm_zone_str

    grid.x_min = x_min
    grid.x_max = x_max
    grid.dx = dx

    grid.y_min = y_min
    grid.y_max = y_max
    grid.dy = dy

    grid.z_min = z_min
    grid.z_max = z_max
    grid.dz = dz

    grid.x_origin = x_origin
    grid.y_origin = y_origin
    grid.z_origin = z_origin

    grid.ref_time_str = '17/11/2011 00:00:00'    ## onset of injection, can change to onset of seismicity
    grid.t_min_input = t_min_input
    grid.t_max_input = t_max_input
    grid.dt_input = dt_input

    grid.process_inputs()
    return grid


def L2(N_obs, N_fore):
    L2_norm = np.sqrt(np.sum((N_obs - N_fore)**2))
    return L2_norm


def main():
    start_time = time.time()

    # grid_dxdy = [1000]
    ## Get figure 3c for many grid sizes
    grid_dxdy = [1000, 800, 650, 500, 300, 200, 100, 50]
    # grid_dxdy = [200]

    # training_percentages = [100]
    training_percentages = np.linspace(1, 100, num=100, dtype=int)
    # print(training_percentages)

    results = {}
    event_count_array = None
    N_smooth_dict = {}
    # grid_shapes = {}
    # catalog = None

    for grid_size in grid_dxdy:
        results[grid_size] = {}
        N_smooth_dict[grid_size] = {}

        for training_percent in training_percentages:
            results[grid_size][training_percent] = {}
            grid = create_grid(0.0, 3175, grid_size, 0.0, 2775., grid_size, 1900.0, 2300.75, 400., 335572.5, 4415004.5,
                               0.0, 0, 1130, 11.3)    ## set t_max to 1000 and dt to 10 as default
            print(grid.shape)

            catalog = seismic_catalog.SeismicCatalog(no_figures=True)
            catalog.t_origin = 0.0
            catalog.catalog_source = '/Users/geffers1/Documents/Testing_ORION/decaturSeismic.hdf5'
            catalog.load_data(grid)
            mc = -0.8
            # catalog.set_slice(time_range=[2422640.0, 98215891.2])
            catalog.set_slice(time_range=[2422640.0, 98215891.2], magnitude_range=[mc, 10])
            # catalog.set_slice(magnitude_range=[mc, 10])
            catalog._magnitude_completeness = mc
            catalog._b_value = 1.0
            catalog.calculate_spatial_parameters(grid)

            ## (first value is large because the first event above -0.8 is late on)

            ### time of first seismic event: 2422640.0 seconds
            ### time of first injection point: 0.00 seconds (reference time string)

            pm = pressure_table.PressureTableModel()
            pm.file_name = '/Users/geffers1/Documents/Testing_ORION/decatur_pressure.hdf5'
            pm.run(grid, None, None)
            dpdt = pm.dpdt_grid

            seismogenic_index_model = SeismogenicIndexModel()
            seismogenic_index_model.process_inputs()

            seismogenic_index_model.training_percentage = training_percent
            event_count = catalog.spatial_count
            event_count_array = event_count

            temporal, spatial = seismogenic_index_model.generate_forecast(grid, catalog, pm, None, None)

            results[grid_size][training_percent]['temporal'] = temporal
            results[grid_size][training_percent]['spatial'] = spatial

        N_smooth_dict[grid_size] = seismogenic_index_model.N_smooth

    end_time = time.time()
    print(event_count_array)
    elapsed_time = end_time - start_time
    print("Elapsed time:", elapsed_time, "seconds")
    # print(grid_shapes)

    N_smooth_16x14 = N_smooth_dict[200]
    N_smooth_6x6 = N_smooth_dict[500]
    N_smooth_1000 = N_smooth_dict[1000]
    N_smooth_800 = N_smooth_dict[800]
    N_smooth_650 = N_smooth_dict[650]
    N_smooth_300 = N_smooth_dict[300]
    N_smooth_100 = N_smooth_dict[100]
    N_smooth_50 = N_smooth_dict[50]

    forecast_time = grid.t
    # forecast_number = temporal_100_16x14
    magnitude_thresholds = np.arange(1, 5, 0.5)

    # forecast_duration = grid.t.max() - grid.t.min() ## same as injection
    forecast_duration = 15768000    ## 100 days are 8640000 seconds, this is 6 months, 182.5 days
    magnitude_completeness = mc
    b_value = 1.0

    def calculate_ppe(grid_size, training_percent, magnitude_threshold):
        spatial = results[grid_size][training_percent]['spatial']
        ppe = np.zeros((spatial.shape[1], spatial.shape[2], len(magnitude_threshold)))

        for x in range(spatial.shape[1]):
            for y in range(spatial.shape[2]):
                forecast_number = spatial[:, x, y]

                ppe[x, y, :] = poisson_probability(forecast_time, forecast_number, b_value, magnitude_completeness,
                                                   magnitude_threshold, forecast_duration)

        return ppe

    ## get all grid sizes at 100%
    ppe_1000_100 = calculate_ppe(grid_size=1000, training_percent=100, magnitude_threshold=magnitude_thresholds)
    ppe_800_100 = calculate_ppe(grid_size=800, training_percent=100, magnitude_threshold=magnitude_thresholds)
    ppe_650_100 = calculate_ppe(grid_size=650, training_percent=100, magnitude_threshold=magnitude_thresholds)
    ppe_500_100 = calculate_ppe(grid_size=500, training_percent=100, magnitude_threshold=magnitude_thresholds)
    ppe_300_100 = calculate_ppe(grid_size=300, training_percent=100, magnitude_threshold=magnitude_thresholds)
    ppe_200_100 = calculate_ppe(grid_size=200, training_percent=100, magnitude_threshold=magnitude_thresholds)
    ppe_100_100 = calculate_ppe(grid_size=100, training_percent=100, magnitude_threshold=magnitude_thresholds)
    ppe_50_100 = calculate_ppe(grid_size=50, training_percent=100, magnitude_threshold=magnitude_thresholds)

    ## get two grid sizes at 20, 50, 100%
    ppe_500_50 = calculate_ppe(grid_size=500, training_percent=50, magnitude_threshold=magnitude_thresholds)
    ppe_500_20 = calculate_ppe(grid_size=500, training_percent=20, magnitude_threshold=magnitude_thresholds)
    ppe_200_50 = calculate_ppe(grid_size=200, training_percent=50, magnitude_threshold=magnitude_thresholds)
    ppe_200_20 = calculate_ppe(grid_size=200, training_percent=20, magnitude_threshold=magnitude_thresholds)

    magnitude_threshold_index = 2    ## M > 3. M > 2 is index 2

    global_max_prob_500_20 = (np.max(ppe_500_20[:, :,
                                                magnitude_threshold_index]))    ## only required for plotting of Fig 2.
    # print(global_max_prob_500_20)

    temporal_100_16x14 = results[200][100]['temporal']
    temporal_100_6x6 = results[500][100]['temporal']
    temporal_50_16x14 = results[200][50]['temporal']
    temporal_50_6x6 = results[500][50]['temporal']
    temporal_20_16x14 = results[200][20]['temporal']
    temporal_20_6x6 = results[500][20]['temporal']

    plt.rcParams['axes.facecolor'] = 'white'    # Set background color of axes to white
    plt.rcParams['legend.facecolor'] = 'white'    # Set legend background color to white
    plt.rcParams['figure.facecolor'] = 'white'    # Set background color of the figure to white
    plt.rcParams['text.color'] = 'black'    # Set text color to black
    plt.rcParams['xtick.color'] = 'black'    # Set x-axis tick color to black
    plt.rcParams['ytick.color'] = 'black'
    plt.rcParams['axes.labelcolor'] = 'black'    # Set color of axis labels to black
    plt.rcParams['xtick.labelsize'] = 11
    plt.rcParams['ytick.labelsize'] = 11
    plt.rcParams['axes.labelsize'] = 11

    fig, ((ax1, ax2, ax3), (ax4, ax5, ax6),
          (ax7, ax8, ax9)) = plt.subplots(nrows=3, ncols=3,
                                          figsize=(10,
                                                   9))    ## add , linewidth=1, edgecolor="#04253a" to get a boundary

    grid_x_range = tuple(x / 1000. for x in (np.min(grid.x), np.max(grid.x)))
    grid_x_range = tuple(x / 1000. for x in [335572.5, 338747.5])
    print(grid_x_range)
    grid_y_range = tuple(y / 1000. for y in [4415004.5, 4417779.5])
    # grid_y_range = tuple(y / 1000. for y in (np.min(grid.y), np.max(grid.y)))
    print(grid_y_range)

    t_scale = 60 * 60 * 24 * 365.25
    # ax1.set_ylabel('20%')
    # ax1.text(-0.5,
    #          0.5,
    #          '20%',
    #          transform=ax1.transAxes,
    #          fontsize=12,
    #          va='center',
    #          ha='center',
    #          weight='bold',
    #          rotation='vertical')
    # ax1.text(-0.1, 1.05, '(a)', transform=ax1.transAxes, fontsize=12, va='center', ha='center', c='k')

    # ax1.set_ylabel('Northing (km)')
    # # ax1.set_title(r'$\Delta (x,y)$ = 200m ')
    # ppe20_orig = ax1.imshow(np.transpose(ppe_200_20[:, :, magnitude_threshold_index]),
    #                         cmap='plasma',
    #                         origin='lower',
    #                         vmin=0,
    #                         vmax=global_max_prob_500_20,
    #                         extent=[grid_x_range[0], grid_x_range[1], grid_y_range[0], grid_y_range[1]])

    # # ax2.set_title(r'$\Delta (x,y)$ = 500m')
    # ax2.text(-0.1, 1.05, '(b)', transform=ax2.transAxes, fontsize=12, va='center', ha='center', c='k')

    # ppe20_g2_orig = ax2.imshow(np.transpose(ppe_500_20[:, :, magnitude_threshold_index]),
    #                            cmap='plasma',
    #                            origin='lower',
    #                            vmin=0,
    #                            vmax=global_max_prob_500_20,
    #                            extent=[grid_x_range[0], grid_x_range[1], grid_y_range[0], grid_y_range[1]])

    # # ax3.set_title('Temporal Forecast')
    # ax3.text(-0.1, 1.0, '(c)', transform=ax3.transAxes, fontsize=12, va='center', ha='center', c='k')
    # ax3.plot(grid.t / t_scale, np.sum(N_smooth_16x14, axis=(1, 2)), label='observed N (smoothed)', c='k')
    # ax3.plot(grid.t / t_scale, np.sum(N_smooth_6x6, axis=(1, 2)), label='observed N (smoothed)', c='g')
    # ax3.axvline(x=grid.t[19] / t_scale, ls='-.')
    # ax3.plot(grid.t / t_scale, temporal_20_16x14, label='forecast N Grid 1', c='k', ls=':')
    # ax3.plot(grid.t / t_scale, temporal_20_6x6, label='forecast N Grid 2', c='g', ls=':')
    # ax3.set_ylabel('cumulative N')
    # ax3.set_ylim(-80, 2999)

    # ax4.set_ylabel('Northing (km)')
    # ax4.text(-0.5,
    #          0.5,
    #          '50%',
    #          transform=ax4.transAxes,
    #          fontsize=12,
    #          va='center',
    #          ha='center',
    #          weight='bold',
    #          rotation='vertical')
    # ax4.text(-0.1, 1.05, '(d)', transform=ax4.transAxes, fontsize=12, va='center', ha='center', c='k')

    # ppe50_orig = ax4.imshow(np.transpose(ppe_200_50[:, :, magnitude_threshold_index]),
    #                         cmap='plasma',
    #                         origin='lower',
    #                         vmin=0,
    #                         vmax=global_max_prob_500_20,
    #                         extent=[grid_x_range[0], grid_x_range[1], grid_y_range[0], grid_y_range[1]])
    # ax5.text(-0.1, 1.05, '(e)', transform=ax5.transAxes, fontsize=12, va='center', ha='center', c='k')
    # ppe50_g2_orig = ax5.imshow(np.transpose(ppe_500_50[:, :, magnitude_threshold_index]),
    #                            cmap='plasma',
    #                            origin='lower',
    #                            vmin=0,
    #                            vmax=global_max_prob_500_20,
    #                            extent=[grid_x_range[0], grid_x_range[1], grid_y_range[0], grid_y_range[1]])

    # ax6.text(-0.1, 1.0, '(f)', transform=ax6.transAxes, fontsize=12, va='center', ha='center', c='k')
    # ax6.plot(grid.t / t_scale, np.sum(N_smooth_16x14, axis=(1, 2)), label='observed N (smoothed)', c='k')
    # ax6.plot(grid.t / t_scale, np.sum(N_smooth_6x6, axis=(1, 2)), label='observed N (smoothed)', c='g')
    # ax6.axvline(x=grid.t[49] / t_scale, ls='-.')
    # ax6.plot(grid.t / t_scale, temporal_50_16x14, label='forecast N Grid 1', c='k', ls=':')
    # ax6.plot(grid.t / t_scale, temporal_50_6x6, label='forecast N Grid 2', c='g', ls=':')
    # ax6.set_ylabel('cumulative N')
    # ax6.set_ylim(-80, 2450)

    # ax7.set_ylabel('100%')
    # ax7.set_xlabel('Easting (km)')
    # ax7.set_ylabel('Northing (km)')
    # ax7.text(-0.5,
    #          0.5,
    #          '100%',
    #          transform=ax7.transAxes,
    #          fontsize=12,
    #          va='center',
    #          ha='center',
    #          weight='bold',
    #          rotation='vertical')
    # ax7.text(-0.1, 1.05, '(g)', transform=ax7.transAxes, fontsize=12, va='center', ha='center', c='k')

    # ppe100_orig = ax7.imshow(np.transpose(ppe_200_100[:, :, magnitude_threshold_index]),
    #                          cmap='plasma',
    #                          origin='lower',
    #                          vmin=0,
    #                          vmax=global_max_prob_500_20,
    #                          extent=[grid_x_range[0], grid_x_range[1], grid_y_range[0], grid_y_range[1]])
    # ax8.set_xlabel('Easting (km)')
    # ax8.text(-0.1, 1.05, '(h)', transform=ax8.transAxes, fontsize=12, va='center', ha='center', c='k')

    # ppe100_g2_orig = ax8.imshow(np.transpose(ppe_500_100[:, :, magnitude_threshold_index]),
    #                             cmap='plasma',
    #                             origin='lower',
    #                             vmin=0,
    #                             vmax=global_max_prob_500_20,
    #                             extent=[grid_x_range[0], grid_x_range[1], grid_y_range[0], grid_y_range[1]])

    # ax9.text(-0.1, 1.0, '(i)', transform=ax9.transAxes, fontsize=12, va='center', ha='center', c='k')
    # ax9.plot(grid.t / t_scale, np.sum(N_smooth_16x14, axis=(1, 2)), label=r'obs. N ($\Delta (x,y)$=200m)', c='k')
    # ax9.plot(grid.t / t_scale, np.sum(N_smooth_6x6, axis=(1, 2)), label=r'obs. N ($\Delta (x,y)$=500m)', c='g')
    # ax9.plot(grid.t / t_scale, temporal_100_16x14, label=r'for. N ($\Delta (x,y)$=200m)', c='k', ls=':')
    # ax9.axvline(x=grid.t[99] / t_scale, ls='-.')
    # ax9.plot(grid.t / t_scale, temporal_100_6x6, label=r'for. N ($\Delta (x,y)$=500m)', c='g', ls=':')
    # ax9.set_ylabel('cumulative N')
    # ax9.set_xlabel('time (yrs)')
    # ax9.set_ylim(-80, 2300)

    # cbar_ax = fig.add_axes([0.15, 0.1, 0.45, 0.02])    # Adjust these values as needed
    # cbar = fig.colorbar(ppe100_g2_orig, cax=cbar_ax, orientation='horizontal')
    # cbar.set_label('% probability of exceedance M > 2', fontsize=12)
    # plt.subplots_adjust(bottom=0.18, wspace=0.5)    # Increase this value as needed

    # # ax9.legend(fontsize=6)
    # plt.show()

    # fig.savefig('2_spatio_temporal_forecast.png', dpi=300, bbox_inches='tight',
    #             transparent=True)    ## add edgecolor=fig.get_edgecolor() for frame

    misfit_array_16x14 = np.zeros((len(training_percentages), ))
    misfit_array_6x6 = np.zeros((len(training_percentages), ))
    misfit_array_1000 = np.zeros((len(training_percentages), ))
    misfit_array_800 = np.zeros((len(training_percentages), ))
    misfit_array_650 = np.zeros((len(training_percentages), ))
    misfit_array_300 = np.zeros((len(training_percentages), ))
    misfit_array_100 = np.zeros((len(training_percentages), ))
    misfit_array_50 = np.zeros((len(training_percentages), ))

    custom_cbar1 = plt.cm.get_cmap('autumn_r', 100)
    custom_cbar2 = plt.cm.get_cmap('winter_r', 100)

    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(nrows=2, ncols=2, figsize=(12, 10))
    t_slices = np.arange(0, 100)    ## create an x-axis as %t, not time

    cumulative_1000 = np.sum(N_smooth_1000, axis=(1, 2))
    cumulative_800 = np.sum(N_smooth_800, axis=(1, 2))
    cumulative_650 = np.sum(N_smooth_650, axis=(1, 2))
    cumulative_6x6 = np.sum(N_smooth_6x6, axis=(1, 2))
    cumulative_300 = np.sum(N_smooth_300, axis=(1, 2))
    cumulative_16x14 = np.sum(N_smooth_16x14, axis=(1, 2))
    cumulative_100 = np.sum(N_smooth_100, axis=(1, 2))
    cumulative_50 = np.sum(N_smooth_50, axis=(1, 2))

    for i, training_percent in enumerate(training_percentages):
        misfit_array_1000[i] = L2(cumulative_1000[i], results[1000][training_percent]['temporal'])
        misfit_array_800[i] = L2(cumulative_800[i], results[800][training_percent]['temporal'])
        misfit_array_650[i] = L2(cumulative_650[i], results[650][training_percent]['temporal'])
        misfit_array_6x6[i] = L2(cumulative_6x6[i], results[500][training_percent]['temporal'])
        misfit_array_300[i] = L2(cumulative_300[i], results[300][training_percent]['temporal'])
        misfit_array_16x14[i] = L2(cumulative_16x14[i], results[200][training_percent]['temporal'])
        misfit_array_100[i] = L2(cumulative_100[i], results[100][training_percent]['temporal'])
        misfit_array_50[i] = L2(cumulative_50[i], results[50][training_percent]['temporal'])

        ax1.plot(t_slices, results[200][training_percent]['temporal'], c=custom_cbar1(i / 100))
        print(training_percent)
        print(results[200][training_percent]['temporal'])
        print(cumulative_16x14)
        ax1.plot(t_slices, cumulative_16x14, c='k', lw=2)

        ax1.set_ylabel('cumulative N')
        ax1.text(-0.1, 1., '(a)', transform=ax1.transAxes, fontsize=12, va='center', ha='center', c='k')
        # ax1.set_title(r'$\Delta (x,y)$ = 200m')
        ax1.set_xlabel('% t')
        ax1.set_ylim(-100, 3700)

        ax2.plot(t_slices, results[500][training_percent]['temporal'], c=custom_cbar2(i / 100))
        ax2.plot(t_slices, cumulative_6x6, c='k', lw=2)
        ax2.set_ylim(-100, 3700)
        # ax2.text(-0.2, -0.15, '% t', transform=ax2.transAxes, fontsize=12, va='center', ha='center')
        ax2.text(-0.1, 1., '(b)', transform=ax2.transAxes, fontsize=12, va='center', ha='center', c='k')
        # ax2.set_title(r'$\Delta (x,y)$ = 500m')
        ax2.set_xlabel('% t')

        N_fore_200 = results[200][training_percent]['temporal'][
            -1]    ## misfit at just the last point for all 100 lines
        N_obs_200 = np.sum(N_smooth_16x14, axis=(1, 2))[-1]

        full_fore_200 = results[200][training_percent]['temporal']
        full_obs_200 = np.sum(N_smooth_16x14, axis=(1, 2))

        L2_ax3_200 = L2(N_obs_200, N_fore_200)
        L2_ax3_200_full = L2(full_obs_200, full_fore_200)
        ax3.scatter(training_percent, L2_ax3_200, c='r', s=15, edgecolor='k', lw=0.5, label='final point')
        ax3.scatter(training_percent,
                    L2_ax3_200_full,
                    c='orange',
                    s=15,
                    edgecolor='k',
                    lw=0.5,
                    label='full line misfit')
        ax3.text(-0.1, 1.0, '(c)', transform=ax3.transAxes, fontsize=12, va='center', ha='center', c='k')
        ax3.set_ylabel('L2 norm')
        ax3.set_xlabel('% t')
        ax3.set_ylim(-10, 13500)

        N_fore_500 = results[500][training_percent]['temporal'][
            -1]    ## misfit at just the last point for all 100 lines
        N_obs_500 = np.sum(N_smooth_6x6, axis=(1, 2))[-1]

        full_fore_500 = results[500][training_percent]['temporal']
        full_obs_500 = np.sum(N_smooth_6x6, axis=(1, 2))

        L2_ax3_500 = L2(N_obs_500, N_fore_500)
        L2_ax3_500_full = L2(full_obs_500, full_fore_500)
        ax4.scatter(training_percent, L2_ax3_500, c='b', s=15, edgecolor='k', lw=0.5, label='final point')
        ax4.scatter(training_percent, L2_ax3_500_full, c='cyan', s=15, edgecolor='k', lw=0.5, label='full line misfit')
        ax4.text(-0.1, 1., '(d)', transform=ax4.transAxes, fontsize=12, va='center', ha='center', c='k')
        ax4.set_xlabel('% t')
        ax4.set_ylim(-10, 13500)

    sm = plt.cm.ScalarMappable(cmap=custom_cbar1)
    sm.set_array([])

    cbar1 = plt.colorbar(sm, ax=ax1, pad=0.01)
    cbar1.set_label('Calibration %')
    cbar1.set_ticklabels(['0', '20', '40', '60', '80', '100'])
    cbar1.ax.tick_params(labelsize=8)    # Adjust the fontsize as needed

    smb = plt.cm.ScalarMappable(cmap=custom_cbar2)
    smb.set_array([])

    cbar2 = plt.colorbar(smb, ax=ax2, pad=0.02)
    cbar2.set_label('Calibration %')
    cbar2.set_ticklabels(['0', '20', '40', '60', '80', '100'])
    cbar2.ax.tick_params(labelsize=8)    # Adjust the fontsize as needed

    plt.subplots_adjust(wspace=0.3, bottom=0.2)    # Increase this value as needed
    plt.show()
    # fig.savefig('3_individual_misfits.png', dpi=300, bbox_inches='tight', transparent=True)

    ### Figure 4 ppe & misfits
    fig, ((ax1, ax2)) = plt.subplots(nrows=1, ncols=2, figsize=(15, 6))
    # mpl.rcParams['xtick.labelsize'] = 15
    # mpl.rcParams['ytick.labelsize'] = 15

    custom = plt.cm.viridis_r

    colors = [custom(i) for i in np.linspace(0, 1, 8)]    # Adjust 7 according to the number of lines

    ax1.plot(t_slices,
             misfit_array_1000,
             marker='o',
             mfc=colors[7],
             markeredgecolor='k',
             ms=5,
             markeredgewidth=0.5,
             color=colors[7],
             lw=0.8)
    ax1.plot(t_slices,
             misfit_array_800,
             marker='o',
             mfc=colors[6],
             markeredgecolor='k',
             ms=5,
             markeredgewidth=0.5,
             color=colors[6],
             lw=0.8)
    ax1.plot(t_slices,
             misfit_array_650,
             marker='o',
             mfc=colors[5],
             markeredgecolor='k',
             ms=5,
             markeredgewidth=0.5,
             color=colors[5],
             lw=0.8)
    ax1.plot(t_slices,
             misfit_array_6x6,
             marker='o',
             mfc=colors[4],
             markeredgecolor='k',
             ms=5,
             markeredgewidth=0.5,
             color=colors[4],
             lw=0.8)
    ax1.plot(t_slices,
             misfit_array_300,
             marker='o',
             mfc=colors[3],
             markeredgecolor='k',
             ms=5,
             markeredgewidth=0.5,
             color=colors[3],
             lw=0.8)
    ax1.plot(t_slices,
             misfit_array_16x14,
             marker='o',
             mfc=colors[2],
             markeredgecolor='k',
             ms=5,
             markeredgewidth=0.5,
             color=colors[2],
             lw=0.8)
    ax1.plot(t_slices,
             misfit_array_100,
             marker='o',
             mfc=colors[1],
             markeredgecolor='k',
             ms=5,
             markeredgewidth=0.5,
             color=colors[1],
             lw=0.8)
    ax1.plot(t_slices,
             misfit_array_50,
             marker='o',
             mfc=colors[0],
             markeredgecolor='k',
             ms=5,
             markeredgewidth=0.5,
             color=colors[0],
             lw=0.8)
    ax1.set_ylim(-200, 15000)

    smG = plt.cm.ScalarMappable(cmap=custom)
    smG.set_array([])

    cax = ax1.inset_axes([0.85, 0.01, 0.018, 0.45])    # Adjust the position and size as needed

    cbarG = plt.colorbar(smG, cax=cax)
    cbarG.set_label(r'$\Delta (x,y)$')

    cbarG.set_ticks([50, 100, 200, 300, 500, 650, 800, 1000])
    cbarG.ax.set_yticklabels(['50', '100', '200', '300', '500', '650', '800', '1000'], fontsize=7.5)
    smG.set_clim(0, 1000)    # Adjust the range according to your data

    cbarG.ax.yaxis.set_label_coords(5.5, 0.5)    # Adjust the x-coordinate (1 for default) as needed

    ax2_1 = ax1.twinx()
    ax2_1.plot(t_slices, np.sum(event_count_array, axis=(1, 2)), c='k', lw=1.5)
    ax2_1.set_ylabel('observed number of events')

    ax1.set_ylabel('L2 norm')
    ax1.set_xlabel(r'$t_c$')
    ax1.text(-0.07, 1., '(a)', transform=ax1.transAxes, fontsize=12, va='center', ha='center')

    global_max_prob_1000 = (np.max(ppe_1000_100[:, :, magnitude_threshold_index]))
    global_max_prob_800 = (np.max(ppe_800_100[:, :, magnitude_threshold_index]))
    global_max_prob_650 = (np.max(ppe_650_100[:, :, magnitude_threshold_index]))
    global_max_prob_500 = (np.max(ppe_500_100[:, :, magnitude_threshold_index]))
    global_max_prob_300 = (np.max(ppe_300_100[:, :, magnitude_threshold_index]))
    global_max_prob_200 = (np.max(ppe_200_100[:, :, magnitude_threshold_index]))
    global_max_prob_100 = (np.max(ppe_100_100[:, :, magnitude_threshold_index]))
    global_max_prob_50 = (np.max(ppe_50_100[:, :, magnitude_threshold_index]))

    ax2.scatter(50, global_max_prob_50, c='b', s=18, edgecolor='k', lw=0.5)
    ax2.scatter(100, global_max_prob_100, c='b', s=18, edgecolor='k', lw=0.5)
    ax2.scatter(200, global_max_prob_200, c='b', s=18, edgecolor='k', lw=0.5)
    ax2.scatter(300, global_max_prob_300, c='b', s=18, edgecolor='k', lw=0.5)
    ax2.scatter(500, global_max_prob_500, c='b', s=18, edgecolor='k', lw=0.5)
    ax2.scatter(650, global_max_prob_650, c='b', s=18, edgecolor='k', lw=0.5)
    ax2.scatter(800, global_max_prob_800, c='b', s=18, edgecolor='k', lw=0.5)
    ax2.scatter(1000, global_max_prob_1000, c='b', s=18, edgecolor='k', lw=0.5)
    ax2.set_xlabel(r'$\Delta (x,y)$')
    ax2.set_ylabel('Max. % Probability of exceedance M > 2')
    ax2.text(-0.07, 1., '(b)', transform=ax2.transAxes, fontsize=12, va='center', ha='center')

    plt.subplots_adjust(wspace=0.4, bottom=0.2)    # Increase this value as needed
    plt.show()

    fig.savefig('4_ppe_misfits.png', dpi=300, bbox_inches='tight', transparent=True)


if __name__ == '__main__':
    main()
