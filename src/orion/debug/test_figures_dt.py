import sys
import os
from pathlib import Path

mod_path = os.path.abspath(Path(__file__).resolve().parents[2])
sys.path.append(mod_path)
strive_path = os.path.abspath(os.path.join(mod_path, '..', '..', 'strive', 'src'))
sys.path.append(strive_path)

import orion
from orion.managers import grid_manager, seismic_catalog
from orion.pressure_models import pressure_table
from orion.forecast_models.seismogenic_index_model import SeismogenicIndexModel
from orion.utilities.statistical_methods import poisson_probability

import numpy as np
import matplotlib as mpl

import matplotlib.pyplot as plt
import h5py
import datetime
from datetime import *
import pickle
from scipy.signal import windows
from scipy.ndimage import convolve, median_filter
import time
from mpl_toolkits.axes_grid1 import make_axes_locatable

import logging

logging.basicConfig(level=logging.ERROR)
logging.getLogger('strive').setLevel(logging.ERROR)


def create_grid(
        x_min,
        x_max,    ### this is just the range in m (i.e. 338500 - 335500)
        dx,
        y_min,
        y_max,
        dy,
        z_min,
        z_max,
        dz,
        x_origin,
        y_origin,
        z_origin,
        t_min_input,
        t_max_input,
        dt_input,
        utm_zone='16S'):
    grid = grid_manager.GridManager()
    grid.utm_zone = utm_zone

    grid.x_min = x_min
    grid.x_max = x_max
    grid.dx = dx

    grid.y_min = y_min
    grid.y_max = y_max
    grid.dy = dy

    grid.z_min = z_min
    grid.z_max = z_max
    grid.dz = dz

    grid.x_origin = x_origin
    grid.y_origin = y_origin
    grid.z_origin = z_origin

    grid.ref_time_str = '17/11/2011 00:00:00'    ## onset of injection, can change to onset of seismicity
    grid.t_min_input = t_min_input
    grid.t_max_input = t_max_input
    grid.dt_input = dt_input

    grid.process_inputs()
    return grid


def L2(N_obs, N_fore):
    L2_norm = np.sqrt(np.sum((N_obs - N_fore)**2))
    return L2_norm


def main():
    start_time = time.time()

    grid_dxdy = [200, 500]
    dt_input = [0.5, 1, 5, 11.3, 35.3125]
    # dt_input = [1, 11.3]

    results = {}
    event_count_array = None
    N_smooth_dict = {}

    for grid_size in grid_dxdy:
        results[grid_size] = {}
        N_smooth_dict[grid_size] = {}

        for dt in dt_input:
            results[grid_size][dt] = {}
            N_smooth_dict[grid_size][dt] = {}

            grid = create_grid(0.0, 3175, grid_size, 0.0, 2775., grid_size, 1900.0, 2300.75, 400., 335572.5, 4415004.5,
                               0.0, 0, 1130, dt)    ## set t_max to 1000 and dt to 10 as default

            catalog = seismic_catalog.SeismicCatalog(no_figures=True)
            catalog.t_origin = 0.0
            catalog.catalog_source = '/Users/geffers1/Documents/Testing_ORION/decaturSeismic.hdf5'
            catalog.load_data(grid)
            mc = -0.8
            # print(len(catalog.magnitude_slice)) ### 5420 events in entire catalog
            ## epoch first event (prior to cutting mag completeness 1731801600)
            ## 2422640s after onset of injection to first event (~ 28 days)
            ## 98215891.2s after onset of injection to last event used during injection (1136.758)
            catalog.set_slice(time_range=[2422640.0, 98215891.2], magnitude_range=[mc, 10])
            catalog._magnitude_completeness = mc
            catalog._b_value = 1.0
            catalog.calculate_spatial_parameters(grid)
            # print(len(catalog.magnitude_slice)) ### 2168 events within time and > mc

            pm = pressure_table.PressureTableModel()
            pm.file_name = '/Users/geffers1/Documents/Testing_ORION/decatur_pressure.hdf5'
            pm.run(grid, None, None)
            dpdt = pm.dpdt_grid

            seismogenic_index_model = SeismogenicIndexModel()
            seismogenic_index_model.process_inputs()

            event_count = catalog.spatial_count
            event_count_array = event_count

            temporal, spatial = seismogenic_index_model.generate_forecast(grid, catalog, pm, None, None)

            results[grid_size][dt]['temporal'] = temporal
            results[grid_size][dt]['spatial'] = spatial

            N_smooth_dict[grid_size][dt] = seismogenic_index_model.N_smooth

    end_time = time.time()
    elapsed_time = end_time - start_time
    print("Elapsed time:", elapsed_time, "seconds")

    event_time_since_injection_days = catalog.relative_time / (86400)

    # # Example data for testing (replace with your actual data)
    # N_smooth_dict = {
    #     200: {
    #         0.5: np.random.rand(10, 10, 10),
    #         1: np.random.rand(10, 10, 10),
    #         5: np.random.rand(10, 10, 10),
    #         11.3: np.random.rand(10, 10, 10),
    #         35.3125: np.random.rand(10, 10, 10)
    #     },
    #     500: {
    #         0.5: np.random.rand(10, 10, 10),
    #         1: np.random.rand(10, 10, 10),
    #         5: np.random.rand(10, 10, 10),
    #         11.3: np.random.rand(10, 10, 10),
    #         35.3125: np.random.rand(10, 10, 10)
    #     }
    # }

    # results = {
    #     200: {
    #         0.5: {'temporal': np.random.rand(10)},
    #         1: {'temporal': np.random.rand(10)},
    #         5: {'temporal': np.random.rand(10)},
    #         11.3: {'temporal': np.random.rand(10)},
    #         35.3125: {'temporal': np.random.rand(10)}
    #     },
    #     500: {
    #         0.5: {'temporal': np.random.rand(10)},
    #         1: {'temporal': np.random.rand(10)},
    #         5: {'temporal': np.random.rand(10)},
    #         11.3: {'temporal': np.random.rand(10)},
    #         35.3125: {'temporal': np.random.rand(10)}
    #     }
    # }

    # target_N = [100, 200, 300]

    # # Define parameters
    # parameters = [0.5, 1, 5, 11.3, 35.3125]
    # keys = [200, 500]

    # # Initialize dictionaries
    # sums = {}
    # closest_indices = {}
    # N_indices = {}
    # mis = {}
    # misfits = {}

    # # Calculate sums
    # for key in keys:
    #     sums[key] = {param: np.sum(N_smooth_dict[key][param], axis=(1, 2)) for param in parameters}

    # # Find closest indices
    # for key in keys:
    #     closest_indices[key] = {param: [np.abs(sums[key][param] - target).argmin() for target in target_N] for param in parameters}

    # # Initialize mis dictionaries
    # for key in keys:
    #     mis[key] = {param: [] for param in parameters}

    # # Calculate N_indices
    # for key in keys:
    #     N_indices[key] = {param: [index for target, index in zip(target_N, closest_indices[key][param])] for param in parameters}

    # # Calculate misfits
    # for i in range(len(target_N)):
    #     for key in keys:
    #         for param in parameters:
    #             misfit = L2(sums[key][param][N_indices[key][param][i]], results[key][param]['temporal'][N_indices[key][param][i]])
    #             mis[key][param].append(misfit)

    # # Separate misfits into lists for plotting
    # mis_05 = mis[200][0.5]
    # mis_1 = mis[200][1]
    # mis_5 = mis[200][5]
    # mis_113 = mis[200][11.3]
    # mis_35 = mis[200][35.3125]

    # mis_05_b = mis[500][0.5]
    # mis_1_b = mis[500][1]
    # mis_5_b = mis[500][5]
    # mis_113_b = mis[500][11.3]
    # mis_35_b = mis[500][35.3125]

    target_N = np.arange(100, 2200, 100)

    N_smooth_16x14_05 = N_smooth_dict[200][0.5]
    N_smooth_16x14_1 = N_smooth_dict[200][1]
    N_smooth_16x14_5 = N_smooth_dict[200][5]
    N_smooth_16x14_113 = N_smooth_dict[200][11.3]
    N_smooth_16x14_35 = N_smooth_dict[200][35.3125]

    N_smooth_500_05 = N_smooth_dict[500][0.5]
    N_smooth_500_1 = N_smooth_dict[500][1]
    N_smooth_500_5 = N_smooth_dict[500][5]
    N_smooth_500_113 = N_smooth_dict[500][11.3]
    N_smooth_500_35 = N_smooth_dict[500][35.3125]

    sums_05 = np.sum(N_smooth_16x14_05, axis=(1, 2))
    sums_1 = np.sum(N_smooth_16x14_1, axis=(1, 2))
    sums_5 = np.sum(N_smooth_16x14_5, axis=(1, 2))
    sums_113 = np.sum(N_smooth_16x14_113, axis=(1, 2))
    sums_35 = np.sum(N_smooth_16x14_35, axis=(1, 2))

    sums_05_b = np.sum(N_smooth_500_05, axis=(1, 2))
    sums_1_b = np.sum(N_smooth_500_1, axis=(1, 2))
    sums_5_b = np.sum(N_smooth_500_5, axis=(1, 2))
    sums_113_b = np.sum(N_smooth_500_113, axis=(1, 2))
    sums_35_b = np.sum(N_smooth_500_35, axis=(1, 2))

    closest_indices_05 = [np.abs(sums_05 - target).argmin() for target in target_N]
    closest_indices_1 = [np.abs(sums_1 - target).argmin() for target in target_N]
    closest_indices_5 = [np.abs(sums_5 - target).argmin() for target in target_N]
    closest_indices_113 = [np.abs(sums_113 - target).argmin() for target in target_N]
    closest_indices_35 = [np.abs(sums_35 - target).argmin() for target in target_N]

    closest_indices_05_b = [np.abs(sums_05_b - target).argmin() for target in target_N]
    closest_indices_1_b = [np.abs(sums_1_b - target).argmin() for target in target_N]
    closest_indices_5_b = [np.abs(sums_5_b - target).argmin() for target in target_N]
    closest_indices_113_b = [np.abs(sums_113_b - target).argmin() for target in target_N]
    closest_indices_35_b = [np.abs(sums_35_b - target).argmin() for target in target_N]

    N_indices_05 = []
    for target, index in zip(target_N, closest_indices_05):
        N_indices_05.append(index)

    N_indices_1 = []
    for target, index in zip(target_N, closest_indices_1):
        # print(f"Closest sum to {target} is at index {index} with sum {sums_1[index]}")
        N_indices_1.append(index)

    N_indices_5 = []
    for target, index in zip(target_N, closest_indices_5):
        N_indices_5.append(index)

    N_indices_113 = []
    for target, index in zip(target_N, closest_indices_113):
        N_indices_113.append(index)

    N_indices_35 = []
    for target, index in zip(target_N, closest_indices_35):
        N_indices_35.append(index)

    N_indices_05_b = []
    for target, index in zip(target_N, closest_indices_05_b):
        N_indices_05_b.append(index)

    N_indices_1_b = []
    for target, index in zip(target_N, closest_indices_1_b):
        # print(f"Closest sum to {target} is at index {index} with sum {sums_1[index]}")
        N_indices_1_b.append(index)

    N_indices_05_b = []
    for target, index in zip(target_N, closest_indices_5_b):
        N_indices_05_b.append(index)

    N_indices_113_b = []
    for target, index in zip(target_N, closest_indices_113_b):
        N_indices_113_b.append(index)

    N_indices_35_b = []
    for target, index in zip(target_N, closest_indices_35_b):
        N_indices_35_b.append(index)

    mis_05 = []
    mis_1 = []
    mis_5 = []
    mis_113 = []
    mis_35 = []

    mis_05_b = []
    mis_1_b = []
    mis_5_b = []
    mis_113_b = []
    mis_35_b = []

    results_05 = results[200][0.5]['temporal']
    results_1 = results[200][1]['temporal']    ### for the entire series
    results_5 = results[200][5]['temporal']
    results_113 = results[200][11.3]['temporal']    ### for the entire series
    results_35 = results[200][35.3125]['temporal']    ### for the entire series

    results_05_b = results[500][0.5]['temporal']
    results_1_b = results[500][1]['temporal']    ### for the entire series
    results_5_b = results[500][5]['temporal']
    results_113_b = results[500][11.3]['temporal']    ### for the entire series
    results_35_b = results[500][35.3125]['temporal']    ### for the entire series

    for i in range(len(N_indices_1)):
        misfit_05 = L2(sums_05[N_indices_05[i]], results_05[N_indices_05[i]])
        misfit_1 = L2(sums_1[N_indices_1[i]], results_1[N_indices_1[i]])
        misfit_5 = L2(sums_5[N_indices_5[i]], results_5[N_indices_5[i]])
        misfit_113 = L2(sums_113[N_indices_113[i]], results_113[N_indices_113[i]])
        misfit_35 = L2(sums_35[N_indices_35[i]], results_35[N_indices_35[i]])

        misfit_05_b = L2(sums_05_b[N_indices_05_b[i]], results_05_b[N_indices_05_b[i]])
        misfit_1_b = L2(sums_1_b[N_indices_1_b[i]], results_1_b[N_indices_1_b[i]])
        misfit_5_b = L2(sums_5_b[N_indices_05_b[i]], results_5_b[N_indices_05_b[i]])
        misfit_113_b = L2(sums_113_b[N_indices_113_b[i]], results_113_b[N_indices_113_b[i]])
        misfit_35_b = L2(sums_35_b[N_indices_35_b[i]], results_35_b[N_indices_35_b[i]])

        mis_05.append(misfit_05)
        mis_1.append(misfit_1)
        mis_5.append(misfit_5)
        mis_113.append(misfit_113)
        mis_35.append(misfit_35)

        mis_05_b.append(misfit_05_b)
        mis_1_b.append(misfit_1_b)
        mis_5_b.append(misfit_5_b)
        mis_113_b.append(misfit_113_b)
        mis_35_b.append(misfit_35_b)

    # misfit_full_05 = []
    # for i in range(len(sums_05)):
    #     mf = L2(sums_05[i], results_05[i])
    #     misfit_full_05.append(mf)

    # misfit_full_1 = []
    # for i in range(len(sums_1)):
    #     mf = L2(sums_1[i], results_1[i])
    #     misfit_full_1.append(mf)

    # misfit_full_5 = []
    # for i in range(len(sums_5)):
    #     mf = L2(sums_5[i], results_5[i])
    #     misfit_full_5.append(mf)

    # misfit_full_113 = []
    # for i in range(len(sums_113)):
    #     mf = L2(sums_113[i], results_113[i])
    #     misfit_full_113.append(mf)

    # misfit_full_35 = []
    # for i in range(len(sums_35)):
    #     mf = L2(sums_35[i], results_35[i])
    #     misfit_full_35.append(mf)

    # min_values_05 = []
    # min_indices_05 = []
    # current_min_05 = float('inf')

    # for i in range(len(misfit_full_05)):
    #     if misfit_full_05[i] < current_min_05:
    #         current_min_05 = misfit_full_05[i]
    #         min_values_05.append(current_min_05)
    #         min_indices_05.append(i)

    # min_values_1 = []
    # min_indices_1 = []
    # current_min_1 = float('inf')

    # for i in range(len(misfit_full_1)):
    #     if misfit_full_1[i] < current_min_1:
    #         current_min_1 = misfit_full_1[i]
    #         min_values_1.append(current_min_1)
    #         min_indices_1.append(i)

    # min_values_5 = []
    # min_indices_5 = []
    # current_min_5 = float('inf')

    # for i in range(len(misfit_full_5)):
    #     if misfit_full_5[i] < current_min_5:
    #         current_min_5 = misfit_full_5[i]
    #         min_values_5.append(current_min_5)
    #         min_indices_5.append(i)

    # min_values_113 = []
    # min_indices_113 = []
    # current_min_113 = float('inf')

    # for i in range(len(misfit_full_113)):
    #     if misfit_full_113[i] < current_min_113:
    #         current_min_113 = misfit_full_113[i]
    #         min_values_113.append(current_min_113)
    #         min_indices_113.append(i)

    # min_values_35 = []
    # min_indices_35 = []
    # current_min_35 = float('inf')

    # for i in range(len(misfit_full_35)):
    #     if misfit_full_35[i] < current_min_35:
    #         current_min_35 = misfit_full_35[i]
    #         min_values_35.append(current_min_35)
    #         min_indices_35.append(i)

    plt.rcParams['axes.facecolor'] = 'white'    # Set background color of axes to white
    plt.rcParams['legend.facecolor'] = 'white'    # Set legend background color to white
    plt.rcParams['figure.facecolor'] = 'white'    # Set background color of the figure to white
    plt.rcParams['text.color'] = 'black'    # Set text color to black
    plt.rcParams['xtick.color'] = 'black'    # Set x-axis tick color to black
    plt.rcParams['ytick.color'] = 'black'
    plt.rcParams['axes.labelcolor'] = 'black'    # Set color of axis labels to black
    plt.rcParams['xtick.labelsize'] = 11
    plt.rcParams['ytick.labelsize'] = 11
    plt.rcParams['axes.labelsize'] = 11

    fig, ((ax1, ax2)) = plt.subplots(nrows=1, ncols=2, figsize=(12, 6))

    ## x should be target_N
    ## y should be misfit between forecasted, observed at specific index.
    plt.suptitle(r'$\Delta (x,y)$ = 200m')
    ax1.scatter(target_N, mis_05, c='k', s=15, edgecolor='k', lw=0.5, label='dt=0.5 days')
    ax1.scatter(target_N, mis_1, c='b', s=15, edgecolor='k', lw=0.5, label='dt=1 day')
    ax1.scatter(target_N, mis_5, c='g', s=15, edgecolor='k', lw=0.5, label='dt=5 days')
    ax1.scatter(target_N, mis_113, c='r', s=15, edgecolor='k', lw=0.5, label='dt=11.3 days')
    ax1.scatter(target_N, mis_35, c='orange', s=15, edgecolor='k', lw=0.5, label='dt=35.3125 days')

    ax1.set_ylim(0, 600)
    ax1.plot(target_N, mis_05, linestyle='-', color='k', lw=0.5)
    ax1.plot(target_N, mis_1, linestyle='-', color='b', lw=0.5)
    ax1.plot(target_N, mis_5, linestyle='-', color='g', lw=0.5)
    ax1.plot(target_N, mis_113, linestyle='-', color='r', lw=0.5)
    ax1.plot(target_N, mis_35, linestyle='-', color='orange', lw=0.5)

    ax1.set_ylabel('L2 norm')
    ax1.set_xlabel('N')
    ax1.legend()

    Ns = [
        100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000,
        2100
    ]

    ax2_2 = ax2.twinx()
    ax2_2.scatter(event_time_since_injection_days, catalog.magnitude_slice, c='b', s=2, alpha=0.1)
    ax2_2.set_ylabel('Magnitude')

    event_times_mis = []

    mis_values_list = [(mis_05, 'k', 'dt=0.5 days'), (mis_1, 'b', 'dt=1 days'), (mis_5, 'g', 'dt=5 days'),
                       (mis_113, 'r', 'dt=11.3 days'), (mis_35, 'orange', 'dt=35.3125 days')]

    for mis_values, color, label in mis_values_list:
        event_times_mis = []
        for N, mis_value in zip(Ns, mis_values):
            if N < len(event_time_since_injection_days):
                event_times_mis.append(event_time_since_injection_days[N])
                ax2.scatter(event_time_since_injection_days[N],
                            mis_value,
                            c=color,
                            s=15,
                            edgecolor='k',
                            lw=0.5,
                            label=label)

    ax2.plot(event_times_mis, mis_05, linestyle='-', color='k', lw=0.5)
    ax2.plot(event_times_mis, mis_1, linestyle='-', color='b', lw=0.5)
    ax2.plot(event_times_mis, mis_5, linestyle='-', color='g', lw=0.5)
    ax2.plot(event_times_mis, mis_113, linestyle='-', color='r', lw=0.5)
    ax2.plot(event_times_mis, mis_35, linestyle='-', color='orange', lw=0.5)

    ax2.set_xlabel('t (days)')

    ax2.axvline(x=event_time_since_injection_days[0], label='onset of first event', c='fuchsia', lw=0.5)

    ax2.set_xlim(0, 1130)
    ax2.set_ylim(0, 600)
    ax2.set_xlabel('t (days)')

    plt.subplots_adjust(wspace=0.3, bottom=0.2)    # Increase this value as needed
    plt.show()
    # fig.savefig('3_individual_misfits.png', dpi=300, bbox_inches='tight', transparent=True)

    #### Same plot as above but for 500m grid sizes

    fig, ((ax1, ax2)) = plt.subplots(nrows=1, ncols=2, figsize=(12, 6))

    ## x should be target_N
    ## y should be misfit between forecasted, observed at specific index.
    plt.suptitle(r'$\Delta (x,y)$ = 500m')
    ax1.scatter(target_N, mis_05_b, c='k', s=15, edgecolor='k', lw=0.5, label='dt=0.5 days')
    ax1.scatter(target_N, mis_1_b, c='b', s=15, edgecolor='k', lw=0.5, label='dt=1 day')
    ax1.scatter(target_N, mis_5_b, c='g', s=15, edgecolor='k', lw=0.5, label='dt=5 days')
    ax1.scatter(target_N, mis_113_b, c='r', s=15, edgecolor='k', lw=0.5, label='dt=11.3 days')
    ax1.scatter(target_N, mis_35_b, c='orange', s=15, edgecolor='k', lw=0.5, label='dt=35.3125 days')

    ax1.set_ylim(0, 600)
    ax1.plot(target_N, mis_05_b, linestyle='-', color='k', lw=0.5)
    ax1.plot(target_N, mis_1_b, linestyle='-', color='b', lw=0.5)
    ax1.plot(target_N, mis_5_b, linestyle='-', color='g', lw=0.5)
    ax1.plot(target_N, mis_113_b, linestyle='-', color='r', lw=0.5)
    ax1.plot(target_N, mis_35_b, linestyle='-', color='orange', lw=0.5)

    ax1.set_ylabel('L2 norm')
    ax1.set_xlabel('N')
    ax1.legend()

    Ns = [
        100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000,
        2100
    ]

    ax2_2 = ax2.twinx()
    ax2_2.scatter(event_time_since_injection_days, catalog.magnitude_slice, c='b', s=2, alpha=0.1)
    ax2_2.set_ylabel('Magnitude')

    event_times_mis = []

    mis_values_list_b = [(mis_05_b, 'k', 'dt=0.5 days'), (mis_1_b, 'b', 'dt=1 days'), (mis_5_b, 'g', 'dt=5 days'),
                         (mis_113_b, 'r', 'dt=11.3 days'), (mis_35_b, 'orange', 'dt=35.3125 days')]

    for mis_values, color, label in mis_values_list_b:
        event_times_mis = []
        for N, mis_value in zip(Ns, mis_values):
            if N < len(event_time_since_injection_days):
                event_times_mis.append(event_time_since_injection_days[N])
                ax2.scatter(event_time_since_injection_days[N],
                            mis_value,
                            c=color,
                            s=15,
                            edgecolor='k',
                            lw=0.5,
                            label=label)

    ax2.plot(event_times_mis, mis_05_b, linestyle='-', color='k', lw=0.5)
    ax2.plot(event_times_mis, mis_1_b, linestyle='-', color='b', lw=0.5)
    ax2.plot(event_times_mis, mis_5_b, linestyle='-', color='g', lw=0.5)
    ax2.plot(event_times_mis, mis_113_b, linestyle='-', color='r', lw=0.5)
    ax2.plot(event_times_mis, mis_35_b, linestyle='-', color='orange', lw=0.5)

    ax2.set_xlabel('t (days)')

    ax2.axvline(x=event_time_since_injection_days[0], label='onset of first event', c='fuchsia', lw=0.5)

    ax2.set_xlim(0, 1130)
    ax2.set_ylim(0, 600)
    ax2.set_xlabel('t (days)')

    plt.subplots_adjust(wspace=0.3, bottom=0.2)    # Increase this value as needed
    plt.show()


if __name__ == '__main__':
    main()
