# ------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2020-, Lawrence Livermore National Security, LLC
# All rights reserved
#
# See top level LICENSE, COPYRIGHT, CONTRIBUTORS, NOTICE, and ACKNOWLEDGEMENTS files for details.
# ------------------------------------------------------------------------------------------------

import sys
import os
from pathlib import Path

mod_path = os.path.abspath(Path(__file__).resolve().parents[2])
sys.path.append(mod_path)
strive_path = os.path.abspath(os.path.join(mod_path, '..', '..', 'strive', 'src'))
sys.path.append(strive_path)

import orion
from orion.examples import built_in_manager


def debug_orion(frontend='tkinter',
                clear_cache=True,
                built_in='',
                config_name='',
                variant_id='',
                profile_run=False,
                verbose=True,
                debug=True):
    """
    Trigger a run to debug orion
    Args:
        frontend (str): Launch the gui ('', 'tkinter', 'strive')
        clear_cache (bool): Clear the orion cache before running
        built_in (str): Name of a built-in debug case
        config_name (str): Configuration file (overriden by built_in)
        variant_id (str): Identifier for built in variant
    """

    # To prevent any issues, you may need to remove existing cache files
    cache_root = os.path.expanduser('~/.cache/orion')
    cache_file = os.path.join(cache_root, 'orion_config.json')
    os.makedirs(cache_root, exist_ok=True)
    if clear_cache and os.path.isfile(cache_file):
        print('(clearing cache before run)')
        os.remove(cache_file)

    # Optionally choose one of the built in files
    if built_in:
        config_name = ''
        built_in_manager.compile_built_in(built_in + variant_id, cache_file)

    # Launch orion
    if frontend == 'tkinter':
        orion._frontend = 'tkinter'
        from orion.gui import orion_gui
        orion_gui.launch_gui(config_name, profile_run=profile_run, verbose=verbose)
    elif frontend in ['strive', 'webview', 'browser']:
        orion._frontend = 'strive'
        from orion.gui import orion_strive
        orion_strive.run(config_name, profile_run=profile_run, verbose=verbose, debug=debug, option=frontend)
    else:
        orion._frontend = ''
        from orion.managers import orion_manager
        orion_manager.run_manager(config_name, verbose=verbose)


def debug_download():
    example_manager = built_in_manager.BuiltInExampleManager()
    example_manager.download_examples = ['Decatur']
    example_manager.download_data()


if (__name__ == '__main__'):
    # debug_orion(frontend='')
    # debug_orion(frontend='tkinter')
    # debug_orion(frontend='strive')

    # debug_orion(frontend='webview', built_in='Decatur')
    debug_orion(frontend='strive', built_in='Decatur')
    # debug_orion(frontend='tkinter', built_in='Decatur')
    # debug_orion(frontend='', built_in='Decatur')

    # debug_orion(frontend='strive', built_in='Texas_Permian')
    # debug_orion(frontend='tkinter', built_in='Texas_Permian')

    # debug_orion(frontend='strive', built_in='Oklahoma_Cushing2014')
    # debug_orion(frontend='tkinter', built_in='Oklahoma_Cushing2014')
    # debug_orion(frontend='', built_in='Oklahoma_Cushing2014')

    # debug_orion(frontend='strive', built_in='Oklahoma_Cushing2014', variant_id='_ComCat')
    # debug_orion(frontend='tkinter', built_in='Oklahoma_Cushing2014', variant_id='_ComCat')
    # debug_orion(frontend='', built_in='Oklahoma_Cushing2014', variant_id='_ComCat')

    # debug_orion(frontend='strive', built_in='California_BayArea', variant_id='_latest')
    # debug_orion(frontend='tkinter', built_in='California_BayArea', variant_id='_latest')
    # debug_orion(frontend='', built_in='California_BayArea', variant_id='_latest')

    # debug_orion(frontend='tkinter', built_in='Quest')
