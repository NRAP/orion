# ------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2020-, Lawrence Livermore National Security, LLC
# All rights reserved
#
# See top level LICENSE, COPYRIGHT, CONTRIBUTORS, NOTICE, and ACKNOWLEDGEMENTS files for details.
# ------------------------------------------------------------------------------------------------
"""
coupled_coulomb_rate_state_model.py
--------------------------------------
"""

import itertools
import numpy as np
from orion.forecast_models import forecast_model_base
from orion.managers.manager_base import recursive
from orion.forecast_models.saif._saif_kernel import SAIF_Kernel
from orion.forecast_models.saif._peak_detector import pk_indxs


class SAIFModel(forecast_model_base.ForecastModel):
    """
    SAIF forecast model

    Attributes:
        active (bool): Flag to indicate whether the model is active
        pressure_method (str): Pressure calculation method
        forecast_time (ndarray): Time values for forecast calculation
    """

    def set_class_options(self, **kwargs):
        """
        SAIF model initialization

        """
        # Call the parent's setup
        super().set_class_options(**kwargs)

        # Initialize model-specific parameters
        self.short_name = 'SAIF'
        self.long_name = 'Seismic Activity due to Injection Forecast'

        # Forecast variables
        self.mu = 0.73
        self.background_rate_input = 0.1    # events/year
        self.tectonicShearStressingRate_input = 3.5e-4    # MPa/year
        self.tectonicNormalStressingRate_input = 0    # MPa/year
        self.rate_coefficient = 0.002
        self.alpha = 0.155
        self.sigma_input = 30.0    # MPa
        self.biot = 0.3
        self.rate_factor_input = 163.7424    # 1/MPa

        self.sigma = 0.0
        self.tectonicShearStressingRate = 0.0
        self.tectonicNormalStressingRate = 0.0
        self.background_rate = 0.0
        self.rate_factor = 0.0
        self.enable_clustering = False

        self.max_epochs = 20
        self.loss_threshold = 10000
        self.initial_learning_rate = 1e-4
        self.final_learning_rate = 1e-5
        self.learning_momentum = 0.2
        self.peak_detect_threshold = 0.025
        self.peak_detect_min_dist = 10

    def set_gui_options(self, **kwargs):
        """
        Setup interface options
        """
        super().set_gui_options(**kwargs)

        # Add the variable to the gui
        self.gui_elements['mu'] = {
            'element_type': 'entry',
            'label': u'\u03BC: Coefficient of Friction',
            'units': '(0-1)',
            'position': [4, 0]
        }

        self.gui_elements['sigma_input'] = {
            'element_type': 'entry',
            'label': 'Normal Stress',
            'position': [4, 1],
            'units': '(MPa)'
        }

        self.gui_elements['rate_coefficient'] = {
            'element_type': 'entry',
            'label': 'Rate-coefficient',
            'position': [5, 0]
        }

        self.gui_elements['biot'] = {'element_type': 'entry', 'label': 'Biot-Coefficient', 'position': [5, 1]}

        self.gui_elements['alpha'] = {
            'element_type': 'entry',
            'label': u'\u03B1: [Normal Stress Dependency]',
            'position': [6, 0],
            'units': '(0-\u03BC)'
        }

        self.gui_elements['tectonicShearStressingRate_input'] = {
            'element_type': 'entry',
            'label': 'Tectonic Shear Stressing Rate',
            'position': [6, 1],
            'units': '(MPa/year)'
        }

        self.gui_elements['tectonicNormalStressingRate_input'] = {
            'element_type': 'entry',
            'label': 'Tectonic Normal Stressing Rate',
            'position': [7, 0],
            'units': '(MPa/year)'
        }

        self.gui_elements['background_rate_input'] = {
            'element_type': 'entry',
            'label': 'Background Seismicity Rate',
            'position': [7, 1],
            'units': '(events/year)'
        }

        self.gui_elements['rate_factor_input'] = {
            'element_type': 'entry',
            'label': 'Rate Scaling Factor',
            'position': [8, 0],
            'units': '(1/MPa)'
        }
        self.gui_elements['enable_clustering'] = {
            'element_type': 'check',
            'label': 'Enable Clustering Effects',
            'position': [8, 1]
        }
        self.gui_elements['max_epochs'] = {'element_type': 'entry', 'label': 'Optimization Steps', 'position': [9, 0]}
        self.gui_elements['loss_threshold'] = {'element_type': 'entry', 'label': 'Loss threshold', 'position': [9, 1]}
        self.gui_elements['initial_learning_rate'] = {
            'element_type': 'entry',
            'label': 'Initial learning rate',
            'position': [10, 0]
        }
        self.gui_elements['final_learning_rate'] = {
            'element_type': 'entry',
            'label': 'Final learning rate',
            'position': [10, 1]
        }
        self.gui_elements['peak_detect_threshold'] = {
            'element_type': 'entry',
            'label': 'Peak threshold',
            'position': [11, 0]
        }
        self.gui_elements['peak_detect_min_dist'] = {
            'element_type': 'entry',
            'label': 'Peak distance',
            'position': [11, 1]
        }

    @recursive
    def process_inputs(self):
        """
        Process any required gui inputs
        """
        mpa_yr2pa_s = 1e6 / 365.25 / 86400    # MPa/year to Pa/s
        self.sigma = self.sigma_input * 1e6    # Pa
        self.tectonicShearStressingRate = self.tectonicShearStressingRate_input * mpa_yr2pa_s    # Pa/s
        self.tectonicNormalStressingRate = self.tectonicNormalStressingRate_input * mpa_yr2pa_s    # Pa/s
        self.background_rate = self.background_rate_input / (365.25 * 86400)    # event/second
        self.rate_factor = self.rate_factor_input / 1e6    # 1/Pa

    def generate_forecast(self, grid, seismic_catalog, pressure, wells, geologic_model):
        import torch
        import torch.optim as optim
        import torch.nn as nn
        import torch.nn.functional as F

        # Get the catalog rates
        count = seismic_catalog.spatial_count.copy()
        spatial_rate = seismic_catalog.spatial_rate.copy()
        spatial_rate[spatial_rate < self.background_rate] = self.background_rate
        temporal_rate = np.sum(spatial_rate, axis=(1, 2))

        # Run the peak detector
        peak_indices = pk_indxs(temporal_rate, trshd=self.peak_detect_threshold, min_dist=self.peak_detect_min_dist)
        catalog_segments = np.concatenate([[0], peak_indices, [grid.shape[3] - 1]])

        # Setup kernel inputs
        torch.set_grad_enabled(True)
        params = torch.FloatTensor([[
            self.mu - self.alpha,
            np.log(self.rate_coefficient),
            np.log(self.rate_factor),
        ]])
        params.requires_grad = True
        params.register_hook(lambda g: F.normalize(g))

        # Generate the kernel
        dt = grid.t[1:] - grid.t[:-1]
        kernel = SAIF_Kernel(tectonic_shear_stressing_rate=self.tectonicShearStressingRate,
                             tectonic_normal_stressing_rate=self.tectonicNormalStressingRate,
                             sigma=self.sigma,
                             biot=self.biot,
                             background_rate=self.background_rate,
                             init_delta_t=dt[0])

        # Generate the optimizer and loss function
        optimizer = optim.SGD(params=[params], lr=self.initial_learning_rate, momentum=self.learning_momentum)
        criterion = nn.MSELoss(reduction='mean')

        # Generate the spatial forecast
        Nx, Ny, Nz, Nt = grid.shape
        p = pressure.p_grid.copy()
        dpdt = pressure.dpdt_grid.copy()
        count_xyt = np.moveaxis(count, 0, -1)
        count_reshaped = np.reshape(count_xyt, (-1, Nt))

        p_tensor = torch.FloatTensor(np.reshape(p[:, :, 0, 1:], (-1, Nt - 1)))
        dpdt_tensor = torch.FloatTensor(np.reshape(dpdt[:, :, 0, 1:], (-1, Nt - 1)))
        count_tensor = torch.FloatTensor(count_reshaped)
        mask_tensor = torch.FloatTensor(count_reshaped > 0)
        spatial_rate_tensor = torch.FloatTensor(np.reshape(spatial_rate, (-1, Nt, 1)))
        initial_count_tensor = torch.FloatTensor(np.reshape(count_xyt, (-1, Nt, 1)))
        dt_tensor = torch.FloatTensor(np.array([dt for _ in range(Nx * Ny)]))

        cell_rate = None
        cell_number = None
        for epoch in range(self.max_epochs):
            cell_rate = torch.zeros(Nx * Ny, 1)
            cell_number = torch.zeros(Nx * Ny, 1)
            for segment_start, segment_stop in itertools.pairwise(catalog_segments):
                p_segment = p_tensor[:, segment_start:segment_stop]
                dpdt_segment = dpdt_tensor[:, segment_start:segment_stop]
                dt_segment = dt_tensor[:, segment_start:segment_stop]
                if segment_start:
                    # kernel.R0 = spatial_rate_tensor.detach()[:, segment_start, :]
                    kernel.R0 = spatial_rate_tensor[:, segment_start, :]
                    kernel.N0 = initial_count_tensor[:, segment_start, :]

                segment_r, segment_n = kernel(params, p_segment, dpdt_segment, dt_segment)
                cell_rate = torch.cat((cell_rate, segment_r[:, 1:]), dim=-1)
                # segment_n = torch.add(segment_n, cell_number.detach()[:, -1:])
                cell_number = torch.cat((cell_number, segment_n[:, 1:]), dim=-1)

            optimizer.zero_grad()
            # loss = criterion(cell_number*mask_tensor, count_tensor*mask_tensor)
            loss = criterion(cell_number, count_tensor)
            loss.backward()
            optimizer.step()

            if (epoch % 5 == 0):
                lv = loss.item()
                pa = params[0, 0]
                pb, pc = torch.exp(params[0, 1:])
                self.logger.debug(f'SAIF step {epoch}: loss={lv:.3e}, mu-alpha={pa:.3f}, rc={pb:.3e}, rf={pc:.3e}')
                print('\n\nspatial: model, observed')
                print(torch.floor(cell_number[-20:, -1]).detach().numpy())
                print(count_tensor[-20:, -1].detach().numpy())
                print('temporal: model, observed')
                print(torch.floor(cell_number[-10, -20:]).detach().numpy())
                print(count_tensor[-10, -20:].detach().numpy())

            # Reduce the learning rate when loss is small
            if (loss.item() < self.loss_threshold):
                optimizer = optim.SGD(params=[params], lr=self.final_learning_rate, momentum=self.learning_momentum)

        # Format the spatial forecast and calculate the temporal forecast
        C = np.reshape(cell_number.data.numpy(), (Nx, Ny, Nt))
        self.spatial_forecast = np.ascontiguousarray(np.moveaxis(C, -1, 0))
        self.temporal_forecast = np.sum(self.spatial_forecast, axis=(1, 2))
        return self.temporal_forecast, self.spatial_forecast
