import numpy as np
import pandas as pd
import itertools
import torch
from torchmin import minimize
from orion.forecast_models.saif._peak_detector import pk_indxs
from orion.forecast_models.saif._utilities import extract_sections
from orion.forecast_models.saif._batch_model import BatchCRSModel
from orion.forecast_models.saif._optimizer import (generate_train_loss_fn, generate_test_loss_fn, ValidationCallback)
from orion.forecast_models import forecast_model_base
from orion.managers.manager_base import recursive


class SAIF_CRS_Interface(forecast_model_base.ForecastModel):

    def set_class_options(self, **kwargs):
        """
        SAIF model initialization

        """
        # Call the parent's setup
        super().set_class_options(**kwargs)

        # Initialize model-specific parameters
        self.short_name = 'SAIF'
        self.long_name = 'Seismic Activity due to Injection Forecast'

        # Forcast variables
        self.mu = 0.73
        self.background_rate_input = 0.1    # events/year
        self.tectonicShearStressingRate_input = 3.5e-4    # MPa/year
        self.tectonicNormalStressingRate_input = 0    # MPa/year
        self.rate_coefficient = 0.002
        self.alpha = 0.155
        self.sigma_input = 30.0    # MPa
        self.biot = 0.3
        self.rate_factor_input = 163.7424    # 1/MPa

        self.sigma = 0.0
        self.tectonicShearStressingRate = 0.0
        self.tectonicNormalStressingRate = 0.0
        self.background_rate = 0.0
        self.rate_factor = 0.0
        self.enable_clustering = False

        # Partitioning
        self.do_partition = True
        self.signal_to_partition = 'rate'
        self.peak_detect_threshold = 0.025
        self.peak_detect_min_dist = 10
        self.percent_train_input = 90.0
        self.percent_train = 1.0

        # Optimization
        self.optimizer_method = 'newton-exact'
        self.max_iterations = 10
        self.optimize_disp = 2
        self.learning_rate = 1e-3
        self.loss_threshold = 1.0

    def set_data(self, **kwargs):
        """
        Setup data holders
        """
        super().set_data(**kwargs)
        self.train_cutoff = 0
        self.init_params = torch.FloatTensor([[0.5, 1e-3, 1e-4]])
        self.lower_bounds = torch.zeros(3)[None, :] + 1e-7
        self.upper_bounds = torch.ones(3)[None, :] - 1e-7
        self.data_df = None
        self.model = None

    def set_gui_options(self, **kwargs):
        """
        Setup interface options
        """
        super().set_gui_options(**kwargs)

        # Add the variable to the gui
        self.gui_elements['mu'] = {
            'element_type': 'entry',
            'label': u'\u03BC: Coefficient of Friction',
            'units': '(0-1)',
            'position': [4, 0]
        }

        self.gui_elements['sigma_input'] = {
            'element_type': 'entry',
            'label': 'Normal Stress',
            'position': [4, 1],
            'units': '(MPa)'
        }

        self.gui_elements['rate_coefficient'] = {
            'element_type': 'entry',
            'label': 'Rate-coefficient',
            'position': [5, 0]
        }

        self.gui_elements['biot'] = {'element_type': 'entry', 'label': 'Biot-Coefficient', 'position': [5, 1]}

        self.gui_elements['alpha'] = {
            'element_type': 'entry',
            'label': u'\u03B1: [Normal Stress Dependency]',
            'position': [6, 0],
            'units': '(0-\u03BC)'
        }

        self.gui_elements['tectonicShearStressingRate_input'] = {
            'element_type': 'entry',
            'label': 'Tectonic Shear Stressing Rate',
            'position': [6, 1],
            'units': '(MPa/year)'
        }

        self.gui_elements['tectonicNormalStressingRate_input'] = {
            'element_type': 'entry',
            'label': 'Tectonic Normal Stressing Rate',
            'position': [7, 0],
            'units': '(MPa/year)'
        }

        self.gui_elements['background_rate_input'] = {
            'element_type': 'entry',
            'label': 'Background Seismicity Rate',
            'position': [7, 1],
            'units': '(events/year)'
        }

        self.gui_elements['rate_factor_input'] = {
            'element_type': 'entry',
            'label': 'Rate Scaling Factor',
            'position': [8, 0],
            'units': '(1/MPa)'
        }
        self.gui_elements['max_iterations'] = {
            'element_type': 'entry',
            'label': 'Optimization Steps',
            'position': [9, 0]
        }
        self.gui_elements['loss_threshold'] = {'element_type': 'entry', 'label': 'Loss threshold', 'position': [9, 1]}
        self.gui_elements['percent_train_input'] = {
            'element_type': 'entry',
            'label': 'Training percent',
            'position': [10, 0]
        }
        self.gui_elements['learning_rate'] = {'element_type': 'entry', 'label': 'earning rate', 'position': [10, 1]}
        self.gui_elements['peak_detect_threshold'] = {
            'element_type': 'entry',
            'label': 'Peak threshold',
            'position': [11, 0]
        }
        self.gui_elements['peak_detect_min_dist'] = {
            'element_type': 'entry',
            'label': 'Peak distance',
            'position': [11, 1]
        }

    @recursive
    def process_inputs(self):
        """
        Process any required gui inputs
        """
        mpa_yr2pa_s = 1e6 / 365.25 / 86400    # MPa/year to Pa/s
        self.sigma = self.sigma_input * 1e6    # Pa
        self.tectonicShearStressingRate = self.tectonicShearStressingRate_input * mpa_yr2pa_s    # Pa/s
        self.tectonicNormalStressingRate = self.tectonicNormalStressingRate_input * mpa_yr2pa_s    # Pa/s
        self.background_rate = self.background_rate_input / (365.25 * 86400)    # event/second
        self.rate_factor = self.rate_factor_input / 1e6    # 1/Pa
        self.percent_train = min(max(self.percent_train_input / 100.0, 0.0), 1.0)

    def set_lower_bounds(self, lower_bounds):
        self.lower_bounds = lower_bounds

    def set_upper_bounds(self, upper_bounds):
        self.upper_bounds = upper_bounds

    def set_init_params(self, init_params):
        self.init_params = init_params

    @property
    def train_df(self):
        return self.data_df.iloc[:self.train_cutoff]

    @property
    def test_df(self):
        return self.data_df.iloc[self.train_cutoff:]

    def infer_partition(self):
        if self.do_partition:
            self.pks = pk_indxs(self.train_df[self.signal_to_partition],
                                trshd=self.peak_detect_threshold,
                                min_dist=self.peak_detect_min_dist)
        else:
            self.pks = np.array([])

        self.pks = self.pks.astype(int)
        all_sections_idx = np.append(self.pks, self.train_cutoff)
        signal_starts, signal_sections = extract_sections(self.data_df, all_sections_idx)

        self.signal_starts = signal_starts
        self.signal_sections = signal_sections

    def fit(self):
        scale = self.train_df.number.values.std()

        param_projector, train_loss_fn = generate_train_loss_fn(self.signal_sections, self.signal_starts, self.model,
                                                                self.lower_bounds, self.upper_bounds, scale)

        test_loss_fn = generate_test_loss_fn(self.signal_sections, self.signal_starts, self.model, param_projector,
                                             scale)

        self.callback = ValidationCallback(train_loss_fn, test_loss_fn)

        init_pre_params = param_projector.inverse(self.init_params)

        result = minimize(train_loss_fn,
                          init_pre_params,
                          method=self.optimizer_method,
                          max_iter=self.max_iterations,
                          disp=self.optimize_disp,
                          options={'lr': self.learning_rate},
                          callback=self.callback)
        self.result = result

        self.fitted_params = param_projector(result.x)
        return self.fitted_params

    def evaluate_model(self):
        mask = (self.signal_sections['number'] != -1).float()
        _, Nt = self.model.masked_forward(self.fitted_params,
                                          p=self.signal_sections['pressure'],
                                          dpdt=self.signal_sections['dpdt'],
                                          delta_t=self.signal_sections['delta_t'],
                                          R0=self.signal_starts['rate'],
                                          mask=mask)
        return Nt

    def compute_delta_cfs_terms(self):
        pass

    def generate_forecast(self, grid, seismic_catalog, pressure, wells, geologic_model):
        # Get the catalog rates
        count = seismic_catalog.spatial_count.copy()
        spatial_rate = seismic_catalog.spatial_rate.copy()
        max_magnitude = seismic_catalog.spatial_max_magnitude.copy()
        spatial_rate[spatial_rate < self.background_rate] = self.background_rate
        temporal_count = np.sum(count, axis=(1, 2))
        temporal_rate = np.sum(spatial_rate, axis=(1, 2))
        temporal_mmax = np.amax(max_magnitude, axis=(1, 2))

        # Get the pressure
        Imax = np.unravel_index(np.argmax(count), np.shape(count))
        # p = pressure.p_grid.copy()[Imax[1], Imax[2], 0, :]
        # dpdt = pressure.dpdt_grid.copy()[Imax[1], Imax[2], 0, :]
        p = np.amax(pressure.p_grid, axis=(0, 1, 2))
        dpdt = np.amax(pressure.dpdt_grid, axis=(0, 1, 2))

        # Get other information
        t = grid.t.copy()
        t_days = (t - t[0]) / (60 * 60 * 24.0)
        dt = grid.t[1:] - grid.t[:-1]
        dt = np.concatenate([dt[:1], dt], axis=0)
        data = {
            'epoch': t,
            'days': t_days,
            'delta_t': dt,
            'pressure': p,
            'dpdt': dpdt,
            'number': temporal_count,
            'rate': temporal_rate,
            'magnitude': temporal_mmax
        }
        self.data_df = pd.DataFrame(data)

        # Setup the initial model
        self.init_params = torch.FloatTensor([[
            self.mu - self.alpha,
            self.rate_coefficient,
            self.rate_factor,
        ]])
        site_info = {
            'tectonic_shear_stressing_rate': self.tectonicShearStressingRate,
            'tectonic_normal_stressing_rate': self.tectonicNormalStressingRate,
            'sigma': self.sigma,
            'biot': self.biot
        }
        self.model = BatchCRSModel(site_info)

        # Evaluate the forecast
        Nx, Ny, Nz, Nt = grid.shape
        self.train_cutoff = max(min(int(np.floor(len(t) * self.percent_train)), Nt - 1), 1)
        self.infer_partition()
        self.fit()

        # Unpack the segments
        tmp = self.evaluate_model().numpy()[0, ...]
        Ns = np.shape(tmp)[0]
        segment_end = []
        for ii in range(Ns):
            Ia = np.where(tmp[ii, :] > 0.0)[0]
            if len(Ia):
                segment_end.append(Ia[-1])
            else:
                segment_end.append(0)
        segment_end = np.array(segment_end, dtype=int)
        tmp_b = np.concatenate([tmp[ii, :segment_end[ii]] for ii in range(Ns)], axis=0)
        Nevents = np.zeros(Nt)
        Nevents[:len(tmp_b)] = tmp_b
        Nevents[len(tmp_b):] = tmp_b[-1]

        # Build the weighted spatial forecast
        N = np.sum(seismic_catalog.spatial_count[-1, :, :])
        self.spatial_forecast = np.zeros((Nt, Nx, Ny))
        for ii, jj in itertools.product(range(Nx), range(Ny)):
            self.spatial_forecast[:, ii, jj] = Nevents * seismic_catalog.spatial_count[-1, ii, jj] / N

        self.temporal_forecast = Nevents

        return self.temporal_forecast, self.spatial_forecast
