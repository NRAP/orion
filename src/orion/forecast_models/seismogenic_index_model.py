# ------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2020-, Lawrence Livermore National Security, LLC
# All rights reserved
#
# See top level LICENSE, COPYRIGHT, CONTRIBUTORS, NOTICE, and ACKNOWLEDGEMENTS files for details.
# ------------------------------------------------------------------------------------------------
"""
seismogenic_index_model.py
----------------------------
"""

import numpy as np
from orion.forecast_models import forecast_model_base
from orion.utilities import plot_tools
from orion.utilities.plot_config import gui_colors
from orion.managers.manager_base import recursive
from scipy.signal import windows
from scipy.ndimage import convolve


class SeismogenicIndexModel(forecast_model_base.ForecastModel):
    """
    Seismogenic Index forecast model
    """

    def set_class_options(self, **kwargs):
        """
        Seismogenic Index model initialization

        """
        super().set_class_options(**kwargs)

        # Initialize model-specific parameters
        self.short_name = 'SI'
        self.long_name = 'Seismogenic Index Model'

        # default parameters
        self.mu = 0.6
        self.tectonicShearStressingRate_input = 7e-4    ## MPa/yr
        self.tectonicNormalStressingRate_input = 1e-6    ## MPa/yr

        self.tectonicShearStressingRate = 1.0    # Pa/s
        self.tectonicNormalStressingRate = 1.0    # Pa/s

        # self.training_percentage = 100.
        # self.training_event_count = 100. ## default but should probably be the entire value, i.e. sum(final event count step)
        self.smoothing_kernel = 5.

    def set_gui_options(self, **kwargs):
        """
        Setup interface options
        """
        super().set_gui_options(**kwargs)

        # Add the variable to the gui
        self.gui_elements['mu'] = {
            'element_type': 'entry',
            'label': u'\u03BC: Coefficient of Friction',
            'units': '(0-1)',
            'position': [4, 0]
        }

        self.gui_elements['tectonicShearStressingRate_input'] = {
            'element_type': 'entry',
            'label': u'\u03C4: Tectonic Shear Stressing Rate',
            'position': [5, 0],
            'units': '(MPa/year)'
        }

        self.gui_elements['tectonicNormalStressingRate_input'] = {
            'element_type': 'entry',
            'label': u'\u03C3: Tectonic Normal Stressing Rate',
            'position': [6, 0],
            'units': '(MPa/year)'
        }

        # self.gui_elements['training_percentage'] = {
        #     'element_type': 'slider',
        #     'label': 'Training Percentage',
        #     'min': 0.,
        #     'max': 3000., ## default 100
        #     'position': [7, 0]
        # }

        self.gui_elements['smoothing_kernel'] = {
            'element_type': 'slider',
            'label': 'Smoothing Kernel',
            'min': 1,
            'max': 10,
            'step': 1,
            'position': [8, 0]
        }

    @recursive
    def process_inputs(self):
        """
        Process any gui inputs, i.e. unit conversions
        """

        mpa_yr2pa_s = 1e6 / (365.25 * 86400)    # MPa/year to Pa/s
        self.tectonicShearStressingRate = self.tectonicShearStressingRate_input * mpa_yr2pa_s    # Pa/s
        self.tectonicNormalStressingRate = self.tectonicNormalStressingRate_input * mpa_yr2pa_s    # Pa/s

    def Coulomb_stressing_rate(self, dpdt):
        """
        Calculates the Coulomb stressing rate from dpdt

        Args:
            dpdt (numpy.ndarray): pressurisation rate (Pa/s)

        Returns:
            sDot (numpy.ndarray): Coulomb stressing rate (MPa/yr)
        """

        sDot = self.tectonicShearStressingRate - self.mu * (self.tectonicNormalStressingRate - dpdt)
        return sDot

    def seismogenic_index(self, dpdt, event_count, b_value, magnitude_completeness, smoothing_kernel=5):
        """
        Calculates the seismogenic index prior to or during injection

        Args:
            dpdt (np.array): pore-fluid pressurization rate (Pa/s)
            event_count (np.ndarray): Seismicity event count (count)
            b_value (float): catalog b-value
            magnitude_completeness (float): catalog magnitude of completeness
            smoothing_kernel (int): size of kernel for smoothing
            # training_percentage (int): %tage of data used for SI calibration
            # training_event_count (int): number of events used for SI calibration 

        Returns:
            si (np.ndarray): Seismogenic index
            sirate (np.ndarray): event rate
            cumulative_no (np.ndarray): Cumulative number of forecasted events

        """

        Nt = len(event_count)
        final_event_count = event_count.copy()    ## shape 100, x, y (i.e. for every time step)
        training_event_count = np.arange(0, 2200, 100)

        k = windows.gaussian(smoothing_kernel, std=1)
        k = np.reshape(k, (-1, 1)) * np.reshape(k, (1, -1))
        k /= np.sum(k)

        N_smooth = np.zeros_like(final_event_count)

        for t in range(final_event_count.shape[0]):
            N_smooth[t, :, :] = convolve(final_event_count[t, :, :], k, mode='nearest')

        self.N_smooth = N_smooth    ## so can be accessed in an external script

        # training_index = int((training_percentage / 100) * (Nt))

        sDot_squared = self.Coulomb_stressing_rate(dpdt**2)
        sDot_squared_cumsum = np.cumsum(sDot_squared, axis=0)

        # cumulative_sums = np.sum(final_event_count, axis=(1,2))

        SI = np.zeros_like(dpdt)
        SIrate = np.zeros_like(dpdt)
        cumulative_events = np.zeros_like(dpdt)

        for x in range(event_count.shape[1]):
            for y in range(event_count.shape[2]):
                for t in range(event_count.shape[0]):
                    if N_smooth[t, x, y] == 0:
                        SI[t, x, y] = np.log10(sDot_squared_cumsum[t, x, y])
                    else:
                        SI[t, x, y] = np.log10(N_smooth[t, x, y]) - np.log10(
                            sDot_squared_cumsum[t, x, y]) + (b_value * magnitude_completeness)
                # SIrate[:, x, y] = sDot_squared[:, x, y] * 10**((SI[training_index - 1, x, y]) - ## training_index - 1 for %t
                #    (b_value * magnitude_completeness))
                ##TODO: This should be separately so we don't have to calibrate again every time.
                SIrate[:, x, y] = sDot_squared[:, x, y] * 10**((SI[t - 1, x, y]) -    ## training_index - 1 for %t
                                                               (b_value * magnitude_completeness))
                cumulative_events[:, x, y] = np.cumsum(SIrate[:, x, y], axis=0)

        return SI, SIrate, cumulative_events

    def generate_forecast(self, grid, seismic_catalog, pressure, wells, geologic_model):
        """
        Model forecast run function.
        """
        Nx, Ny, Nz, Nt = grid.shape

        self.spatial_forecast = np.zeros((Nt, Nx, Ny))

        b_value = seismic_catalog.b_value
        magnitude_completeness = seismic_catalog.magnitude_completeness

        event_count = seismic_catalog.spatial_count
        dpdt = np.moveaxis(pressure.dpdt_grid[:, :, 0, :], -1, 0)

        si, sirate, cumulative_no_events = self.seismogenic_index(dpdt,
                                                                  event_count,
                                                                  b_value,
                                                                  magnitude_completeness,
                                                                  smoothing_kernel=int(self.smoothing_kernel))

        self.spatial_forecast = cumulative_no_events
        self.temporal_forecast = np.sum(self.spatial_forecast, axis=(1, 2))

        return self.temporal_forecast, self.spatial_forecast
