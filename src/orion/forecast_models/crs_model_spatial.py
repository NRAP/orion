# ------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2020-, Lawrence Livermore National Security, LLC
# All rights reserved
#
# See top level LICENSE, COPYRIGHT, CONTRIBUTORS, NOTICE, and ACKNOWLEDGEMENTS files for details.
# ------------------------------------------------------------------------------------------------
"""
coupled_coulomb_rate_state_model.py
--------------------------------------
"""

from orion.forecast_models import forecast_model_base
import numpy as np
from orion.managers.manager_base import recursive
from sklearn.cluster import DBSCAN


class CRSModel_Spatial(forecast_model_base.ForecastModel):
    """
    CRS forecast model

    Attributes:
        active (bool): Flag to indicate whether the model is active
        pressure_method (str): Pressure calculation method
        forecast_time (ndarray): Time values for forecast calculation
    """

    def set_class_options(self, **kwargs):
        """
        CRS model initialization

        """
        # Call the parent's setup
        super().set_class_options(**kwargs)

        # Initialize model-specific parameters
        self.short_name = 'CRS_Spatial'
        self.long_name = 'Coupled Coulomb Rate-State Spatial Model'

        # Forecast variables
        self.muPrime = 0.53    # effective coefficient of friction muPrime = (mu - alpha)
        self.background_rate_input = 0.032686    # events/year
        self.tectonicShearStressingRate_input = 0.00832325    # MPa/year
        self.tectonicNormalStressingRate_input = 0    # MPa/year
        self.rate_coefficient = 0.0015
        self.sigma_input = 9.0    #MPa
        self.biot = 0.2
        self.rate_factor_input = 150.0    # 1/MPa
        self.initial_stress_change = 0.0    # Pa

        # Enable earthquake clustering
        self.enable_clustering = True

        # training data ratio
        # TODO: implement training data ratio
        # self.training_data_ratio = 1.0

        # Peak detection parameters
        self.rate_density_lag_input = 1.0    # lag for calculating rate density (% of total data points)
        self.detect_peak_threshold = 2.0    # 2 standard deviations, peak is detetced when the value is higher than the threshold*std
        self.detect_peak_influence = 0.01    # 1% influence, the influence of a peak to the mean and standard deviation, 0 if assuming stationary, 1 if assuming permanent influence.

        # trivial coulomb stress change
        self.trivial_coulomb_stress = 0.1    # Pa

        # use fast method
        self.use_fast_method = True

    def set_gui_options(self, **kwargs):
        """
        Setup interface options
        """
        super().set_gui_options(**kwargs)

        # Add the variable to the gui
        self.gui_elements['enable_clustering'] = {
            'element_type': 'check',
            'label': 'Enable Clustering Effects',
            'position': [3, 0]
        }

        # use fast method
        self.gui_elements['use_fast_method'] = {'element_type': 'check', 'label': 'Use Fast Method', 'position': [3, 1]}
        # TODO: implement training data ratio
        # self.gui_elements['training_data_ratio'] = {
        #     'element_type': 'entry',
        #     'label': '% of Training Data',
        #     'units': '(0-1)',
        #     'position': [3, 1]
        # }

        self.gui_elements['rate_coefficient'] = {
            'element_type': 'entry',
            'label': 'Rate-coefficient',
            'position': [4, 0]
        }

        self.gui_elements['rate_factor_input'] = {
            'element_type': 'entry',
            'label': 'Rate Scaling Factor',
            'position': [4, 1],
            'units': '(1/MPa)'
        }

        self.gui_elements['muPrime'] = {
            'element_type': 'entry',
            'label': u'\u03BC: Effective Friction',
            'units': '(0-1)',
            'position': [5, 0]
        }

        self.gui_elements['sigma_input'] = {
            'element_type': 'entry',
            'label': 'Normal Stress',
            'position': [5, 1],
            'units': '(MPa)'
        }

        self.gui_elements['biot'] = {'element_type': 'entry', 'label': 'Biot-Coefficient', 'position': [5, 2]}

        self.gui_elements['tectonicShearStressingRate_input'] = {
            'element_type': 'entry',
            'label': 'Tectonic Shear Stressing Rate',
            'position': [6, 0],
            'units': '(MPa/year)'
        }

        self.gui_elements['tectonicNormalStressingRate_input'] = {
            'element_type': 'entry',
            'label': 'Tectonic Normal Stressing Rate',
            'position': [6, 1],
            'units': '(MPa/year)'
        }

        self.gui_elements['background_rate_input'] = {
            'element_type': 'entry',
            'label': 'Background Seismicity Rate',
            'position': [7, 0],
            'units': '(events/year)'
        }

        self.gui_elements['rate_density_lag_input'] = {
            'element_type': 'entry',
            'label': 'Rate Density Window',
            'position': [8, 0],
            'units': '(% data points)'
        }
        self.gui_elements['detect_peak_threshold'] = {
            'element_type': 'entry',
            'label': 'Cluster Detection Threshold',
            'position': [8, 1],
            'units': '(std multiples)'
        }
        self.gui_elements['detect_peak_influence'] = {
            'element_type': 'entry',
            'label': 'Cluster Detection Influence',
            'position': [8, 2],
            'units': '(0.0, 1.0)'
        }
        self.gui_elements['trivial_coulomb_stress'] = {
            'element_type': 'entry',
            'label': 'Trivial Coulomb Stress',
            'position': [9, 0],
            'units': '(Pa)'
        }

    @recursive
    def process_inputs(self):
        """
        Process any required gui inputs
        """
        mpa_yr2pa_s = 1e6 / 365.25 / 86400    # MPa/year to Pa/s
        self.sigma = self.sigma_input * 1e6    # Pa
        self.tectonicShearStressingRate = self.tectonicShearStressingRate_input * mpa_yr2pa_s    # Pa/s
        self.tectonicNormalStressingRate = self.tectonicNormalStressingRate_input * mpa_yr2pa_s    # Pa/s
        self.background_rate = self.background_rate_input / 365.25 / 86400    # event/second
        if self.rate_factor_input == 0:
            """
            rate_factor (float): proportional to the background seismicity rate divided by the reference
                    tectonic stressing. Background rate is the steady event rate that would 
                    be produced by constant stressing at the reference stressing rate) 
                    (units: 1/stress rate; with stress in same units as the stress units 
                    in sigma_effective; Pa/second by default)
            """
            self.logger.warning('Rate scaling factor is zero. Setting to 1.0')
            self.rate_factor = 1.0
        else:
            self.rate_factor = self.rate_factor_input / 1e6    # 1/Pa
        if self.rate_density_lag_input == None:
            self.rate_density_lag_input = 1.0
            self.logger.warning('Rate density lag is not set. Setting to 1 percent of forcast time data series.')
        self.rate_density_lag = self.rate_density_lag_input / 100    # lag for calculating rate density (% of total data points)

    def SigmaEffective(self, pressure):
        """
        Effective (time-dependent) normal stress that varies with pressure

        Args:
            sigma (float): initial normal stress (Pa)
            biot (float): biot coefficient
            pressure (numpy array): pore-fluid pressure time series (Pa)

        Returns:
            numpy array: effective normal stress time series
        """
        return (self.sigma - (self.biot * pressure))

    def CoulombStressingRate(self, dpdt):
        """
        Effective (time-dependent) normal stress that varies with pressure

        Args:
            dpdt (numpy array): pore-fluid pressurization rate time
                series (Pa/s)

        Returns:
            numpy array: Coulomb stressing rate time series

        """
        return (self.tectonicShearStressingRate - (self.muPrime * (self.tectonicNormalStressingRate - dpdt)))

    def InstantRateChange(self, coulomb_stress_change, rate_at_prev_step, sigma_effective):
        """
        Instantaneous change in Rate (R) due to step change in Coulomb stress

        Args:
            coulomb_stress_change (float): Coulomb stress step (Pa by default)
            background_rate (float): long-term background seismicity rate before stress step (seconds by default)
            rate_coefficient (float): rate-coefficient in rate-state formulation
            sigma_effective (numpy array): effective normal stress (Pa; sigma_effective = sigma - biot*pressure)

        Returns:
            float: Instantaneous change in rate
        
        """
        asigma = max(self.rate_coefficient * sigma_effective, 1e-4)
        temp = min(coulomb_stress_change / asigma, 700)    # to avoid overflow
        return (rate_at_prev_step * np.exp(temp))

    def InterseismicRate(self, forecast_time, coulomb_stressing_rate, rate_at_prev_step, sigma_effective):
        """
        Evolution of the Rate during a time of constant stressing rate

        Args:
            forecast_time (numpy array): Times at which number should be computed (units:
                            same as the time units in sigma_effective; seconds by default)
            coulomb_stressing_rate(numpy array): Constant Coulomb stressing rate (units: same stress units
                               as used in eta, same time units as t; Pa/second by default)
            rate_at_prev_step (float): event rate at the previous time step (units: events/time, same time units
                        as forecast_time (seconds by default))
            sigma_effective (numpy array): effective normal stress (Pa; sigma_effective = sigma - biot*pressure)

        Returns:
            numpy array: interseismic rate
        """
        asigma = max(self.rate_coefficient * sigma_effective, 1e-4)
        eta = 1.0 / self.rate_factor
        if (coulomb_stressing_rate == 0):
            return (1.0 / (1.0 / rate_at_prev_step + eta * forecast_time / asigma))
        else:
            temp = min(-coulomb_stressing_rate * forecast_time / asigma, 700)    # to avoid overflow
            x = np.exp(temp)
            return (1.0 / (eta / coulomb_stressing_rate + (1.0 / rate_at_prev_step - eta / coulomb_stressing_rate) * x))

    def InterseismicNumber(self, forecast_time, coulomb_stressing_rate, rate_at_prev_step, sigma_effective):
        """
        Compute expected total number of events during a time of constant stressing rate

        Args:
            forecast_time (numpy array): np.array of times at which number should be computed
                         (units: same as the time units in sigma_effective; seconds by default)
            coulomb_stressing_rate(numpy array): np.array of constant Coulomb stressing rate (units: same stress units
                                             as used in eta, same time units as forecast_time; Pa/second by default)
            rate_at_prev_step (float): event rate at the previous time step (units: events/time, same time units
                                      as forecast_time (seconds by default))
            rate_coefficient (float): rate-coefficient in rate-state formulation
            sigma_effective (numpy array): effective normal stress (Pa; sigma_effective = sigma - biot*pressure)

        Returns:
            numpy array: interseismic number
        """
        asigma = max(self.rate_coefficient * sigma_effective, 1e-4)
        eta = 1.0 / self.rate_factor
        if (coulomb_stressing_rate == 0):
            return ((asigma / eta) * np.log(1 + rate_at_prev_step * eta * forecast_time / asigma))
        else:
            temp = min(coulomb_stressing_rate * forecast_time / asigma, 700)    # to avoid overflow
            x = np.exp(temp)
            x *= (rate_at_prev_step * eta / coulomb_stressing_rate)
            return ((asigma / eta) * np.log((1 - rate_at_prev_step * eta / coulomb_stressing_rate) + x))

    def RateEvolution(self, forecast_time, time_of_static_stress_step, coulomb_stress_change, coulomb_stressing_rate,
                      sigma_effective):
        """
        Calculate the earthquake rate for all time steps passed into forecast_time.
        forecast_time and time_of_static_stress_step  must have the same time units.

        Instantaneous stress step vector (static_coulomb_stress_change) must be [length(sigma_effective) - 1]
        (stressing rate vector) beginning at the time of the first change

        Args:
            forecast_time (numpy array): Times at which number should be computed
                          (units: same as the time units in sigma_effective; seconds by default)
            time_of_static_stress_step (np.array): Times of the stress steps (seconds by default)
            coulomb_stress_change (numpy array): Amplitude of the stress steps (+/- ; Pa by default)
                           must be the same length as time_of_static_stress_step
            coulomb_stressing_rate: (numpy array): constant Coulomb stressing rate (units: same stress units
                              as used in eta, same time units as t; Pa/second by default)
                              len(sigma_effective) must equal len(forecast_time)
            sigma_effective (numpy array: effective normal stress (Pa; sigma_effective = sigma - biot*pressure)

        Returns:
           R (numpy array): Rate evolution
        
        """
        R = np.empty(len(forecast_time))
        eta = 1.0 / self.rate_factor

        if self.enable_clustering:
            if len(forecast_time) != len(sigma_effective):
                self.logger.error("forecast_time must be the same length as sigma_effective")
                raise ValueError("forecast_time must be the same length as sigma_effective")

            if len(coulomb_stress_change) != len(time_of_static_stress_step):
                self.logger.error("time_of_static_stress_step must be the same length as static_coulomb_stress_change")
                raise ValueError("time_of_static_stress_step must be the same length as static_coulomb_stress_change")

            # Separate the time array into individual periostatic_coulomb_stress_change defining the interseismic period
            # and the time of the stress steps
            mask_to_apply_stress_change = np.digitize(forecast_time, bins=time_of_static_stress_step, right=True)
            R[mask_to_apply_stress_change == 0] = self.background_rate

            # Loop over all times, t, check if t[i] is the time of a stress step. If it is,
            # compute the instantaneous rate change. If it is not, then compute the gamma evolution
            # due to changes in sigma_effective
            for i in range(1, len(forecast_time)):
                dt = forecast_time[i] - forecast_time[i - 1]
                if mask_to_apply_stress_change[i] != mask_to_apply_stress_change[i - 1]:
                    R[i] = self.InstantRateChange(coulomb_stress_change[mask_to_apply_stress_change[i - 1]], R[i - 1],
                                                  sigma_effective[i])
                else:
                    R[i] = self.InterseismicRate(dt, coulomb_stressing_rate[i], R[i - 1], sigma_effective[i])
        else:
            R[0] = self.background_rate
            for i in range(1, len(forecast_time)):
                dt = forecast_time[i] - forecast_time[i - 1]
                R[i] = self.InterseismicRate(dt, coulomb_stressing_rate[i], R[i - 1], sigma_effective[i])
        return R

    def NumberEvolution(self, forecast_time, time_of_static_stress_step, coulomb_stress_change, coulomb_stressing_rate,
                        sigma_effective):
        """
        Calculate the number of events for all times passed into forecast_time.
        forecast_time and time_of_static_stress_step  must all have the same units of time.

        Instantaneous stress step vector (static_coulomb_stress_change) must be [length(sigma_effective) - 1]
        (stressing rate vector) beginning at the time of the first change

        Args:
            forecast_time (numpy array): Times at which number should be computed
                                      (units same as the time units in sigma_effective; seconds by default)
            time_of_static_stress_step (numpy array): Times of the stress steps (seconds by default)
            coulomb_stress_change (numpy array): Amplitude of the stress steps (+/- ; Pa by default)
                                                     must be the same length as time_of_static_stress_step
            coulomb_stressing_rate(numpy array): Constant Coulomb stressing rate (units: same stress units
                                              as used in eta, same time units as t; Pa/second by default)
            sigma_effective (numpy array): effective normal stress (Pa; sigma_effective = sigma - biot*pressure,
                                        same length as forecast_time)

        Returns:
            N (numpy array): Number evolution
        
        """
        N = np.empty(len(forecast_time))
        R = np.empty(len(forecast_time))
        eta = 1 / self.rate_factor

        if self.enable_clustering:
            if len(forecast_time) != len(sigma_effective):
                self.logger.error("forecast_time must be the same length as sigma_effective")
                raise ValueError("forecast_time must be the same length as sigma_effective")

            if len(coulomb_stress_change) != len(time_of_static_stress_step):
                self.logger.error("time_of_static_stress_step must be the same length as static_coulomb_stress_change")
                raise ValueError("time_of_static_stress_step must be the same length as static_coulomb_stress_change")

            # Separate the time array into individual periostatic_coulomb_stress_change defining the interseismic period
            # and the time of the stress steps
            mask_to_apply_stress_change = np.digitize(forecast_time, bins=time_of_static_stress_step, right=True)

            useT = np.where(mask_to_apply_stress_change == 0)
            R[useT] = self.background_rate
            N[useT] = self.background_rate * (forecast_time[useT] - forecast_time[0])

            for i in range(1, len(forecast_time)):
                dt = forecast_time[i] - forecast_time[i - 1]
                if mask_to_apply_stress_change[i] != mask_to_apply_stress_change[i - 1]:
                    R[i] = self.InstantRateChange(coulomb_stress_change[mask_to_apply_stress_change[i - 1]], R[i - 1],
                                                  sigma_effective[i])
                    N[i] = N[i - 1] + self.InterseismicNumber(dt, coulomb_stressing_rate[i], R[i - 1],
                                                              sigma_effective[i])
                else:
                    R[i] = self.InterseismicRate(dt, coulomb_stressing_rate[i], R[i - 1], sigma_effective[i])
                    N[i] = N[i - 1] + self.InterseismicNumber(dt, coulomb_stressing_rate[i], R[i - 1],
                                                              sigma_effective[i])
        else:
            R[0] = self.background_rate
            N[0] = self.background_rate * (forecast_time[1] - forecast_time[0])

            for i in range(1, len(forecast_time)):
                dt = forecast_time[i] - forecast_time[i - 1]
                R[i] = self.InterseismicRate(dt, coulomb_stressing_rate[i], R[i - 1], sigma_effective[i])
                N[i] = N[i - 1] + self.InterseismicNumber(dt, coulomb_stressing_rate[i], R[i - 1], sigma_effective[i])
        return N

    def rate_density(self, x, lag):
        """Calculate the rate density of the input array

        Args:
            x (numpy array): input array (event time in catalog).
            self.lag (int): window for calculating rates in x.
        
        Returns:
            x_lag (numpy array): lag modified input array
            P (numpy array): rate density of input x
        """
        N = len(x)
        if x.dtype == 'int32':
            x = x.astype(float)    # convert x to float if x is integer
        x = np.sort(x)    # reorder x if x is not an acending array
        x_mid = 0.5 * (x[1:] + x[:-1])    # midway between each array element
        x_lag = x_mid[:-lag] + (x_mid[lag:] - x_mid[:-lag]) / 2    # midpoint of estimation intervals
        weight = np.ones(N)
        cumulated_weight = np.cumsum(weight)[:-1]    # cumulated w
        P = (cumulated_weight[lag:] - cumulated_weight[:-lag]) / (x_mid[lag:] - x_mid[:-lag])
        P = (P - P.min()) / (P.max() - P.min())    # normalize P to [0,1]
        return x_lag, P

    def detect_peak_zscore(self, Y, detect_peak_window):
        """
        Detect peaks in the input array using a moving "z_score" algorithm
        The algorithem is described in this post: 
        https://stackoverflow.com/questions/22583391/peak-signal-detection-in-realtime-timeseries-data

        Args:
            Y (numpy array): input array
            detect_peak_window (int): number of contiguous data points for calculating mean and standard deviation

        Returns:
            peak_index (list): the indices of selected peaks in the input array
        """
        peak_index = []
        filteredY = np.zeros(len(Y))
        filteredY[:detect_peak_window] = Y[:detect_peak_window]
        avg = np.mean(Y[:detect_peak_window])
        std = np.std(Y[:detect_peak_window])
        for i in range(detect_peak_window + 1, len(Y)):
            # Negative peaks are not considered, 1e-13 is a small number to avoid false detection where std*threshold is zero
            if (Y[i] - avg) - self.detect_peak_threshold * std > 1e-13:
                peak_index.append(i)
                filteredY[i] = self.detect_peak_influence * Y[i] + (1 - self.detect_peak_influence) * filteredY[i - 1]
            else:
                filteredY[i] = Y[i]
            avg = np.mean(filteredY[i - detect_peak_window:i])
            std = np.std(filteredY[i - detect_peak_window:i])
        return peak_index

    def detect_peak_mean_cutoff(self, Y):
        """
        detect peaks if the value is greater than the mean of the entire array.

        Args:
            Y (numpy array): input array

        Returns:
            peak_index (list): the indices of selected peaks in the input array
        """
        peak_index = []
        avg = np.mean(Y)
        for i in range(1, len(Y)):
            if Y[i] > avg:
                peak_index.append(i)
        return peak_index

    def auto_cluster(self, peak_indices):
        """
        Perform clustering for detected rate changing periods (peaks).
        
        Args:
            peak_indices (numpy array): indices of detected peaks

        Returns:
            clusters (list): list of clusters (list) of peak indices
        """
        # Automatically group the detected peaks to clusters
        index_diff = np.diff(peak_indices)
        diff_threshold = np.mean(index_diff) + np.std(index_diff)    # threshold for grouping peaks
        clusters = []
        current_cluster = [peak_indices[0]]    # initialize the first cluster
        for i in range(0, len(index_diff)):
            if index_diff[
                    i] < diff_threshold:    # if the difference is less than the threshold, add to the current cluster
                current_cluster.append(peak_indices[i + 1])
            else:    # if the difference is greater than the threshold, start a new cluster
                clusters.append(current_cluster)
                current_cluster = [peak_indices[i + 1]]
        clusters.append(current_cluster)    # add the last cluster
        # fill the clusters with missing indices
        filled_clusters = []
        for cluster in clusters:
            filled_clusters.append(list(np.linspace(cluster[0], cluster[-1], cluster[-1] - cluster[0] + 1, dtype=int)))
        return filled_clusters

    def find_cell(self, grid, easting, northing):
        """
        Find a grid cell based on the northing and easting

        Args:
            grid (Grid): grid object
            northing (scalar): northing coordinate
            easting (scalar): easting coordinate

        Returns:
            i_cell (int): i index of the cell
            j_cell (int): j index of the cell
        """
        dx = np.diff(grid.x_nodes)[0]    # true grid cell size in x direction
        dy = np.diff(grid.y_nodes)[0]    # true grid cell size in y direction

        # find the cell index
        i_cell = int((easting - grid.x_origin - grid.x_min) // dx)
        if i_cell >= len(grid.x):    # if the easting is out of the grid, set the index to the last cell
            i_cell = len(grid.x) - 1
        elif i_cell < 0:    # if the easting is out of the grid, set the index to the first cell
            i_cell = 0
        j_cell = int((northing - grid.y_origin - grid.y_min) // dy)
        if j_cell >= len(grid.y):    # if the northing is out of the grid, set the index to the last cell
            j_cell = len(grid.y) - 1
        elif j_cell < 0:    # if the northing is out of the grid, set the index to the first cell
            j_cell = 0
        return i_cell, j_cell

    def find_affected_cells(self, centroid_easting, centroid_northing, radius, grid):
        """
        Find the affected cells based on the centroid and radius

        Args:
            centroid_easting (scalar): easting coordinate of the centroid
            centroid_northing (scalar): northing coordinate of the centroid
            radius (scalar): radius of the affected area
            grid (Grid): grid object

        Returns:
            affected_cells (list): list of affected cells
        """
        dx = np.diff(grid.x_nodes)[0]
        dy = np.diff(grid.y_nodes)[0]
        left_boundary = centroid_easting - radius
        right_boundary = centroid_easting + radius
        bottom_boundary = centroid_northing - radius
        top_boundary = centroid_northing + radius
        i_left = int((left_boundary - grid.x_origin - grid.x_min) // dx)
        i_right = int((right_boundary - grid.x_origin - grid.x_min) // dx)
        j_bottom = int((bottom_boundary - grid.y_origin - grid.y_min) // dy)
        j_top = int((top_boundary - grid.y_origin - grid.y_min) // dy)
        affected_cells = []
        for i in range(i_left, i_right + 1):
            for j in range(j_bottom, j_top + 1):
                if i >= 0 and i < len(grid.x) and j >= 0 and j < len(grid.y):
                    affected_cells.append((i, j))
                else:
                    continue
        return affected_cells

    def cal_equivalent_moment_magnitude(self, event_indices, catalog):
        """
        Calculate the equivalent moment magnitude for each sub-group

        Args:
            event_indices (list): list of event indices
            catalog (Catalog): seismic catalog object

        Returns:
            sum_seismic_moment (scalar): sum of seismic moment for all events in the sub-group
            equivalent_moment_magnitude (scalar): equivalent moment magnitude
        """
        sum_seismic_moment = 0
        for event_index in event_indices:
            magnitude = catalog.magnitude_slice[event_index]
            sum_seismic_moment += 10**(1.5 * magnitude + 9.1)
        equivalent_moment_magnitude = 2 / 3 * (np.log10(sum_seismic_moment) - 9.1)
        return sum_seismic_moment, equivalent_moment_magnitude

    def trivial_coulomb_stress_radius(self, seismic_moment):
        """
        Calculate the rupture radius when coulomb stress change is trivial

        Args:
            magnitude (scalar): equivalent moment magnitude

        Returns:
            rupture_radius (scalar): rupture radius
        """
        trivial_stress_change = self.trivial_coulomb_stress    # a trivial stress change in Pa
        trivial_radius = np.cbrt(seismic_moment / (6 * np.pi * trivial_stress_change))
        # trivial_radius = np.sqrt(seismic_moment/(np.pi*trivial_stress_change))
        return trivial_radius

    def get_clusters(self, grid, seismic_catalog):
        """
        Get the clusters of detected peaks, first find clusters in time, then find clusters in space by filtering out background events
        (background events: events that are not strongly associated with other events in space)
        """
        # find clusters in time
        time = seismic_catalog.epoch_slice
        lag = int(len(time) * self.rate_density_lag)
        if lag < 2:
            self.logger.debug(f"The number of events in the catalogs is {len(time)}, using a default window of 2...")
            lag = 2
        time_lag, P = self.rate_density(time, lag)
        padded_P = np.pad(P, pad_width=(lag, 0), mode='constant', constant_values=np.mean(P))
        peak_ids1 = self.detect_peak_zscore(Y=padded_P, detect_peak_window=lag)
        peak_ids2 = self.detect_peak_mean_cutoff(Y=padded_P)
        # remove indices smaller than lag
        peak_ids2 = [index for index in peak_ids2 if index > lag]
        peak_ids = sorted(set(peak_ids1 + peak_ids2))    # combine the detected peaks from two methods
        peak_ids = [index - lag for index in peak_ids]
        clusters = self.auto_cluster(peak_ids)
        # find closest time to the detected peak times
        clusters_ids_original_time = []
        for cluster in clusters:
            peak_times = time_lag[cluster]
            start_index = np.argmin(np.abs(time - peak_times[0]))
            end_index = np.argmin(np.abs(time - peak_times[-1]))
            peak_ids_original_time = np.linspace(start_index, end_index, end_index - start_index + 1,
                                                 dtype=int)    # remove duplicate indices
            clusters_ids_original_time.append(sorted(list(peak_ids_original_time)))
        # filter the clusters in space using DBSCAN
        mag = seismic_catalog.magnitude_slice

        easting = seismic_catalog.easting_slice - grid.x_origin
        northing = seismic_catalog.northing_slice - grid.y_origin

        cluster_labels = []
        x_limit = np.diff(grid.x_nodes)[0]
        y_limit = np.diff(grid.y_nodes)[0]
        eps_limit = np.sqrt(x_limit**2 + y_limit**2) / np.sqrt(
            (grid.x_max - grid.x_min)**2 + (grid.y_max - grid.y_min)**2)    # normalized eps estimate for DBSCAN
        for cluster in clusters_ids_original_time:
            if len(cluster) > 3:    # check if the cluster has more than 3 events
                x_extent = np.abs(np.max(easting[cluster]) - np.min(easting[cluster]))
                y_extent = np.abs(np.max(northing[cluster]) - np.min(northing[cluster]))
                if x_extent <= x_limit and y_extent <= y_limit:    # check if the events are within one grid cell, if so, assign all events to the same cluster labeled by [0]
                    cluster_labels.append([0] * len(cluster))
                else:    # if the events are not within one grid cell, use DBSCAN to cluster the events in space.
                    x_coords = easting[cluster] / np.abs(
                        grid.x_max - grid.x_min)    # normalize the coordinates to [0,1]
                    y_coords = northing[cluster] / np.abs(grid.y_max - grid.y_min)
                    X = np.column_stack((x_coords, y_coords))
                    clusterer = DBSCAN(eps=eps_limit, min_samples=3, metric='euclidean')
                    cluster_label = clusterer.fit_predict(X)
                    cluster_labels.append(list(cluster_label))
            else:    # if the cluster has less than 3 events, check if there is a large magnitude event in the cluster
                if np.max(
                        mag[cluster]
                ) > 1.0:    # if there is a large magnitude event (>1.0), assign [0] to all events, marking them as a cluster
                    cluster_mag = np.array(mag[cluster])
                    label = [-1] * len(cluster)
                    current_label = 0
                    for i in range(len(cluster)):
                        if cluster_mag[i] > 1.0:
                            label[i] = current_label
                            current_label += 1
                    cluster_labels.append(label)
                else:    # if there is no large magnitude event, assign [-1] to all events, marking them as background events
                    cluster_labels.append([-1] * len(cluster))

        return time, clusters_ids_original_time, cluster_labels

    def get_sub_clusters(self, cluster, cluster_label, catalog, grid):
        """
        Locate the sub-clusters (space) in a cluster (time) using cluster label

        Args:
            cluster (list): list of cluster indices
            cluster_label (list): list of labels used to identify sub-clusters
            catalog (Catalog): seismic catalog object
            grid (Grid): grid object

        Returns:
            sub_clusters (dictionary): {label:{"event_idx":[event_index1, event_index2, ...],
                                               "centroid": (centroid_easting, centroid_northing),
                                               "host_cell": (i_cell, j_cell),
                                               "seismic_moment": sum_seismic_moment,
                                               "equivalent_moment_magnitude": equivalent_moment_magnitude,
                                               "trivial_coulomb_stress_radius": trivial_radius,
                                               "affected_cells": [(i1, j1), (i2, j2), ...]}
                                        }
        """
        sub_clusters = {}
        for event_index, label in zip(cluster, cluster_label):
            if label != -1:    # ignore background events
                if label not in sub_clusters:
                    sub_clusters[label] = {"event_idx": [event_index]}    # create a new sub-cluster
                else:
                    sub_clusters[label]["event_idx"].append(event_index)
        for label, sub_cluster_metadata in sub_clusters.items():
            sub_cluster_metadata["event_idx"] = sorted(sub_cluster_metadata["event_idx"])
            sub_cluster_metadata["centroid"] = (np.mean(catalog.easting_slice[sub_cluster_metadata["event_idx"]]),
                                                np.mean(catalog.northing_slice[sub_cluster_metadata["event_idx"]]))
            sub_cluster_metadata["host_cell"] = self.find_cell(grid, sub_cluster_metadata["centroid"][0],
                                                               sub_cluster_metadata["centroid"][1])

            sum_seismic_moment, equivalent_moment_magnitude = self.cal_equivalent_moment_magnitude(
                sub_cluster_metadata["event_idx"], catalog)
            sub_cluster_metadata["seismic_moment"] = sum_seismic_moment
            sub_cluster_metadata["equivalent_moment_magnitude"] = equivalent_moment_magnitude

            num_events = len(sub_cluster_metadata["event_idx"])
            trivial_radius = self.trivial_coulomb_stress_radius(sum_seismic_moment)
            sub_cluster_metadata["trivial_coulomb_stress_radius"] = trivial_radius
            sub_cluster_metadata["affected_cells"] = self.find_affected_cells(sub_cluster_metadata["centroid"][0],
                                                                              sub_cluster_metadata["centroid"][1],
                                                                              trivial_radius, grid)
        return sub_clusters

    def find_closest_indices(self, time1, time2):
        """
        Find the closest indices in time1 to time2

        Args:
            time1 (numpy array): time array
            time2 (numpy array): time array

        Returns:
            closest_indices (list): list of indices in time1 that are closest to time2
        """
        time1 = np.array(time1)
        time2 = np.array(time2)

        closest_indices = []
        for t2 in time2:
            index = np.abs(time1 - t2).argmin()
            closest_indices.append(index)

        return closest_indices

    def cal_static_coulomb_stress_change(self, grid, seismic_catalog):
        """
        Calculate the static coulomb stress change for each detected cluster
        Args:
            seismic_catalog (Catalog): seismic catalog object
            grid (Grid): grid object
        Returns:
            time (numpy array): time array
            time_to_apply_stress_change (numpy array): time array to apply stress change
            stress_change_map (numpy array): stress change map
        """
        time, clusters_ids_original_time, cluster_labels = self.get_clusters(grid, seismic_catalog)
        Nx = len(grid.x)
        Ny = len(grid.y)

        # initialize the stress change map
        stress_change_times = [time[event_indicies[0]] for event_indicies in clusters_ids_original_time]
        initial_stress_change = list(np.zeros(len(stress_change_times)))
        time_to_apply_stress_change = np.array([time[0]] + stress_change_times)
        stress_change_map = np.zeros((Nx, Ny, len(time_to_apply_stress_change)))
        stress_change_map[:, :, 0] = self.initial_stress_change / (Nx * Ny)    # initial stress change for each cell

        cluster_number = 1
        for cluster, cluster_label in zip(clusters_ids_original_time, cluster_labels):
            sub_clusters = self.get_sub_clusters(cluster, cluster_label, seismic_catalog, grid)
            if sub_clusters == {}:
                cluster_number += 1
                continue
            else:
                for sub_cluster_label, sub_cluster_metadata in sub_clusters.items():
                    sum_seismic_moment = sub_cluster_metadata["seismic_moment"]
                    equivalent_moment_magnitude = sub_cluster_metadata["equivalent_moment_magnitude"]
                    centroid_easting, centroid_northing = sub_cluster_metadata["centroid"]
                    for i in range(Nx):
                        for j in range(Ny):
                            distance_to_centroid = np.sqrt((grid.x[i] - centroid_easting)**2 +
                                                           (grid.y[j] - centroid_northing)**2)
                            half_grid_cell_diag = np.sqrt(grid.dx**2 + grid.dy**2) / 2
                            if distance_to_centroid <= half_grid_cell_diag:
                                distance_to_centroid = half_grid_cell_diag
                            exponent = 2.0 + (np.log(distance_to_centroid) / np.log(half_grid_cell_diag) - 1)
                            stress_change = sum_seismic_moment / (6 * np.pi * distance_to_centroid**exponent)
                            stress_change_map[i, j, cluster_number] += stress_change
            cluster_number += 1

        return time, time_to_apply_stress_change, stress_change_map

    def estimate_normal_stress(self, z):
        """
        Estimate the normal stress at depth z

        Args:
            z (scalar): depth

        Returns:
            normal_stress (scalar): normal stress
        """
        return self.horizontal_stress_ratio * (self.rock_mass + self.water) * self.gravity * z

    def fit_training_data(self, grid, seismic_catalog, pressure, wells, geologic_model):
        """
        Fit to the training data using the model parameters. The training data is the seismic catalog.

        Args:
            grid (Grid): grid object
            seismic_catalog (Catalog): seismic catalog object
            pressure (Pressure): pressure object
            wells (Wells): wells object
            geologic_model (GeologicModel): geologic model object

        Returns:
            temporal_forecast (numpy array): temporal number forecast
            spatial_forecast (numpy array): spatial number forecast
        """
        time, time_to_apply_stress_change, stress_change_map = self.cal_static_coulomb_stress_change(
            grid, seismic_catalog)

        Nx = len(grid.x)
        Ny = len(grid.y)
        Nz = len(grid.z)
        grid_time = grid.t + grid.t_origin

        spatial_number_forecast = np.zeros((len(time), Nx, Ny))
        temporal_number_forecast = np.zeros((len(time)))

        for i in range(Nx):
            for j in range(Ny):
                sigma_effective = self.SigmaEffective(pressure.p(grid.x[i], grid.y[j], grid.z[0], time - grid.t_origin))
                coulomb_stressing_rate = self.CoulombStressingRate(
                    pressure.dpdt(grid.x[i], grid.y[j], grid.z[0], time - grid.t_origin))
                stress_change = stress_change_map[i, j]
                N = self.NumberEvolution(time, time_to_apply_stress_change, stress_change, coulomb_stressing_rate,
                                         sigma_effective)
                spatial_number_forecast[:, i, j] = N

        temporal_number_forecast = np.sum(spatial_number_forecast, axis=(1, 2))
        closest_indices = self.find_closest_indices(time, grid_time)
        self.temporal_forecast = temporal_number_forecast[closest_indices]
        self.spatial_forecast = spatial_number_forecast[closest_indices, :, :]
        return self.temporal_forecast, self.spatial_forecast

    def fit_to_grid_time(self, time, grid_time, times_to_apply_stress_change):
        """
        Fit to the grid time using catalog time and stress change time
        to accelerate the computation

        Args:
            time (numpy array): catalog time
            grid_time (numpy array): grid time
            times_to_apply_stress_change (numpy array): times to apply stress change
        
        Returns:
            combined_time (numpy array): combined time
        """
        combined_time = np.unique(np.sort(np.concatenate((grid_time, times_to_apply_stress_change))))
        filtered_combined_time = combined_time[(combined_time >= time[0]) & (combined_time <= time[-1])]
        return filtered_combined_time

    def fit_training_data_fast(self, grid, seismic_catalog, pressure, wells, geologic_model):
        """
        Fit to the training data using the model parameters. The training data is the seismic catalog.

        Args:
            grid (Grid): grid object
            seismic_catalog (Catalog): seismic catalog object
            pressure (Pressure): pressure object
            wells (Wells): wells object
            geologic_model (GeologicModel): geologic model object

        Returns:
            temporal_forecast (numpy array): temporal number forecast
            spatial_forecast (numpy array): spatial number forecast
        """
        time, time_to_apply_stress_change, stress_change_map = self.cal_static_coulomb_stress_change(
            grid, seismic_catalog)

        Nx = len(grid.x)
        Ny = len(grid.y)
        Nz = len(grid.z)
        grid_time = grid.t + grid.t_origin

        filtered_combined_time = self.fit_to_grid_time(time, grid_time, time_to_apply_stress_change)

        spatial_number_forecast = np.zeros((len(filtered_combined_time), Nx, Ny))
        temporal_number_forecast = np.zeros((len(filtered_combined_time)))

        # Create a mask to identify times in grid_time
        mask_in_grid_time = np.isin(filtered_combined_time, grid_time)

        # Find the indices in grid_time that correspond to filtered_combined_time
        grid_indices = np.where(mask_in_grid_time, np.searchsorted(grid_time, filtered_combined_time), -1)

        for i in range(Nx):
            for j in range(Ny):
                stress_change = stress_change_map[i, j]

                # Initialize pressure_combined and dpdt_combined
                pressure_combined = np.zeros_like(filtered_combined_time)
                dpdt_combined = np.zeros_like(filtered_combined_time)

                # Vectorized assignment for times in grid_time
                pressure_combined[mask_in_grid_time] = pressure.p_grid[i, j, 0, grid_indices[mask_in_grid_time]]
                dpdt_combined[mask_in_grid_time] = pressure.dpdt_grid[i, j, 0, grid_indices[mask_in_grid_time]]

                # Vectorized evaluation for times not in grid_time (i.e., from picked_time)
                pressure_combined[~mask_in_grid_time] = pressure.p(
                    grid.x[i], grid.y[j], grid.z[0], filtered_combined_time[~mask_in_grid_time] - grid.t_origin)
                dpdt_combined[~mask_in_grid_time] = pressure.dpdt(
                    grid.x[i], grid.y[j], grid.z[0], filtered_combined_time[~mask_in_grid_time] - grid.t_origin)

                sigma_effective = self.SigmaEffective(pressure_combined)
                coulomb_stressing_rate = self.CoulombStressingRate(dpdt_combined)
                N = self.NumberEvolution(filtered_combined_time, time_to_apply_stress_change, stress_change,
                                         coulomb_stressing_rate, sigma_effective)
                spatial_number_forecast[:, i, j] = N

        temporal_number_forecast = np.sum(spatial_number_forecast, axis=(1, 2))
        closest_indices = self.find_closest_indices(filtered_combined_time, grid_time)
        self.temporal_forecast = temporal_number_forecast[closest_indices]
        self.spatial_forecast = spatial_number_forecast[closest_indices, :, :]
        return self.temporal_forecast, self.spatial_forecast

    def generate_forecast(self, grid, seismic_catalog, pressure, wells, geologic_model):
        """
        Generate the forecast for the entire grid

        Args:
            grid (Grid): grid object
            seismic_catalog (Catalog): seismic catalog object
            pressure (Pressure): pressure object
            wells (Wells): wells object
            geologic_model (GeologicModel): geologic model object

        Returns:
            temporal_forecast (numpy array): temporal number forecast
            spatial_forecast (numpy array): spatial number forecast
        """
        if self.use_fast_method:
            return self.fit_training_data_fast(grid, seismic_catalog, pressure, wells, geologic_model)
        else:
            return self.fit_training_data(grid, seismic_catalog, pressure, wells, geologic_model)
