# ------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2020-, Lawrence Livermore National Security, LLC
# All rights reserved
#
# See top level LICENSE, COPYRIGHT, CONTRIBUTORS, NOTICE, and ACKNOWLEDGEMENTS files for details.
# ------------------------------------------------------------------------------------------------
"""
pressure_model_base.py
-----------------------
"""

from orion.managers import manager_base
from orion.managers.manager_base import read_only_array
import numpy as np


class PressureModelBase(manager_base.ManagerBase):
    """
    Pressure model base class

    """

    def set_class_options(self, **kwargs):
        self.short_name = 'PressureModelBase'

    def set_data(self, **kwargs):
        """
        Pressure model initialization

        """
        # Gridded values
        self._t_origin = 0.0
        self._p_grid = np.zeros(0)
        self._dpdt_grid = np.zeros(0)
        self._valid_grid = {}
        self._valid_projection_str = ''

    def __bool__(self):
        """
        Override for the default boolean operation
        """
        return True

    def __add_base(self, other, scale=1.0):
        """
        Add two pressure models

        Args:
            other (PressureModelBase): Pressure model to add to the current object

        Returns:
            orion.pressure_models.pressure_table.PressureTableModel: Combined pressure model
        """
        from orion.pressure_models.pressure_table import PressureTableModel

        # Check the input type
        if not isinstance(other, PressureModelBase):
            t = type(value)
            self.logger.error(f"Attempted to add an unexpected type to a pressure model: {t}")
            raise Exception("Pressure models can only be added to objects derived from PressureModelBase")

        # Copy the secondary pressure values
        p_b = other.p_grid.copy()
        dpdt_b = other.dpdt_grid.copy()

        # Check model grids and re-interpolate if necessary
        ga = self._valid_grid
        gb = other._valid_grid
        grids_equal = all([np.array_equal(ga[k], gb[k]) for k in ga])
        if not grids_equal:
            G = np.meshgrid(ga['x'], ga['y'], ga['z'], ga['t'], indexing='ij')
            p_b = other.p(*G)
            dpdt_b = other.dpdt(*G)

        # Add the pressure models
        data = {
            'pressure': self.p_grid + scale * p_b,
            'dpdt': self.dpdt_grid + scale * dpdt_b,
        }
        data.update(self._valid_grid)
        pressure_model_new = PressureTableModel()
        pressure_model_new.source_projection_str = self._valid_projection_str
        pressure_model_new.target_projection_str = self._valid_projection_str
        pressure_model_new.parse_table_data(data)

        # Finalize the object
        pressure_model_new._t_origin = self._t_origin
        pressure_model_new._p_grid = data['pressure']
        pressure_model_new._dpdt_grid = data['dpdt']
        pressure_model_new._valid_grid = self._valid_grid
        return pressure_model_new

    def __add__(self, other):
        return self.__add_base(other, 1.0)

    def __sub__(self, other):
        return self.__add_base(other, -1.0)

    def __mul__(self, value):
        """
        Multiply a pressure model by a scalar

        Args:
            value (float): Scalar value to multipy pressure model

        Returns:
            PressureModelBase: Scaled pressure model
        """
        # Check the input type
        if not isinstance(value, (int, float)):
            t = type(value)
            self.logger.error(f"Attempted to multiply pressure model by incorrect type {t}")
            raise Exception("Pressure models can only be multiplied by a scalar")

        def scaled_p(x, y, z, t):
            return self.p(x, y, z, t) * value

        def scaled_dpdt(x, y, z, t):
            return self.dpdt(x, y, z, t) * value

        pressure_model_new = PressureModelBase()
        pressure_model_new.short_name = f'{self.short_name}_mult_{value}'
        pressure_model_new.source_projection_str = self._valid_projection_str
        pressure_model_new.target_projection_str = self._valid_projection_str
        pressure_model_new._t_origin = self._t_origin
        pressure_model_new._p_grid = self._p_grid * value
        pressure_model_new._dpdt_grid = self._dpdt_grid * value
        pressure_model_new._valid_grid = self._valid_grid
        pressure_model_new.p = scaled_p
        pressure_model_new.dpdt = scaled_dpdt
        return pressure_model_new

    def __truediv__(self, value):
        inv_scale = 1.0 / float(value)
        return self * inv_scale

    def crop(self, t_start=0, t_end=1e20):

        def cropped_p(x, y, z, t):
            c = (t < t_start) or (t > t_end)
            p = self.p(x, y, z, t)
            if isinstance(t, (float, int)):
                if c:
                    return 0.0
                else:
                    return p
            else:
                p[np.where(c)] = 0.0
                return p

        def cropped_dpdt(x, y, z, t):
            c = (t < t_start) or (t > t_end)
            dpdt = self.dpdt(x, y, z, t)
            if isinstance(t, (float, int)):
                if c:
                    return 0.0
                else:
                    return dpdt
            else:
                dpdt[np.where(c)] = 0.0
                return dpdt

        pressure_model_new = PressureModelBase()
        pressure_model_new.short_name = f'{self.short_name}_crop'
        pressure_model_new.source_projection_str = self._valid_projection_str
        pressure_model_new.target_projection_str = self._valid_projection_str
        pressure_model_new._t_origin = self._t_origin

        Ia = np.where((self._valid_grid['t'] < t_start) | (self._valid_grid['t'] > t_end))
        pressure_model_new._p_grid = self._p_grid.copy()
        pressure_model_new._dpdt_grid = self._dpdt_grid.copy()
        if len(Ia):
            pressure_model_new._p_grid[:, :, :, Ia] = 0.0
            pressure_model_new._dpdt_grid[:, :, :, Ia] = 0.0

        pressure_model_new._valid_grid = self._valid_grid
        pressure_model_new.p = cropped_p
        pressure_model_new.dpdt = cropped_dpdt
        return pressure_model_new

    def p(self, x, y, z, t):
        """
        Evaluate the pressure model for a given location(s)

        Args:
            x (float, array): X location in the local coordinate system (m)
            y (float, array): Y location in the local coordinate system (m)
            z (float, array): Z location in the local coordinate system (m)
            t (float, array): Time in the local coordinate system (seconds)

        Returns:
            float: Pressure

        """
        return 0.0

    def dpdt(self, x, y, z, t):
        """
        Evaluate the pressure dpdt model for a given location(s)

        Args:
            x (float, array): X location in the local coordinate system (m)
            y (float, array): Y location in the local coordinate system (m)
            z (float, array): Z location in the local coordinate system (m)
            t (float, array): Time in the local coordinate system (seconds)

        Returns:
            float: First derivative of pressure with time

        """
        return 0.0

    def grid_values(self, grid):
        """
        Evaluates the pressure model on the current grid

        Args:
            grid (orion.managers.grid_manager.GridManager): The Orion grid manager
        """
        G = np.meshgrid(grid.x, grid.y, grid.z, grid.t, indexing='ij')
        self._t_origin = grid.t_origin
        self._p_grid = self.p(*G)
        self._dpdt_grid = self.dpdt(*G)
        self._valid_grid = {'x': grid.x, 'y': grid.y, 'z': grid.z, 't': grid.t}
        self._valid_projection_str = grid.projection_str

    def run(self, grid, well_manager, geologic_model):
        """
        Runs the pressure model

        Args:
            grid (orion.managers.grid_manager.GridManager): The Orion grid manager
            well_manager (orion.managers.well_manager.WellManager): The Orion well manager
            geologic_model (orion.managers.geologic_model_manager.GeologicModelManager): The current geological model
        """
        pass

    @property
    def p_grid(self):
        """Gridded pressure data"""
        return read_only_array(self._p_grid)

    @property
    def dpdt_grid(self):
        """Gridded pressurization rate data"""
        return read_only_array(self._dpdt_grid)
