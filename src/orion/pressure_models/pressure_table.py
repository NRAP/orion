# ------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: MIT
#
# Copyright (c) 2020-, Lawrence Livermore National Security, LLC
# All rights reserved
#
# See top level LICENSE, COPYRIGHT, CONTRIBUTORS, NOTICE, and ACKNOWLEDGEMENTS files for details.
# ------------------------------------------------------------------------------------------------
"""
pressure_table.py
-----------------------
"""

import os
import numpy as np
from scipy import integrate
from orion.pressure_models import pressure_model_base
from orion.utilities import (hdf5_wrapper, table_files, file_io, function_wrappers, other, openIAM_parser,
                             timestamp_conversion, unit_conversion, spatial)


class PressureTableModel(pressure_model_base.PressureModelBase):
    """
    Pressure model based off of Theis Solution

    Attributes:
        file_name (string): Table filename
        p_interp (scipy.interpolate.LinearNDInterpolator): pressure interpolator
        dpdt_interp (scipy.interpolate.LinearNDInterpolator): dpdt interpolator

    """

    def set_class_options(self, **kwargs):
        """
        Initialization function

        """
        # Model configuration
        self.short_name = 'Pressure Table'
        self.model_type = 'Pressure Table Model'
        self.file_name = ''

        # Table units
        self.available_spatial_units = ['meter', 'kilometer', 'feet', 'mile']
        self.spatial_units = self.available_spatial_units[0]
        self.available_time_units = ['second', 'minute', 'hour', 'day', 'year']
        self.time_units = self.available_time_units[0]
        self.spatial_scale = 1.0
        self.time_scale = 1.0

        # Projection
        self.utm_zone_str = ''
        self.projection_str = ''
        self.source_projection_str = ''
        self.target_projection_str = ''

        # Table offsets
        self.table_offset_x = 0.0
        self.table_offset_y = 0.0
        self.table_offset_z = 0.0
        self.table_offset_t = 0.0
        self.table_offset_t_str = '0'

        self._loader_map = {
            ".hdf5": self.load_table_hdf5,
            ".csv": self.load_table_csv,
            ".zip": self.parse_openIAMModel,
        }

    def set_data(self, **kwargs):
        """
        Setup data holders
        """
        super().set_data(**kwargs)
        self.p_interp = None
        self.dpdt_interp = None
        self.model = None

    def set_gui_options(self, **kwargs):
        """
        Setup interface options
        """
        # Add values to gui
        self.gui_elements['model_type'] = {'element_type': 'text', 'position': [0, 0]}
        self.gui_elements['file_name'] = {
            'element_type': 'file',
            'command': 'file',
            'label': 'File',
            'position': [1, 0],
            'filetypes': [('hdf5', '*.hdf5'), ('zip', '*.zip'), ('csv', '*.csv'), ('all', '*')]
        }
        self.gui_elements['spatial_units'] = {
            'element_type': 'dropdown',
            'label': 'Distance units',
            'position': [2, 0],
            'values': self.available_spatial_units
        }
        self.gui_elements['time_units'] = {
            'element_type': 'dropdown',
            'label': 'Time units',
            'position': [3, 0],
            'values': self.available_time_units
        }
        self.gui_elements['table_offset_x'] = {'element_type': 'entry', 'label': 'Spatial origin', 'position': [4, 0]}
        self.gui_elements['table_offset_y'] = {'element_type': 'entry', 'position': [4, 1]}
        self.gui_elements['table_offset_z'] = {'element_type': 'entry', 'units': '(x, y, z)', 'position': [4, 2]}
        self.gui_elements['table_offset_t_str'] = {'element_type': 'entry', 'label': 'Time origin', 'position': [5, 0]}
        self.gui_elements['projection_str'] = {'element_type': 'entry', 'label': 'Projection code', 'position': [6, 0]}
        self.gui_elements['utm_zone_str'] = {'element_type': 'entry', 'label': '(or) UTM zone', 'position': [6, 1]}

    def process_inputs(self):
        """
        Process unit and offset information
        """
        unit_converter = unit_conversion.UnitManager()
        self.spatial_scale = unit_converter(self.spatial_units)
        self.time_scale = unit_converter(self.time_units)

        if '/' in self.table_offset_t_str:
            self.table_offset_t = timestamp_conversion.convert_timestamp(self.table_offset_t_str) / self.time_scale
        else:
            self.table_offset_t = float(self.table_offset_t_str)

    def reset_data(self, grid):
        self.p_interp = function_wrappers.constant_fn(0.0)
        self.dpdt_interp = function_wrappers.constant_fn(0.0)
        self.grid_values(grid)

    def p(self, x, y, z, t):
        return self.p_interp(x, y, z, t + self._t_origin)

    def dpdt(self, x, y, z, t):
        return self.dpdt_interp(x, y, z, t + self._t_origin)

    def run(self, grid, well_manager, geologic_model):
        f = os.path.expanduser(self.file_name)
        if not os.path.isfile(f):
            self.logger.warning(f'Cannot find pressure table file: {f}')
            self.reset_data(grid)
            return

        # Check the projection information
        if self.utm_zone_str:
            self.utm_zone_str = spatial.parse_utm_zone_str(self.utm_zone_str)
            self.source_projection_str = f'+proj=utm +zone={self.utm_zone_str}'
        elif self.projection_str:
            self.source_projection_str = self.projection_str
        else:
            self.source_projection_str = grid.projection_str

        # Load the data
        ext = os.path.splitext(f)[-1]
        if ext in self._loader_map:
            self.logger.debug(f'Loading pressure table from file: {f}')
            try:
                self._loader_map[ext](f)
            except Exception as e:
                self.logger.error(f'Failed to load table from file: {f}')
                self.logger.error(repr(e))
                self.reset_data(grid)
                return

        else:
            valid_ext = ', '.join(sorted(self._loader_map.keys()))
            self.logger.error(f'Table file format not recognized: {f}')
            self.logger.error(f'Available options: {valid_ext}')
            self.reset_data(grid)
            return

        self.grid_values(grid)

    def parse_table_data(self, data):
        # Check to see whether we need to calculate pressure or dpdt
        if ('t' not in data):
            raise Exception('The pressure table file is missing t')
        if ('pressure' not in data):
            if ('dpdt' in data):
                data['pressure'] = integrate.cumulative_trapezoid(data['dpdt'], data['t'], initial=0.0, axis=-1)
            else:
                raise Exception('The pressure table file requires either pressure or dpdt')
        if ('dpdt' not in data):
            if ('pressure' in data):
                scale_shape = np.ones(len(np.shape(data['pressure'])), dtype=int)
                scale_shape[-1] = -1
                data['dpdt'] = other.derivative(data['pressure'], data['t'], axis=-1)
            else:
                raise Exception('The pressure table file requires either pressure or dpdt')

        offset = {
            'x': self.table_offset_x,
            'y': self.table_offset_y,
            'z': self.table_offset_z,
            't': self.table_offset_t
        }
        interps = table_files.load_table_files(data,
                                               source_projection=self.source_projection_str,
                                               target_projection=self.target_projection_str,
                                               offset=offset)

        self.p_interp = interps['pressure']
        self.dpdt_interp = interps['dpdt']

    def load_table_hdf5(self, fname):
        tmp = hdf5_wrapper.hdf5_wrapper(fname)
        data = tmp.get_copy()
        self.parse_table_data(data)

    def load_table_csv(self, fname):
        data = file_io.parse_csv(fname)
        self.parse_table_data(data)

    def parse_openIAMModel(self, fname):
        spatial_offset = np.array([self.table_offset_x, self.table_offset_y, self.table_offset_z])
        model = openIAM_parser.OpenIAMParser(fname,
                                             time_scale=self.time_scale,
                                             time_offset=self.table_offset_t,
                                             spatial_scale=self.spatial_scale,
                                             spatial_offset=spatial_offset)
        properties = list(model.keys())
        if ('pressure' not in properties):
            if ('dpdt' in properties):
                dpdt = np.array(model.properties['dpdt'])
                p = integrate.cumulative_trapezoid(dpdt, model.t, initial=0.0, axis=0)
                model.properties['pressure'] = [np.ascontiguousarray(np.squeeze(x)) for x in np.split(p, model.Nt)]
            else:
                raise Exception('The OpenIAM model requires pressure or dpdt')
        if ('dpdt' not in properties):
            if ('pressure' in properties):
                p = np.array(model.properties['pressure'])
                dpdt = other.derivative(p, model.t, axis=0)
                model.properties['dpdt'] = [np.ascontiguousarray(np.squeeze(x)) for x in np.split(dpdt, model.Nt)]
            else:
                raise Exception('The OpenIAM model requires pressure or dpdt')

        self.model = model

        # Handle model dimensions
        if self.model.Ndim == 3:
            mask = [0, 1, 3]
            self.p_interp = function_wrappers.masked_fn(self.model['pressure'], mask)
            self.dpdt_interp = function_wrappers.masked_fn(self.model['dpdt'], mask)
        else:
            self.p_interp = self.model['pressure']
            self.dpdt_interp = self.model['dpdt']
